package org.jamgo.model.valueobjects;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IBAN implements ValueObject {
	private static final Logger logger = LoggerFactory.getLogger(IBAN.class);
	public static final Integer IBAN_LENGTH_SPAIN = 24;

	private final String value;

	public IBAN(@JsonProperty("value") final String value) {
		// TODO: Remove spaces (ex: "ES91 2100 0418 4502 0005 1234" -> "ES9121000418450200051234")
		if (this.validate(value)) {
			this.value = value;
		} else {
			IBAN.logger.error("Invalid IBAN number {}", value);
			throw new IllegalArgumentException("Invalid IBAN number " + value + ".");
		}
	}

	@Override
	public String getValue() {
		return this.value;
	}

	/** Checks if IBAN number matches with Spanish IBAN rules. It will fail if
	 * you try to set a foreign IBAN number.
	 *
	 * @param number
	 * @return
	 */
	private boolean validate(final String number) {
		final boolean isValid = true;
		if (number.isEmpty()) {
			IBAN.logger.error("IBAN number is empty.");
			return false;
		}

		if (number.length() != IBAN.IBAN_LENGTH_SPAIN) {
			IBAN.logger.error(String.format("IBAN number must have %d characters in length.", IBAN.IBAN_LENGTH_SPAIN));
			return false;
		}

		if (!number.startsWith("ES")) {
			IBAN.logger.error("IBAN number must start with ES for Spanish IBAN numbers.");
			return false;
		}

		if (!StringUtils.isNumeric(number.substring(2, 4))) {
			IBAN.logger.error("IBAN number must have a check number.");
			return false;
		}

		if (!StringUtils.isNumeric(number.substring(4))) {
			IBAN.logger.error("IBAN account number must contain only numeric characters.");
			return false;
		}

		return isValid;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || this.getClass() != o.getClass()) {
			return false;
		}
		final IBAN iban = (IBAN) o;
		return Objects.equals(this.value, iban.value);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.value);
	}

	@Override
	public String toString() {
		return "IBAN{" +
			"value='" + this.value + '\'' +
			'}';
	}
}
