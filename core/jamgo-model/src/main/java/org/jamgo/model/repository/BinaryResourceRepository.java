package org.jamgo.model.repository;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.BinaryResourceImage;
import org.jamgo.model.jpa.MetadataJPAService;
import org.jamgo.model.jpa.MetadataJPAService.EntityAttributePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blazebit.persistence.CriteriaBuilder;

@Repository
public class BinaryResourceRepository extends ModelRepository<BinaryResource> {
	public static final Logger logger = LoggerFactory.getLogger(BinaryResourceRepository.class);

	@Autowired
	private MetadataJPAService metadataJPAService;

	private List<EntityAttributePair> binaryResourceJPADependencies;

	public List<BinaryResource> getPurgeCandidates() {
//		return this.purgeCandidatesUsingJDBCMetadata();
		return this.purgeCandidatesUsingJPAMetadata();
	}

	private List<BinaryResource> purgeCandidatesUsingJPAMetadata() {
		if (this.binaryResourceJPADependencies == null) {
			// Search pairs entity-attribute that contains a BinaryResource (needed for purge)
			this.binaryResourceJPADependencies = this.metadataJPAService.getReferences(BinaryResource.class).stream()
				// - Skip "binary_resource_image" table, because they will be deleted in cascade
				.filter(pair -> !pair.getEntityClass().equals(BinaryResourceImage.class))
				.collect(Collectors.toList());
		}

		if (this.binaryResourceJPADependencies.isEmpty()) {
			BinaryResourceRepository.logger.info("No JPA dependencies found for BinaryResource.class");
			return Collections.emptyList();
		}

		// Build the criteriabuilder using the dependencies
		final CriteriaBuilder<BinaryResource> cb = this.createCriteriaBuilder("br");
		final AtomicInteger index = new AtomicInteger(1);

		this.binaryResourceJPADependencies.forEach(dep -> {
			final Class<?> dependencyClass = dep.getEntityClass();
			final String alias = "dep" + index.getAndIncrement();
			cb.leftJoinOn(dependencyClass, alias)
				.on(alias + "." + dep.getAttributeName()).eqExpression("br")
				.end();
			cb.where(alias).isNull();
		});

		return cb.getResultList();
	}

}
