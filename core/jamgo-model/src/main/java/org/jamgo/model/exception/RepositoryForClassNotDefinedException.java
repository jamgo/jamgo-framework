package org.jamgo.model.exception;

public class RepositoryForClassNotDefinedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public RepositoryForClassNotDefinedException(final String message) {
		super(message);
	}
}
