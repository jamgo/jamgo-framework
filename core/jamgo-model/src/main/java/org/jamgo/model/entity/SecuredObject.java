package org.jamgo.model.entity;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.jamgo.model.enums.SecuredObjectType;

@Entity
@Table(name = "secured_object")
@Cacheable(false)
public class SecuredObject extends Model {

	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	protected String name;

	@Column(name = "secured_object_type")
	@Enumerated(EnumType.STRING)
	protected SecuredObjectType securedObjectType;

	@Column(name = "secured_object_key")
	private String securedObjectKey;

	@ManyToOne
	@JoinColumn(name = "parent_id")
	private SecuredObject parent;

	@OneToMany(mappedBy = "parent")
	private List<SecuredObject> children;

	@OneToMany(mappedBy = "securedObject", cascade = CascadeType.ALL)
	private List<Acl> acl;

	public SecuredObjectType getSecuredObjectType() {
		return this.securedObjectType;
	}

	public void setSecuredObjectType(final SecuredObjectType securedObjectType) {
		this.securedObjectType = securedObjectType;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getSecuredObjectKey() {
		return this.securedObjectKey;
	}

	public void setSecuredObjectKey(final String securedObjectKey) {
		this.securedObjectKey = securedObjectKey;
	}

	public SecuredObject getParent() {
		return this.parent;
	}

	public void setParent(final SecuredObject parent) {
		this.parent = parent;
	}

	public List<SecuredObject> getChildren() {
		return this.children;
	}

	public void setChildren(final List<SecuredObject> children) {
		this.children = children;
	}

	public String getTypedName() {
		return "(" + this.securedObjectType + ") " + this.name;
	}

	@Override
	public String toString() {
		return this.getName();
	}

	public List<Acl> getAcl() {
		return this.acl;
	}

	public void setAcl(final List<Acl> acl) {
		this.acl = acl;
	}

}
