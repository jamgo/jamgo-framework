package org.jamgo.model.view;

import java.util.Objects;

import org.jamgo.model.BasicModel;

import com.blazebit.persistence.view.IdMapping;

public abstract class BasicModelView<ID_TYPE> implements BasicModel<ID_TYPE> {

	@Override
	@IdMapping
	public abstract ID_TYPE getId();

	@Override
	public abstract Long getVersion();

	@Override
	public int hashCode() {
		return Objects.hash(this.getId(), this.getVersion());
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		return (obj instanceof BasicModelView)
			&& Objects.equals(this.getId(), ((BasicModelView<?>) obj).getId())
			&& Objects.equals(this.getVersion(), ((BasicModelView<?>) obj).getVersion());
	}

}
