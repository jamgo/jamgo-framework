package org.jamgo.model.entity;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import org.hibernate.engine.jdbc.BlobProxy;

@Cacheable(false)
@Entity
public class BinaryResource extends Model implements BinaryObject, Downloadable {

	private static final long serialVersionUID = 1L;

	@Column
	private LocalDateTime timeStamp;

	@Column
	private String description;

	@Column
	private String mimeType;

	@Column
	private String fileName;

	@Column
	private Integer fileLength;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column
	private Blob contents;

	@OneToMany(mappedBy = "source", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
	private Set<BinaryResourceImage> images = new LinkedHashSet<>();

	public BinaryResource() {
		super();
	}

	public BinaryResource(final byte[] contents, final String mimeType, final String fileName) {
		super();
		this.contents = BlobProxy.generateProxy(contents);
		this.mimeType = mimeType;
		this.fileName = fileName;
		this.fileLength = contents.length;
	}

	public BinaryResource(final byte[] contents, final String mimeType, final String fileName, final String description) {
		super();
		this.contents = BlobProxy.generateProxy(contents);
		this.mimeType = mimeType;
		this.fileName = fileName;
		this.fileLength = contents.length;
		this.description = description;
	}

	public BinaryResource(final BinaryObject binaryObject) {
		super();
		this.timeStamp = binaryObject.getTimeStamp();
		this.description = binaryObject.getDescription();
		this.mimeType = binaryObject.getMimeType();
		this.fileName = binaryObject.getFileName();
		this.fileLength = binaryObject.getFileLength();
		this.contents = binaryObject.getContents();
		if (binaryObject instanceof BinaryResource) {
			this.setId(((BinaryResource) binaryObject).getId());
			this.setVersion(((BinaryResource) binaryObject).getVersion());
		}
	}

	@Override
	public String getFileName() {
		return this.fileName;
	}

	@Override
	public void setFileName(final String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String getMimeType() {
		return this.mimeType;
	}

	@Override
	public void setMimeType(final String mimeType) {
		this.mimeType = mimeType;
	}

	@Override
	public Integer getFileLength() {
		return this.fileLength;
	}

	@Override
	public void setFileLength(final Integer fileLength) {
		this.fileLength = fileLength;
	}

	@Override
	public LocalDateTime getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public void setTimeStamp(final LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public Blob getContents() {
		return this.contents;
	}

	@Override
	public void setContents(final Blob contents) {
		this.contents = contents;
	}

	public Set<BinaryResourceImage> getImages() {
		return this.images;
	}

	public void setImages(final Set<BinaryResourceImage> images) {
		this.images = images;
	}

	public void addImage(final BinaryResourceImage image) {
		this.images.add(image);
	}

	/**
	 * Returns a copy of the binary resource data (without id and version)
	 * @param binaryResource
	 * @return
	 */
	public static BinaryResource copyOf(final BinaryResource binaryResource) {
		final BinaryResource copy = new BinaryResource();
		copy.setFileName(binaryResource.getFileName());
		copy.setDescription(binaryResource.getDescription());
		copy.setFileLength(binaryResource.getFileLength());
		copy.setContents(binaryResource.getContents());
		copy.setMimeType(binaryResource.getMimeType());
		copy.setTimeStamp(binaryResource.getTimeStamp());
		return copy;
	}

}
