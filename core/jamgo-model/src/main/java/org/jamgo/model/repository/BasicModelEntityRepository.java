package org.jamgo.model.repository;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.OneToMany;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.SetUtils;
import org.hibernate.Session;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.jpa.QueryHints;
import org.hibernate.proxy.pojo.bytebuddy.ByteBuddyProxyFactory;
import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.BasicModelEntity;
import org.jamgo.model.exception.CouldNotRemoveEntity;
import org.jamgo.model.search.SearchSpecification;
import org.jamgo.model.util.OffsetSizePageRequest;
import org.jamgo.model.view.BasicModelView;
import org.jamgo.model.view.ModelView;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ResolvableType;
import org.springframework.transaction.annotation.Transactional;

import com.blazebit.persistence.CriteriaBuilder;
import com.blazebit.persistence.CriteriaBuilderFactory;
import com.blazebit.persistence.DeleteCriteriaBuilder;
import com.blazebit.persistence.PagedList;
import com.blazebit.persistence.PaginatedCriteriaBuilder;
import com.blazebit.persistence.UpdateCriteriaBuilder;
import com.blazebit.persistence.view.EntityViewManager;
import com.blazebit.persistence.view.EntityViewSetting;
import com.blazebit.persistence.view.Sorter;
import com.blazebit.persistence.view.Sorters;
import com.blazebit.persistence.view.UpdatableEntityView;

public abstract class BasicModelEntityRepository<ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE> implements InitializingBean {

	@Value("${spring.jpa.properties.hibernate.default_schema:none}")
	private String defaultSchema;

	private final static String NO_SCHEMA = "none";

	@PersistenceContext
	protected EntityManager entityManager;
	@Autowired
	protected EntityViewManager entityViewManager;
	@Autowired
	protected CriteriaBuilderFactory criteriaBuilderFactory;

	/**
	 * {@link BasicModelEntity} managed by this repository. This class is
	 * automatically obtained during repository bean initialization using the
	 * parameterized generic type in the repository class definition.
	 */
	protected Class<ENTITY> modelClass;

	@SuppressWarnings("unchecked")
	@Override
	public void afterPropertiesSet() throws Exception {
		this.modelClass = (Class<ENTITY>) ResolvableType.forClass(this.getClass()).getSuperType().getGeneric(0).resolve();
	}

	/**
	 * Returns all instances of the {@link #modelClass} type managed by this
	 * repository.
	 *
	 * @return all instances of {@link #modelClass}
	 */
	public List<ENTITY> findAll() {
		return this.findAll(this.getModelClass());
	}

	/**
	 * Returns all instances of the given {@code resultClass} type.
	 * <p>
	 * The {@code resultClass} can be the {@link #modelClass} itself or any
	 * {@link BasicModelView} of it.
	 *
	 * @param <RESULT> the type of {@code resultClass}
	 * @param resultClass entity or view class
	 * @return all instances of {@code resultClass}
	 */
	public <RESULT extends BasicModel<ID_TYPE>> List<RESULT> findAll(final Class<RESULT> resultClass) {
		final CriteriaBuilder<RESULT> cb = this.createCriteriaBuilder(resultClass);
		return cb.getResultList();
	}

	/**
	 * Returns instances of the {@link #modelClass} type managed by this
	 * repository, matching the given {@link SearchSpecification}.
	 *
	 * @param specification search specification
	 * @return matching instances of {@link #modelClass}
	 */
	public List<ENTITY> findAll(final SearchSpecification specification) {
		return this.findAll(specification, this.getModelClass());
	}

	/**
	 * Returns instances of the given {@code resultClass} type, matching the
	 * given {@link SearchSpecification}.
	 * <p>
	 * The {@code resultClass} can be the {@link #modelClass} itself or any
	 * {@link BasicModelView} of it.
	 *
	 * @param <RESULT> the type of {@code resultClass}
	 * @param specification search specification
	 * @param resultClass entity or view class
	 * @return matching instances of {@code resultClass}
	 */
	public <RESULT extends BasicModel<ID_TYPE>> List<RESULT> findAll(final SearchSpecification specification, final Class<RESULT> resultClass) {
		final CriteriaBuilder<RESULT> cb = this.createCriteriaBuilder(null, specification, resultClass);
		return cb.getResultList();
	}

	/**
	 * Returns a {@link java.util.stream.Stream Stream} of all instances of the
	 * {@link #modelClass} type managed by this repository.
	 *
	 * @return a stream of {@link #modelClass}
	 */
	public Stream<ENTITY> streamAll() {
		return this.streamAll(this.getModelClass());
	}

	/**
	 * Returns a {@link java.util.stream.Stream Stream} of the given
	 * {@code resultClass} type.
	 * <p>
	 * The {@code resultClass} can be the {@link #modelClass} itself or any
	 * {@link BasicModelView} of it.
	 *
	 * @param <RESULT> the type of {@code resultClass}
	 * @param resultClass entity or view class
	 * @return a stream of {@code resultClass}
	 */
	public <RESULT extends BasicModel<ID_TYPE>> Stream<RESULT> streamAll(final Class<RESULT> resultClass) {
		final CriteriaBuilder<RESULT> cb = this.createCriteriaBuilder(resultClass);
		final TypedQuery<RESULT> query = cb.getQuery();
		query.setHint(QueryHints.HINT_FETCH_SIZE, 50);
		return query.getResultStream();
	}

	/**
	 * Returns a {@link java.util.stream.Stream Stream} of all instances of the
	 * {@link #modelClass} type managed by this repository, matching the given
	 * {@link SearchSpecification}.
	 *
	 * @param specification search specification
	 * @return stream of {@link #modelClass}
	 */
	public Stream<ENTITY> streamAll(final SearchSpecification specification) {
		return this.streamAll(specification, this.getModelClass());
	}

	/**
	 * Returns a {@link Stream} of all instances of the given
	 * {@code resultClass} type, matching the given {@link SearchSpecification}.
	 * <p>
	 * The {@code resultClass} can be the {@link #modelClass} itself or any
	 * {@link BasicModelView} of it.
	 *
	 * @param <RESULT> the type of {@code resultClass}
	 * @param specification search specification
	 * @param resultClass entity or view class
	 * @return stream of {@code resultClass}
	 */
	public <RESULT extends BasicModel<ID_TYPE>> Stream<RESULT> streamAll(final SearchSpecification specification, final Class<RESULT> resultClass) {
		final CriteriaBuilder<RESULT> cb = this.createCriteriaBuilder(null, specification, resultClass);
		specification.apply(cb);
		final TypedQuery<RESULT> query = cb.getQuery();
		query.setHint(QueryHints.HINT_FETCH_SIZE, 50);
		return query.getResultStream();
	}

	/**
	 * Counts all instances of the {@link #modelClass} type managed by this
	 * repository, matching the given {@link SearchSpecification}.
	 *
	 * @param specification search specification
	 * @return the number of instances
	 */
	public Long countAll(final SearchSpecification specification) {
		return this.countAll(specification, this.getModelClass());
	}

	/**
	 * Counts all instances of the given {@code resultClass} type, matching the
	 * given {@link SearchSpecification}.
	 * <p>
	 * The {@code resultClass} can be the {@link #modelClass} itself or any
	 * {@link BasicModelView} of it.
	 *
	 * @param <RESULT> the type of {@code resultClass}
	 * @param specification search specification
	 * @param resultClass entity or view class
	 * @return the number of instances
	 */
	public <RESULT extends BasicModel<ID_TYPE>> Long countAll(
		final SearchSpecification specification,
		final Class<RESULT> resultClass) {

		final CriteriaBuilder<RESULT> cb = this.createCriteriaBuilder(null, specification, resultClass);
		return cb.getCountQuery().getSingleResult();
	}

	/**
	 * Returns an instances page ({@link PagedList}) of the {@link #modelClass}
	 * type managed by this repository, matching the given
	 * {@link SearchSpecification}.
	 * <p>
	 * The page offset and size is determined by the given
	 * {@link OffsetSizePageRequest}.
	 *
	 * @param specification search specification
	 * @param pageRequest page offset and size
	 * @return an instances page of {@link #modelClass}
	 */
	public PagedList<ENTITY> findAll(
		final SearchSpecification specification,
		final OffsetSizePageRequest pageRequest) {

		final CriteriaBuilder<ENTITY> cb = this.createEntityPaginatedCriteriaBuilder(specification, pageRequest);
		final int offset = Optional.ofNullable(pageRequest).map(o -> (int) o.getOffset()).orElse(0);
		final int pageSize = Optional.ofNullable(pageRequest).map(o -> o.getPageSize()).orElse(Integer.MAX_VALUE);
		return cb.page(offset, pageSize).getResultList();
	}

	/**
	 * Returns an instances page ({@link PagedList}) of the given
	 * {@code resultClass} type, matching the given {@link SearchSpecification}.
	 * <p>
	 * The page offset and size is determined by the given
	 * {@link OffsetSizePageRequest}.
	 * <p>
	 * The {@code resultClass} can be the {@link #modelClass} itself or any
	 * {@link BasicModelView} of it.
	 *
	 * @param <RESULT> the type of {@code resultClass}
	 * @param specification search specification
	 * @param pageRequest page offset and size
	 * @param resultClass entity or view class
	 * @return an instances page of {@link #modelClass}
	 */
	@SuppressWarnings("unchecked")
	public <RESULT extends BasicModel<ID_TYPE>> PagedList<RESULT> findAll(
		final SearchSpecification specification,
		final OffsetSizePageRequest pageRequest,
		final Class<RESULT> resultClass) {

		if (this.getModelClass().equals(resultClass)) {
			return (PagedList<RESULT>) this.findAll(specification, pageRequest);
		} else {
			final PaginatedCriteriaBuilder<RESULT> cbv = this.createViewPaginatedCriteriaBuilder(specification, pageRequest, resultClass);
			return cbv.getResultList();
		}
	}

	/**
	 * Returns the instance with the given {@code id}, of the
	 * {@link #modelClass} type managed by this repository.
	 *
	 * @param id entity id, must not be {@literal null}
	 * @return an instance of {@link #modelClass}
	 */
	public ENTITY findById(final ID_TYPE id) {
		return this.findById(id, this.getModelClass());
	}

	/**
	 * Returns the instance with the given {@code id}, of the given
	 * {@code resultClass} type.
	 * <p>
	 * The {@code resultClass} can be the {@link #modelClass} itself or any
	 * {@link BasicModelView} of it.
	 *
	 * @param <RESULT> the type of {@code resultClass}
	 * @param id entity id, must not be {@literal null}
	 * @param resultClass entity or view class
	 * @return an instance of {@link #modelClass}
	 */
	public <RESULT extends BasicModel<ID_TYPE>> RESULT findById(final ID_TYPE id, final Class<RESULT> resultClass) {
		final CriteriaBuilder<RESULT> cb = this.createCriteriaBuilder(resultClass);
		cb.where("id").eq(id);
		return cb.getSingleResult();
	}

	/**
	 * Returns the instances with the given {@code id}s, of the
	 * {@link #modelClass} type managed by this repository.
	 *
	 * @param ids entity ids
	 * @return instances of {@link #modelClass}
	 */
	public List<ENTITY> findByIds(final Collection<?> ids) {
		return this.findByIds(ids, this.getModelClass());
	}

	/**
	 * Returns the instances with the given {@code id}s, of the given
	 * {@code resultClass} type.
	 * <p>
	 * The {@code resultClass} can be the {@link #modelClass} itself or any
	 * {@link BasicModelView} of it.
	 *
	 * @param <RESULT> the type of {@code resultClass}
	 * @param ids entity ids
	 * @param resultClass entity or view class
	 * @return instances of {@code resultClass}
	 */
	public <RESULT extends BasicModel<ID_TYPE>> List<RESULT> findByIds(final Collection<?> ids, final Class<RESULT> resultClass) {
		if (CollectionUtils.isEmpty(ids)) {
			return new ArrayList<>();
		}
		final CriteriaBuilder<RESULT> cb = this.createCriteriaBuilder(resultClass);
		cb.where("id").in(ids);
		return cb.getResultList();
	}

	protected Class<ENTITY> getModelClass() {
		return this.modelClass;
	}

	protected CriteriaBuilder<ENTITY> createCriteriaBuilder() {
		return this.createCriteriaBuilder(this.getModelClass());
	}

	protected <RESULT extends BasicModel<ID_TYPE>> CriteriaBuilder<RESULT> createCriteriaBuilder(final Class<RESULT> resultClass) {
		return this.createCriteriaBuilder(null, resultClass);
	}

	protected CriteriaBuilder<ENTITY> createCriteriaBuilder(final String alias) {
		return this.createCriteriaBuilder(alias, this.getModelClass());
	}

	protected <RESULT extends BasicModel<ID_TYPE>> CriteriaBuilder<RESULT> createCriteriaBuilder(final String alias, final Class<RESULT> resultClass) {
		return this.createCriteriaBuilder(alias, null, resultClass);
	}

	@SuppressWarnings("unchecked")
	protected <RESULT extends BasicModel<ID_TYPE>> CriteriaBuilder<RESULT> createCriteriaBuilder(
		final String alias,
		final SearchSpecification specification,
		final Class<RESULT> resultClass) {

		final CriteriaBuilder<RESULT> cb;
		final CriteriaBuilder<ENTITY> entityCb = Optional.ofNullable(alias)
			.map(o -> this.criteriaBuilderFactory.create(this.entityManager, this.getModelClass(), o))
			.orElse(this.criteriaBuilderFactory.create(this.entityManager, this.getModelClass()));
		if (specification != null) {
			specification.apply(entityCb);
		}
		if (BasicModelView.class.isAssignableFrom(resultClass)) {
			final EntityViewSetting<RESULT, CriteriaBuilder<RESULT>> setting = EntityViewSetting.create(resultClass);
			cb = this.entityViewManager.applySetting(setting, entityCb);
		} else {
			cb = (CriteriaBuilder<RESULT>) entityCb;
		}
		return cb;
	}

	private CriteriaBuilder<ENTITY> createEntityPaginatedCriteriaBuilder(
		final SearchSpecification specification,
		final OffsetSizePageRequest pageRequest) {

		final CriteriaBuilder<ENTITY> cb = this.createCriteriaBuilder(null, specification, this.getModelClass());
		if (pageRequest != null) {
			pageRequest.getPageOrders().forEach(each -> {
				if (each.isAscending()) {
					cb.orderByAsc(each.getExpression());
				} else {
					cb.orderByDesc(each.getExpression());
				}
			});
		} else {
			// Add default order by clause: id ASC
			// (otherwise, java.lang.IllegalStateException: Pagination requires at least one order by item! is thrown)
			cb.orderByAsc("id");
		}
		return cb;
	}

	private <RESULT extends BasicModel<ID_TYPE>> PaginatedCriteriaBuilder<RESULT> createViewPaginatedCriteriaBuilder(
		final SearchSpecification specification,
		final OffsetSizePageRequest pageRequest,
		final Class<RESULT> resultClass) {

		final CriteriaBuilder<ENTITY> cb = this.createCriteriaBuilder(null, specification, this.getModelClass());
		final int offset = Optional.ofNullable(pageRequest).map(o -> (int) o.getOffset()).orElse(0);
		final int pageSize = Optional.ofNullable(pageRequest).map(o -> o.getPageSize()).orElse(Integer.MAX_VALUE);
		final EntityViewSetting<RESULT, PaginatedCriteriaBuilder<RESULT>> setting = EntityViewSetting.create(resultClass, offset, pageSize);
		if (pageRequest != null) {
			pageRequest.getPageOrders().forEach(each -> {
				final Sorter sorter = each.isAscending() ? Sorters.ascending() : Sorters.descending();
				setting.addAttributeSorter(each.getExpression(), sorter);
			});
		}
		final PaginatedCriteriaBuilder<RESULT> cbv = this.entityViewManager.applySetting(setting, cb);
		return cbv;
	}

	protected UpdateCriteriaBuilder<ENTITY> updateCriteriaBuilder() {
		final UpdateCriteriaBuilder<ENTITY> cb = this.criteriaBuilderFactory.update(this.entityManager, this.getModelClass());
		return cb;
	}

	protected DeleteCriteriaBuilder<ENTITY> deleteCriteriaBuilder() {
		final DeleteCriteriaBuilder<ENTITY> cb = this.criteriaBuilderFactory.delete(this.entityManager, this.getModelClass());
		return cb;
	}

	/**
	 * Saves changes made on the entity view.
	 * The entityView must be annotated with {@link UpdatableEntityView} with ENTITY strategy:
	 *
	 * <pre>@UpdatableEntityView(strategy = FlushStrategy.ENTITY)</pre>
	 *
	 * in order to let JPA manage the persistence (optimistic-locking version update, and any modelEntityListener events if configured).
	 * <br/>
	 *
	 * Furthermore, the entityView must have some setters defined to modify the property value
	 * .
	 * @param entityView
	 */
	public void save(final ModelView entityView) {
		this.entityViewManager.save(this.entityManager, entityView);
	}

	@Transactional(rollbackFor = Throwable.class)
	public ENTITY persist(final ENTITY model) {
		ENTITY result = null;
		if (model.getId() == null) {
			this.entityManager.persist(model);
			result = model;
		} else {
			final ENTITY mergedModel = this.entityManager.merge(model);
			this.entityManager.persist(mergedModel);
			result = mergedModel;
		}
		return result;
	}

	@Transactional(rollbackFor = Throwable.class)
	public List<ENTITY> persistAll(final List<ENTITY> models) {
		for (final ENTITY eachModel : models) {
			this.persist(eachModel);
		}
		return models;
	}

	@SuppressWarnings("unchecked")
	@Transactional(rollbackFor = Throwable.class)
	public void remove(final ENTITY model) {
		this.remove((Class<ENTITY>) model.getClass(), model.getId());
	}

	@Transactional(rollbackFor = Throwable.class)
	public void remove(final List<ENTITY> modelList) {
		if (CollectionUtils.isEmpty(modelList)) {
			return;
		}
		for (final ENTITY eachItem : modelList) {
			this.remove(eachItem);
		}
	}

	@Transactional(rollbackFor = Throwable.class)
	public void remove(final Class<ENTITY> modelClass, final ID_TYPE id) {
		this.entityManager.remove(this.entityManager.getReference(modelClass, id));
	}

	@Transactional(rollbackFor = Throwable.class)
	public void remove(final ID_TYPE idToRemove) {
		final DeleteCriteriaBuilder<ENTITY> deleteCriteriaBuilder = this.criteriaBuilderFactory.delete(this.entityManager, this.getModelClass())
			.where("id").eq(idToRemove);
		final int deletedEntities = deleteCriteriaBuilder.executeUpdate();
		if (deletedEntities != 1) {
			throw new CouldNotRemoveEntity("ERROR: Could not remove entity of type " + this.getModelClass().getName() + " with id: " + idToRemove);
		}
	}

	@Transactional(rollbackFor = Throwable.class)
	public void removeAll(final List<ID_TYPE> idsToRemove) {
		final DeleteCriteriaBuilder<ENTITY> deleteCriteriaBuilder = this.criteriaBuilderFactory.delete(this.entityManager, this.getModelClass())
			.where("id").in(idsToRemove);
		final int deletedEntities = deleteCriteriaBuilder.executeUpdate();
		if (deletedEntities != idsToRemove.size()) {
			throw new CouldNotRemoveEntity("ERROR: Could not remove some entities of type " + this.getModelClass().getName() + " with ids: " + idsToRemove);
		}
	}

	public void updateCollection(final ENTITY sourceContainer, final ENTITY targetContainer, final String attributeName) throws Exception {
		final Field collectionField = sourceContainer.getClass().getDeclaredField(attributeName);
		final String mappedBy = collectionField.getAnnotation(OneToMany.class).mappedBy();
		final ParameterizedType collectionType = (ParameterizedType) collectionField.getGenericType();
		@SuppressWarnings("unchecked")
		final Class<? extends BasicModelEntity<ID_TYPE>> modelClass = (Class<? extends BasicModelEntity<ID_TYPE>>) collectionType.getActualTypeArguments()[0];
		final PropertyDescriptor mappedByProperty = new PropertyDescriptor(mappedBy, modelClass);
		final Method mappedBySetter = mappedByProperty.getWriteMethod();
		final PropertyDescriptor collectionProperty = new PropertyDescriptor(attributeName, sourceContainer.getClass());
		final Method collectionGetter = collectionProperty.getReadMethod();
		@SuppressWarnings("unchecked")
		final Set<ENTITY> sourceCollection = (Set<ENTITY>) collectionGetter.invoke(sourceContainer);
		@SuppressWarnings("unchecked")
		final Set<ENTITY> targetCollection = (Set<ENTITY>) collectionGetter.invoke(targetContainer);

		// ...	If source and target collections are the same, nothing should be done.
		if (sourceCollection == targetCollection) {
			return;
		}

		// ...	Create two groups of elements: those with no id (strictly new) and those with id.
		final Map<Boolean, List<ENTITY>> splitedMap = sourceCollection.stream().collect(Collectors.partitioningBy(each -> each.getId() != null));

		// ...	Remove from target collection all elements not present in source collection.
		targetCollection.removeAll(SetUtils.difference(targetCollection, new HashSet<>(splitedMap.get(true))).toSet());

		// ...	Add or update existing elements from source collection.
		for (final ENTITY each : splitedMap.get(true)) {
			mappedBySetter.invoke(each, targetContainer);
			final ENTITY matching = targetCollection.stream().filter(o -> o.equals(each)).findFirst().orElse(null);
			if (matching != null) {
				each.setVersion(matching.getVersion());
				final ENTITY merged = this.entityManager.merge(each);
				targetCollection.remove(each);
				targetCollection.add(merged);
			} else {
				targetCollection.add(each);
			}
		}

		// ...	Add strictly new elements.
		targetCollection.addAll(splitedMap.get(false));
	}

	@Transactional(rollbackFor = Throwable.class)
	public void update(final ENTITY model) throws Exception {
		final ENTITY current = this.findById(model.getId());
		this.update(model, current);
	}

	@Transactional(rollbackFor = Throwable.class)
	public void update(final ENTITY source, final ENTITY target) throws Exception {
		this.createReferences(source);
		this.updateCollections(source, target);
		source.setVersion(target.getVersion());
		this.persist(source);
	}

	@Transactional(rollbackFor = Throwable.class)
	public void create(final ENTITY model) {
		this.createReferences(model);
		this.persist(model);
	}

	public ENTITY createReference(final ID_TYPE id) {
		return this.entityManager.getReference(this.getModelClass(), id);
	}

	protected void createReferences(final ENTITY model) {
		// ... Do nothing by default.
	}

	protected void updateCollections(final ENTITY source, final ENTITY target) throws Exception {
		// ... Do nothing by default.
	}

	public void flush() {
		this.entityManager.flush();
		this.entityManager.clear();
	}

	/**
	 * <h1>Method to optimize {@link javax.persistence.ManyToMany @ManyToMany}
	 * collections creation.</h1>
	 * <p>
	 * During the creation of an object that contains attributes of the
	 * {@link javax.persistence.ManyToMany @ManyToMany} collection type, each
	 * element of that collection is replaced by a new JPA reference to avoid
	 * unnecessary selects. The {@link javax.persistence.ManyToMany @ManyToMany}
	 * collections have to be defined as {@link java.util.Set Set} to optimize
	 * updates and to be able to create views with <b>Blaze Persistence</b>
	 * where several {@link javax.persistence.ManyToMany @ManyToMany}
	 * collections are retrieved at once. On the other hand, to be able to
	 * create <b>Blaze</b> views, entities must have the
	 * {@link java.lang.Object#equals(Object) equals()} and
	 * {@link java.lang.Object#hashCode() hashCode()} method redefined. But the
	 * JPA references of entities that have the
	 * {@link java.lang.Object#equals(Object) equals()} method redefined are
	 * transformed into the actual object when the
	 * {@link java.lang.Object#hashCode() hashCode()} method is called. This
	 * method is invariably called when the original {@link java.util.Set Set}
	 * is transformed internally into a
	 * {@link org.hibernate.collection.internal.PersistentSet PersistentSet}.
	 * This causes a select to be executed for each element of the
	 * {@link javax.persistence.ManyToMany @ManyToMany} collection to be
	 * inserted.
	 * <p>
	 * When an entity does not have the {@link java.lang.Object#equals(Object)
	 * equals()} and {@link java.lang.Object#hashCode() hashCode()} methods
	 * redefined, the JPA proxy intercepts the
	 * {@link java.lang.Object#hashCode() hashCode()} call and returns the
	 * default proxy implementation. This implementation is the same as that of
	 * all entities that inherit from Model, so there would be no problem in
	 * executing the version of {@link java.lang.Object#hashCode() hashCode()}
	 * implemented in the proxy.
	 * <p>
	 * The workaround to avoid this behavior is to force the proxy factory to
	 * build proxies with the {@code overridesEquals} flag set to false. This
	 * flag is not directly accessible through a method, so reflection must be
	 * used to alter it.
	 * <p>
	 * This method has to be called before the first proxy of the
	 * {@link BasicModelEntity} class is generated.
	 *
	 * @param modelClass
	 */
	protected void initializeProxyFactory(final Class<? extends BasicModelEntity<ID_TYPE>> modelClass) {
		final Session session = this.entityManager.unwrap(Session.class);
		final SessionFactoryImplementor sessionFactoryImplementor = (SessionFactoryImplementor) session.getSessionFactory();
		final ByteBuddyProxyFactory proxyFactory = (ByteBuddyProxyFactory) sessionFactoryImplementor
			.getMetamodel().entityPersister(modelClass).getEntityMetamodel().getTuplizer().getProxyFactory();
		try {
			final Field overridesEqualsField = ByteBuddyProxyFactory.class.getDeclaredField("overridesEquals");
			final boolean accessible = overridesEqualsField.canAccess(proxyFactory);
			overridesEqualsField.setAccessible(true);
			overridesEqualsField.setBoolean(proxyFactory, false);
			overridesEqualsField.setAccessible(accessible);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			// Do nothing, leave the proxy unmodified
		}
	}

	protected String getSchemaTableName(final String tableName) {
		return this.defaultSchema.equals(BasicModelEntityRepository.NO_SCHEMA)
			? tableName
			: this.defaultSchema + "." + tableName;
	}

}
