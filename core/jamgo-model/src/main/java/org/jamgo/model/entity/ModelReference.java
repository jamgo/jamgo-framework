package org.jamgo.model.entity;

import java.util.Objects;

import javax.persistence.Embeddable;

/**
 * Embeddable entity to store a reference to a {@link Model} entity. This
 * reference contains the model class name, the model id and (optionally) its
 * version.
 *
 * @author Jamgo SCCL
 */
@Embeddable
public class ModelReference {

	private String modelClassName;
	private Long modelId;
	private Long modelVersion;

	public String getModelClassName() {
		return this.modelClassName;
	}

	public void setModelClassName(final String modelClassName) {
		this.modelClassName = modelClassName;
	}

	public Long getModelId() {
		return this.modelId;
	}

	public void setModelId(final Long modelId) {
		this.modelId = modelId;
	}

	public Long getModelVersion() {
		return this.modelVersion;
	}

	public void setModelVersion(final Long modelVersion) {
		this.modelVersion = modelVersion;
	}

	@Override
	public String toString() {
		return String.format("%s#%s#%s", this.getModelClassName(), this.getModelId(), this.getModelVersion());
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getModelClassName(), this.getModelId(), this.getModelVersion());
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		return (obj instanceof ModelReference)
				&& Objects.equals(this.getModelClassName(), ((ModelReference) obj).getModelClassName())
				&& Objects.equals(this.getModelId(), ((ModelReference) obj).getModelId())
				&& Objects.equals(this.getModelVersion(), ((ModelReference) obj).getModelVersion());
	}

}
