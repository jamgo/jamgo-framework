package org.jamgo.model.entity;

public interface ModelEntityListenerHelper {

	void onPostPersist(final Model m);

	void onPostUpdate(final Model m);

	void onPostRemove(final Model m);

}
