package org.jamgo.model.repository;

import java.util.List;

import javax.persistence.NoResultException;

import org.jamgo.model.entity.SecuredObject;
import org.jamgo.model.enums.SecuredObjectType;
import org.springframework.stereotype.Repository;

@Repository
public class SecuredObjectRepository extends ModelRepository<SecuredObject> {

	public List<SecuredObject> findByParentIsNull() {
		return this.createCriteriaBuilder()
			.where("parent").isNull()
			.getResultList();
	}

	public List<SecuredObject> findByParent(final SecuredObject parent) {
		return this.createCriteriaBuilder()
			.where("parent").eq(parent)
			.getResultList();
	}

	public SecuredObject findBySecuredObjectTypeAndSecuredObjectKey(final SecuredObjectType securedObjectType, final String securedObjectKey) {
		if (securedObjectKey == null) {
			return null;
		}
		try {
			return this.createCriteriaBuilder()
				.where("securedObjectType").eq(securedObjectType)
				.where("securedObjectKey").eq(securedObjectKey)
				.getSingleResult();
		} catch (final NoResultException e) {
			return null;
		}
	}

	public SecuredObject getSecuredObject(final SecuredObjectType securedObjectType, final String parentSecuredObjectKey, final String securedObjectKey) {
		try {
			return this.createCriteriaBuilder()
				.where("parent.securedObjectKey").eq(parentSecuredObjectKey)
				.where("parent.securedObjectType").eq(securedObjectType)
				.where("securedObjectKey").eq(securedObjectKey)
				.getSingleResult();
		} catch (final NoResultException e) {
			return null;
		}
	}

	public SecuredObject getSecuredObject(final SecuredObjectType securedObjectType, final String securedObjectKey) {
		try {
			return this.createCriteriaBuilder()
				.where("securedObjectType").eq(securedObjectType)
				.where("securedObjectKey").eq(securedObjectKey)
				.getSingleResult();
		} catch (final NoResultException e) {
			return null;
		}
	}

}
