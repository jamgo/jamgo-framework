package org.jamgo.model.entity;

import java.util.Comparator;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "localized_message")
public class LocalizedMessage extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<LocalizedMessage> KEY_ORDER = new Comparator<>() {
		@Override
		public int compare(final LocalizedMessage objectA, final LocalizedMessage objectB) {
			return objectA.getKey().compareToIgnoreCase(objectB.getKey());
		}
	};

	@Column(name = "message_key")
	private String key;

	@Type(type = "json")
	@Column(name = "message_value", columnDefinition = "json")
	private LocalizedString value = new LocalizedString();

	@Type(type = "json")
	@Column(columnDefinition = "json")
	private LocalizedString description = new LocalizedString();

	@Type(type = "json")
	@Column(name = "parameters_info", columnDefinition = "json")
	private LocalizedString parametersInfo = new LocalizedString();

	@Transient
	private boolean missing;

	public LocalizedMessage() {
		super();
	}

	public String getKey() {
		return this.key;
	}

	public void setKey(final String key) {
		this.key = key;
	}

	public LocalizedString getValue() {
		return this.value;
	}

	public void setValue(final LocalizedString value) {
		this.value = value;
	}

	public String getValue(final Locale locale) {
		return this.value.get(locale);
	}

	public String getValue(final String languageCode) {
		return this.value.get(languageCode);
	}

	public LocalizedString getDescription() {
		return this.description;
	}

	public void setDescription(final LocalizedString description) {
		this.description = description;
	}

	public boolean isMissing() {
		return this.missing;
	}

	public void setMissing(final boolean missing) {
		this.missing = missing;
	}

	public void setValue(final String value, final Locale locale) {
		this.setValue(value, null, locale);
	}

	public void setValue(final String value, final String description, final Locale locale) {
		this.value.set(locale.getLanguage(), value);
		this.description.set(locale.getLanguage(), value);
	}

	public LocalizedString getParametersInfo() {
		return this.parametersInfo;
	}

	public void setParametersInfo(final LocalizedString parametersInfo) {
		this.parametersInfo = parametersInfo;
	}
}
