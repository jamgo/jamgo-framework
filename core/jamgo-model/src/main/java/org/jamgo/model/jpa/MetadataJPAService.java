package org.jamgo.model.jpa;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManagerFactory;
import javax.persistence.metamodel.EntityType;

import org.jamgo.model.entity.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MetadataJPAService {

	public static final Logger logger = LoggerFactory.getLogger(MetadataJPAService.class);

	@Autowired
	private EntityManagerFactory factory;

	@Transactional
	public List<EntityAttributePair> getReferences(final Class<? extends Model> modelClass) {
		final Set<EntityType<?>> entityTypes = this.factory.getMetamodel().getEntities();

		// Find the entity type related to the modelClass
		final EntityType<?> modelType = entityTypes.stream().filter(type -> type.getJavaType().equals(modelClass)).findFirst().orElse(null);
		MetadataJPAService.logger.debug("Model type found: {}", modelType);

		// DEBUG
		entityTypes.forEach(type -> {
			MetadataJPAService.logger.trace("- EntityType: {}", type.getName());
			type.getAttributes().forEach(attr -> MetadataJPAService.logger.trace("-- {}: {} ({})", attr.getName(), attr.getJavaType(), attr.getPersistentAttributeType()));
		});

		// Find the entities related with modelType
		final List<EntityType<?>> types = entityTypes.stream()
			.filter(type -> type.getAttributes().stream().anyMatch(attr -> attr.getJavaType().equals(modelClass)))
			.collect(Collectors.toList());

		// Return references
		final List<EntityAttributePair> list = new ArrayList<>();
		types.stream().forEach(type -> {
			final Class<?> entityClass = type.getJavaType();
			type.getAttributes().stream()
				.filter(attr -> attr.getJavaType().equals(modelClass))
				.forEach(attr -> {
					final String attributeName = attr.getName();
					final EntityAttributePair reference = new EntityAttributePair(entityClass, attributeName);
					list.add(reference);
					MetadataJPAService.logger.debug("- reference: {}", reference);
				});
		});
		return list;
	}

	public static final class EntityAttributePair {

		private final Class<?> entityClass;
		private final String attributeName;

		EntityAttributePair(final Class<?> enityClass, final String attributeName) {
			this.entityClass = enityClass;
			this.attributeName = attributeName;
		}

		public Class<?> getEntityClass() {
			return this.entityClass;
		}

		public String getAttributeName() {
			return this.attributeName;
		}

		@Override
		public String toString() {
			return String.format("%s-%s", this.entityClass.getCanonicalName(), this.attributeName);
		}
	}

}
