package org.jamgo.model.enums;

public enum Permission {
	NONE,
	READ,
	WRITE,
	ALL;

	public boolean canRead() {
		return this == READ || this == ALL;
	}

	public boolean canWrite() {
		return this == READ || this == ALL;
	}

}
