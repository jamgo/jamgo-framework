package org.jamgo.model.entity;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

import org.jamgo.model.BasicModel;

@MappedSuperclass
public abstract class BasicModelEntity<ID_TYPE> implements Serializable, BasicModel<ID_TYPE> {

	public static final String VALID_ENTITY_REMOVAL = "OK";

	private static final long serialVersionUID = 1L;

	public BasicModelEntity() {
		super();
	}

	@Override
	public abstract ID_TYPE getId();

	public abstract void setId(ID_TYPE id);

	// ... By defualt, there are a unique version for basic models
	@Override
	public Long getVersion() {
		return 1L;
	}

	// ... By defualt, there are a unique version for basic models
	public void setVersion(final Long version) {
	}

	@Override
	public int hashCode() {
		return this.getId() != null ? this.getId().hashCode() : 0;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}

		return (obj instanceof BasicModelEntity<?>) && (((BasicModelEntity<?>) obj).getId() != null) && ((BasicModelEntity<?>) obj).getId().equals(this.getId());
	}

}