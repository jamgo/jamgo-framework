package org.jamgo.model.converter;

import java.util.Optional;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.jamgo.model.valueobjects.IBAN;

@Converter
public class IBANConverter implements AttributeConverter<IBAN, String> {

	@Override
	public String convertToDatabaseColumn(final IBAN iban) {
		return Optional.ofNullable(iban).map(i -> i.getValue()).orElse(null);
	}

	@Override
	public IBAN convertToEntityAttribute(final String dbData) {
		return Optional.ofNullable(dbData).map(data -> new IBAN(data)).orElse(null);
	}

}
