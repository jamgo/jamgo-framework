package org.jamgo.model.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OffsetSizePageRequest {

	private long offset;
	private int pageSize;
	private List<Order> pageOrders;

	public OffsetSizePageRequest() {
		this(0, 20);
	}

	public OffsetSizePageRequest(final long offset, final int pageSize) {
		this(offset, pageSize, new ArrayList<>());
	}

	public OffsetSizePageRequest(final long offset, final int pageSize, final List<Order> pageOrders) {
		this.offset = offset;
		this.pageSize = pageSize;
		this.pageOrders = pageOrders;
	}

	public int getPageNumber() {
		// this is the odd case, when the offset is > 0 but smaller than the page size.
		// That means that there actually is a previous page.
		if (this.getOffset() > 0 && this.getOffset() < this.getPageSize()) {
			return 1;
		}
		return (int) this.getOffset() / this.getPageSize();
	}

	public void setOffset(final long offset) {
		this.offset = offset;
	}

	public void setPageSize(final int pageSize) {
		this.pageSize = pageSize;
	}

	public void setPageOrders(final List<Order> pageOrders) {
		this.pageOrders = pageOrders;
	}

	public int getPageSize() {
		return this.pageSize;
	}

	public long getOffset() {
		return this.offset;
	}

	public List<Order> getPageOrders() {
		return this.pageOrders;
	}

	public OffsetSizePageRequest next() {
		return new OffsetSizePageRequest(this.getOffset() + this.getPageSize(), this.getPageSize(), this.getPageOrders());
	}

	public OffsetSizePageRequest previousOrFirst() {
		if (this.getOffset() - this.getPageSize() <= 0) {
			return this.first();
		}
		return new OffsetSizePageRequest(this.getOffset() - this.getPageSize(), this.getPageSize(), this.getPageOrders());
	}

	public OffsetSizePageRequest first() {
		if (this.getOffset() == 0) {
			return this;
		}
		return new OffsetSizePageRequest(0, this.getPageSize(), this.getPageOrders());
	}

	public boolean hasPrevious() {
		return this.getOffset() > 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.offset, this.pageSize, this.pageOrders, super.hashCode());
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}
		final OffsetSizePageRequest other = (OffsetSizePageRequest) obj;
		return Objects.equals(this.offset, other.offset)
			&& Objects.equals(this.pageSize, other.pageSize)
			&& Objects.equals(this.pageOrders, other.pageOrders)
			&& super.equals(obj);
	}

	@Override
	public String toString() {
		return "OffsetSizePageRequest [offset=" + this.offset + ", pageSize=" + this.pageSize + ", pageOrders=" + this.pageOrders + "]";
	}

}
