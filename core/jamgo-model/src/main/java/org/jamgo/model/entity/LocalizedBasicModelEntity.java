package org.jamgo.model.entity;

import java.io.Serializable;
import java.util.Objects;

import org.springframework.core.ResolvableType;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class LocalizedBasicModelEntity<ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE extends Serializable> extends LocalizedObject<ENTITY, ID_TYPE> {

	private static final long serialVersionUID = 1L;

	@JsonIgnore
	protected Class<ENTITY> modelClass;

	@SuppressWarnings("unchecked")
	public LocalizedBasicModelEntity() {
		this.modelClass = (Class<ENTITY>) ResolvableType.forClass(this.getClass()).getSuperType().getGeneric(0).resolve();
	}

	public Class<ENTITY> getModelClass() {
		return this.modelClass;
	}

	@Override
	public ID_TYPE get(final String lang) {
		return super.get(lang);
	}

	@Override
	public void set(final String lang, final ENTITY value) {
		this.getInternalMap().remove(lang);
		if (value != null) {
			this.getInternalMap().put(lang, value.getId());
		}
	}

	@Override
	public boolean equals(final Object obj) {
		if (super.equals(obj)) {
			final LocalizedBasicModelEntity<?, ?> other = (LocalizedBasicModelEntity<?, ?>) obj;
			return Objects.equals(this.modelClass, other.modelClass);
		}
		return false;
	}
}
