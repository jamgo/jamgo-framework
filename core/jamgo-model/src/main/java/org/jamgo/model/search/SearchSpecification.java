package org.jamgo.model.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.jamgo.model.entity.Role;
import org.jamgo.model.entity.SecuredObject;
import org.jamgo.model.entity.User;
import org.jamgo.model.util.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.blazebit.persistence.CriteriaBuilder;

public abstract class SearchSpecification {

	private static final Logger logger = LoggerFactory.getLogger(SearchSpecification.class);
	private static final String DEFAULT_LOCALE_CODE = "ca-ES";

	private User user;
	private Properties properties;
	private List<Order> orders;
	private Locale locale;

	@Value("${permissions.enabled:true}")
	private boolean permissionsEnabled;

	public SearchSpecification() {
	}

	public SearchSpecification(final User user, final Properties properties) {
		this.user = user;
		this.setProperties(properties);
	}

	public SearchSpecification(final Locale locale) {
		this.locale = locale;
	}

	public void apply(final CriteriaBuilder<?> cb) {
		if (this.permissionsEnabled) {
			this.applyPermissions(cb);
		}
	}

	protected void applyPermissions(final CriteriaBuilder<?> cb) {
		final List<Long> userRoleIds = new ArrayList<>();
		if (this.user != null) {
			for (final Role role : this.user.getRoles()) {
				userRoleIds.add(role.getId());
			}
		}

		final String modelClassName = cb.getResultType().getName();

		final CriteriaBuilder<SecuredObject> subCb = cb.getCriteriaBuilderFactory().create(cb.getEntityManager(), SecuredObject.class, "securedObjects");
		subCb
			.select("secured_object_key")
			.leftJoin("parent", "p")
			.leftJoin("acl", "a")
			.where("p.securedObjectKey").eq(modelClassName)
			.where("a.readPermission").eq("0");

		if (!userRoleIds.isEmpty()) {
			subCb.where("a.roleId").in(userRoleIds);
		}

		cb.where("id").notIn(subCb);
	}

//	TODO: Review and finish permissions migration

//	@Override
//	public Predicate toPredicate(final Root<T> root, final CriteriaQuery<?> cq, final CriteriaBuilder cb) {
//		final List<Predicate> predicates = new ArrayList<>();
//		return this.andTogether(root, predicates, cb);
//	}
//
//	protected Predicate andTogether(final Root<T> root, final List<Predicate> predicates, final CriteriaBuilder cb) {
//		if (this.permissionsEnabled) {
//			this.andPermissions(root, predicates, cb);
//		}
//		return cb.and(predicates.toArray(new Predicate[0]));
//	}
//
//	protected void andPermissions(final Root<T> root, final List<Predicate> predicates, final CriteriaBuilder cb) {
//		final List<Long> userRoleIds = new ArrayList<>();
//		if (this.user != null) {
//			for (final Role role : this.user.getRoles()) {
//				userRoleIds.add(role.getId());
//			}
//		}
//
//		final CriteriaQuery<SecuredObject> cq = cb.createQuery(SecuredObject.class);
//		final Subquery<SecuredObject> securedObjectSubQuery = cq.subquery(SecuredObject.class);
//		final Root<SecuredObject> securedObjectRoot = securedObjectSubQuery.from(SecuredObject.class);
//		final Join<SecuredObject, SecuredObject> securedObjectJoin = securedObjectRoot.join("parent", JoinType.LEFT);
//		final Join<SecuredObject, Acl> aclJoin = securedObjectRoot.join("acl", JoinType.LEFT);
//		final List<Predicate> securedObjectQueryPredicates = new ArrayList<>(Arrays.asList(cb.equal(securedObjectJoin.get("securedObjectKey"), root.getModel().getJavaType().getName()),
//			cb.equal(securedObjectJoin.get("id"), securedObjectRoot.get("parent").get("id")),
//			cb.equal(aclJoin.get("securedObject").get("id"), securedObjectRoot.get("id")),
//			cb.equal(aclJoin.get("readPermission"), "0")));
//		if (!userRoleIds.isEmpty()) {
//			securedObjectQueryPredicates.add(aclJoin.get("roleId").in(userRoleIds));
//		}
//		securedObjectSubQuery.select(securedObjectRoot.get("securedObjectKey"))
//			.where(securedObjectQueryPredicates.toArray(new Predicate[securedObjectQueryPredicates.size()]));
//		String attributeName = "id";
//		if ((this.properties != null) && this.properties.containsKey(root.getModel().getJavaType().getName())) {
//			attributeName = this.properties.getProperty(root.getModel().getJavaType().getName());
//		}
//		predicates.add(cb.not(root.get(attributeName).as(String.class).in(securedObjectSubQuery)));
//		/*
//		 * select * from demo_entity de
//		 * where de.id not in (
//		 * 		select secured_object_key from secured_object seo
//		 * 		join secured_object seop on seop.id = seo.parent_id and seop.secured_object_key = 'org.jamgo.demo.backoffice.model.DemoEntity)
//		 * 		join acl a on a.secured_object_id = seo.id and a.read_permission = 0 and role_id in (?)
//		 *
//		 */
//	}

	public User getUser() {
		return this.user;
	}

	public void setUser(final User user) {
		this.user = user;
	}

	public Properties getProperties() {
		return this.properties;
	}

	public void setProperties(final Properties properties) {
		this.properties = properties;
	}

	public List<Order> getOrders() {
		return this.orders;
	}

	public void setOrders(final List<Order> orders) {
		this.orders = orders;
	}

	public Locale getLocale() {
		return this.locale;
	}

	public String getLocaleCode() {
		String localeCode;
		if (this.locale != null) {
			SearchSpecification.logger.debug("Using locale from session context: {}", this.locale);
			localeCode = this.locale.toLanguageTag();
		} else {
			SearchSpecification.logger.debug("Using default locale code: {}", SearchSpecification.DEFAULT_LOCALE_CODE);
			localeCode = SearchSpecification.DEFAULT_LOCALE_CODE;
		}
		return localeCode;
	}

}