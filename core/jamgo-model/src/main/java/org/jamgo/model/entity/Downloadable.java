package org.jamgo.model.entity;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;

import org.hibernate.engine.jdbc.BlobProxy;
import org.jamgo.model.exception.BinaryResourceException;

import com.google.common.io.ByteStreams;

public interface Downloadable {

	String getMimeType();

	String getFileName();

	Blob getContents();

	void setContents(final Blob contents);

	default byte[] getByteContents() {
		try {
			return ByteStreams.toByteArray(this.getContents().getBinaryStream());
		} catch (IOException | SQLException e) {
			throw new BinaryResourceException(e.getMessage());
		}
	}

	default void setByteContents(final byte[] byteContents) {
		this.setContents(BlobProxy.generateProxy(byteContents));
	}

	Integer getFileLength();
}
