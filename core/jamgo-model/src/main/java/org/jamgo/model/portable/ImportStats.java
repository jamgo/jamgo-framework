package org.jamgo.model.portable;

import java.util.List;

public class ImportStats<T> {

	private int numberOfInserts;
	private int numberOfDeletes;
	private int numberOfUpdates;
	private List<T> data;
	private List<String> errors;

	public int getNumberOfInserts() {
		return this.numberOfInserts;
	}

	public void setNumberOfInserts(int numberOfInserts) {
		this.numberOfInserts = numberOfInserts;
	}

	public int getNumberOfDeletes() {
		return this.numberOfDeletes;
	}

	public void setNumberOfDeletes(int numberOfDeletes) {
		this.numberOfDeletes = numberOfDeletes;
	}

	public int getNumberOfUpdates() {
		return this.numberOfUpdates;
	}

	public void setNumberOfUpdates(int numberOfUpdates) {
		this.numberOfUpdates = numberOfUpdates;
	}

	public List<T> getData() {
		return this.data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public List<String> getErrors() {
		return this.errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	
	public int getNumberOfErrors() {
		int numErrors = 0;
		if (this.errors != null) {
			numErrors = this.errors.size();
		}
		return numErrors;
	}

}
