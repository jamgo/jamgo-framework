package org.jamgo.model.exception;

public class JmgException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public JmgException(final String message) {
		super(message);
	}
}
