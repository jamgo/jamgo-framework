package org.jamgo.model.entity;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

/**
 * Represents any group or division in a system of classification.
 *
 * @author aheredia
 */
@MappedSuperclass
public abstract class Category extends Model {

	private static final long serialVersionUID = 1L;

	@Transient
	public static final Comparator<? extends Category> NAME_ORDER = new Comparator<Category>() {
		@Override
		public int compare(final Category objectA, final Category objectB) {
			return objectA.getName().compareTo(objectB.getName());
		}
	};

	@Type(type = "json")
	@Column(columnDefinition = "jsonb", nullable = false)
	private LocalizedString name;

	@Type(type = "json")
	@Column(columnDefinition = "jsonb")
	private LocalizedString description;

	public Category() {
		super();
	}

	public Category(final Long id) {
		super(id);
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(final LocalizedString name) {
		this.name = name;
	}

	public LocalizedString getDescription() {
		return this.description;
	}

	public void setDescription(final LocalizedString description) {
		this.description = description;
	}

	@Override
	public String toListString() {
		return this.getName().getDefaultText();
	}

}