package org.jamgo.model.valueobjects;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Email implements ValueObject {

	private static final String REGEX_PATTERN = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
		+ "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
	private static final Pattern PATTERN = Pattern.compile(Email.REGEX_PATTERN);

	private final String value;

	public Email(@JsonProperty("value") final String value) {
		if (Email.isValid(value)) {
			this.value = value;
		} else {
			throw new IllegalArgumentException("Invalid email address on email address " + value + ".");
		}
	}

	@Override
	public String getValue() {
		return this.value;
	}

	/**
	 * Check if the email is valid, matching a known regular expression.
	 * <code>null</code> emails are considered not valid.
	 * @param email
	 * @return
	 */
	public static boolean isValid(final String email) {
		return Optional.ofNullable(email).map(e -> Email.PATTERN.matcher(e).matches()).orElse(false);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || this.getClass() != o.getClass()) {
			return false;
		}
		final Email email = (Email) o;
		return Objects.equals(this.value, email.value);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.value);
	}

	@Override
	public String toString() {
		return "Email{" +
			"value='" + this.value + '\'' +
			'}';
	}
}
