package org.jamgo.model.valueobjects;

public interface ValueObject {

	String getValue();

}
