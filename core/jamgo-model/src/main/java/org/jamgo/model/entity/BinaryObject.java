package org.jamgo.model.entity;

import java.sql.Blob;
import java.time.LocalDateTime;

public interface BinaryObject {

	String getFileName();

	void setFileName(String fileName);

	String getMimeType();

	void setMimeType(String mimeType);

	Integer getFileLength();

	void setFileLength(Integer fileLength);

	LocalDateTime getTimeStamp();

	void setTimeStamp(LocalDateTime timeStamp);

	String getDescription();

	void setDescription(String description);

	Blob getContents();

	void setContents(Blob contents);

}