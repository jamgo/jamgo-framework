package org.jamgo.model;

public interface BasicModel<ID_TYPE> {

	ID_TYPE getId();

	Long getVersion();

	default String toListString() {
		return this.toString();
	}

}
