package org.jamgo.model.search;

import java.util.Locale;
import java.util.Properties;

import org.jamgo.model.entity.User;

public class ModelSearchSpecification extends SearchSpecification {

	public ModelSearchSpecification() {
		super();
	}

	public ModelSearchSpecification(final Locale locale) {
		super(locale);
	}

	public ModelSearchSpecification(final User user, final Properties properties) {
		super(user, properties);
	}

}
