package org.jamgo.model.repository;

import java.util.List;
import java.util.Locale;

import org.jamgo.model.entity.Category;

public abstract class CategoryRepository<CATEGORY extends Category> extends ModelRepository<CATEGORY> {

	public List<CATEGORY> findByName(final String name, final Locale locale) {
		return this.createCriteriaBuilder()
			.where(String.format("JSON_GET(name, '%s')", locale.toLanguageTag())).eq(name)
			.getResultList();
	}

}
