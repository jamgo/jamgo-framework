package org.jamgo.model.entity;

import java.sql.Blob;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class BinaryResourceImage extends Model implements Downloadable {
	private static final long serialVersionUID = 1L;

	@ManyToOne
	private BinaryResource source;

	@Column
	private String name;

	@Column
	private String size;

	@Column
	private String mimeType;

	@Column
	private Integer fileLength;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column
	private Blob contents;

	public BinaryResource getSource() {
		return this.source;
	}

	public void setSource(final BinaryResource source) {
		this.source = source;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getSize() {
		return this.size;
	}

	public void setSize(final String size) {
		this.size = size;
	}

	@Override
	public String getMimeType() {
		return this.mimeType;
	}

	public void setMimeType(final String mimeType) {
		this.mimeType = mimeType;
	}

	@Override
	public Integer getFileLength() {
		return this.fileLength;
	}

	public void setFileLength(final Integer fileLength) {
		this.fileLength = fileLength;
	}

	@Override
	public Blob getContents() {
		return this.contents;
	}

	@Override
	public void setContents(final Blob contents) {
		this.contents = contents;
	}

	@Override
	public String getFileName() {
		return this.source.getFileName();
	}

}
