package org.jamgo.model.repository;

import org.jamgo.model.entity.Model;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DefaultModelRepository<MODEL extends Model> extends ModelRepository<MODEL> {

	@Override
	public void afterPropertiesSet() throws Exception {
		// Do Nothing.
	}

	public DefaultModelRepository(final Class<MODEL> modelClass) {
		super();
		this.modelClass = modelClass;
	}

}
