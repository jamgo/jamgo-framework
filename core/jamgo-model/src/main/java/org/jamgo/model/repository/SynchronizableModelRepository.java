package org.jamgo.model.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.jamgo.model.entity.SynchronizableModel;

public abstract class SynchronizableModelRepository<T extends SynchronizableModel> extends ModelRepository<T> {

	public List<T> findByLastUpdatedAfter(final LocalDateTime date) {
		return this.createCriteriaBuilder()
			.where("lastUpdated").ge(date)
			.getResultList();
	}

}
