package org.jamgo.model.repository;

import java.util.List;

import org.jamgo.model.entity.Language;
import org.springframework.stereotype.Repository;

@Repository
public class LanguageRepository extends ModelRepository<Language> {

	public List<Language> findByActiveTrue() {
		return this.createCriteriaBuilder("lng")
			.where("active").eq(true)
			.getResultList();
	}

	public Language findByLanguageCodeAndCountryCode(final String languageCode, final String countryCode) {
		return this.createCriteriaBuilder()
			.where("languageCode").eq(languageCode)
			.where("countryCode").eq(countryCode)
			.getSingleResult();
	}

	public Language findByLanguageCode(final String languageCode) {
		return this.createCriteriaBuilder()
			.where("languageCode").eq(languageCode)
			.getSingleResult();
	}

	public Language findDefaultLanguage() {
		return this.createCriteriaBuilder()
			.where("active").eq(true)
			.where("defaultLanguage").eq(true)
			.getSingleResult();
	}

}
