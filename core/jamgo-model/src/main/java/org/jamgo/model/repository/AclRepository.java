package org.jamgo.model.repository;

import java.util.Collection;
import java.util.List;

import javax.persistence.NoResultException;

import org.jamgo.model.entity.Acl;
import org.jamgo.model.entity.SecuredObject;
import org.springframework.stereotype.Repository;

@Repository
public class AclRepository extends ModelRepository<Acl> {

	public List<Acl> findByRoleId(final Long roleId) {
		return this.createCriteriaBuilder()
			.where("roleId").eq(roleId)
			.getResultList();
	}

	public List<Acl> findByUserId(final Long userId) {
		return this.createCriteriaBuilder()
			.where("userId").eq(userId)
			.getResultList();
	}

	public List<Acl> findBySecuredObject(final SecuredObject securedObject) {
		return this.createCriteriaBuilder()
			.where("securedObject").eq(securedObject)
			.getResultList();
	}

	public List<Acl> findBySecuredObjectSecuredObjectKey(final String securedObjectKey) {
		return this.createCriteriaBuilder()
			.where("securedObject.securedObjectKey").eq(securedObjectKey)
			.getResultList();
	}

	public Acl findByRoleIdAndSecuredObject(final Long roleId, final SecuredObject securedObject) {
		try {
			return this.createCriteriaBuilder()
				.where("roleId").eq(roleId)
				.where("securedObject").eq(securedObject)
				.getSingleResult();
		} catch (final NoResultException e) {
			return null;
		}
	}

	public Acl findByUserIdAndSecuredObject(final Long userId, final SecuredObject securedObject) {
		try {
			return this.createCriteriaBuilder()
				.where("userId").eq(userId)
				.where("securedObject").eq(securedObject)
				.getSingleResult();
		} catch (final NoResultException e) {
			return null;
		}
	}

	public List<Acl> findByRoleIdAndSecuredObjectIn(final Long roleId, final Collection<SecuredObject> securedObjects) {
		return this.createCriteriaBuilder()
			.where("roleId").eq(roleId)
			.where("securedObject").in(securedObjects)
			.getResultList();
	}

	public List<Acl> findByUserIdAndSecuredObjectIn(final Long userId, final Collection<SecuredObject> securedObjects) {
		return this.createCriteriaBuilder()
			.where("userId").eq(userId)
			.where("securedObject").in(securedObjects)
			.getResultList();
	}

}
