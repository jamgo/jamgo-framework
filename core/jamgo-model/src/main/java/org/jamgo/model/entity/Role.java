package org.jamgo.model.entity;

public interface Role {

	Long getId();

	String getRolename();

	boolean isAdmin();

}