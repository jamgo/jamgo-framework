package org.jamgo.model.repository;

import org.jamgo.model.entity.LocalizedMessage;
import org.springframework.stereotype.Repository;

@Repository
public class LocalizedMessageRepository extends ModelRepository<LocalizedMessage> {

	public LocalizedMessage findByKey(final String key) {
		return this.createCriteriaBuilder()
			.where("key").eq(key)
			.getSingleResult();
	}

}
