package org.jamgo.model.repository;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.jamgo.model.entity.BasicModelEntity;
import org.jamgo.model.entity.Model;
import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
public class RepositoryManager implements InitializingBean {

	@Autowired
	private DataSource dataSource;

	@Autowired
	private ApplicationContext applicationContext;
	private Map<Class<? extends BasicModelEntity<?>>, BasicModelEntityRepository<? extends BasicModelEntity<?>, ?>> repositories;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		this.repositories = new HashMap<>();
		final Map<String, Object> repositoryBeans = this.applicationContext.getBeansWithAnnotation(Repository.class);
		repositoryBeans.forEach((eachKey, eachValue) -> {
			if (eachValue instanceof BasicModelEntityRepository) {
				final BasicModelEntityRepository<?, ?> basicModelRepository = (BasicModelEntityRepository<?, ?>) eachValue;
				this.repositories.put(basicModelRepository.getModelClass(), basicModelRepository);
			}
		});
	}

	@SuppressWarnings("unchecked")
	public <ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE> BasicModelEntityRepository<ENTITY, ID_TYPE> getRepository(final ENTITY object) throws RepositoryForClassNotDefinedException {
		return this.getRepository((Class<ENTITY>) object.getClass());
	}

	@SuppressWarnings("unchecked")
	public <ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE> BasicModelEntityRepository<ENTITY, ID_TYPE> getRepository(final Class<ENTITY> clazz) throws RepositoryForClassNotDefinedException {
		BasicModelEntityRepository<ENTITY, ID_TYPE> repository = (BasicModelEntityRepository<ENTITY, ID_TYPE>) this.repositories.get(clazz);
		if (repository == null && Model.class.isAssignableFrom(clazz)) {
			repository = this.applicationContext.getBean(DefaultModelRepository.class, clazz);
			this.repositories.put(clazz, repository);
		}
		if (repository == null) {
			throw new RepositoryForClassNotDefinedException(clazz.getName());
		}
		return repository;
	}

	public Blob createBlob() throws SQLException {
		return this.dataSource.getConnection().createBlob();
	}

	public Blob createBlob(final byte[] contents) throws SQLException {
		final Blob blob = this.createBlob();
		blob.setBytes(1, contents);
		return blob;
	}

}
