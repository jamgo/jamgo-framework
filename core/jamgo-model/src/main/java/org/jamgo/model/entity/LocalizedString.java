package org.jamgo.model.entity;

public class LocalizedString extends LocalizedObject<String, String> implements Comparable<LocalizedString> {

	private static final long serialVersionUID = 1L;

	@Override
	public void set(final String lang, final String value) {
		this.getInternalMap().put(lang, value);
	}

	@Override
	public int compareTo(final LocalizedString o) {
		return this.getDefaultText().compareTo(o.getDefaultText());
	}

}
