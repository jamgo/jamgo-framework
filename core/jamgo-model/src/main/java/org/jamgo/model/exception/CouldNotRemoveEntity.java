package org.jamgo.model.exception;

public class CouldNotRemoveEntity extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CouldNotRemoveEntity() {
		super();
	}

	public CouldNotRemoveEntity(final String message) {
		super(message);
	}

}
