package org.jamgo.model.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class SynchronizableModel extends Model {

	private static final long serialVersionUID = 1L;

	@Column
	private LocalDateTime lastUpdated;

	public LocalDateTime getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(final LocalDateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
}
