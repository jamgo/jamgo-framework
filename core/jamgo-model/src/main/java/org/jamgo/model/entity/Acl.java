package org.jamgo.model.entity;

import java.util.Objects;
import java.util.Optional;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.jamgo.model.enums.Permission;

@Entity
@Table(name = "acl")
@Cacheable(false)
public class Acl extends Model {

	private static final long serialVersionUID = 1L;

	@Column(name = "role_id")
	private Long roleId;

	@Column(name = "user_id")
	private Long userId;

	@ManyToOne
	@JoinColumn(name = "secured_object_id")
	private SecuredObject securedObject;

	@Column
	@Enumerated(EnumType.STRING)
	private Permission permission;

	public Long getRoleId() {
		return this.roleId;
	}

	public void setRoleId(final Long roleId) {
		this.roleId = roleId;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(final Long userId) {
		this.userId = userId;
	}

	public SecuredObject getSecuredObject() {
		return this.securedObject;
	}

	public void setSecuredObject(final SecuredObject securedObject) {
		this.securedObject = securedObject;
	}

	public void setReadPermission(final Boolean readPermission) {
		if (readPermission) {
			if (Permission.WRITE == this.permission) {
				this.permission = Permission.ALL;
			} else if (Permission.NONE == this.permission) {
				this.permission = Permission.READ;
			}
		} else {
			if (Permission.ALL == this.permission) {
				this.permission = Permission.WRITE;
			} else if (Permission.READ == this.permission) {
				this.permission = Permission.NONE;
			}
		}
	}

	public void setWritePermission(final Boolean writePermission) {
		if (writePermission) {
			if (Permission.READ == this.permission) {
				this.permission = Permission.ALL;
			} else if (Permission.NONE == this.permission) {
				this.permission = Permission.WRITE;
			}
		} else {
			if (Permission.ALL == this.permission) {
				this.permission = Permission.READ;
			} else if (Permission.WRITE == this.permission) {
				this.permission = Permission.NONE;
			}
		}
	}

	public Permission getPermission() {
		return this.permission;
	}

	public void setPermission(final Permission permission) {
		this.permission = permission;
	}

	public boolean canRead() {
		return Optional.ofNullable(this.permission).map(p -> p.canRead()).orElse(false);
	}

	public boolean canWrite() {
		return Optional.ofNullable(this.permission).map(p -> p.canWrite()).orElse(false);
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}
		final Acl other = (Acl) obj;
		return Objects.equals(this.securedObject, other.securedObject)
			&& super.equals(obj);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.securedObject, super.hashCode());
	}

}
