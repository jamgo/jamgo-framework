package org.jamgo.model.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.blazebit.persistence.integration.view.spring.EnableEntityViews;

@Configuration
@ComponentScan(
	basePackageClasses = {
		org.jamgo.model.entity.PackageMarker.class,
		org.jamgo.model.repository.PackageMarker.class,
		org.jamgo.model.search.PackageMarker.class,
		org.jamgo.model.jpa.PackageMarker.class
	})
@EnableEntityViews(
	basePackageClasses = {
		org.jamgo.model.view.PackageMarker.class
	})
public class ModelConfig {

}
