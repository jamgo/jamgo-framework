package org.jamgo.model.entity;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.Type;

/**
 * @deprecated Use {@link Category} instead.
 */
@Deprecated
@MappedSuperclass
public abstract class AuxiliarObject extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<? extends AuxiliarObject> NAME_ORDER = new Comparator<AuxiliarObject>() {
		@Override
		public int compare(final AuxiliarObject objectA, final AuxiliarObject objectB) {
			return objectA.getName().compareTo(objectB.getName());
		}
	};

	@Type(type = "json")
	@Column(columnDefinition = "json")
	private LocalizedString name;

	public AuxiliarObject() {
		super();
	}

	public AuxiliarObject(final Long id) {
		super(id);
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(final LocalizedString name) {
		this.name = name;
	}

	@Override
	public String toListString() {
		return this.getName().getDefaultText();
	}

}