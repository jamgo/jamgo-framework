package org.jamgo.model.repository;

import javax.persistence.NoResultException;

import org.jamgo.model.entity.BinaryResourceImage;
import org.springframework.stereotype.Repository;

@Repository
public class BinaryResourceImageRepository extends ModelRepository<BinaryResourceImage> {

	public BinaryResourceImage findByName(final Long sourceId, final String name) {
		try {
			return this.createCriteriaBuilder()
				.where("source.id").eq(sourceId)
				.where("name").eq(name)
				.getSingleResult();
		} catch (final NoResultException e) {
			return null;
		}
	}

}
