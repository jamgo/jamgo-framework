package org.jamgo.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Maps;

public abstract class LocalizedObject<TYPE extends Object, MAP_VALUE extends Serializable> implements Serializable {

	private static final long serialVersionUID = 1L;

	protected Map<String, MAP_VALUE> map;

	public LocalizedObject() {
		this.map = new LinkedHashMap<>();
	}

	@JsonAnyGetter
	protected Map<String, MAP_VALUE> getInternalMap() {
		return this.map;
	}

	public MAP_VALUE get(final Language lang) {
		return this.get(lang.getLocaleCode());
	}

	public MAP_VALUE get(final Locale locale) {
		final String localeCode = this.getLocaleCode(locale);
		return this.get(localeCode);
	}

	public MAP_VALUE get(final String lang) {
		return this.map.get(lang);
	}

	public void set(final Language language, final TYPE value) {
		this.set(language.getLocaleCode(), value);
	}

	public void set(final Locale locale, final TYPE value) {
		this.set(this.getLocaleCode(locale), value);
	}

	public abstract void set(final String lang, final TYPE value);

	@JsonAnySetter
	public void setMapEntry(final String lang, final MAP_VALUE value) {
		this.getInternalMap().put(lang, value);
	}

	@JsonIgnore
	public MAP_VALUE getDefaultText() {
		return Optional.ofNullable(this.map)
			.map(o -> o.entrySet().iterator().next())
			.map(o -> o.getValue())
			.orElse(null);
	}

	@JsonIgnore
	public List<String> getLanguageCodes() {
		if (!this.map.isEmpty()) {
			return new ArrayList<>(this.map.keySet());
		}
		return Collections.emptyList();
	}

	private String getLocaleCode(final Locale locale) {
		return String.join("-", locale.getLanguage(), locale.getCountry());
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		// FIXME Not sure this is correct (e.g. LocalModel<A> and LocalModel<B> with same "ids" in each language are not equal)
		final LocalizedObject<?, ?> other = (LocalizedObject<?, ?>) obj;
		if (!Maps.difference(this.map, other.map).areEqual()) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31)
			.append(this.map)
			.toHashCode();
	}

	@Override
	public String toString() {
		return Optional.ofNullable(this.getDefaultText()).map(o -> o.toString()).orElse("(null)");
	}

}
