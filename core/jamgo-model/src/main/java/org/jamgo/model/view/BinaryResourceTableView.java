package org.jamgo.model.view;

import java.time.LocalDateTime;
import java.util.Set;

import org.jamgo.model.entity.BinaryResource;

import com.blazebit.persistence.view.EntityView;
import com.blazebit.persistence.view.Mapping;

@EntityView(BinaryResource.class)
public abstract class BinaryResourceTableView extends ModelView {

	public abstract String getFileName();

	public abstract String getDescription();

	public abstract String getMimeType();

	public abstract Integer getFileLength();

	public abstract LocalDateTime getTimeStamp();

//	@MappingSubquery(BinaryResourceImageCountSubqueryProvider.class)
//	public abstract Long getBinaryResourceImageCount();
//
//	public static final class BinaryResourceImageCountSubqueryProvider implements SubqueryProvider {
//
//		@Override
//		public <T> T createSubquery(final SubqueryInitiator<T> subqueryBuilder) {
//			return subqueryBuilder.from(BinaryResourceImage.class, "image")
//				.select("COUNT(*)")
//				.where("image.source.id").eqExpression("EMBEDDING_VIEW(id)")
//				.end();
//		}
//
//	}

	@Mapping("images.name")
	public abstract Set<String> getBinaryResourceImageNames();

}
