package org.jamgo.model.entity;

import java.util.Comparator;
import java.util.Locale;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "language", uniqueConstraints = {
	@UniqueConstraint(name = "uq_language_languagecode_countrycode", columnNames = { "language_code", "country_code" })
})
public class Language extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<Language> DEFAULT_FIRST = new Comparator<>() {
		@Override
		public int compare(final Language objectA, final Language objectB) {
			if (objectA.isDefaultLanguage()) {
				return -1;
			}
			if (objectB.isDefaultLanguage()) {
				return 1;
			}
			return objectA.getName().compareTo(objectB.getName());
		}
	};

	@Column(name = "language_code")
	private String languageCode;

	@Column(name = "country_code")
	private String countryCode;

	@Column(nullable = false)
	private String name;

	@Column(name = "default_language")
	private boolean defaultLanguage;

	@Column(name = "active")
	private boolean active;

	public Language() {
	}

	public Language(final Locale locale) {
		this.countryCode = locale.getCountry();
		this.languageCode = locale.getLanguage();
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}
		final Language other = (Language) obj;
		return Objects.equals(this.name, other.name)
			&& super.equals(obj);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.name, super.hashCode());
	}

	@Override
	public String toString() {
		return this.getName();
	}

	public boolean isDefaultLanguage() {
		return this.defaultLanguage;
	}

	public void setDefaultLanguage(final boolean defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public String getLanguageCode() {
		return this.languageCode;
	}

	public void setLanguageCode(final String languageCode) {
		this.languageCode = languageCode;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(final String countryCode) {
		this.countryCode = countryCode;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(final boolean active) {
		this.active = active;
	}

	public String getLocaleCode() {
		return String.join("-", this.languageCode, this.countryCode);
	}

	public Locale getLocale() {
		return new Locale(this.languageCode, this.countryCode);
	}

}
