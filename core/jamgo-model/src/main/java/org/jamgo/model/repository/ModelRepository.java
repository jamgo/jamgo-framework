package org.jamgo.model.repository;

import org.jamgo.model.entity.Model;

public abstract class ModelRepository<MODEL extends Model> extends BasicModelEntityRepository<MODEL, Long> {

}
