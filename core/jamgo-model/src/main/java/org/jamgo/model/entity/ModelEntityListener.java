package org.jamgo.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class ModelEntityListener implements InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(ModelEntityListener.class);

	@Autowired(required = false)
	private List<ModelEntityListenerHelper> listenerHelpers;
	private ApplicationContext applicationContext;

	public ModelEntityListener() {
		ModelEntityListener.logger.debug("CONSTRUCTOR of ModelEntityListener");
//		this.listenerHelpers = new HashSet<>();
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (this.listenerHelpers == null) {
			this.listenerHelpers = new ArrayList<>();
		}
//		this.listenerHelpers = this.applicationContext.getBeansOfType(ModelEntityListenerHelper.class);
	}

	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}

	public void addListenerHelper(final ModelEntityListenerHelper listenerHelper) {
		this.listenerHelpers.add(listenerHelper);
	}

	@Autowired
	public void setApplicationContext(final ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	/**
	 * Callback method invoked after storing a new entity in the database (during
	 * commit or flush).
	 */
	@PostPersist
	public void onPostPersist(final Model m) {
		ModelEntityListener.logger.debug("ON POST PERSIST: {}", this.getModelReference(m));
		this.listenerHelpers.forEach(o -> o.onPostPersist(m));
	}

	/**
	 * Callback method invoked after updating an entity in the database (during
	 * commit or flush).
	 */
	@PostUpdate
	public void onPostUpdate(final Model m) {
		ModelEntityListener.logger.debug("ON POST UPDATE: {}", this.getModelReference(m));
		this.listenerHelpers.forEach(o -> o.onPostUpdate(m));
	}

	/**
	 * Callback method invoked after deleting an entity from the database (during
	 * commit or flush).
	 */
	@PostRemove
	public void onPostRemove(final Model m) {
		ModelEntityListener.logger.debug("ON POST REMOVE: {}", this.getModelReference(m));
		this.listenerHelpers.forEach(o -> o.onPostRemove(m));
	}

	private String getModelReference(final Model m) {
		return String.format("%s#%s#%s", m.getClass().getName(), m.getId(), m.getVersion());
	}

}
