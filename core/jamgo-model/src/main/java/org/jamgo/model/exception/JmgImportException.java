package org.jamgo.model.exception;

public class JmgImportException extends JmgException{

	private static final long serialVersionUID = 1L;
	
	public JmgImportException (String message) {
		super (message);
	}
}
