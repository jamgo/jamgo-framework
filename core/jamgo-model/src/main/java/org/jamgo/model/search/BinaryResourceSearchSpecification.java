package org.jamgo.model.search;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.blazebit.persistence.CriteriaBuilder;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BinaryResourceSearchSpecification extends ModelSearchSpecification {

	private String fileName;
	private String mimeType;
	private String dateFrom;
	private String dateTo;
	// TODO: Add more search fields: fileLength, description

	@Override
	public void apply(final CriteriaBuilder<?> cb) {
		if (StringUtils.hasLength(this.getMimeType())) {
			// TODO: Use "like" instead of "eq"?
			cb.where("mimeType").eq(this.getMimeType());
		}

		if (StringUtils.hasLength(this.getFileName())) {
			// TODO: Use "like" instead of "eq"?
			cb.where("fileName").eq(this.getFileName());
		}

		// TODO: Add predicates for dateFrom/dateTo?

		super.apply(cb);
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(final String fileName) {
		this.fileName = fileName;
	}

	public String getMimeType() {
		return this.mimeType;
	}

	public void setMimeType(final String mimeType) {
		this.mimeType = mimeType;
	}

	public String getDateFrom() {
		return this.dateFrom;
	}

	public void setDateFrom(final String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return this.dateTo;
	}

	public void setDateTo(final String dateTo) {
		this.dateTo = dateTo;
	}

}
