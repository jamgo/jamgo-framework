package org.jamgo.model.enums;

public enum SecuredObjectType {
	MENU,
	TAB,
	ENTITY
}
