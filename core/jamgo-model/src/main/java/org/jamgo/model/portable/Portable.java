package org.jamgo.model.portable;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

@Retention(RUNTIME)
public @interface Portable {

	Class<?>exporter() default Object.class;

	Class<?>importer() default Object.class;
}
