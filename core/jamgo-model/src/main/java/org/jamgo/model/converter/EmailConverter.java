package org.jamgo.model.converter;

import java.util.Optional;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.jamgo.model.valueobjects.Email;

@Converter
public class EmailConverter implements AttributeConverter<Email, String> {

	@Override
	public String convertToDatabaseColumn(final Email email) {
		return Optional.ofNullable(email).map(e -> e.getValue()).orElse(null);
	}

	@Override
	public Email convertToEntityAttribute(final String dbData) {
		return Optional.ofNullable(dbData).map(data -> new Email(data)).orElse(null);
	}

}
