package org.jamgo.model.exception;

import java.sql.SQLException;

public class JamgoRemoveEntityException extends SQLException {

	private static final long serialVersionUID = 1L;

	public JamgoRemoveEntityException(String message) {
		super(message);
	}
	
	public JamgoRemoveEntityException(Throwable cause) {
		super(cause);
	}
}
