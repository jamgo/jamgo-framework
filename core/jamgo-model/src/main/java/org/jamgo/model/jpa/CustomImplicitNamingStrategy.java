package org.jamgo.model.jpa;

import java.util.List;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitForeignKeyNameSource;
import org.hibernate.boot.model.naming.ImplicitIndexNameSource;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyComponentPathImpl;
import org.hibernate.boot.model.naming.ImplicitUniqueKeyNameSource;
import org.hibernate.boot.model.naming.NamingHelper;
import org.hibernate.boot.spi.MetadataBuildingContext;

public class CustomImplicitNamingStrategy extends ImplicitNamingStrategyComponentPathImpl {

	private static final long serialVersionUID = 1L;

	@Override
	public Identifier determineForeignKeyName(final ImplicitForeignKeyNameSource source) {
		if (source.getUserProvidedIdentifier() != null) {
			return super.determineForeignKeyName(source);
		}

		return this.createUniqueName("fk", source.getTableName(), source.getColumnNames(), source.getBuildingContext(), source.getReferencedTableName());
	}

	@Override
	public Identifier determineUniqueKeyName(final ImplicitUniqueKeyNameSource source) {
		if (source.getUserProvidedIdentifier() != null) {
			return super.determineUniqueKeyName(source);
		}

		return this.createUniqueName("uk", source.getTableName(), source.getColumnNames(), source.getBuildingContext(), null);
	}

	@Override
	public Identifier determineIndexName(final ImplicitIndexNameSource source) {
		if (source.getUserProvidedIdentifier() != null) {
			return super.determineIndexName(source);
		}

		return this.createUniqueName("ix", source.getTableName(), source.getColumnNames(), source.getBuildingContext(), null);
	}

	private Identifier createUniqueName(final String type, final Identifier tableName, final List<Identifier> columnNames, final MetadataBuildingContext buildingContext, final Identifier referencedTableName) {
		final StringBuilder columnNamesBuilder = new StringBuilder();
		columnNames.forEach(each -> columnNamesBuilder.append(each));
		String uniqueNameSuffix = NamingHelper.withCharset(buildingContext.getBuildingOptions().getSchemaCharset()).hashedName(columnNamesBuilder.toString());
		uniqueNameSuffix = uniqueNameSuffix.substring(Math.max(uniqueNameSuffix.length() - 4, 0));
		final StringBuilder uniqueNameBuilder = new StringBuilder();
		uniqueNameBuilder
			.append(tableName).append("_");
		if (referencedTableName != null) {
			uniqueNameBuilder
				.append(referencedTableName).append("_");
		}
		if (uniqueNameBuilder.length() > 56) {
			uniqueNameBuilder.delete(55, uniqueNameBuilder.length());
			uniqueNameBuilder.setCharAt(54, '_');
		}
		uniqueNameBuilder
			.append(type).append("_")
			.append(uniqueNameSuffix);

		return this.toIdentifier(uniqueNameBuilder.toString(), buildingContext);
	}
}
