package org.jamgo.model.search;

import java.util.Properties;

import org.jamgo.model.entity.User;

/**
 * Dummy search specification used when no search filters are set for the entity but still
 * we need to filter it by user/role permissions.
 *
 * @author jamgo
 *
 */
public class DummySearchSpecification extends ModelSearchSpecification {

	public DummySearchSpecification() {

	}

	public DummySearchSpecification(final User user, final Properties permissionProperties) {
		super(user, permissionProperties);
	}
}
