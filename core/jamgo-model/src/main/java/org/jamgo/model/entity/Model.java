package org.jamgo.model.entity;

import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.TypeDef;

import com.vladmihalcea.hibernate.type.json.JsonType;

@MappedSuperclass
@EntityListeners(ModelEntityListener.class)
@TypeDef(name = "json", typeClass = JsonType.class)
public abstract class Model extends BasicModelEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "jamgoModel")
	@GenericGenerator(name = "jamgoModel", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
		@Parameter(name = "prefer_sequence_per_entity", value = "true"),
		@Parameter(name = "optimizer", value = "pooled-lo") })
	private Long id;

	@Version
	private Long version;

	public Model() {
	}

	public Model(final Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	@Override
	public Long getVersion() {
		return this.version;
	}

	@Override
	public void setVersion(final Long version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		return this.getId() != null ? this.getId().hashCode() : 0;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		return (obj instanceof Model) && (((Model) obj).getId() != null) && ((Model) obj).getId().equals(this.getId());
	}

	public static String getImportValue(final String value) {
		return (value != null) && (!value.trim().isEmpty()) ? value : null;
	}

	public static String getExportValue(final String value) {
		return (value != null) && (!value.trim().isEmpty()) ? value : "";
	}

}
