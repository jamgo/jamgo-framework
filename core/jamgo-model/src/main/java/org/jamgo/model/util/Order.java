package org.jamgo.model.util;

public class Order {

	private final String expression;
	private final boolean ascending;
	private final boolean nullsFirst;

	public Order(final String expression, final Boolean ascending, final Boolean nullsFirst) {
		this.expression = expression;

		if (Boolean.FALSE.equals(ascending)) {
			this.ascending = false;
			// Default NULLS FIRST
			if (nullsFirst == null) {
				this.nullsFirst = true;
			} else {
				this.nullsFirst = nullsFirst;
			}
		} else {
			this.ascending = true;
			// Default NULLS LAST
			if (nullsFirst == null) {
				this.nullsFirst = false;
			} else {
				this.nullsFirst = nullsFirst;
			}
		}
	}

	public String getExpression() {
		return this.expression;
	}

	public boolean isAscending() {
		return this.ascending;
	}

	public boolean isNullsFirst() {
		return this.nullsFirst;
	}

}
