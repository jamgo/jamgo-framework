package org.jamgo.model.repository;

import org.jamgo.model.entity.AuxiliarObject;

/**
 * @deprecated Use {@link CategoryRepository} instead.
 */
@Deprecated
public abstract class AuxiliarObjectRepository<T extends AuxiliarObject> extends ModelRepository<T> {

}
