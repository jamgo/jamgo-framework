package org.jamgo.model.exception;

public class BinaryResourceException extends JmgException {

	private static final long serialVersionUID = 1L;

	public BinaryResourceException(final String message) {
		super(message);
	}

}
