package org.jamgo.model.entity;

import java.util.Set;

public interface User {

	Long getId();

	String getUsername();

	Language getLanguage();

	Set<? extends Role> getRoles();

}