package org.jamgo.p6spy.engine.spy.appender;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.collections4.map.HashedMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.p6spy.engine.common.P6Util;
import com.p6spy.engine.spy.P6SpyOptions;
import com.p6spy.engine.spy.appender.MessageFormattingStrategy;
import com.p6spy.engine.spy.appender.SingleLineFormat;

public class RegexCustomLineFormat implements MessageFormattingStrategy {

	private static final MessageFormattingStrategy FALLBACK_FORMATTING_STRATEGY = new SingleLineFormat();

	public static final String CONNECTION_ID = "%(connectionId)";
	public static final String CURRENT_TIME = "%(currentTime)";
	public static final String EXECUTION_TIME = "%(executionTime)";
	public static final String CATEGORY = "%(category)";
	public static final String EFFECTIVE_SQL = "%(effectiveSql)";
	public static final String EFFECTIVE_SQL_SINGLELINE = "%(effectiveSqlSingleLine)";
	public static final String SQL = "%(sql)";
	public static final String SQL_SINGLE_LINE = "%(sqlSingleLine)";
	public static final String URL = "%(url)";

	private static final String DEFAULT_PATTERN_KEY = "__default__";

	private Map<String, String> patternFormats;

	private static class PatternFormat {

		private String pattern;
		private String format;

		public String getPattern() {
			return this.pattern;
		}

		public String getFormat() {
			return this.format;
		}

	}

	@Override
	public String formatMessage(final int connectionId, final String now, final long elapsed, final String category, final String prepared, final String sql, final String url) {
		if (this.patternFormats == null) {
			try {
				this.initializePatternFormats();
			} catch (final JsonProcessingException e) {
				this.patternFormats = new HashedMap<>();
				return RegexCustomLineFormat.FALLBACK_FORMATTING_STRATEGY.formatMessage(connectionId, now, elapsed, category, prepared, sql, url);
			}
		}

		String messageFormat = this.patternFormats.entrySet().stream()
			.filter(each -> !RegexCustomLineFormat.DEFAULT_PATTERN_KEY.equals(each.getKey()) && sql.matches(each.getKey()))
			.findFirst()
			.map(each -> each.getValue())
			.orElse(null);

		if (messageFormat == null) {
			messageFormat = this.patternFormats.get(RegexCustomLineFormat.DEFAULT_PATTERN_KEY);
		}

		if (messageFormat == null) {
			return RegexCustomLineFormat.FALLBACK_FORMATTING_STRATEGY.formatMessage(connectionId, now, elapsed, category, prepared, sql, url);
		}

		return messageFormat
			.replaceAll(Pattern.quote(RegexCustomLineFormat.CONNECTION_ID), Integer.toString(connectionId))
			.replaceAll(Pattern.quote(RegexCustomLineFormat.CURRENT_TIME), now)
			.replaceAll(Pattern.quote(RegexCustomLineFormat.EXECUTION_TIME), Long.toString(elapsed))
			.replaceAll(Pattern.quote(RegexCustomLineFormat.CATEGORY), category)
			.replaceAll(Pattern.quote(RegexCustomLineFormat.EFFECTIVE_SQL), Matcher.quoteReplacement(prepared))
			.replaceAll(Pattern.quote(RegexCustomLineFormat.EFFECTIVE_SQL_SINGLELINE), Matcher.quoteReplacement(P6Util.singleLine(prepared)))
			.replaceAll(Pattern.quote(RegexCustomLineFormat.SQL), Matcher.quoteReplacement(sql))
			.replaceAll(Pattern.quote(RegexCustomLineFormat.SQL_SINGLE_LINE), Matcher.quoteReplacement(P6Util.singleLine(sql)))
			.replaceAll(Pattern.quote(RegexCustomLineFormat.URL), url);
	}

	private void initializePatternFormats() throws JsonMappingException, JsonProcessingException {
		final ObjectMapper objectMapper = new ObjectMapper();

		final String customLogMessageFormat = P6SpyOptions.getActiveInstance().getCustomLogMessageFormat();

		this.patternFormats = objectMapper.readValue(customLogMessageFormat, new TypeReference<List<PatternFormat>>() {
		})
			.stream()
			.collect(Collectors.toMap(each -> each.getPattern(), each -> each.getFormat()));
	}
}
