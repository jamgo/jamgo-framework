package org.jamgo.model.test.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;
import java.util.Locale;

import org.jamgo.model.entity.LocalizedBinaryResource;
import org.jamgo.model.entity.TestCategory;
import org.jamgo.model.repository.TestCategoryRepository;
import org.jamgo.model.search.DummySearchSpecification;
import org.jamgo.model.test.TestCategoryBuilder;
import org.jamgo.model.test.config.ModelTestConfig;
import org.jamgo.model.test.entity.builder.BinaryResourceBuilder;
import org.jamgo.model.util.OffsetSizePageRequest;
import org.jamgo.test.JamgoRepositoryTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.blazebit.persistence.PagedList;

@SpringBootTest
@ContextConfiguration(classes = { ModelTestConfig.class })
public class TestCategoryRepositoryTest extends JamgoRepositoryTest {

	@Autowired
	private TestCategoryRepository testCategoryRepository;

	@Test
	@Transactional
	public void testFindById() {
		this.entityManager.joinTransaction();
		final TestCategory category = new TestCategoryBuilder(this.entityManager).buildOne();

		this.commitAndStart();

		final TestCategory result = this.testCategoryRepository.findById(category.getId());
		assertNotNull(result);
		assertEquals(category.getId(), result.getId());
		assertEquals(category.getName().getDefaultText(), result.getName().getDefaultText());
	}

	@Test
	@Transactional
	public void testFindOneLocale() {
		this.entityManager.joinTransaction();
		new TestCategoryBuilder(this.entityManager)
			.build(3);

		this.commitAndStart();

		List<TestCategory> result = this.testCategoryRepository.findByName("TestCategory name 1_ca-ES", Locale.forLanguageTag("ca-ES"));
		assertEquals(1, result.size());

		result = this.testCategoryRepository.findByName("TestCategory name 1_ca-ES", Locale.forLanguageTag("es-ES"));
		assertEquals(0, result.size());

		result = this.testCategoryRepository.findByName("TestCategory name 1_ca-ES", Locale.getDefault());
		assertEquals(0, result.size());
	}

	@Test
	@Transactional
	public void testFindAll_withPageRequestNull() {
		this.entityManager.joinTransaction();
		new TestCategoryBuilder(this.entityManager).buildOne();
		this.commit();

		final PagedList<TestCategory> result = this.testCategoryRepository.findAll(new DummySearchSpecification(), (OffsetSizePageRequest) null);
		assertNotNull(result);
		assertEquals(1, result.getTotalSize());
	}

	@Test
	@Transactional
	public void testInsert_withLocalizedBinaryResource() {
		this.entityManager.joinTransaction();

		final LocalizedBinaryResource localizedImage = new LocalizedBinaryResource();
		localizedImage.set("es-ES", new BinaryResourceBuilder(this.entityManager).buildOne());
		localizedImage.set("ca-ES", new BinaryResourceBuilder(this.entityManager).buildOne());

		final TestCategory category = new TestCategoryBuilder(this.entityManager)
			.setImage(localizedImage)
			.buildOne();
		this.commit();

		final TestCategory result = this.testCategoryRepository.findById(category.getId());
		assertEquals(category.getId(), result.getId());
		assertNotNull(result.getImage());
	}
}
