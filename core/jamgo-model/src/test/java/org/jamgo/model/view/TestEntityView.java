package org.jamgo.model.view;

import org.jamgo.model.entity.TestEntity;

import com.blazebit.persistence.view.EntityView;
import com.blazebit.persistence.view.Mapping;

@EntityView(TestEntity.class)
public abstract class TestEntityView extends ModelView {

	public abstract String getName();

	public abstract Integer getAge();

	@Mapping("phone")
	public abstract String getMobilePhone();
}
