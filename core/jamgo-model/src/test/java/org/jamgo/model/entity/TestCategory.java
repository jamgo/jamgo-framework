package org.jamgo.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.annotations.Type;

@Entity
public class TestCategory extends Category {

	private static final long serialVersionUID = 1L;

	@Type(type = "json")
	@Column(columnDefinition = "json")
	private LocalizedBinaryResource image;

	public LocalizedBinaryResource getImage() {
		return this.image;
	}

	public void setImage(final LocalizedBinaryResource image) {
		this.image = image;
	}

}
