package org.jamgo.model.test.entity.builder;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.jamgo.model.entity.BinaryResource;

public class BinaryResourceBuilder extends ModelBuilder<BinaryResource> {
	private LocalDateTime timeStamp = null;

	private String description = null;

	private String mimeType = null;

	private String fileName = null;

	private Integer fileLength = null;

	private byte[] byteContents = null;

	public BinaryResourceBuilder(final EntityManager entityManager) {
		super(entityManager);
	}

	public BinaryResourceBuilder(final EntityManager entityManager, final ModelBuilder<?> parentBuilder) {
		super(entityManager, parentBuilder);
	}

	public BinaryResourceBuilder setTimeStamp(final LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
		return this;
	}

	public BinaryResourceBuilder setDescription(final String description) {
		this.description = description;
		return this;
	}

	public BinaryResourceBuilder setMimeType(final String mimeType) {
		this.mimeType = mimeType;
		return this;
	}

	public BinaryResourceBuilder setFileName(final String fileName) {
		this.fileName = fileName;
		return this;
	}

	public BinaryResourceBuilder setFileLength(final Integer fileLength) {
		this.fileLength = fileLength;
		return this;
	}

	public BinaryResourceBuilder setByteContents(final byte[] byteContents) {
		this.byteContents = byteContents;
		return this;
	}

	@Override
	protected BinaryResource basicBuild() {
		final LocalDateTime timeStamp = Optional.ofNullable(this.timeStamp).orElse(LocalDateTime.parse("2017-10-01T09:00:00"));
		final String description = Optional.ofNullable(this.description).orElse(BinaryResource.class.getSimpleName() + " description " + this.currentSuffix);
		final String mimeType = Optional.ofNullable(this.mimeType).orElse(BinaryResource.class.getSimpleName() + " mimeType " + this.currentSuffix);
		final String fileName = Optional.ofNullable(this.fileName).orElse(BinaryResource.class.getSimpleName() + " fileName " + this.currentSuffix);
		final Integer fileLength = Optional.ofNullable(this.fileLength).orElse(1);
		final byte[] byteContents = Optional.ofNullable(this.byteContents).orElse(new byte[] { 0, 1, 2, 3, 4 });

		final BinaryResource binaryResource = new BinaryResource();
		binaryResource.setTimeStamp(timeStamp);
		binaryResource.setDescription(description);
		binaryResource.setMimeType(mimeType);
		binaryResource.setFileName(fileName);
		binaryResource.setFileLength(fileLength);
		binaryResource.setByteContents(byteContents);

		return binaryResource;
	}
}
