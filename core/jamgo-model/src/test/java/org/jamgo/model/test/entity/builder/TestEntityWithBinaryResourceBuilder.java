package org.jamgo.model.test.entity.builder;

import java.util.Optional;

import javax.persistence.EntityManager;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.TestEntity;
import org.jamgo.model.entity.TestEntityWithBinaryResource;

public class TestEntityWithBinaryResourceBuilder extends ModelBuilder<TestEntityWithBinaryResource> {

	private String name;
	private Integer age;
	private String phone;
	private BinaryResource photo;

	public TestEntityWithBinaryResourceBuilder(final EntityManager entityManager) {
		super(entityManager);
	}

	public TestEntityWithBinaryResourceBuilder(final EntityManager entityManager, final ModelBuilder<?> parentBuilder) {
		super(entityManager, parentBuilder);
	}

	public TestEntityWithBinaryResourceBuilder withName(final String name) {
		this.name = name;
		return this;
	}

	public TestEntityWithBinaryResourceBuilder withAge(final Integer age) {
		this.age = age;
		return this;
	}

	public TestEntityWithBinaryResourceBuilder withPhone(final String phone) {
		this.phone = phone;
		return this;
	}

	public TestEntityWithBinaryResourceBuilder withPhoto(final BinaryResource photo) {
		this.photo = photo;
		return this;
	}

	@Override
	protected TestEntityWithBinaryResource basicBuild() {
		final String name = Optional.ofNullable(this.name).orElse(TestEntity.class.getSimpleName() + " name " + this.currentSuffix);
		final Integer age = Optional.ofNullable(this.age).orElse(42);
		final String phone = Optional.ofNullable(this.phone).orElse(TestEntity.class.getSimpleName() + " phone " + this.currentSuffix);
		final BinaryResource photo = Optional.ofNullable(this.photo).orElse(null);

		final TestEntityWithBinaryResource entity = new TestEntityWithBinaryResource();
		entity.setName(name);
		entity.setAge(age);
		entity.setPhone(phone);
		entity.setPhoto(photo);
		return entity;
	}

}
