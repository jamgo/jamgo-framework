package org.jamgo.model.entity;

import javax.persistence.Entity;

@Entity
public class TestEntity2 extends Model {
	private static final long serialVersionUID = 1L;

	private String name;
	private Integer age;
	private String phone;

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Integer getAge() {
		return this.age;
	}

	public void setAge(final Integer age) {
		this.age = age;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

}
