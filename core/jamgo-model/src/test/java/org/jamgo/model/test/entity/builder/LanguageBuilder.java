package org.jamgo.model.test.entity.builder;

import java.util.Optional;
import javax.persistence.EntityManager;
import org.jamgo.model.entity.Language;

public class LanguageBuilder extends ModelBuilder<Language> {
	private String languageCode = null;

	private String countryCode = null;

	private String name = null;

	private Boolean defaultLanguage = null;

	private Boolean active = null;

	public LanguageBuilder(final EntityManager entityManager) {
		super(entityManager);
	}

	public LanguageBuilder(final EntityManager entityManager, final ModelBuilder<?> parentBuilder) {
		super(entityManager, parentBuilder);
	}

	public LanguageBuilder setLanguageCode(final String languageCode) {
		this.languageCode = languageCode;
		return this;
	}

	public LanguageBuilder setCountryCode(final String countryCode) {
		this.countryCode = countryCode;
		return this;
	}

	public LanguageBuilder setName(final String name) {
		this.name = name;
		return this;
	}

	public LanguageBuilder setDefaultLanguage(final Boolean defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
		return this;
	}

	public LanguageBuilder setActive(final Boolean active) {
		this.active = active;
		return this;
	}

	@Override
	protected Language basicBuild() {
		final String languageCode = Optional.ofNullable(this.languageCode).orElse(Language.class.getSimpleName() + " languageCode " + this.currentSuffix);
		final String countryCode = Optional.ofNullable(this.countryCode).orElse(Language.class.getSimpleName() + " countryCode " + this.currentSuffix);
		final String name = Optional.ofNullable(this.name).orElse(Language.class.getSimpleName() + " name " + this.currentSuffix);
		final Boolean defaultLanguage = Optional.ofNullable(this.defaultLanguage).orElse(false);
		final Boolean active = Optional.ofNullable(this.active).orElse(false);

		final Language language = new Language();
		language.setLanguageCode(languageCode);
		language.setCountryCode(countryCode);
		language.setName(name);
		language.setDefaultLanguage(defaultLanguage);
		language.setActive(active);

		return language;
	}
}
