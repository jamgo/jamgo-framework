package org.jamgo.model.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.LocalizedBinaryResource;
import org.jamgo.model.test.config.ModelTestConfig;
import org.jamgo.test.JamgoTest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = { ModelTestConfig.class })
public class TestLocalizedModel extends JamgoTest {

	@Test
	public void testLocalized_constructor() {
		final LocalizedBinaryResource localizedBinaryResource = new LocalizedBinaryResource();
		assertNotNull(localizedBinaryResource);
		assertEquals(BinaryResource.class, localizedBinaryResource.getModelClass());
	}

}
