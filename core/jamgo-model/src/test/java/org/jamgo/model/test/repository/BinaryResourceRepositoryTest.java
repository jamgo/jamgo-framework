package org.jamgo.model.test.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.repository.BinaryResourceRepository;
import org.jamgo.model.test.config.ModelTestConfig;
import org.jamgo.model.test.entity.builder.BinaryResourceBuilder;
import org.jamgo.model.test.entity.builder.TestEntityWithBinaryResourceBuilder;
import org.jamgo.test.JamgoRepositoryTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@ContextConfiguration(classes = { ModelTestConfig.class })
public class BinaryResourceRepositoryTest extends JamgoRepositoryTest {

	@Autowired
	private BinaryResourceRepository binaryResourceRepository;

	@Test
	@Transactional
	public void testPurgeCandidates() {
		this.entityManager.joinTransaction();
		final List<BinaryResource> list = new BinaryResourceBuilder(this.entityManager).build(5);
		final BinaryResource photo = list.get(0);
		new TestEntityWithBinaryResourceBuilder(this.entityManager).withPhoto(photo).buildOne();
		this.commit();

		assertEquals(5, this.binaryResourceRepository.countAll(null));
		final List<BinaryResource> candidates = this.binaryResourceRepository.getPurgeCandidates();
		assertEquals(4, candidates.size());
		this.binaryResourceRepository.remove(candidates);
		assertEquals(1, this.binaryResourceRepository.countAll(null));
	}

}
