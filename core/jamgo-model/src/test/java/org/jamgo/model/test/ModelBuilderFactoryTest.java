package org.jamgo.model.test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jamgo.model.entity.Acl;
import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.LocalizedMessage;
import org.jamgo.model.entity.Model;
import org.jamgo.model.entity.SecuredObject;
import org.jamgo.model.test.entity.builder.ModelBuilderFactory;
import org.jamgo.test.JamgoTest;
import org.junit.jupiter.api.Test;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.CharSink;
import com.google.common.io.Files;

public class ModelBuilderFactoryTest extends JamgoTest {

	@Test
	public void testCreateBuilder() {
		final ModelBuilderFactory modelBuilderFactory = new ModelBuilderFactory();

		final List<Class<? extends Model>> entityClasses = Lists.newArrayList(
			Acl.class,
			BinaryResource.class,
			Language.class,
			LocalizedMessage.class,
			SecuredObject.class);

		entityClasses.forEach(eachEntityClass -> {
			final String sourceFileName = new StringBuilder()
				.append("src/test/java/")
				.append((eachEntityClass.getPackage().getName() + ModelBuilderFactory.BUILDER_PACKAGE_SUFFIX).replace(".", File.separator))
				.append(File.separator)
				.append(StringUtils.capitalize(eachEntityClass.getSimpleName()))
				.append(ModelBuilderFactory.BUILDER_CLASS_SUFFIX).append(".java")
				.toString();

			final File sourceFile = new File(sourceFileName);
			if (!sourceFile.exists()) {
				final String modelBuilderCode = modelBuilderFactory.createBuilderSource(eachEntityClass);

				final CharSink sink = Files.asCharSink(sourceFile, Charsets.UTF_8);
				try {
					sink.write(modelBuilderCode);
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
}
