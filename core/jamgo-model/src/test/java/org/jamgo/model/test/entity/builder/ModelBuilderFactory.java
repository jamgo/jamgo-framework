package org.jamgo.model.test.entity.builder;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.IntStream;

import javax.lang.model.element.Modifier;
import javax.persistence.EntityManager;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jamgo.model.entity.Model;
import org.reflections.ReflectionUtils;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.WildcardTypeName;

public class ModelBuilderFactory {

	public final static String BUILDER_CLASS_SUFFIX = "Builder";
	public final static String BUILDER_PACKAGE_SUFFIX = ".builder";
	private final static String WITH_FIELD_PREFIX = "with";
	private final static String COUNT_FIELD_SUFFIX = "Count";

	private final static Map<Class<?>, Class<?>> PRIMITIVE_CLASSES = new HashMap<>();
	static {
		ModelBuilderFactory.PRIMITIVE_CLASSES.put(int.class, Integer.class);
		ModelBuilderFactory.PRIMITIVE_CLASSES.put(long.class, Long.class);
		ModelBuilderFactory.PRIMITIVE_CLASSES.put(short.class, Short.class);
		ModelBuilderFactory.PRIMITIVE_CLASSES.put(byte.class, Byte.class);
		ModelBuilderFactory.PRIMITIVE_CLASSES.put(float.class, Float.class);
		ModelBuilderFactory.PRIMITIVE_CLASSES.put(double.class, Double.class);
		ModelBuilderFactory.PRIMITIVE_CLASSES.put(char.class, Character.class);
		ModelBuilderFactory.PRIMITIVE_CLASSES.put(boolean.class, Boolean.class);
	}

	@SuppressWarnings("unchecked")
	public String createBuilderSource(final Class<? extends Model> modelClass) {
		final ClassName builderClassName = ClassName.get(this.getBuilderPackageName(modelClass), this.getBuilderClassName(modelClass));

		final List<Field> instanceFields = new ArrayList<>();
		for (final Field eachField : ReflectionUtils.getAllFields(modelClass)) {
			if (!java.lang.reflect.Modifier.isStatic(eachField.getModifiers())
				&& eachField.getName() != "id" && eachField.getName() != "version") {

				instanceFields.add(eachField);
			}
		}

		final List<FieldSpec> builderFields = new ArrayList<>();
		final List<MethodSpec> builderMethods = new ArrayList<>();

		this.addFields(builderFields, builderMethods, instanceFields, builderClassName);
		this.addConstructors(builderMethods);
		this.addBasicBuild(builderMethods, instanceFields, modelClass);

		final TypeName superclassTypeName = ParameterizedTypeName.get(ModelBuilder.class, modelClass);

		final TypeSpec builderSpec = TypeSpec.classBuilder(builderClassName)
			.addModifiers(Modifier.PUBLIC)
			.superclass(superclassTypeName)
			.addFields(builderFields)
			.addMethods(builderMethods)
			.build();

		final JavaFile builderJavaFile = JavaFile.builder(builderClassName.packageName(), builderSpec)
			.indent("\t")
			.skipJavaLangImports(true)
			.build();

		final StringWriter writer = new StringWriter();
		try {
			builderJavaFile.writeTo(writer);
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return writer.toString();
	}

	private void addFields(final List<FieldSpec> fieldSpecs, final List<MethodSpec> methodSpecs, final List<Field> fields, final ClassName builderClassName) {
		fields.forEach(eachField -> {
			fieldSpecs.add(this.createField(eachField));
			methodSpecs.add(this.createSetter(eachField, builderClassName));
		});
		fields.stream()
			.filter(eachField -> Model.class.isAssignableFrom(eachField.getType()))
			.forEach(eachField -> {
				fieldSpecs.add(this.createWithField(eachField));
				methodSpecs.add(this.createWithSetter(eachField, builderClassName));
			});
		fields.stream()
			.filter(eachField -> Collection.class.isAssignableFrom(eachField.getType()))
			.forEach(eachField -> {
				fieldSpecs.add(this.createCountField(eachField));
				methodSpecs.add(this.createCountSetter(eachField, builderClassName));
			});
	}

	private FieldSpec createField(final Field field) {
		final Type fieldSpecClass = this.getFieldType(field);
		final FieldSpec fieldSpec = FieldSpec.builder(fieldSpecClass, field.getName(), Modifier.PRIVATE)
			.initializer("null")
			.build();
		return fieldSpec;
	}

	private FieldSpec createWithField(final Field field) {
		final FieldSpec fieldSpec = FieldSpec.builder(boolean.class, this.getWithFieldName(field), Modifier.PRIVATE)
			.initializer("false")
			.build();
		return fieldSpec;
	}

	private FieldSpec createCountField(final Field field) {
		final FieldSpec fieldSpec = FieldSpec.builder(int.class, this.getCountFieldName(field), Modifier.PRIVATE)
			.initializer("0")
			.build();
		return fieldSpec;
	}

	private void addConstructors(final List<MethodSpec> methodSpecs) {
		methodSpecs.add(MethodSpec.constructorBuilder()
			.addModifiers(Modifier.PUBLIC)
			.addParameter(EntityManager.class, "entityManager", Modifier.FINAL)
			.addStatement("super($N)", "entityManager")
			.build());

		final ParameterSpec modelBuilderParameter = ParameterSpec
			.builder(ParameterizedTypeName.get(ClassName.get(ModelBuilder.class), WildcardTypeName.subtypeOf(Object.class)), "parentBuilder", Modifier.FINAL)
			.build();
		methodSpecs.add(MethodSpec.constructorBuilder()
			.addModifiers(Modifier.PUBLIC)
			.addParameter(EntityManager.class, "entityManager", Modifier.FINAL)
			.addParameter(modelBuilderParameter)
			.addStatement("super($N, $N)", "entityManager", "parentBuilder")
			.build());
	}

	private MethodSpec createSetter(final Field field, final ClassName builderClassName) {
		final MethodSpec setterSpec = MethodSpec.methodBuilder("set" + StringUtils.capitalize(field.getName()))
			.addModifiers(Modifier.PUBLIC)
			.returns(builderClassName)
			.addParameter(this.getFieldType(field), field.getName(), Modifier.FINAL)
			.addStatement("this.$N = $N", field.getName(), field.getName())
			.addStatement("return this")
			.build();

		return setterSpec;
	}

	private MethodSpec createWithSetter(final Field field, final ClassName builderClassName) {
		final String fieldName = this.getWithFieldName(field);
		final MethodSpec setterSpec = MethodSpec.methodBuilder("set" + StringUtils.capitalize(fieldName))
			.addModifiers(Modifier.PUBLIC)
			.returns(builderClassName)
			.addStatement("this.$N = true", fieldName)
			.addStatement("return this")
			.build();

		return setterSpec;
	}

	private MethodSpec createCountSetter(final Field field, final ClassName builderClassName) {
		final String fieldName = this.getCountFieldName(field);
		final MethodSpec setterSpec = MethodSpec.methodBuilder("set" + StringUtils.capitalize(this.getWithFieldName(field)))
			.addModifiers(Modifier.PUBLIC)
			.returns(builderClassName)
			.addParameter(int.class, fieldName, Modifier.FINAL)
			.addStatement("this.$N = $N", fieldName, fieldName)
			.addStatement("return this")
			.build();

		return setterSpec;
	}

	private void addBasicBuild(final List<MethodSpec> methodSpecs, final List<Field> fields, final Class<? extends Model> modelClass) {
		final MethodSpec.Builder basicBuildSpecBuilder = MethodSpec.methodBuilder("basicBuild")
			.addAnnotation(Override.class)
			.addModifiers(Modifier.PROTECTED)
			.returns(modelClass);

		fields.forEach(eachField -> this.addValueStatement(basicBuildSpecBuilder, eachField, modelClass));
		basicBuildSpecBuilder.addCode("\n");
		this.addResultInstanceStatement(basicBuildSpecBuilder, modelClass);
		fields.forEach(eachField -> this.addSetResultStatement(basicBuildSpecBuilder, eachField, modelClass));
		basicBuildSpecBuilder.addCode("\n");
		this.addReturnInstanceStatement(basicBuildSpecBuilder, modelClass);

		methodSpecs.add(basicBuildSpecBuilder.build());
	}

	private void addValueStatement(final MethodSpec.Builder methodBuilder, final Field field, final Class<? extends Model> modelClass) {
		Class<?> fieldType = field.getType();
		if (fieldType.isArray()) {
			fieldType = fieldType.getComponentType();
		}

		if (String.class.isAssignableFrom(fieldType)) {
			this.addStringValueStatement(methodBuilder, field, modelClass);
		} else if (fieldType == Boolean.class || fieldType == boolean.class) {
			this.addBooleanValueStatement(methodBuilder, field, modelClass);
		} else if (fieldType == Character.class || fieldType == char.class) {
			this.addCharacterValueStatement(methodBuilder, field, modelClass);
		} else if (Number.class.isAssignableFrom(fieldType) || fieldType.isPrimitive()) {
			this.addNumberValueStatement(methodBuilder, field, modelClass);
		} else if (ArrayUtils.contains(fieldType.getInterfaces(), Temporal.class)) {
			this.addTemporalValueStatement(methodBuilder, field, modelClass);
		} else if (Collection.class.isAssignableFrom(fieldType)) {
			this.addCollectionValueStatement(methodBuilder, field, modelClass);
		} else if (Model.class.isAssignableFrom(fieldType)) {
			this.addModelValueStatement(methodBuilder, field, modelClass);
		} else if (fieldType.isEnum()) {
			this.addEnumValueStatement(methodBuilder, field, modelClass);
		} else {
			this.addTodoValueStatement(methodBuilder, field, modelClass);
		}
	}

	private void addStringValueStatement(final MethodSpec.Builder methodSpecBuilder, final Field field, final Class<? extends Model> modelClass) {
		final StringBuilder statementBuilder = new StringBuilder();
		statementBuilder
			.append("final $T ").append(field.getName())
			.append(" = $T.ofNullable(this.").append(field.getName())
			.append(").orElse($T.class.getSimpleName() + \" ")
			.append(field.getName()).append(" \" + this.currentSuffix)");

		methodSpecBuilder.addStatement(statementBuilder.toString(), String.class, Optional.class, modelClass);
	}

	private void addCharacterValueStatement(final MethodSpec.Builder methodSpecBuilder, final Field field, final Class<? extends Model> modelClass) {
		final StringBuilder statementBuilder = new StringBuilder();
		statementBuilder
			.append("final $T ").append(field.getName())
			.append(" = $T.ofNullable(this.").append(field.getName())
			.append(").orElse('A')");

		methodSpecBuilder.addStatement(statementBuilder.toString(), Character.class, Optional.class);
	}

	private void addNumberValueStatement(final MethodSpec.Builder methodSpecBuilder, final Field field, final Class<? extends Model> modelClass) {
		final StringBuilder statementBuilder = new StringBuilder();
		statementBuilder
			.append("final $T ").append(field.getName())
			.append(" = $T.ofNullable(this.").append(field.getName())
			.append(").orElse(");

		if (field.getType().isArray()) {
			statementBuilder.append("new $T{ ");
			IntStream.range(0, 5).forEach(i -> {
				if (i > 0) {
					statementBuilder.append(", ");
				}
				statementBuilder.append(this.getNumberValue(field, i));
			});
			statementBuilder.append(" })");
			methodSpecBuilder.addStatement(statementBuilder.toString(), field.getType(), Optional.class, field.getType());
		} else {
			statementBuilder.append(this.getNumberValue(field, 1)).append(")");
			methodSpecBuilder.addStatement(statementBuilder.toString(), field.getType(), Optional.class);
		}
	}

	private String getNumberValue(final Field field, final int baseValue) {
		String value = String.valueOf(baseValue);
		if (field.getType() == Long.class || field.getType() == long.class) {
			value = value + "L";
		} else if (field.getType() == Double.class || field.getType() == double.class) {
			value = value + ".0";
		} else if (field.getType() == Float.class || field.getType() == float.class) {
			value = value + ".0f";
		}

		return value;
	}

	private void addTemporalValueStatement(final MethodSpec.Builder methodSpecBuilder, final Field field, final Class<? extends Model> modelClass) {
		String defaultValue = null;
		if (field.getType() == LocalDate.class) {
			defaultValue = "2017-10-01";
		} else if (field.getType() == LocalTime.class) {
			defaultValue = "09:00:00";
		} else if (field.getType() == LocalDateTime.class) {
			defaultValue = "2017-10-01T09:00:00";
		}

		final StringBuilder statementBuilder = new StringBuilder();
		if (defaultValue != null) {
			statementBuilder
				.append("final $T ").append(field.getName())
				.append(" = $T.ofNullable(this.").append(field.getName())
				.append(").orElse($T.parse(\"").append(defaultValue).append("\"))");

			methodSpecBuilder.addStatement(statementBuilder.toString(), field.getType(), Optional.class, field.getType());
		} else {
			this.addTodoValueStatement(methodSpecBuilder, field, modelClass);
		}
	}

	private void addBooleanValueStatement(final MethodSpec.Builder methodSpecBuilder, final Field field, final Class<? extends Model> modelClass) {
		final StringBuilder statementBuilder = new StringBuilder();
		statementBuilder
			.append("final $T ").append(field.getName())
			.append(" = $T.ofNullable(this.").append(field.getName()).append(").orElse(false)");

		methodSpecBuilder.addStatement(statementBuilder.toString(), Boolean.class, Optional.class);
	}

	private void addEnumValueStatement(final MethodSpec.Builder methodSpecBuilder, final Field field, final Class<? extends Model> modelClass) {
		final StringBuilder statementBuilder = new StringBuilder();
		statementBuilder
			.append("final $T ").append(field.getName())
			.append(" = $T.ofNullable(this.").append(field.getName())
			.append(").orElse($T.").append(field.getType().getEnumConstants()[0].toString())
			.append(")");

		methodSpecBuilder.addStatement(statementBuilder.toString(), field.getType(), Optional.class, field.getType());
	}

	private void addTodoValueStatement(final MethodSpec.Builder methodSpecBuilder, final Field field, final Class<? extends Model> modelClass) {
		final StringBuilder statementBuilder = new StringBuilder();
		statementBuilder
			.append("// TODO: initialize ").append(field.getName()).append(" default value");

		methodSpecBuilder.addStatement(statementBuilder.toString());
	}

	private void addCollectionValueStatement(final MethodSpec.Builder methodSpecBuilder, final Field field, final Class<? extends Model> modelClass) {
		final ParameterizedType genericType = (ParameterizedType) field.getGenericType();
		final Class<?> collectionModelClass = (Class<?>) genericType.getActualTypeArguments()[0];

		if (collectionModelClass.isInterface()) {
			this.addTodoValueStatement(methodSpecBuilder, field, modelClass);
		} else {
			ParameterizedTypeName collectionClassName;
			if (Set.class.isAssignableFrom(field.getType())) {
				collectionClassName = ParameterizedTypeName.get(LinkedHashSet.class, collectionModelClass);
			} else {
				collectionClassName = ParameterizedTypeName.get(ArrayList.class, collectionModelClass);
			}

			final ClassName modelBuilder = ClassName.get(this.getBuilderPackageName(collectionModelClass), this.getBuilderClassName(collectionModelClass));

			final String countVariableName = field.getName() + "Count";

			final StringBuilder statementBuilder = new StringBuilder();
			statementBuilder
				.append("final $T ").append(field.getName())
				.append(" = this.").append(countVariableName).append(" > 0 ")
				.append("? new $T(new $T(this.entityManager, this).build(this.")
				.append(countVariableName).append(")) : this.").append(field.getName());

			methodSpecBuilder.addStatement(statementBuilder.toString(), this.getFieldType(field), collectionClassName, modelBuilder);
		}
	}

	private void addModelValueStatement(final MethodSpec.Builder methodSpecBuilder, final Field field, final Class<? extends Model> modelClass) {
		final StringBuilder statementBuilder = new StringBuilder();
		statementBuilder
			.append("final $T ").append(field.getName())
			.append(" = this.with").append(StringUtils.capitalize(field.getName()))
			.append(" ? new $T(this.entityManager, this).buildOne() : this.").append(field.getName());

		final ClassName modelBuilder = ClassName.get(this.getBuilderPackageName(field.getType()), this.getBuilderClassName(field.getType()));

		methodSpecBuilder.addStatement(statementBuilder.toString(), field.getType(), modelBuilder);
	}

	private void addResultInstanceStatement(final MethodSpec.Builder methodBuilder, final Class<? extends Model> modelClass) {
		final StringBuilder statementBuilder = new StringBuilder();
		statementBuilder
			.append("final $T ").append(StringUtils.uncapitalize(modelClass.getSimpleName()))
			.append(" = new $T()");

		methodBuilder.addStatement(statementBuilder.toString(), modelClass, modelClass);
	}

	private void addSetResultStatement(final MethodSpec.Builder methodBuilder, final Field field, final Class<? extends Model> modelClass) {
		final StringBuilder statementBuilder = new StringBuilder();
		statementBuilder
			.append(StringUtils.uncapitalize(modelClass.getSimpleName()))
			.append(".set").append(StringUtils.capitalize(field.getName()))
			.append("(").append(field.getName()).append(")");

		methodBuilder.addStatement(statementBuilder.toString());
	}

	private void addReturnInstanceStatement(final MethodSpec.Builder methodBuilder, final Class<? extends Model> modelClass) {
		final StringBuilder statementBuilder = new StringBuilder();
		statementBuilder
			.append("return ").append(StringUtils.uncapitalize(modelClass.getSimpleName()));

		methodBuilder.addStatement(statementBuilder.toString());
	}

	private Type getFieldType(final Field field) {
		if (Collection.class.isAssignableFrom(field.getType())) {
			return field.getGenericType();
		} else if (!field.getType().isArray() && field.getType().isPrimitive()) {
			return ModelBuilderFactory.PRIMITIVE_CLASSES.get(field.getType());
		} else {
			return field.getType();
		}
	}

	private String getBuilderClassName(final Class<?> modelClass) {
		return modelClass.getSimpleName() + ModelBuilderFactory.BUILDER_CLASS_SUFFIX;
	}

	private String getBuilderPackageName(final Class<?> modelClass) {
		return modelClass.getPackage().getName() + ModelBuilderFactory.BUILDER_PACKAGE_SUFFIX;
	}

	private String getWithFieldName(final Field field) {
		return ModelBuilderFactory.WITH_FIELD_PREFIX + StringUtils.capitalize(field.getName());
	}

	private String getCountFieldName(final Field field) {
		return field.getName() + ModelBuilderFactory.COUNT_FIELD_SUFFIX;
	}
}
