package org.jamgo.model.test.entity.builder;

import java.util.Optional;

import javax.persistence.EntityManager;

import org.jamgo.model.entity.TestEntity;

public class TestEntityBuilder extends ModelBuilder<TestEntity> {

	private String name;
	private Integer age;
	private String phone;

	public TestEntityBuilder(final EntityManager entityManager) {
		super(entityManager);
	}

	public TestEntityBuilder(final EntityManager entityManager, final ModelBuilder<?> parentBuilder) {
		super(entityManager, parentBuilder);
	}

	public TestEntityBuilder withName(final String name) {
		this.name = name;
		return this;
	}

	public TestEntityBuilder withAge(final Integer age) {
		this.age = age;
		return this;
	}

	public TestEntityBuilder withPhone(final String phone) {
		this.phone = phone;
		return this;
	}

	@Override
	protected TestEntity basicBuild() {
		final String name = Optional.ofNullable(this.name).orElse(TestEntity.class.getSimpleName() + " name " + this.currentSuffix);
		final Integer age = Optional.ofNullable(this.age).orElse(42);
		final String phone = Optional.ofNullable(this.phone).orElse(TestEntity.class.getSimpleName() + " phone " + this.currentSuffix);

		final TestEntity entity = new TestEntity();
		entity.setName(name);
		entity.setAge(age);
		entity.setPhone(phone);
		return entity;
	}

}
