package org.jamgo.model.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class TestSimpleObject extends Model {

	private static final long serialVersionUID = 1L;

	private String name;
	private Integer age;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<TestCategory> categories;

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Integer getAge() {
		return this.age;
	}

	public void setAge(final Integer age) {
		this.age = age;
	}

	public Set<TestCategory> getCategories() {
		return this.categories;
	}

	public void setCategories(final Set<TestCategory> categories) {
		this.categories = categories;
	}

}
