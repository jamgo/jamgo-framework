package org.jamgo.model.test.entity.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import javax.persistence.EntityManager;

import org.jamgo.model.entity.LocalizedString;

public abstract class ModelBuilder<T> {

	protected EntityManager entityManager;
	private String suffix = null;
	protected String currentSuffix = null;
	protected int currentSuffixIndex;
	protected ModelBuilder<?> parentBuilder;

	public ModelBuilder(final EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}

	public ModelBuilder(final EntityManager entityManager, final ModelBuilder<?> parentBuilder) {
		this(entityManager);
		this.parentBuilder = parentBuilder;
	}

	public ModelBuilder(final ModelBuilder<?> modelBuilder) {
		this.entityManager = modelBuilder.entityManager;
		this.suffix = modelBuilder.suffix;
		this.currentSuffix = modelBuilder.currentSuffix;
	}

	public EntityManager getEntityManager() {
		return this.entityManager;
	}

	public ModelBuilder<T> setSuffix(final String suffix) {
		this.suffix = suffix;
		return this;
	}

	public ModelBuilder<?> getParentBuilder() {
		return this.parentBuilder;
	}

	public void setParentBuilder(final ModelBuilder<?> parentBuilder) {
		this.parentBuilder = parentBuilder;
	}

	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public String getCurrentSuffix() {
		return this.currentSuffix;
	}

	// ... TODO: parametrize locales
	public LocalizedString createLocalizedString(final String baseName) {
		final LocalizedString localizedString = new LocalizedString();
		localizedString.set("ca-ES", baseName + "_ca-ES");
		localizedString.set("es-ES", baseName + "_es-ES");
		localizedString.set("en-US", baseName + "_en-US");
		return localizedString;
	}

	public T buildOne() {
		return this.build(1, 1).get(0);
	}

	public T buildOne(final Integer suffixIndex) {
		return this.build(1, suffixIndex).get(0);
	}

	public List<T> build(final Integer instancesCount) {
		return this.build(instancesCount, 1);
	}

	public List<T> build(final Integer instancesCount, final Integer startSuffixIndex) {
		final List<T> models = new ArrayList<>();
		IntStream.range(0, instancesCount).forEach(i -> {
			this.updateCurrentSuffix(startSuffixIndex + i);
			final T model = this.basicBuild();
			this.entityManager.persist(model);
			this.buildChildren(model, this.currentSuffix);
			models.add(model);
		});
		return models;
	}

	protected void updateCurrentSuffix(final Integer suffixIndex) {
		final StringBuilder currentSuffixBuilder = new StringBuilder();
		if (this.suffix != null) {
			currentSuffixBuilder.append(this.suffix);
		} else {
			if (this.parentBuilder != null) {
				currentSuffixBuilder.append(this.parentBuilder.getCurrentSuffix());
			}
			currentSuffixBuilder.append(suffixIndex);
		}
		this.currentSuffix = currentSuffixBuilder.toString();
		this.currentSuffixIndex = suffixIndex;
	}

	protected abstract T basicBuild();

	protected List<T> buildChildren(final T parent, final String parentSuffix) {
		return null;
	}
}
