package org.jamgo.model.test.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.jamgo.model.entity.Language;
import org.jamgo.model.repository.LanguageRepository;
import org.jamgo.model.test.config.ModelTestConfig;
import org.jamgo.test.JamgoRepositoryTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@ContextConfiguration(classes = { ModelTestConfig.class })
public class LanguageRepositoryTest extends JamgoRepositoryTest {

	@Autowired
	private LanguageRepository languageRepository;

	@Test
	@Transactional
	public void testFindAllLanguagesEntities() {
		final List<Language> languages = this.languageRepository.findAll();
		assertNotNull(languages);
	}

}
