package org.jamgo.model.test.entity.builder;

import java.util.Optional;
import javax.persistence.EntityManager;
import org.jamgo.model.entity.LocalizedMessage;
import org.jamgo.model.entity.LocalizedString;

public class LocalizedMessageBuilder extends ModelBuilder<LocalizedMessage> {
	private String key = null;

	private LocalizedString value = null;

	private LocalizedString description = null;

	private LocalizedString parametersInfo = null;

	private Boolean missing = null;

	public LocalizedMessageBuilder(EntityManager entityManager) {
		super(entityManager);
	}

	public LocalizedMessageBuilder(EntityManager entityManager, ModelBuilder<?> parentBuilder) {
		super(entityManager, parentBuilder);
	}

	public LocalizedMessageBuilder setKey(String key) {
		this.key = key;
		return this;
	}

	public LocalizedMessageBuilder setValue(LocalizedString value) {
		this.value = value;
		return this;
	}

	public LocalizedMessageBuilder setDescription(LocalizedString description) {
		this.description = description;
		return this;
	}

	public LocalizedMessageBuilder setParametersInfo(LocalizedString parametersInfo) {
		this.parametersInfo = parametersInfo;
		return this;
	}

	public LocalizedMessageBuilder setMissing(Boolean missing) {
		this.missing = missing;
		return this;
	}

	@Override
	protected LocalizedMessage basicBuild() {
		final String key = Optional.ofNullable(this.key).orElse(LocalizedMessage.class.getSimpleName() + " key " + this.currentSuffix);
		// TODO: initialize value default value;
		// TODO: initialize description default value;
		// TODO: initialize parametersInfo default value;
		final Boolean missing = Optional.ofNullable(this.missing).orElse(false);

		final LocalizedMessage localizedMessage = new LocalizedMessage();
		localizedMessage.setKey(key);
		localizedMessage.setValue(value);
		localizedMessage.setDescription(description);
		localizedMessage.setParametersInfo(parametersInfo);
		localizedMessage.setMissing(missing);

		return localizedMessage;
	}
}
