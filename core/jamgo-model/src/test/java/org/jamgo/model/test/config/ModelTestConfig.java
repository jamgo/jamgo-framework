package org.jamgo.model.test.config;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.sql.DataSource;

import org.apache.commons.lang3.ArrayUtils;
import org.hibernate.cfg.AvailableSettings;
import org.jamgo.model.filter.IgnoreDuringScan;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate5.SpringBeanContainer;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.blazebit.persistence.Criteria;
import com.blazebit.persistence.CriteriaBuilderFactory;
import com.blazebit.persistence.integration.view.spring.EnableEntityViews;
import com.blazebit.persistence.spi.CriteriaBuilderConfiguration;
import com.blazebit.persistence.view.EntityViewManager;
import com.blazebit.persistence.view.spi.EntityViewConfiguration;
import com.zaxxer.hikari.HikariDataSource;

import io.zonky.test.db.postgres.embedded.EmbeddedPostgres;

@Configuration
@EnableTransactionManagement
@ComponentScan(
	basePackageClasses = {
		org.jamgo.model.entity.PackageMarker.class,
		org.jamgo.model.repository.PackageMarker.class,
		org.jamgo.model.search.PackageMarker.class,
		org.jamgo.model.jpa.PackageMarker.class
	})
@EnableEntityViews(
	basePackageClasses = {
		org.jamgo.model.view.PackageMarker.class
	})
@IgnoreDuringScan
public class ModelTestConfig {

	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;

	@Bean
	public EntityManager entityManager() {
		return this.entityManagerFactory.createEntityManager();
	}

	@Bean
	public DataSource dataSource() {
		final EmbeddedPostgres embeddedPostgres = this.postgresServer();
		final String jdbcUrl = String.format("jdbc:p6spy:postgresql://localhost:%s/postgres", String.valueOf(embeddedPostgres.getPort()));

		final HikariDataSource dataSource = new HikariDataSource();
		dataSource.setDriverClassName("com.p6spy.engine.spy.P6SpyDriver");
		dataSource.setJdbcUrl(jdbcUrl);
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres");
		return dataSource;
	}

	@Bean
	public EmbeddedPostgres postgresServer() {
		EmbeddedPostgres embeddedPostgres = null;
		try {
			embeddedPostgres = EmbeddedPostgres.builder()
				.setPGStartupWait(Duration.ofSeconds(30))
				.start();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return embeddedPostgres;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(final ConfigurableListableBeanFactory beanFactory, final DataSource dataSource, final Environment environment) {
		final LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(false);
		vendorAdapter.setShowSql(false);
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan(this.getPackagesToScan());
		factory.setDataSource(dataSource);
		factory.setJpaPropertyMap(this.getJpaPropertiesMap(beanFactory));
		return factory;
	}

	public String[] getPackagesToScan() {
		return ArrayUtils.toArray(org.jamgo.model.PackageMarker.class.getPackage().getName());
	}

	@Bean
	public PlatformTransactionManager transactionManager(final ConfigurableListableBeanFactory beanFactory, final DataSource dataSource, final Environment environment) {
		final JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(this.entityManagerFactory(beanFactory, dataSource, environment).getObject());
		transactionManager.setDataSource(dataSource);
		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	private Map<String, Object> getJpaPropertiesMap(final ConfigurableListableBeanFactory beanFactory) {
		final Map<String, Object> properties = new HashMap<>();
		properties.put("hibernate.hbm2ddl.auto", "create-only");
		properties.put("hibernate.implicit_naming_strategy", "org.jamgo.model.jpa.CustomImplicitNamingStrategy");
		properties.put("hibernate.physical_naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");
		properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
		properties.put("hibernate.enable_lazy_load_no_trans", "true");
		properties.put("hibernate.globally_quoted_identifiers", "true");
		properties.put(AvailableSettings.BEAN_CONTAINER, this.springBeanContainer(beanFactory));
		return properties;
	}

	@Bean
	public SpringBeanContainer springBeanContainer(final ConfigurableListableBeanFactory beanFactory) {
		return new SpringBeanContainer(beanFactory);
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
	@Lazy(false)
	public CriteriaBuilderFactory createCriteriaBuilderFactory() {
		final CriteriaBuilderConfiguration config = Criteria.getDefault();
		return config.createCriteriaBuilderFactory(this.entityManagerFactory);
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
	@Lazy(false)
	public EntityViewManager createEntityViewManager(final CriteriaBuilderFactory cbf, final EntityViewConfiguration entityViewConfiguration) {
		entityViewConfiguration.setTypeTestValue(LocalDate.class, LocalDate.ofEpochDay(1));
		return entityViewConfiguration.createEntityViewManager(cbf);
	}

}
