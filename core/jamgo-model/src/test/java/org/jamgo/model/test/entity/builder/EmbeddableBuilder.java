package org.jamgo.model.test.entity.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public abstract class EmbeddableBuilder<T> {

	protected String suffix = null;
	protected String currentSuffix = null;
	protected ModelBuilder<?> parentBuilder;

	public EmbeddableBuilder() {
		super();
	}

	public EmbeddableBuilder(final ModelBuilder<?> parentBuilder) {
		this.parentBuilder = parentBuilder;
	}

	public EmbeddableBuilder<T> setSuffix(final String suffix) {
		this.suffix = suffix;
		return this;
	}

//	protected I18nString createI18nString(String baseName) {
//		return new I18nString(baseName + "ca_ES", baseName + "es_ES", baseName + "en_US");
//	}

	public T buildOne() {
		return this.build(1, 1).get(0);
	}

	public T buildOne(final Integer suffixIndex) {
		return this.build(1, suffixIndex).get(0);
	}

	public List<T> build(final Integer instancesCount, final Integer startSuffixIndex) {
		final List<T> models = new ArrayList<>();
		IntStream.range(0, instancesCount).forEach(i -> {
			this.updateCurrentSuffix(startSuffixIndex + i);
			final T model = this.basicBuild();
			models.add(model);
		});
		return models;
	}

	protected void updateCurrentSuffix(final Integer suffixIndex) {
		final StringBuilder currentSuffixBuilder = new StringBuilder();
		if (this.suffix != null) {
			currentSuffixBuilder.append(this.suffix);
		} else {
			if (this.parentBuilder != null) {
				currentSuffixBuilder.append(this.parentBuilder.getCurrentSuffix());
			}
			currentSuffixBuilder.append(suffixIndex);
		}
		this.currentSuffix = currentSuffixBuilder.toString();
	}

	protected abstract T basicBuild();

}