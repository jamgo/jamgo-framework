package org.jamgo.model.repository;

import org.jamgo.model.entity.TestSimpleObject;
import org.springframework.stereotype.Repository;

@Repository
public class TestSimpleObjectRepository extends ModelRepository<TestSimpleObject> {

	@Override
	protected Class<TestSimpleObject> getModelClass() {
		return TestSimpleObject.class;
	}

}
