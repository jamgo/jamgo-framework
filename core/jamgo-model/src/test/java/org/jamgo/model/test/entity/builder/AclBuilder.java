package org.jamgo.model.test.entity.builder;

import java.util.Optional;
import javax.persistence.EntityManager;
import org.jamgo.model.entity.Acl;
import org.jamgo.model.entity.SecuredObject;
import org.jamgo.model.enums.Permission;

public class AclBuilder extends ModelBuilder<Acl> {
	private Long roleId = null;

	private Long userId = null;

	private SecuredObject securedObject = null;

	private Permission permission = null;

	private boolean withSecuredObject = false;

	public AclBuilder(EntityManager entityManager) {
		super(entityManager);
	}

	public AclBuilder(EntityManager entityManager, ModelBuilder<?> parentBuilder) {
		super(entityManager, parentBuilder);
	}

	public AclBuilder setRoleId(Long roleId) {
		this.roleId = roleId;
		return this;
	}

	public AclBuilder setUserId(Long userId) {
		this.userId = userId;
		return this;
	}

	public AclBuilder setSecuredObject(SecuredObject securedObject) {
		this.securedObject = securedObject;
		return this;
	}

	public AclBuilder setPermission(Permission permission) {
		this.permission = permission;
		return this;
	}

	public AclBuilder setWithSecuredObject() {
		this.withSecuredObject = true;
		return this;
	}

	@Override
	protected Acl basicBuild() {
		final Long roleId = Optional.ofNullable(this.roleId).orElse(1L);
		final Long userId = Optional.ofNullable(this.userId).orElse(1L);
		final SecuredObject securedObject = this.withSecuredObject ? new SecuredObjectBuilder(this.entityManager, this).buildOne() : this.securedObject;
		final Permission permission = Optional.ofNullable(this.permission).orElse(Permission.NONE);

		final Acl acl = new Acl();
		acl.setRoleId(roleId);
		acl.setUserId(userId);
		acl.setSecuredObject(securedObject);
		acl.setPermission(permission);

		return acl;
	}
}
