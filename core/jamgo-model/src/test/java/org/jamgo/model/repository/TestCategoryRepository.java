package org.jamgo.model.repository;

import java.util.List;
import java.util.Locale;

import org.jamgo.model.entity.TestCategory;
import org.springframework.stereotype.Repository;

@Repository
public class TestCategoryRepository extends CategoryRepository<TestCategory> {

	@Override
	protected Class<TestCategory> getModelClass() {
		return TestCategory.class;
	}

	public List<TestCategory> findByName(final String name, final Locale locale) {
		return this.createCriteriaBuilder()
			.where(String.format("JSON_GET(name, '%s')", locale.toLanguageTag())).eq(name)
			.getResultList();
	}
}
