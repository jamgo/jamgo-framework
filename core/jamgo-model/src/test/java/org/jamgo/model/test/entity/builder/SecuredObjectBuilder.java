package org.jamgo.model.test.entity.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.jamgo.model.entity.Acl;
import org.jamgo.model.entity.SecuredObject;
import org.jamgo.model.enums.SecuredObjectType;

public class SecuredObjectBuilder extends ModelBuilder<SecuredObject> {
	private String name = null;

	private SecuredObjectType securedObjectType = null;

	private String securedObjectKey = null;

	private SecuredObject parent = null;

	private List<SecuredObject> children = null;

	private List<Acl> acl = null;

	private boolean withParent = false;

	private int childrenCount = 0;

	private int aclCount = 0;

	public SecuredObjectBuilder(EntityManager entityManager) {
		super(entityManager);
	}

	public SecuredObjectBuilder(EntityManager entityManager, ModelBuilder<?> parentBuilder) {
		super(entityManager, parentBuilder);
	}

	public SecuredObjectBuilder setName(String name) {
		this.name = name;
		return this;
	}

	public SecuredObjectBuilder setSecuredObjectType(SecuredObjectType securedObjectType) {
		this.securedObjectType = securedObjectType;
		return this;
	}

	public SecuredObjectBuilder setSecuredObjectKey(String securedObjectKey) {
		this.securedObjectKey = securedObjectKey;
		return this;
	}

	public SecuredObjectBuilder setParent(SecuredObject parent) {
		this.parent = parent;
		return this;
	}

	public SecuredObjectBuilder setChildren(List<SecuredObject> children) {
		this.children = children;
		return this;
	}

	public SecuredObjectBuilder setAcl(List<Acl> acl) {
		this.acl = acl;
		return this;
	}

	public SecuredObjectBuilder setWithParent() {
		this.withParent = true;
		return this;
	}

	public SecuredObjectBuilder setWithChildren(int childrenCount) {
		this.childrenCount = childrenCount;
		return this;
	}

	public SecuredObjectBuilder setWithAcl(int aclCount) {
		this.aclCount = aclCount;
		return this;
	}

	@Override
	protected SecuredObject basicBuild() {
		final String name = Optional.ofNullable(this.name).orElse(SecuredObject.class.getSimpleName() + " name " + this.currentSuffix);
		final SecuredObjectType securedObjectType = Optional.ofNullable(this.securedObjectType).orElse(SecuredObjectType.MENU);
		final String securedObjectKey = Optional.ofNullable(this.securedObjectKey).orElse(SecuredObject.class.getSimpleName() + " securedObjectKey " + this.currentSuffix);
		final SecuredObject parent = this.withParent ? new SecuredObjectBuilder(this.entityManager, this).buildOne() : this.parent;
		final List<SecuredObject> children = this.childrenCount > 0 ? new ArrayList<SecuredObject>(new SecuredObjectBuilder(this.entityManager, this).build(this.childrenCount)) : this.children;
		final List<Acl> acl = this.aclCount > 0 ? new ArrayList<Acl>(new AclBuilder(this.entityManager, this).build(this.aclCount)) : this.acl;

		final SecuredObject securedObject = new SecuredObject();
		securedObject.setName(name);
		securedObject.setSecuredObjectType(securedObjectType);
		securedObject.setSecuredObjectKey(securedObjectKey);
		securedObject.setParent(parent);
		securedObject.setChildren(children);
		securedObject.setAcl(acl);

		return securedObject;
	}
}
