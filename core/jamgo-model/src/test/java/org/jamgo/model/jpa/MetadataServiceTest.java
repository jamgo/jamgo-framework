package org.jamgo.model.jpa;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.BinaryResourceImage;
import org.jamgo.model.entity.TestEntity;
import org.jamgo.model.entity.TestEntityWithBinaryResource;
import org.jamgo.model.jpa.MetadataJPAService.EntityAttributePair;
import org.jamgo.model.test.config.ModelTestConfig;
import org.jamgo.test.JamgoTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@ContextConfiguration(classes = { ModelTestConfig.class })
public class MetadataServiceTest extends JamgoTest {

	@Autowired
	private MetadataJPAService metadataService;

	@Test
	@Transactional
	public void test() {
		final List<EntityAttributePair> references = this.metadataService.getReferences(BinaryResource.class);
		assertNotNull(references);
		assertEquals(2, references.size());
		assertTrue(references.stream().anyMatch(ref -> ref.getEntityClass().equals(BinaryResourceImage.class)));
		assertTrue(references.stream().anyMatch(ref -> ref.getEntityClass().equals(TestEntityWithBinaryResource.class)));
		assertFalse(references.stream().anyMatch(ref -> ref.getEntityClass().equals(TestEntity.class)));
	}
}
