package org.jamgo.model.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class TestEntityWithBinaryResource extends Model {
	private static final long serialVersionUID = 1L;

	private String name;
	private Integer age;
	private String phone;
	@ManyToOne
	private BinaryResource photo;

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Integer getAge() {
		return this.age;
	}

	public void setAge(final Integer age) {
		this.age = age;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	public BinaryResource getPhoto() {
		return this.photo;
	}

	public void setPhoto(final BinaryResource photo) {
		this.photo = photo;
	}

}
