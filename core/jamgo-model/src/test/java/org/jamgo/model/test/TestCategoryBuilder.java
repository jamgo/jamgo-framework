package org.jamgo.model.test;

import javax.persistence.EntityManager;

import org.jamgo.model.entity.LocalizedBinaryResource;
import org.jamgo.model.entity.TestCategory;
import org.jamgo.model.test.entity.builder.CategoryBuilder;
import org.jamgo.model.test.entity.builder.ModelBuilder;

public class TestCategoryBuilder extends CategoryBuilder<TestCategory> {

	private LocalizedBinaryResource image = null;

	public TestCategoryBuilder(final EntityManager entityManager) {
		super(entityManager);
	}

	public TestCategoryBuilder(final EntityManager entityManager, final ModelBuilder<?> parentBuilder) {
		super(entityManager, parentBuilder);
	}

	public TestCategoryBuilder setImage(final LocalizedBinaryResource image) {
		this.image = image;
		return this;
	}

	@Override
	protected TestCategory basicBuild() {
		final TestCategory category = super.basicBuild();
		category.setImage(this.image);
		return category;
	}

}
