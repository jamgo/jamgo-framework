package org.jamgo.model.test;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;

import org.jamgo.model.entity.TestCategory;
import org.jamgo.model.entity.TestSimpleObject;
import org.jamgo.model.test.entity.builder.ModelBuilder;

public class TestSimpleObjectBuilder extends ModelBuilder<TestSimpleObject> {

	private String name;
	private Integer age;
	private Set<TestCategory> categories;
	private int categoriesCount;

	public TestSimpleObjectBuilder(final EntityManager entityManager) {
		super(entityManager);
	}

	public TestSimpleObjectBuilder(final EntityManager entityManager, final ModelBuilder<?> parentBuilder) {
		super(entityManager, parentBuilder);
	}

	public TestSimpleObjectBuilder setName(final String name) {
		this.name = name;
		return this;
	}

	public TestSimpleObjectBuilder setAge(final Integer age) {
		this.age = age;
		return this;
	}

	public void setCategories(final Set<TestCategory> categories) {
		this.categories = categories;
	}

	public TestSimpleObjectBuilder withCategories(final int categoriesCount) {
		this.categoriesCount = categoriesCount;
		return this;
	}

	@Override
	protected TestSimpleObject basicBuild() {
		final String name = Optional.ofNullable(this.name).orElse(String.format("name-%s", this.currentSuffix));
		final Integer age = Optional.ofNullable(this.age).orElse(this.currentSuffixIndex);
		final Set<TestCategory> categories = this.categoriesCount > 0 ? new LinkedHashSet<>(new TestCategoryBuilder(this.entityManager, this).build(this.categoriesCount)) : this.categories;

		final TestSimpleObject testSimpleObject = new TestSimpleObject();
		testSimpleObject.setName(name);
		testSimpleObject.setAge(age);
		testSimpleObject.setCategories(categories);
		return testSimpleObject;
	}

}
