package org.jamgo.model.test.entity.builder;

import java.util.Optional;

import javax.persistence.EntityManager;

import org.jamgo.model.entity.Category;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.TestCategory;
import org.springframework.core.ResolvableType;

public class CategoryBuilder<T extends Category> extends ModelBuilder<T> {

	private LocalizedString name = null;
	private LocalizedString description = null;

	public CategoryBuilder(final EntityManager entityManager) {
		super(entityManager);
	}

	public CategoryBuilder(final EntityManager entityManager, final ModelBuilder<?> parentBuilder) {
		super(entityManager, parentBuilder);
	}

	public CategoryBuilder<T> setName(final LocalizedString name) {
		this.name = name;
		return this;
	}

	public CategoryBuilder<T> setDescription(final LocalizedString description) {
		this.description = description;
		return this;
	}

	@Override
	protected T basicBuild() {
		final LocalizedString name = Optional.ofNullable(this.name).orElse(this.createLocalizedString(TestCategory.class.getSimpleName() + " name " + this.currentSuffix));
		final LocalizedString description = Optional.ofNullable(this.description).orElse(this.createLocalizedString(TestCategory.class.getSimpleName() + " description " + this.currentSuffix));

		final T category = this.createNewInstance();
		category.setName(name);
		category.setDescription(description);

		return category;
	}

	@SuppressWarnings("unchecked")
	private T createNewInstance() {
		final Class<?> type = ResolvableType.forClass(this.getClass()).getSuperType().getGeneric(0).resolve();
		try {
			return (T) type.getDeclaredConstructor().newInstance();
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

}
