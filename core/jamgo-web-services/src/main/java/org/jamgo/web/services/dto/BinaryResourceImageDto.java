package org.jamgo.web.services.dto;

public class BinaryResourceImageDto extends ModelDto {

	private String name;
	private String mimeType;
	private String fileName;
	private Integer fileLength;

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getMimeType() {
		return this.mimeType;
	}

	public void setMimeType(final String mimeType) {
		this.mimeType = mimeType;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(final String fileName) {
		this.fileName = fileName;
	}

	public Integer getFileLength() {
		return this.fileLength;
	}

	public void setFileLength(final Integer fileLength) {
		this.fileLength = fileLength;
	}

}
