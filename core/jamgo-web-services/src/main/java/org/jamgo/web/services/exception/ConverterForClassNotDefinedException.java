package org.jamgo.web.services.exception;

public class ConverterForClassNotDefinedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ConverterForClassNotDefinedException(final String message) {
		super(message);
	}
}
