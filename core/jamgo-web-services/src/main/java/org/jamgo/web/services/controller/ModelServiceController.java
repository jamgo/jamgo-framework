package org.jamgo.web.services.controller;

import java.util.List;
import java.util.Optional;

import org.jamgo.model.entity.Model;
import org.jamgo.model.util.OffsetSizePageRequest;
import org.jamgo.services.impl.ModelService;
import org.jamgo.services.impl.ServiceManager;
import org.jamgo.web.services.dto.ModelDto;
import org.jamgo.web.services.dto.OffsetSizePageRequestDto;
import org.jamgo.web.services.dto.PageDto;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.blazebit.persistence.PagedList;

public abstract class ModelServiceController<MODEL extends Model, DTO extends ModelDto> extends ServiceController implements InitializingBean {

	@Autowired
	protected ServiceManager serviceManager;

	protected Class<MODEL> modelClass;
	protected Class<DTO> modelDtoClass;

	@SuppressWarnings("unchecked")
	@Override
	public void afterPropertiesSet() throws Exception {
		this.modelClass = (Class<MODEL>) ResolvableType.forClass(this.getClass()).getSuperType().getGeneric(0).resolve();
		this.modelDtoClass = (Class<DTO>) ResolvableType.forClass(this.getClass()).getSuperType().getGeneric(1).resolve();
	}

	@GetMapping(value = "")
	@ResponseBody
	public ResponseEntity<PageDto<DTO>> getAll() {
		return new ResponseEntity<>(this.findAll(), HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	@ResponseBody
	public ResponseEntity<DTO> getOne(@PathVariable final Long id) {
		return new ResponseEntity<>(this.findOne(id), HttpStatus.OK);
	}

	public void create(@RequestBody final DTO modelDto) {

	}

	public void update(@RequestBody final DTO modelDto) {

	}

	public void delete(@RequestBody final DTO modelDto) {

	}

	protected PageDto<DTO> findAll() {
		return this.findAll((OffsetSizePageRequestDto) null);
	}

	protected PageDto<DTO> findAll(final Class<DTO> dtoClass) {
		return this.findAll((OffsetSizePageRequestDto) null, dtoClass);
	}

	protected PageDto<DTO> findAll(final OffsetSizePageRequestDto pageRequestDto) {
		return this.findAll(pageRequestDto, this.modelDtoClass);
	}

	protected PageDto<DTO> findAll(final OffsetSizePageRequestDto pageRequestDto, final Class<DTO> dtoClass) {
		final OffsetSizePageRequest pageRequest = Optional.ofNullable(pageRequestDto).map(dto -> dto.convert()).orElse(null);
		return this.getConverter(this.modelClass, dtoClass).convertToDto(this.getModelService(this.modelClass).findAll(pageRequest), dtoClass);
	}

	protected DTO findOne(final Long id) {
		return this.findOne(id, this.modelDtoClass);
	}

	protected DTO findOne(final Long id, final Class<DTO> dtoClass) {
		return this.getConverter(this.modelClass, dtoClass).convertToDto(this.getModelService(this.modelClass).findById(id), dtoClass);
	}

	private ModelService<MODEL> getModelService(final Class<MODEL> modelClass) {
		return (ModelService<MODEL>) this.serviceManager.getService(modelClass);
	}

	public PageDto<DTO> convertToDto(final PagedList<MODEL> page, final Class<DTO> dtoClass) {
		return this.convertToDto(page, this.modelClass, dtoClass);
	}

	public List<DTO> convertToDto(final List<MODEL> elements, final Class<DTO> dtoClass) {
		return this.convertToDto(elements, this.modelClass, dtoClass);
	}

	public DTO convertToDto(final MODEL object, final Class<DTO> dtoClass) {
		return this.convertToDto(object, this.modelClass, dtoClass);
	}

}
