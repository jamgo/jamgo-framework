package org.jamgo.web.services.dto;

public abstract class CategoryDto extends ModelDto {

	private String name;
	private String description;

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

}
