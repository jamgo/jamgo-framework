package org.jamgo.web.services.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.BasicModelEntity;
import org.jamgo.services.impl.BasicModelEntityService;
import org.jamgo.services.impl.ServiceManager;
import org.jamgo.services.session.SessionContext;
import org.jamgo.web.services.converter.BasicModelConverter;
import org.jamgo.web.services.converter.ConverterManager;
import org.jamgo.web.services.dto.BasicModelDto;
import org.jamgo.web.services.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;

import com.blazebit.persistence.PagedList;

public abstract class ServiceController {

	public static final DateFormat DATE_FORMATTER = new SimpleDateFormat("yyyyMMddHHmmss");

	@Autowired
	protected SessionContext sessionContext;

	@Autowired
	private ServiceManager serviceManager;

	@Autowired
	protected ConverterManager converterManager;

	protected <SOURCE extends BasicModelEntity<ID_TYPE>, ID_TYPE> BasicModelEntityService<SOURCE, ID_TYPE> getService(final Class<SOURCE> sourceClass) {
		return this.serviceManager.getService(sourceClass);
	}

//	public <MODEL extends Model, DTO extends ModelDto> PageDto<DTO> convertToDto(final PagedList<MODEL> page, final Class<MODEL> modelClass, final Class<DTO> dtoClass) {
//		return this.getConverter(modelClass, dtoClass).convertToDto(page, dtoClass);
//	}
//
//	public <MODEL extends Model, DTO extends ModelDto> List<DTO> convertToDto(final List<MODEL> elements, final Class<MODEL> modelClass, final Class<DTO> dtoClass) {
//		return this.getConverter(modelClass, dtoClass).convertToDto(elements, dtoClass);
//	}
//
//	public <MODEL extends Model, DTO extends ModelDto> DTO convertToDto(final MODEL object, final Class<MODEL> modelClass, final Class<DTO> dtoClass) {
//		return this.getConverter(modelClass, dtoClass).convertToDto(object, dtoClass);
//	}

	public <SOURCE extends BasicModel<ID_TYPE>, ID_TYPE, DTO extends BasicModelDto<ID_TYPE>> PageDto<DTO> convertToDto(final PagedList<SOURCE> page, final Class<SOURCE> modelClass, final Class<DTO> dtoClass) {
		return this.getConverter(modelClass, dtoClass).convertToDto(page, dtoClass);
	}

	public <SOURCE extends BasicModel<ID_TYPE>, ID_TYPE, DTO extends BasicModelDto<ID_TYPE>> List<DTO> convertToDto(final List<SOURCE> elements, final Class<SOURCE> modelClass, final Class<DTO> dtoClass) {
		return this.getConverter(modelClass, dtoClass).convertToDto(elements, dtoClass);
	}

	public <SOURCE extends BasicModel<ID_TYPE>, ID_TYPE, DTO extends BasicModelDto<ID_TYPE>> DTO convertToDto(final SOURCE object, final Class<SOURCE> modelClass, final Class<DTO> dtoClass) {
		return this.getConverter(modelClass, dtoClass).convertToDto(object, dtoClass);
	}

	@SuppressWarnings("unchecked")
	protected <SOURCE extends BasicModel<ID_TYPE>, ID_TYPE, DTO extends BasicModelDto<ID_TYPE>> BasicModelConverter<SOURCE, ID_TYPE, DTO> getConverter(final Class<SOURCE> modelClass, final Class<DTO> dtoClass) {
		return (BasicModelConverter<SOURCE, ID_TYPE, DTO>) this.converterManager.getConverter(modelClass, dtoClass);
	}

	protected <SOURCE extends BasicModel<ID_TYPE>, ID_TYPE, DTO extends BasicModelDto<ID_TYPE>> SOURCE convertToEntity(final DTO dto) {
		throw new RuntimeException("This method must be implemented in subclasses that use it.");
	}

}
