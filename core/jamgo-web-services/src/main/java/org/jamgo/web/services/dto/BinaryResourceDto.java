package org.jamgo.web.services.dto;

import java.util.List;

public class BinaryResourceDto extends ModelDto {

	private String timeStamp;
	private String description;
	private String mimeType;
	private String fileName;
	private Integer fileLength;
	private byte[] contents;
	private List<BinaryResourceImageDto> images;

	public String getTimeStamp() {
		return this.timeStamp;
	}

	public void setTimeStamp(final String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getMimeType() {
		return this.mimeType;
	}

	public void setMimeType(final String mimeType) {
		this.mimeType = mimeType;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(final String fileName) {
		this.fileName = fileName;
	}

	public Integer getFileLength() {
		return this.fileLength;
	}

	public void setFileLength(final Integer fileLength) {
		this.fileLength = fileLength;
	}

	public byte[] getContents() {
		return this.contents;
	}

	public void setContents(final byte[] contents) {
		this.contents = contents;
	}

	public List<BinaryResourceImageDto> getImages() {
		return this.images;
	}

	public void setImages(final List<BinaryResourceImageDto> images) {
		this.images = images;
	}

}
