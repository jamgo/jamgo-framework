package org.jamgo.web.services.dto;

import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "response", "status", "message" })
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class ResponseDto<T> {

	public static final DateFormat DATE_FORMATTER = new SimpleDateFormat("yyyyMMddHHmmss");

	@JsonProperty("status")
	@JsonView({ Object.class })
	private int status;

	@JsonProperty("message")
	@JsonView({ Object.class })
	private String message;

	@JsonProperty("response")
	@JsonView({ Object.class })
	private T response;

	public ResponseDto() {
		this.status = HttpURLConnection.HTTP_OK;
	}

	public ResponseDto(T response) {
		this();
		this.response = response;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getResponse() {
		return this.response;
	}

	public void setResponse(T response) {
		this.response = response;
	}

}
