package org.jamgo.web.services.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jamgo.web.services.dto.BinaryResourceDto;
import org.jamgo.web.services.dto.PageDto;
import org.jamgo.web.services.exception.NotFoundException;
import org.springframework.http.ResponseEntity;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "BinaryResourceController", description = "Methods to retrieve binary resources and its metadata")
public interface BinaryResourceControllerInterface {

	@Operation(summary = "Gets all the binary resources defined in the application.")
	ResponseEntity<PageDto<BinaryResourceDto>> getAll();

	@Operation(summary = "Gets the binary resource metadata.")
	ResponseEntity<BinaryResourceDto> getOne(
		@Parameter(description = "ID of the binary resource") final Long id);

	@Operation(summary = "Downloads the binary resource content.")
	void getResource(
		final HttpServletRequest request,
		final HttpServletResponse response,
		@Parameter(description = "ID of the binary resource") final Long id,
		@Parameter(description = "Image size to download", required = false) final String imageSize)
		throws NotFoundException,
		IOException;

}