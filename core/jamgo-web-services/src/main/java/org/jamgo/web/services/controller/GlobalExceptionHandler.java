package org.jamgo.web.services.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jamgo.web.services.dto.ResponseDto;
import org.jamgo.web.services.exception.ForbiddenException;
import org.jamgo.web.services.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(ServiceController.class);

	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ResponseDto<String> handleNotFoundException(final NotFoundException e, final HttpServletResponse response) {
		final ResponseDto<String> responseDto = new ResponseDto<>();
		responseDto.setStatus(HttpStatus.NOT_FOUND.value());
		responseDto.setMessage(ExceptionUtils.getMessage(e));
		return responseDto;
	}

	@ExceptionHandler(ForbiddenException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ResponseDto<String> handleForbiddenException(final ForbiddenException e, final HttpServletResponse response) {
		final ResponseDto<String> responseDto = new ResponseDto<>();
		responseDto.setStatus(HttpStatus.FORBIDDEN.value());
		responseDto.setMessage(e.getMessage());
		return responseDto;
	}

	@ExceptionHandler({ Exception.class, RuntimeException.class })
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ResponseDto<String> handleNotFoundException(final Exception e, final HttpServletResponse response) {
		GlobalExceptionHandler.logger.error(e.getMessage(), e);
		final ResponseDto<String> responseDto = new ResponseDto<>();
		responseDto.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		responseDto.setMessage(ExceptionUtils.getMessage(e));
		return responseDto;
	}

}
