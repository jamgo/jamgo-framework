package org.jamgo.web.services.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.compress.utils.IOUtils;
import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.Downloadable;
import org.jamgo.services.impl.BinaryResourceImageService;
import org.jamgo.services.impl.BinaryResourceService;
import org.jamgo.services.impl.DownloadService;
import org.jamgo.web.services.converter.BinaryResourceConverter;
import org.jamgo.web.services.dto.BinaryResourceDto;
import org.jamgo.web.services.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/binaryresource")
public class BinaryResourceController extends ModelServiceController<BinaryResource, BinaryResourceDto> implements BinaryResourceControllerInterface {

	private static final Logger logger = LoggerFactory.getLogger(BinaryResourceController.class);

	@Autowired
	private BinaryResourceService binaryResourceService;
	@Autowired
	private BinaryResourceImageService binaryResourceImageService;
	@Autowired
	private DownloadService downloadService;
	@Autowired
	private BinaryResourceConverter converter;

	@Override
	@GetMapping(value = "/{id}/download")
	@ResponseBody
	public void getResource(final HttpServletRequest request, final HttpServletResponse response, @PathVariable final Long id, @RequestParam(required = false) final String imageSize) throws NotFoundException, IOException {
		final Downloadable downloadable;
		if (imageSize == null) {
			// Download original binary resource
			downloadable = Optional.ofNullable(this.binaryResourceService.findById(id)).orElseThrow(() -> new NotFoundException("BinaryResource not found"));
			BinaryResourceController.logger.debug("BinaryResource found for id {}", id);
		} else {
			// Download binary resource image with that name
			downloadable = Optional.ofNullable(this.binaryResourceImageService.findByName(id, imageSize)).orElseThrow(() -> new NotFoundException("BinaryResourceImage not found"));
			BinaryResourceController.logger.debug("BinaryResourceImage found for id {} and name {}", id, imageSize);
		}

		response.setContentType(this.downloadService.getContentMimeType(downloadable));
		response.setContentLength(this.downloadService.getContentLength(downloadable));
		final InputStream contentsStream = new ByteArrayInputStream(downloadable.getByteContents());
		BinaryResourceController.logger.debug("Starting stream copy...");
		IOUtils.copy(contentsStream, response.getOutputStream());
		BinaryResourceController.logger.debug("Stream copied successfully");
		response.getOutputStream().close();
		BinaryResourceController.logger.debug("Response outputStream closed");
	}

	public BinaryResource createBinaryResource(final BinaryResourceDto dto) {
		final BinaryResource binaryResource = this.converter.convertFromDto(dto);
		return this.binaryResourceService.createBinaryResource(binaryResource);
	}

}
