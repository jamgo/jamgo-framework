package org.jamgo.web.services.converter;

import org.jamgo.model.entity.Model;
import org.jamgo.web.services.dto.ModelDto;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DefaultModelConverter<MODEL extends Model, DTO extends ModelDto> extends ModelConverter<MODEL, DTO> {

	@Override
	public void afterPropertiesSet() throws Exception {
		// Do Nothing.
	}

	public DefaultModelConverter(final Class<MODEL> modelClass, final Class<DTO> dtoClass) {
		super();
		this.sourceClass = modelClass;
		this.dtoClass = dtoClass;
	}

}
