package org.jamgo.web.services.converter;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.Model;
import org.jamgo.web.services.dto.BasicModelDto;
import org.jamgo.web.services.exception.ConverterForClassNotDefinedException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ConverterManager implements InitializingBean {

	@Autowired
	private ApplicationContext applicationContext;
	private Map<ConverterKey, BasicModelConverter<? extends BasicModel<?>, ?, ?>> converters;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		this.converters = new HashMap<>();
		final Map<String, Object> converterBeans = this.applicationContext.getBeansWithAnnotation(Converter.class);
		converterBeans.forEach((eachKey, eachValue) -> {
			if (eachValue instanceof BasicModelConverter) {
				final BasicModelConverter<?, ?, ?> converter = (BasicModelConverter<?, ?, ?>) eachValue;
				final ConverterKey key = new ConverterKey(converter.getSourceClass(), converter.getDtoClass());
				this.converters.put(key, converter);
			}
		});
	}

	@SuppressWarnings("unchecked")
	public <SOURCE extends BasicModel<ID_TYPE>, ID_TYPE, DTO extends BasicModelDto<ID_TYPE>> BasicModelConverter<? extends BasicModel<?>, ?, ?> getConverter(final Class<SOURCE> sourceClass, final Class<DTO> dtoClass) {
		final ConverterKey key = new ConverterKey(sourceClass, dtoClass);
		BasicModelConverter<? extends BasicModel<?>, ?, ?> converter = this.converters.get(key);
		if (converter == null && Model.class.isAssignableFrom(sourceClass)) {
			converter = this.applicationContext.getBean(DefaultModelConverter.class, sourceClass, dtoClass);
			this.converters.put(key, converter);
		}
		if (converter == null && BasicModel.class.isAssignableFrom(sourceClass)) {
			converter = this.applicationContext.getBean(DefaultBasicModelConverter.class, sourceClass, dtoClass);
			this.converters.put(key, converter);
		}
		if (converter == null) {
			throw new ConverterForClassNotDefinedException(String.format("%s - %s", sourceClass.getName(), dtoClass.getName()));
		}
		return converter;
	}

	private static final class ConverterKey {
		private final Class<? extends BasicModel<?>> modelClass;
		private final Class<? extends BasicModelDto<?>> dtoClass;

		public ConverterKey(final Class<? extends BasicModel<?>> modelClass, final Class<? extends BasicModelDto<?>> dtoClass) {
			this.modelClass = modelClass;
			this.dtoClass = dtoClass;
		}

		@Override
		public int hashCode() {
			return Objects.hash(this.modelClass, this.dtoClass);
		}

		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (this.getClass() != obj.getClass()) {
				return false;
			}
			final ConverterKey key = (ConverterKey) obj;
			return Objects.equals(this.modelClass, key.modelClass) && Objects.equals(this.dtoClass, key.dtoClass);
		}
	}

}
