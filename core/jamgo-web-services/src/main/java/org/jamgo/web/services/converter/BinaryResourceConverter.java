package org.jamgo.web.services.converter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.web.services.dto.BinaryResourceDto;

@Converter
public class BinaryResourceConverter extends ModelConverter<BinaryResource, BinaryResourceDto> {

	public BinaryResource convertFromDto(final BinaryResourceDto dto) {
		final BinaryResource binaryResource = new BinaryResource();
		binaryResource.setId(dto.getId());
		binaryResource.setVersion(dto.getVersion());
		binaryResource.setTimeStamp(LocalDateTime.parse(dto.getTimeStamp(), DateTimeFormatter.ISO_DATE_TIME));
		binaryResource.setDescription(dto.getDescription());
		binaryResource.setMimeType(dto.getMimeType());
		binaryResource.setFileName(dto.getFileName());
		binaryResource.setFileLength(dto.getFileLength());
		binaryResource.setByteContents(dto.getContents());
		return binaryResource;
	}

}
