package org.jamgo.web.services.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.jamgo.model.BasicModel;
import org.jamgo.web.services.dto.BasicModelDto;
import org.jamgo.web.services.dto.PageDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;

import com.blazebit.persistence.PagedList;

public abstract class BasicModelConverter<SOURCE extends BasicModel<ID_TYPE>, ID_TYPE, DTO extends BasicModelDto<ID_TYPE>> implements InitializingBean {

	protected Class<SOURCE> sourceClass;
	protected Class<DTO> dtoClass;

	@Autowired
	protected ModelMapper modelMapper;

	@SuppressWarnings("unchecked")
	@Override
	public void afterPropertiesSet() throws Exception {
		this.sourceClass = (Class<SOURCE>) ResolvableType.forClass(this.getClass()).getSuperType().getGeneric(0).resolve();
		this.dtoClass = (Class<DTO>) ResolvableType.forClass(this.getClass()).getSuperType().getGeneric(1).resolve();
	}

	public Class<SOURCE> getSourceClass() {
		return this.sourceClass;
	}

	public Class<DTO> getDtoClass() {
		return this.dtoClass;
	}

	public PageDto<DTO> convertToDto(final PagedList<SOURCE> page) {
		return this.convertToDto(page, this.getDtoClass());
	}

	public PageDto<DTO> convertToDto(final PagedList<SOURCE> page, final Class<DTO> dtoClass) {
		final PageDto<DTO> pageDto = new PageDto<>();
		pageDto.setTotalElements(page.getTotalSize());
		pageDto.setLastPage(page.getPage() == page.getTotalPages());
		pageDto.setContent(this.convertToDto((List<SOURCE>) page, dtoClass));
		return pageDto;
	}

	public List<DTO> convertToDto(final List<SOURCE> elements) {
		return this.convertToDto(elements, this.getDtoClass());
	}

	public List<DTO> convertToDto(final List<SOURCE> elements, final Class<DTO> dtoClass) {
		return elements.stream().map(each -> this.convertToDto(each, dtoClass)).collect(Collectors.toList());
	}

	public DTO convertToDto(final SOURCE object) {
		return this.convertToDto(object, this.getDtoClass());
	}

	public DTO convertToDto(final SOURCE object, final Class<DTO> dtoClass) {
		return this.modelMapper.map(object, dtoClass);
	}

}
