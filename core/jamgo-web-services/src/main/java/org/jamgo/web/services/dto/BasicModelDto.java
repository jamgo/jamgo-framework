package org.jamgo.web.services.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

public abstract class BasicModelDto<ID_TYPE> {

	@JsonProperty
	@JsonView(Object.class)
	private ID_TYPE id;
	@JsonProperty
	@JsonView(Object.class)
	private Long version;

	public ID_TYPE getId() {
		return this.id;
	}

	public void setId(final ID_TYPE id) {
		this.id = id;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(final Long version) {
		this.version = version;
	}

}
