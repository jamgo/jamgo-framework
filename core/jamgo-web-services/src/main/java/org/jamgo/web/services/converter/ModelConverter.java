package org.jamgo.web.services.converter;

import org.jamgo.model.entity.Model;
import org.jamgo.web.services.dto.ModelDto;

public abstract class ModelConverter<MODEL extends Model, DTO extends ModelDto> extends BasicModelConverter<MODEL, Long, DTO> {

	public ModelConverter() {
		super();
	}

}
