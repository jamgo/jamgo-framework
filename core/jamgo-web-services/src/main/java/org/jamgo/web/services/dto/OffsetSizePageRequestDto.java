package org.jamgo.web.services.dto;

import java.util.ArrayList;
import java.util.List;

import org.jamgo.model.util.OffsetSizePageRequest;
import org.jamgo.model.util.Order;

public class OffsetSizePageRequestDto {

	private long offset;
	private int pageSize;
	private List<OrderDto> pageOrders;

	public long getOffset() {
		return this.offset;
	}

	public void setOffset(final long offset) {
		this.offset = offset;
	}

	public int getPageSize() {
		return this.pageSize;
	}

	public void setPageSize(final int pageSize) {
		this.pageSize = pageSize;
	}

	public List<OrderDto> getPageOrders() {
		return this.pageOrders;
	}

	public void setPageOrders(final List<OrderDto> pageOrders) {
		this.pageOrders = pageOrders;
	}

	public OffsetSizePageRequest convert() {
		final List<Order> orders = new ArrayList<>();
		if (this.pageOrders != null) {
			this.pageOrders.forEach(dto -> orders.add(dto.convert()));
		}
		return new OffsetSizePageRequest(this.offset, this.pageSize, orders);
	}
}
