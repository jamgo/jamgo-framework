package org.jamgo.web.services.dto;

import org.jamgo.model.util.Order;

public class OrderDto {

	private String expression;
	private boolean ascending;
	private boolean nullsFirst;

	public String getExpression() {
		return this.expression;
	}

	public void setExpression(final String expression) {
		this.expression = expression;
	}

	public boolean isAscending() {
		return this.ascending;
	}

	public void setAscending(final boolean ascending) {
		this.ascending = ascending;
	}

	public boolean isNullsFirst() {
		return this.nullsFirst;
	}

	public void setNullsFirst(final boolean nullsFirst) {
		this.nullsFirst = nullsFirst;
	}

	public Order convert() {
		return new Order(this.expression, this.ascending, this.nullsFirst);
	}

}
