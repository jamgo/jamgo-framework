package org.jamgo.web.services.converter;

import org.jamgo.model.BasicModel;
import org.jamgo.web.services.dto.BasicModelDto;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DefaultBasicModelConverter<MODEL extends BasicModel<ID_TYPE>, ID_TYPE, DTO extends BasicModelDto<ID_TYPE>> extends BasicModelConverter<MODEL, ID_TYPE, DTO> {

	@Override
	public void afterPropertiesSet() throws Exception {
		// Do Nothing.
	}

	public DefaultBasicModelConverter(final Class<MODEL> sourceClass, final Class<DTO> dtoClass) {
		super();
		this.sourceClass = sourceClass;
		this.dtoClass = dtoClass;
	}

}
