package org.jamgo.app.support;

import org.jamgo.app.config.WebServicesConfig;
import org.jamgo.services.session.SessionContext;
import org.jamgo.services.session.SessionContextImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Import({ WebServicesConfig.class })
@EnableWebMvc
public abstract class WebApiServletInitializer extends JamgoServletInitializer {

	@Bean
	@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
	public SessionContext sessionContext() {
		return new SessionContextImpl();
	}

	@Bean
	public RequestInterceptor requestInterceptor() {
		return new RequestInterceptor();
	}

}
