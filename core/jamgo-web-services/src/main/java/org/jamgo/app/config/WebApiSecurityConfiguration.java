package org.jamgo.app.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

public abstract class WebApiSecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static final String LOGIN_PROCESSING_URL = "/login";
	private static final String LOGIN_FAILURE_URL = "/login";
	private static final String LOGIN_URL = "/login";
	private static final String LOGOUT_SUCCESS_URL = "/login";

	@Autowired
	protected DataSource dataSource;

	// @formatter:off

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http
			.csrf().disable();
//			.cors();

		this.configurePermittedRequests(http);

		http
			// ...	Authorize vaadin intelnal requests.
			.authorizeRequests()
				.anyRequest().authenticated()
				.and()

			.formLogin()
				.loginPage(WebApiSecurityConfiguration.LOGIN_URL).permitAll()
				.loginProcessingUrl(WebApiSecurityConfiguration.LOGIN_PROCESSING_URL)
				.failureUrl(WebApiSecurityConfiguration.LOGIN_FAILURE_URL)
				.and()

			.logout()
				.logoutSuccessUrl(WebApiSecurityConfiguration.LOGOUT_SUCCESS_URL)
				.and()

			.headers()
				.frameOptions().disable()
				.and()

			.httpBasic();
	}

	// @formatter:on

	protected void configurePermittedRequests(final HttpSecurity http) throws Exception {
		// ... Do nothing by default.
	}

	@Override
	public void configure(final WebSecurity web) throws Exception {
		web.ignoring().antMatchers(
			// (development mode) H2 debugging console
			"/h2-console/**");
	}

}
