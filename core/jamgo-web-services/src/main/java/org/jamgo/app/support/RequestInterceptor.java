package org.jamgo.app.support;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jamgo.services.session.SessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

public class RequestInterceptor implements HandlerInterceptor {

	private static final Logger logger = LoggerFactory.getLogger(RequestInterceptor.class);

	@Autowired
	private SessionContext sessionContext;

	@Override
	public boolean preHandle(
		final HttpServletRequest request,
		final HttpServletResponse response,
		final Object handler)
		throws Exception {

		final Locale requestLocale = request.getLocale();
		RequestInterceptor.logger.debug("Locale from request: " + requestLocale);
		this.sessionContext.setCurrentLocale(requestLocale);

		return true;
	}
}
