package org.jamgo.app.config;

import org.jamgo.services.config.ServicesConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ ServicesConfig.class })
@ComponentScan(
	basePackageClasses = {
		org.jamgo.web.services.PackageMarker.class
	})
public class WebServicesConfig {

}
