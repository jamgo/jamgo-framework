package org.jamgo.app.config;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.LocalizedModel;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;
import org.jamgo.model.valueobjects.Email;
import org.jamgo.model.valueobjects.IBAN;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.web.services.dto.BinaryResourceDto;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

/**
 * Model mapper configuration for web-api application.
 * You must create a subclass in your project to enable this configuration,
 * and add extra configuration specific for your project.
 */
public abstract class WebApiModelMapperConfiguration {

	@Autowired
	private LocalizedObjectService localizedObjectService;

	@Bean
	public ModelMapper modelMapper() {
		final ModelMapper modelMapper = new ModelMapper();
		this.configureModelMapper(modelMapper);
		return modelMapper;
	}

	/**
	 * Add configuration to model mapper (converters, typeMaps, mappings,...)
	 * Override this method in subclass to add extra configuration specific for your project.
	 * @param modelMapper
	 */
	protected void configureModelMapper(final ModelMapper modelMapper) {
		modelMapper.addConverter(this.getLocalizedStringConverter());
		modelMapper.addConverter(this.getLocalizedModelConverter());
		modelMapper.addConverter(this.getLocalDateConverter());
		modelMapper.addConverter(this.getLocalDateTimeConverter());
		modelMapper.addConverter(this.getBinaryResourceConverter());
		modelMapper.addConverter(this.getEnumConverter());
		modelMapper.addConverter(this.getEmailConverter());
		modelMapper.addConverter(this.getIBANConverter());

		// BinaryResource -> BinaryResourceDto: skip field "setContents"
		modelMapper.createTypeMap(BinaryResource.class, BinaryResourceDto.class)
			.addMappings(mapping -> mapping.skip(BinaryResourceDto::setContents));
	}

	private Converter<LocalizedString, String> getLocalizedStringConverter() {
		return new Converter<>() {
			@Override
			public String convert(final MappingContext<LocalizedString, String> context) {
				return Optional.ofNullable(context.getSource()).map(localizedObject -> WebApiModelMapperConfiguration.this.localizedObjectService.getValue(localizedObject)).orElse(null);
			}
		};
	}

	private <MODEL extends Model> Converter<LocalizedModel<MODEL>, MODEL> getLocalizedModelConverter() {
		return new Converter<>() {
			@Override
			public MODEL convert(final MappingContext<LocalizedModel<MODEL>, MODEL> context) {
				return Optional.ofNullable(context.getSource()).map(localizedObject -> WebApiModelMapperConfiguration.this.localizedObjectService.getValue(localizedObject)).orElse(null);
			}
		};
	}

	private Converter<LocalDate, String> getLocalDateConverter() {
		return new Converter<>() {
			@Override
			public String convert(final MappingContext<LocalDate, String> context) {
				return Optional.ofNullable(context.getSource()).map(localDate -> DateTimeFormatter.ISO_DATE.format(localDate)).orElse(null);
			}
		};
	}

	private Converter<LocalDateTime, String> getLocalDateTimeConverter() {
		return new Converter<>() {
			@Override
			public String convert(final MappingContext<LocalDateTime, String> context) {
				return Optional.ofNullable(context.getSource()).map(localDateTime -> DateTimeFormatter.ISO_DATE_TIME.format(localDateTime)).orElse(null);
			}
		};
	}

	private Converter<BinaryResource, Long> getBinaryResourceConverter() {
		return new Converter<>() {
			@Override
			public Long convert(final MappingContext<BinaryResource, Long> context) {
				return Optional.ofNullable(context.getSource()).map(binaryResource -> binaryResource.getId()).orElse(null);
			}
		};
	}

	private Converter<Enum<?>, String> getEnumConverter() {
		return new Converter<>() {
			@Override
			public String convert(final MappingContext<Enum<?>, String> context) {
				return Optional.ofNullable(context.getSource()).map(enumeration -> enumeration.name()).orElse(null);
			}
		};
	}

	// FIXME: I've tried but I couldn't create a single converter for all ValueObjects
	// It seems there is a problem when you define a ModelMapper using an interface instead of a concreteClass...
	// Solution: create one converter for each implementation of ValueObject
	private Converter<Email, String> getEmailConverter() {
		return new Converter<>() {

			@Override
			public String convert(final MappingContext<Email, String> context) {
				return Optional.ofNullable(context.getSource()).map(email -> email.getValue()).orElse(null);
			}
		};
	}

	private Converter<IBAN, String> getIBANConverter() {
		return new Converter<>() {

			@Override
			public String convert(final MappingContext<IBAN, String> context) {
				return Optional.ofNullable(context.getSource()).map(iban -> iban.getValue()).orElse(null);
			}
		};
	}

}
