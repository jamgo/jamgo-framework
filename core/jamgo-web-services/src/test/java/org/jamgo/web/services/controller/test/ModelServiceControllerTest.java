package org.jamgo.web.services.controller.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.jamgo.model.test.config.ModelTestConfig;
import org.jamgo.model.test.entity.builder.TestEntityBuilder;
import org.jamgo.services.config.ServicesTestConfig;
import org.jamgo.test.JamgoControllerTest;
import org.jamgo.web.services.config.WebApiModelMapperTestConfiguration;
import org.jamgo.web.services.config.WebServicesTestConfig;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@AutoConfigureWebMvc
@AutoConfigureMockMvc
@ContextConfiguration(classes = { ModelTestConfig.class, ServicesTestConfig.class, WebServicesTestConfig.class, WebApiModelMapperTestConfiguration.class })
public class ModelServiceControllerTest extends JamgoControllerTest {

	@Test
	@Transactional
	public void testEntityController_get() throws Exception {
		this.entityManager.joinTransaction();
		new TestEntityBuilder(this.entityManager).buildOne();
		this.commitAndStart();

		this.mockMvc.perform(get("/test/1").accept("application/json"))
			.andDo(print()).andExpect(status().isOk());
	}

}
