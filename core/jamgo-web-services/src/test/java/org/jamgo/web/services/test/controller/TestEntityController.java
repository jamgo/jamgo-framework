package org.jamgo.web.services.test.controller;

import org.jamgo.model.entity.TestEntity;
import org.jamgo.web.services.controller.ModelServiceController;
import org.jamgo.web.services.test.model.TestEntityDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/test")
@ResponseBody
public class TestEntityController extends ModelServiceController<TestEntity, TestEntityDto> {

}
