package org.jamgo.web.services.config;

import org.jamgo.app.config.WebApiModelMapperConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebApiModelMapperTestConfiguration extends WebApiModelMapperConfiguration {

}
