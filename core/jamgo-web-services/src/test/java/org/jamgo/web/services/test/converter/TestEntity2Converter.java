package org.jamgo.web.services.test.converter;

import org.jamgo.model.entity.TestEntity2;
import org.jamgo.web.services.converter.Converter;
import org.jamgo.web.services.converter.ModelConverter;
import org.jamgo.web.services.test.model.TestEntity2Dto;

@Converter
public class TestEntity2Converter extends ModelConverter<TestEntity2, TestEntity2Dto> {

	@Override
	public TestEntity2Dto convertToDto(final TestEntity2 object) {
		final TestEntity2Dto dto = super.convertToDto(object);
		dto.setMobilePhone(object.getPhone());
		return dto;
	}
}
