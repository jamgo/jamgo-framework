package org.jamgo.web.services.config;

import org.jamgo.app.support.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(
	basePackageClasses = {
		org.jamgo.web.services.PackageMarker.class
	},
	excludeFilters = {
		@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = WebApiModelMapperTestConfiguration.class)
	})
public class WebServicesTestConfig {

	@Bean
	public RequestInterceptor requestInterceptor() {
		return new RequestInterceptor();
	}

}
