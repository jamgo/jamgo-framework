package org.jamgo.web.services.converter.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.TestEntity;
import org.jamgo.model.entity.TestEntity2;
import org.jamgo.model.repository.BasicModelEntityRepository;
import org.jamgo.model.repository.RepositoryManager;
import org.jamgo.model.test.config.ModelTestConfig;
import org.jamgo.model.test.entity.builder.TestEntityBuilder;
import org.jamgo.model.view.TestEntityView;
import org.jamgo.services.config.ServicesTestConfig;
import org.jamgo.test.JamgoRepositoryTest;
import org.jamgo.web.services.config.WebApiModelMapperTestConfiguration;
import org.jamgo.web.services.config.WebServicesTestConfig;
import org.jamgo.web.services.converter.BasicModelConverter;
import org.jamgo.web.services.converter.ConverterManager;
import org.jamgo.web.services.converter.DefaultBasicModelConverter;
import org.jamgo.web.services.converter.DefaultModelConverter;
import org.jamgo.web.services.converter.ModelConverter;
import org.jamgo.web.services.test.model.TestEntity2Dto;
import org.jamgo.web.services.test.model.TestEntityDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@ContextConfiguration(classes = { ModelTestConfig.class, ServicesTestConfig.class, WebServicesTestConfig.class, WebApiModelMapperTestConfiguration.class })
public class ConverterManagerTest extends JamgoRepositoryTest {

	@Autowired
	private ConverterManager converterManager;
	@Autowired
	private RepositoryManager repositoryManager;

	@Test
	public void getConverterTest_entityWithoutConverter() {
		final BasicModelConverter<? extends BasicModel<?>, ?, ?> converter = this.converterManager.getConverter(TestEntity.class, TestEntityDto.class);
		assertNotNull(converter);
		assertTrue(converter instanceof DefaultModelConverter);
		final ModelConverter<TestEntity, TestEntityDto> modelConverter = (ModelConverter<TestEntity, TestEntityDto>) converter;
		assertNotNull(modelConverter);

		final TestEntity entity = new TestEntity();
		entity.setId(1L);
		entity.setName("name-1");
		entity.setAge(43);
		entity.setPhone("666777888");
		final TestEntityDto dto = modelConverter.convertToDto(entity);
		assertNotNull(dto);
		assertEquals(1L, dto.getId());
		assertEquals("name-1", dto.getName());
		assertEquals(43, dto.getAge());
		assertNull(dto.getMobilePhone());
	}

	@Test
	public void getConverterTest_entityWithConverter() {
		final BasicModelConverter<? extends BasicModel<?>, ?, ?> converter = this.converterManager.getConverter(TestEntity2.class, TestEntity2Dto.class);
		assertNotNull(converter);
		assertFalse(converter instanceof DefaultModelConverter);
		final ModelConverter<TestEntity2, TestEntity2Dto> modelConverter = (ModelConverter<TestEntity2, TestEntity2Dto>) converter;
		assertNotNull(modelConverter);

		final TestEntity2 entity = new TestEntity2();
		entity.setId(1L);
		entity.setName("name-1");
		entity.setAge(43);
		entity.setPhone("666777888");
		final TestEntity2Dto dto = modelConverter.convertToDto(entity);
		assertNotNull(dto);
		assertEquals(1L, dto.getId());
		assertEquals("name-1", dto.getName());
		assertEquals(43, dto.getAge());
		assertEquals("666777888", dto.getMobilePhone());
	}

	@Test
	@Transactional
	public void getConverterTest_viewWithoutConverter() {
		final BasicModelConverter<? extends BasicModel<?>, ?, ?> converter = this.converterManager.getConverter(TestEntityView.class, TestEntityDto.class);
		assertNotNull(converter);
		assertTrue(converter instanceof DefaultBasicModelConverter);
		final BasicModelConverter<TestEntityView, Long, TestEntityDto> modelConverter = (BasicModelConverter<TestEntityView, Long, TestEntityDto>) converter;
		assertNotNull(modelConverter);

		this.entityManager.joinTransaction();
		new TestEntityBuilder(this.entityManager).withName("name-1").withAge(43).withPhone("666777888").buildOne();
		this.commitAndStart();

		final BasicModelEntityRepository<TestEntity, Long> repository = this.repositoryManager.getRepository(TestEntity.class);
		final TestEntityView view = repository.findById(1L, TestEntityView.class);
		final TestEntityDto dto = modelConverter.convertToDto(view);
		assertNotNull(dto);
		assertEquals(1L, dto.getId());
		assertEquals("name-1", dto.getName());
		assertEquals(43, dto.getAge());
		assertEquals("666777888", dto.getMobilePhone());
	}

}
