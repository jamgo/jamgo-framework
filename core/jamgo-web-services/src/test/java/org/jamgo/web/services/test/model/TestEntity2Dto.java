package org.jamgo.web.services.test.model;

import org.jamgo.web.services.dto.ModelDto;

public class TestEntity2Dto extends ModelDto {
	private String name;
	private Integer age;
	private String mobilePhone;

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Integer getAge() {
		return this.age;
	}

	public void setAge(final Integer age) {
		this.age = age;
	}

	public String getMobilePhone() {
		return this.mobilePhone;
	}

	public void setMobilePhone(final String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
}
