package org.jamgo.services.test;

import static org.junit.jupiter.api.Assertions.assertNull;

import org.jamgo.model.test.config.ModelTestConfig;
import org.jamgo.services.config.ServicesTestConfig;
import org.jamgo.services.impl.BinaryResourceImageService;
import org.jamgo.test.JamgoRepositoryTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = { ModelTestConfig.class, ServicesTestConfig.class })
public class BinaryResourceImageServiceTest extends JamgoRepositoryTest {

	@Autowired
	private BinaryResourceImageService service;

	@Test
	public void testInitializationWithoutProperty() {
		assertNull(this.service.getBinaryResourceImagesProperties());
	}

}
