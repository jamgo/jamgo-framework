package org.jamgo.services.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.LocalizedBinaryResource;
import org.jamgo.model.entity.TestCategory;
import org.jamgo.model.repository.TestCategoryRepository;
import org.jamgo.model.test.TestCategoryBuilder;
import org.jamgo.model.test.config.ModelTestConfig;
import org.jamgo.model.test.entity.builder.BinaryResourceBuilder;
import org.jamgo.services.config.ServicesTestConfig;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.test.JamgoRepositoryTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@ContextConfiguration(classes = { ModelTestConfig.class, ServicesTestConfig.class })
public class LocalizedObjectServiceTest extends JamgoRepositoryTest {

	@Autowired
	private TestCategoryRepository testCategoryRepository;

	@Autowired
	private LocalizedObjectService localizedObjectService;

	@Test
	@Transactional
	public void testInsert_withLocalizedBinaryResource() {
		this.entityManager.joinTransaction();

		final LocalizedBinaryResource localizedImage = new LocalizedBinaryResource();
		localizedImage.set("es-ES", new BinaryResourceBuilder(this.entityManager).buildOne());
		localizedImage.set("ca-ES", new BinaryResourceBuilder(this.entityManager).buildOne());

		final TestCategory category = new TestCategoryBuilder(this.entityManager)
			.setImage(localizedImage)
			.buildOne();
		this.commit();

		final TestCategory result = this.testCategoryRepository.findById(category.getId());
		assertEquals(category.getId(), result.getId());
		assertNotNull(result.getImage());
		final BinaryResource binaryResource = this.localizedObjectService.getValue(result.getImage());
		assertNotNull(binaryResource);
	}

}
