package org.jamgo.services.config;

import java.util.Properties;

import org.jamgo.services.UserService;
import org.jamgo.services.session.SessionContext;
import org.jamgo.services.session.SessionContextImpl;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@ComponentScan(
	basePackageClasses = {
		org.jamgo.services.PackageMarker.class
	},
	excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
		classes = ServicesConfig.class))
public class ServicesTestConfig {

	@Bean
	public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		final PropertySourcesPlaceholderConfigurer propsConfig = new PropertySourcesPlaceholderConfigurer();
		return propsConfig;
	}

	@Bean
	public Properties permissionProperties() {
		final Properties props = new Properties();
		return props;
	}

	@Bean
	public SessionContext sessionContext() {
		return new SessionContextImpl();
	}

	@Bean
	public UserService userService() {
		return Mockito.mock(UserService.class);
	}

}
