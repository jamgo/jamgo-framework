package org.jamgo.services.ie.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.jamgo.model.entity.SecuredObject;
import org.jamgo.model.test.config.ModelTestConfig;
import org.jamgo.services.NestedDataProvider;
import org.jamgo.services.config.ServicesTestConfig;
import org.jamgo.test.JamgoRepositoryTest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = { ModelTestConfig.class, ServicesTestConfig.class })
public class NestedDataProviderTest extends JamgoRepositoryTest {

	@SuppressWarnings("unchecked")
	@Test
	public void testCollections() {
		final SecuredObject securedObjectParent = new SecuredObject();
		final List<SecuredObject> securedObjectChildren = new ArrayList<>();
		for (long i = 0; i < 3; i++) {
			final SecuredObject securedObjectChild = new SecuredObject();
			securedObjectChild.setId(i);
			securedObjectChild.setName("name" + i);
			securedObjectChildren.add(securedObjectChild);
		}
		securedObjectParent.setChildren(securedObjectChildren);

		// construct with single nested data provider
		final NestedDataProvider<SecuredObject, SecuredObject> singleNestedDataProvider = new NestedDataProvider<>(SecuredObject::getChildren, SecuredObject::getName);
		List<Object> value = (List<Object>) singleNestedDataProvider.apply(securedObjectParent);
		assertNotNull(value);
		assertTrue(value instanceof Collection);
		assertEquals(3, value.size());

		for (int i = 0; i < 3; i++) {
			assertEquals("name" + i, value.get(i));
		}

		// construct with multiple nested data provider
		final NestedDataProvider<SecuredObject, SecuredObject> multiNestedDataProvider = new NestedDataProvider<>(SecuredObject::getChildren, Arrays.asList(SecuredObject::getId, SecuredObject::getName));
		value = (List<Object>) multiNestedDataProvider.apply(securedObjectParent);
		assertNotNull(value);
		assertTrue(value instanceof Collection);
		assertEquals(3, value.size());
		for (int i = 0; i < 3; i++) {
			final List<Object> values = (List<Object>) value.get(i);
			assertEquals(Long.valueOf(i), values.get(0));
			assertEquals("name" + i, values.get(1));
		}
	}
}
