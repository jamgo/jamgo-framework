package org.jamgo.services.message.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Locale;

import org.jamgo.model.test.config.ModelTestConfig;
import org.jamgo.model.test.entity.builder.LanguageBuilder;
import org.jamgo.services.config.ServicesTestConfig;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.session.SessionContext;
import org.jamgo.test.JamgoRepositoryTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@ContextConfiguration(classes = { ModelTestConfig.class, ServicesTestConfig.class })
public class LocalizedMessageServiceTest extends JamgoRepositoryTest {

	@Autowired
	private LocalizedMessageService messageService;
	@Autowired
	private SessionContext sessionContext;

	@Test
	@Transactional
	public void getMessageTest() {
		this.entityManager.joinTransaction();
		new LanguageBuilder(this.entityManager)
			.setLanguageCode("es").setCountryCode("ES")
			.buildOne();
		this.commitAndStart();

		this.sessionContext.setCurrentLocale(new Locale("es", "ES"));

		String message = this.messageService.getMessage("message.doesnotexist");
		assertEquals("#message.doesnotexist [es-ES]", message);

		message = this.messageService.getMessage("message.exists");
		assertEquals("Este mensaje existe", message);

		message = this.messageService.getMessage("message.withOneParameter", new String[] { "un" });
		assertEquals("Mensaje con un parametro", message);

		message = this.messageService.getMessage("message.withTwoParameter", new String[] { "two", "english" }, new Locale("en", "EN"));
		assertEquals("Message with two parameters with english locale", message);

	}
}
