package org.jamgo.services.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Map;

import org.jamgo.model.test.config.ModelTestConfig;
import org.jamgo.services.config.ServicesTestConfig;
import org.jamgo.services.impl.BinaryResourceImageService;
import org.jamgo.test.JamgoRepositoryTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
@ContextConfiguration(classes = { ModelTestConfig.class, ServicesTestConfig.class })
@TestPropertySource(properties = { "binaryResource.images = {LOW: \"200x100\", MEDIUM: \"400x200\" }" })
public class BinaryResourceImageServiceWithPropertyTest extends JamgoRepositoryTest {

	@Autowired
	private BinaryResourceImageService service;

	@Test
	public void testInitializationWithoutProperty() {
		final Map<String, String> map = this.service.getBinaryResourceImagesProperties();
		assertNotNull(map);
		assertEquals(2, map.size());
		final String lowValue = map.get("LOW");
		assertNotNull(lowValue);
		assertEquals("200x100", lowValue);
		final String mediumValue = map.get("MEDIUM");
		assertNotNull(mediumValue);
		assertEquals("400x200", mediumValue);
	}

}
