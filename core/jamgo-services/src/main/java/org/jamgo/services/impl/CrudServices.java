package org.jamgo.services.impl;

import java.util.Collection;
import java.util.List;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.BasicModelEntity;
import org.jamgo.model.entity.SecuredObject;
import org.jamgo.services.exception.CrudException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class CrudServices {

	@Autowired
	protected ServiceManager serviceManager;

	public <ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE> List<ENTITY> findAll(final Class<ENTITY> entityClass) {
		final BasicModelEntityService<ENTITY, ID_TYPE> service = this.serviceManager.getService(entityClass);
		return service.findAll();
	}

	@Transactional
	public <ENTITY extends BasicModelEntity<ID_TYPE>, BASIC_MODEL extends BasicModel<ID_TYPE>, ID_TYPE> void delete(
		final Collection<BASIC_MODEL> items,
		final Class<ENTITY> entityClass)
		throws CrudException {
		try {
			final BasicModelEntityService<ENTITY, ID_TYPE> service = this.serviceManager.getService(entityClass);
			service.delete(items);
		} catch (final Exception e) {
			throw new CrudException(e);
		}
	}

	@Transactional
	public <ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE> ENTITY save(final ENTITY item) throws CrudException {
		return this.save(item, null);
	}

	@Transactional
	public <ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE> ENTITY save(final ENTITY item, final SecuredObject securedObject) throws CrudException {
		ENTITY savedItem = null;
		try {
			final BasicModelEntityService<ENTITY, ID_TYPE> service = this.serviceManager.getService(item);
			if (securedObject == null) {
				savedItem = service.save(item);
			} else {
				savedItem = service.save(item, securedObject);
			}
		} catch (final Exception e) {
			throw new CrudException(e);
		}
		return savedItem;
	}

	@Transactional
	public <ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE> List<ENTITY> save(final List<ENTITY> items) throws CrudException {
		List<ENTITY> savedItems = null;
		try {
			final BasicModelEntityService<ENTITY, ID_TYPE> service = this.serviceManager.getService(items.get(0));
			savedItems = service.save(items);
		} catch (final Exception e) {
			throw new CrudException(e);
		}
		return savedItems;
	}

}
