package org.jamgo.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import org.jamgo.model.entity.Model;

/**
 * <pre>
 * Data provider that allows nested attributes data access. It works either with single 
 * instances and with lists. 
 * 
 * <b>Usage</b>:
 * 
 * {@code
 * 	class Organization {
 * 		List<Member> member;
 * 		Type type;
 * 
 * 		List<Member> getMembers() {
 * 			return members;
 * 		}
 * 		Type getType() {
 * 			return type;
 * 		}
 * 	}
 * 
 * 	class Member {
 *		String name;
 *
 *		List<Member> getName() {
 * 			return name;
 * 		}
 *	}
 *
 *	class Type {
 *		String name;
 *
 *		String getName() {
 * 			return name;
 * 		}
 *	}
 *	}
 *	Accessing organization's member's attribute name: 
 *	{@code 
 *		new NestedDataProvider<Member,Organization>(Organization::get, Members::getName)
 *	}
 *	Accessing organization's type's attribute name:
 *
 *	{@code 
 *		new NestedDataProvider<Member,Organization>(Organization::getType, Type::getName)
 *	}
 *	</pre>
 * 
 * 
 * @author jamgo
 *
 * @param <MODEL> instance class where the desired attribute is located
 * @param <PARENT_MODEL> parent instance class
 */
public class NestedDataProvider <PARENT_MODEL extends Model, MODEL extends Model> implements Function<PARENT_MODEL, Object> {

	private Function<PARENT_MODEL, Object> parentFunction;
	private Collection<Function<MODEL, Object>> childFunctions;

	/**
	 * Constructor
	 * 
	 * @param parentFunction main class data provider (getter) function
	 * @param childFunction target attribute data provider (getter function)
	 */
	public NestedDataProvider(Function<PARENT_MODEL, Object> parentFunction, Function<MODEL, Object> childFunction) {
		this(parentFunction, Collections.singletonList(childFunction));
	}
	
	public NestedDataProvider(Function<PARENT_MODEL, Object> parentFunction, Collection<Function<MODEL, Object>> childFunctions) {
		this.parentFunction = parentFunction;
		this.childFunctions = childFunctions;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object apply(PARENT_MODEL parent) {
		Object value = this.parentFunction.apply(parent);
		if (value == null) {
			return null;
		}
		if (this.childFunctions == null) {
			return value;
		}
		if (value instanceof Collection) {
			List<Object> items = new ArrayList<>();
			if (this.childFunctions.size() > 1) {
				for(MODEL eachItem : (Collection<MODEL>)value) {
					List<Object> values = new ArrayList<>();
					this.childFunctions.stream().forEach(childFunction -> {
						values.add(childFunction.apply(eachItem));
					});
					items.add(values);
				};
			} else {
				for(MODEL eachItem : (Collection<MODEL>)value) {
					items.add(this.childFunctions.iterator().next().apply(eachItem));
				};
			}
			return items;
		}
		return this.childFunctions.iterator().next().apply((MODEL)value);

	}

}