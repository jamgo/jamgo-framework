package org.jamgo.services.config;

import java.util.Properties;

import org.jamgo.model.config.ModelConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ ModelConfig.class })
@ComponentScan(
	basePackageClasses = {
		org.jamgo.services.PackageMarker.class
	})
public class ServicesConfig {

	@Bean
	public Properties permissionProperties() {
		final Properties props = new Properties();
		return props;
	}

}
