package org.jamgo.services.impl;

import java.io.Serializable;
import java.util.Locale;

import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.LocalizedBasicModelEntity;
import org.jamgo.model.entity.LocalizedObject;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.repository.RepositoryManager;
import org.jamgo.services.LanguageServices;
import org.jamgo.services.session.SessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocalizedObjectService {

	private static final Logger logger = LoggerFactory.getLogger(LocalizedObjectService.class);

	@Autowired
	protected SessionContext sessionContext;

	@Autowired
	protected LanguageServices languageServices;

	@Autowired
	protected RepositoryManager repositoryManager;

	public <TYPE, MAP_VALUE extends Serializable> TYPE getValue(final LocalizedObject<TYPE, MAP_VALUE> localizedObject) {
		return this.getValue(localizedObject, this.getCurrentLocale());
	}

	public <TYPE, MAP_VALUE extends Serializable> TYPE getValue(final LocalizedObject<TYPE, MAP_VALUE> localizedObject, final Language language) {
		return this.getValue(localizedObject, language.getLocale());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <TYPE, MAP_VALUE extends Serializable> TYPE getValue(final LocalizedObject<TYPE, MAP_VALUE> localizedObject, final Locale locale) {
		TYPE value = null;
		if (locale != null) {
			if (localizedObject instanceof LocalizedString) {
				value = (TYPE) localizedObject.get(locale);
			} else if (localizedObject instanceof LocalizedBasicModelEntity) {
				LocalizedObjectService.logger.debug("getValue for LocalizedBasicModelEntity");
				final MAP_VALUE entityId = localizedObject.get(locale);
				if (entityId != null) {
					final Class modelClass = ((LocalizedBasicModelEntity) localizedObject).getModelClass();
					LocalizedObjectService.logger.debug("modelClass = {}, entityId = {}", modelClass, entityId);
					value = (TYPE) this.repositoryManager.getRepository(modelClass).findById(entityId);
					LocalizedObjectService.logger.debug("Entity retrieved from repository: {}", value);
				}
			}
		}
		return value;
	}

	private Locale getCurrentLocale() {
		Locale currentLocale = this.sessionContext.getCurrentLocale();
		LocalizedObjectService.logger.trace("retrieved locale from sessionContext: {}", currentLocale);
		if (currentLocale == null) {
			final Language defaultLanguage = this.languageServices.getDefaultLanguage();
			currentLocale = defaultLanguage.getLocale();
			LocalizedObjectService.logger.trace("using default language: {}", currentLocale);
		}
		return currentLocale;
	}

}
