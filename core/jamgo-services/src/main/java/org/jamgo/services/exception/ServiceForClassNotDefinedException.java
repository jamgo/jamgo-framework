package org.jamgo.services.exception;

public class ServiceForClassNotDefinedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ServiceForClassNotDefinedException(final String message) {
		super(message);
	}
}
