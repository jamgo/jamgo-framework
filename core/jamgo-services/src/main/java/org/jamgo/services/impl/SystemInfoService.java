package org.jamgo.services.impl;

import java.util.Iterator;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.stereotype.Service;

import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;

@Service
public class SystemInfoService implements InitializingBean {

	public static final String OTHER_GROUP = "Other";

	@Autowired
	private Environment env;

	private Table<String, String, String> systemPropertiesTable = TreeBasedTable.create();

	@Override
	public void afterPropertiesSet() throws Exception {
//		System.getenv().forEach((key, value) -> this.systemPropertiesTable.put(SystemInfoService.ENVIRONMENT_GROUP, key, value));
//		try {
//			System.getProperties().forEach((key, value) -> this.systemPropertiesTable.put(SystemInfoService.PROPERTIES_GROUP, (String) key, value.toString()));
//		} catch (final SecurityException e) {
//			this.systemPropertiesTable.put(SystemInfoService.PROPERTIES_GROUP, "*", "Security manager restricts access to system properties");
//		}

		for (final Iterator<PropertySource<?>> it = ((AbstractEnvironment) this.env).getPropertySources().iterator(); it.hasNext();) {
			final PropertySource<?> propertySource = it.next();
			if (propertySource instanceof MapPropertySource) {
				((MapPropertySource) propertySource).getSource().forEach((key, value) -> this.systemPropertiesTable.put(propertySource.getName(), key, value.toString()));
			}
		}

	}

	public Table<String, String, String> getSystemPropertiesTable() {
		return this.systemPropertiesTable;
	}

	public void setSystemPropertiesTable(final Table<String, String, String> systemPropertiesTable) {
		this.systemPropertiesTable = systemPropertiesTable;
	}

	public void addSystemProperty(final String group, final String key, final String value) {
		this.systemPropertiesTable.put(group, key, value);
	}

}
