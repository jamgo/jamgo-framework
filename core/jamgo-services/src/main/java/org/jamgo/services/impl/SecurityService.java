package org.jamgo.services.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Predicate;

import org.jamgo.model.entity.SecuredObject;
import org.jamgo.model.entity.User;
import org.jamgo.model.enums.Permission;
import org.jamgo.model.enums.SecuredObjectType;
import org.jamgo.model.repository.AclRepository;
import org.jamgo.model.repository.SecuredObjectRepository;
import org.jamgo.services.UserService;
import org.jamgo.services.session.SessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SecurityService implements InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(SecurityService.class);

//	public enum PermissionType {
//		Read, Write, All
//	}

	@Autowired
	protected SessionContext sessionContext;
	@Autowired
	protected AclRepository aclRepository;
	@Autowired
	protected SecuredObjectRepository securedObjectRepository;
	@Autowired
	protected UserService userService;
	@Autowired
	private SettingsService settingsService;
	@Autowired
	@Qualifier("permissionProperties")
	protected Properties permissionProperties;

	@Value("${permissions.enabled:true}")
	private boolean permissionsEnabled;

//	private Map<Long, SecuredObject> securedObjectsByParent;
	private final Map<String, SecuredObject> securedObjectsByTypeAndKey = new HashMap<>();
	private final Map<String, SecuredObject> securedObjectsByTypeAndParentKeyAndKey = new HashMap<>();

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		this.securedObjectRepository.findByParentIsNull().forEach(eachSecureObject -> {
			this.securedObjectsByTypeAndKey.put(
				this.createTypeAndKeyId(eachSecureObject.getSecuredObjectType(), eachSecureObject.getSecuredObjectKey()),
				eachSecureObject);
			if (eachSecureObject.getParent() != null) {
				this.securedObjectsByTypeAndParentKeyAndKey.put(
					this.createTypeAndParentKeyAndKeyId(
						eachSecureObject.getSecuredObjectType(),
						eachSecureObject.getParent().getSecuredObjectKey(),
						eachSecureObject.getSecuredObjectKey()),
					eachSecureObject);
			}
		});
	}

	public boolean isPermissionEnabled() {
		return this.permissionsEnabled;
	}

	public String getSecurizedField(final String securedObjectKey) {
		return (String) this.permissionProperties.get(securedObjectKey);
	}

	public SecuredObject getSecuredObject(final SecuredObjectType securedObjectType, final String securedObjectKey) {
		final String flatId = this.createTypeAndKeyId(securedObjectType, securedObjectKey);
		return this.securedObjectsByTypeAndKey.get(flatId);

//		return this.securedObjectRepository.getSecuredObject(securedObjectType, securedObjectKey);
	}

	public SecuredObject getSecuredObject(final SecuredObjectType securedObjectType, final String parentSecuredObjectKey, final String securedObjectKey) {
		final String flatId = this.createTypeAndParentKeyAndKeyId(securedObjectType, parentSecuredObjectKey, securedObjectKey);
		return this.securedObjectsByTypeAndParentKeyAndKey.get(flatId);

//		return this.securedObjectRepository.getSecuredObject(securedObjectType, parentSecuredObjectKey, securedObjectKey);
	}

	private String createTypeAndKeyId(final SecuredObjectType type, final String key) {
		return String.join("-", type.name(), key);
	}

	private String createTypeAndParentKeyAndKeyId(final SecuredObjectType type, final String parentKey, final String key) {
		return String.join("-", type.name(), parentKey, key);
	}

	public Boolean hasPermission(final Class<?> clazz) {
		if (clazz != null) {
			final String flatId = this.createTypeAndKeyId(SecuredObjectType.ENTITY, clazz.getName());
			return this.securedObjectsByTypeAndKey.containsKey(flatId);
		} else {
			return true;
		}

//		final boolean hasPermission = true;
//		if (clazz != null) {
//			final SecuredObject securedObject = this.securedObjectRepository.findBySecuredObjectTypeAndSecuredObjectKey(SecuredObjectType.ENTITY, clazz.getName());
//			return securedObject != null;
//		}
//		return hasPermission;
	}

	public Boolean hasPermission(final Object targetObject) {
		return this.hasPermission(targetObject, Permission.ALL);
	}

	public Boolean hasPermission(final Object targetObject, final Permission permission) {
		boolean hasPermission = true;
		if (targetObject != null) {
			final Class<?> targetObjectClass = targetObject.getClass();
			final String targetObjectClassName = targetObjectClass.getName();
			final Object attributeValue = this.getSecuredObjectKey(targetObject);
			if (attributeValue != null) {
				switch (permission) {
					case READ:
						hasPermission = this.hasReadPermission(SecuredObjectType.ENTITY, attributeValue.toString(), targetObjectClassName);
						break;
					case WRITE:
						hasPermission = this.hasWritePermission(SecuredObjectType.ENTITY, attributeValue.toString(), targetObjectClassName);
						break;
					case ALL:
						hasPermission = this.hasReadPermission(SecuredObjectType.ENTITY, attributeValue.toString(), targetObjectClassName)
							|| this.hasWritePermission(SecuredObjectType.ENTITY, attributeValue.toString(), targetObjectClassName);
						break;
					default:
						break;
				}
			}
		}
		return hasPermission;
	}

	public String getSecuredObjectKey(final Object targetObject) {
		final Class<?> targetObjectClass = targetObject.getClass();
		final String targetObjectClassName = targetObjectClass.getName();
		String attributeName = "id";
		if (this.permissionProperties.containsKey(targetObjectClassName)) {
			attributeName = this.permissionProperties.getProperty(targetObjectClassName);
		}
		try {
			final String getterMethodName = "get" + attributeName.substring(0, 1).toUpperCase() + attributeName.substring(1);
			if (targetObjectClass.getMethod(getterMethodName) != null) {
				final Object attributeValue = targetObject.getClass().getMethod(getterMethodName).invoke(targetObject);
				return attributeValue != null ? attributeValue.toString() : "";
			}
			return null;
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SecurityException e) {
			SecurityService.logger.error(e.getMessage(), e);
		} catch (final NoSuchMethodException e) {
			// do nothing
		}
		return null;
	}

	public Boolean hasReadPermission(final SecuredObjectType securedObjectType, final String securedObjectKey, final boolean inherits) {
		return this.hasPermission(o -> Optional.ofNullable(o).map(p -> p.canRead()).orElse(false), securedObjectType, securedObjectKey, inherits);
	}

	public Boolean hasReadPermission(final SecuredObject securedObject, final boolean inherits) {
		return this.hasPermission(o -> Optional.ofNullable(o).map(p -> p.canRead()).orElse(false), securedObject, inherits);
	}

//	public Boolean hasReadPermission(final SecuredObject securedObject, final User user, final boolean inherits) {
//		return this.hasPermission(o -> Optional.ofNullable(o).map(p -> p.canRead()).orElse(false), securedObject, user, inherits);
//	}

	public Boolean hasReadPermission(final SecuredObjectType securedObjectType, final String securedObjectKey, final String parentSecuredObjectKey) {
		return this.hasPermission(o -> Optional.ofNullable(o).map(p -> p.canRead()).orElse(false), securedObjectType, securedObjectKey, parentSecuredObjectKey);
	}

	public Boolean hasWritePermission(final SecuredObjectType securedObjectType, final String securedObjectKey, final boolean inherits) {
		return this.hasPermission(o -> Optional.ofNullable(o).map(p -> p.canWrite()).orElse(false), securedObjectType, securedObjectKey, inherits);
	}

	public Boolean hasWritePermission(final SecuredObject securedObject, final boolean inherits) {
		return this.hasPermission(o -> Optional.ofNullable(o).map(p -> p.canWrite()).orElse(false), securedObject, inherits);
	}

//	public Boolean hasWritePermission(final SecuredObject securedObject, final User user, final boolean inherits) {
//		return this.hasPermission(o -> Optional.ofNullable(o).map(p -> p.canWrite()).orElse(false), securedObject, user, inherits);
//	}

	public Boolean hasWritePermission(final SecuredObjectType securedObjectType, final String securedObjectKey, final String parentSecuredObjectKey) {
		return this.hasPermission(o -> Optional.ofNullable(o).map(p -> p.canWrite()).orElse(false), securedObjectType, securedObjectKey, parentSecuredObjectKey);
	}

	public Boolean hasPermission(final Predicate<Permission> permisionCheck, final SecuredObjectType securedObjectType, final String securedObjectKey, final String parentSecuredObjectKey) {
		final String flatId = this.createTypeAndParentKeyAndKeyId(securedObjectType, parentSecuredObjectKey, securedObjectKey);
		final SecuredObject securedObject = this.securedObjectsByTypeAndParentKeyAndKey.get(flatId);

//		final SecuredObject securedObject = this.securedObjectRepository.getSecuredObject(securedObjectType, parentSecuredObjectKey, securedObjectKey);
		if (securedObject != null) {
			return this.hasPermission(permisionCheck, securedObject, false);
		} else {
			return this.settingsService.isMissingHasPermission();
		}
		/**
		 * 	SecuredObjectType securedObjectType, 	ENTITY
			String securedObjectKey					indx
			String parentSecuredObjectKey			Indicator
			String parendSecuredObjectKey			cat.xes.bs.model.entity.Indicator
		 */

	}

	private Boolean hasPermission(final Predicate<Permission> permisionCheck, final SecuredObjectType securedObjectType, final String securedObjectKey, final boolean inherits) {
		final String flatId = this.createTypeAndKeyId(securedObjectType, securedObjectKey);
		final SecuredObject securedObject = this.securedObjectsByTypeAndKey.get(flatId);

//		final SecuredObject securedObject = this.securedObjectRepository.findBySecuredObjectTypeAndSecuredObjectKey(securedObjectType, securedObjectKey);
		if (securedObject != null) {
			return this.hasPermission(permisionCheck, securedObject, inherits);
		} else {
			return this.settingsService.isMissingHasPermission();
		}
	}

//	private Boolean hasPermission(final Predicate<Permission> permisionCheck, final SecuredObject securedObject, final boolean inherits) {
//		return this.hasPermission(permisionCheck, securedObject, this.userService.getCurrentUser(), inherits);
//	}

//	private Boolean hasPermission(final Predicate<Permission> permisionCheck, final SecuredObject securedObject, final User user, final boolean inherits) {
//		if (this.hasUserPermission(permisionCheck, securedObject, user, inherits)) {
//			return true;
//		}
//		for (final Role eachRole : user.getRoles()) {
//			if (eachRole.isAdmin()) {
//				return true;
//			} else if (this.hasRolePermission(permisionCheck, securedObject, eachRole, inherits)) {
//				return true;
//			}
//		}
//		return false;
//	}

//	private Boolean hasUserPermission(final Predicate<Permission> permisionCheck, final SecuredObject securedObject, final User user, final boolean inherits) {
//		final Acl matchingAcl = this.aclRepository.findByUserIdAndSecuredObject(user.getId(), securedObject);
//		return this.hasPermission(permisionCheck, securedObject, matchingAcl, inherits);
//	}

//	private Boolean hasRolePermission(final Predicate<Permission> permisionCheck, final SecuredObject securedObject, final Role role, final boolean inherits) {
//		final Acl matchingAcl = this.aclRepository.findByRoleIdAndSecuredObject(role.getId(), securedObject);
//		return this.hasPermission(permisionCheck, securedObject, matchingAcl, inherits);
//	}

	private Boolean hasPermission(final Predicate<Permission> permisionCheck, final SecuredObject securedObject, final boolean inherits) {
		final Permission permission = this.sessionContext.getPermission(securedObject.getId());
		if (permission != null) {
			return Optional.ofNullable(permisionCheck.test(permission)).orElse(false);
		} else {
			if (inherits && (securedObject.getParent() != null)) {
				return this.hasPermission(permisionCheck, securedObject.getParent(), inherits);
			} else {
				return false;
			}
		}
	}

//	private Boolean hasPermission(final Predicate<Permission> permission, final SecuredObject securedObject, final Acl acl, final boolean inherits) {
//		if (acl != null) {
//			return Optional.ofNullable(permission.test(acl)).orElse(false);
//		} else {
//			if (inherits && (securedObject.getParent() != null)) {
//				return this.hasPermission(permission, securedObject.getParent(), acl, inherits);
//			} else {
//				return false;
//			}
//		}
//	}

	public void save(final SecuredObject securedObject) {
		this.securedObjectRepository.persist(securedObject);
	}

//	public boolean hasWritePermission(final SecuredObjectType securedObjectType, final String securedObjectKey, final String name, final User user) {
//		final SecuredObject securedObject = this.getSecuredObject(securedObjectType, securedObjectKey);
//		if (securedObject == null) {
//			return true;
//		}
//		return this.hasWritePermission(securedObject, user, false);
//	}

	public void removeSecurityFromEntity(final Object object) {
		final String securedObjectKey = this.getSecuredObjectKey(object);
		final SecuredObject securedObject = this.getSecuredObject(SecuredObjectType.ENTITY, object.getClass().getName(), securedObjectKey);
		if (securedObject != null) {
			this.securedObjectRepository.remove(securedObject);
		}
	}

	public User getCurrentUser() {
		return this.userService.getCurrentUser();
	}

}
