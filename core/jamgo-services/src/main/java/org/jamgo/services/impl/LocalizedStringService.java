package org.jamgo.services.impl;

import org.jamgo.model.entity.LocalizedString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Deprecated
/**
 *
 * @author aheredia
 * @deprecated Use {@link LocalizedObjectService} instead.
 */
public class LocalizedStringService {

	@Autowired
	private LocalizedObjectService localizedObjectService;

	public String getValue(final LocalizedString localizedString) {
		return this.localizedObjectService.getValue(localizedString);
	}

}
