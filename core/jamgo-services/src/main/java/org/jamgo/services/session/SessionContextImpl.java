package org.jamgo.services.session;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.jamgo.model.entity.Acl;
import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.User;
import org.jamgo.model.enums.Permission;
import org.jamgo.model.repository.AclRepository;
import org.jamgo.model.repository.LanguageRepository;
import org.jamgo.services.impl.SettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

public class SessionContextImpl implements SessionContext, InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(SessionContextImpl.class);

	@Autowired
	private LanguageRepository languageRepository;
	@Autowired
	private AclRepository aclRepository;
	@Autowired
	private SettingsService settingsService;

	protected User currentUser;
	protected Language currentLanguage;
	protected Locale currentLocale;
	protected Map<Long, Permission> permissionBySecuredObjectId = new HashMap<>();

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		this.currentUser = null;
		this.currentLanguage = null;
		this.currentLocale = null;
		this.permissionBySecuredObjectId = new HashMap<>();

		this.initDefaultLanguageAndLocale();
	}

	private void initDefaultLanguageAndLocale() {
		if (this.settingsService.getDefaultLanguageId() != null) {
			this.currentLanguage = this.languageRepository.findById(Long.valueOf(this.settingsService.getDefaultLanguageId()));
			if (this.currentLanguage != null) {
				this.currentLocale = this.currentLanguage.getLocale();
			}
		}
		if (this.currentLocale == null) {
			this.currentLocale = new Locale("ca", "ES");
		}
	}

	@Override
	public void reset() {
		this.init();
	}

	@Override
	public Language getCurrentLanguage() {
		return this.currentLanguage;
	}

	@Override
	public void setCurrentLanguage(final Language currentLanguage) {
		this.currentLanguage = currentLanguage;
		if (currentLanguage != null) {
			this.currentLocale = currentLanguage.getLocale();
		}
	}

	@Override
	public Locale getCurrentLocale() {
		return this.currentLocale;
	}

	@Override
	public void setCurrentLocale(final Locale locale) {
		if (locale == null) {
			return;
		}

		SessionContextImpl.logger.debug("Received locale: language = {}, country = {}", locale.getLanguage(), locale.getCountry());
		// Only set locale associated to a language defined in the application.
		try {
			final Language language = this.languageRepository.findByLanguageCodeAndCountryCode(locale.getLanguage(), locale.getCountry());
			SessionContextImpl.logger.debug("Language found: {}", language.getName());
			this.currentLanguage = language;
			this.currentLocale = locale;
		} catch (final NoResultException | EmptyResultDataAccessException ex) {
			SessionContextImpl.logger.debug("No language found, setting default language and locale");
			this.initDefaultLanguageAndLocale();
		}
	}

	public Map<Long, Permission> getPermissionBySecuredObjectId() {
		return this.permissionBySecuredObjectId;
	}

	public void setPermissionBySecuredObjectId(final Map<Long, Permission> permissionBySecuredObjectId) {
		this.permissionBySecuredObjectId = permissionBySecuredObjectId;
	}

	@Override
	public Permission getPermission(final Long securedObejctId) {
		return Optional.ofNullable(this.permissionBySecuredObjectId).map(map -> map.get(securedObejctId)).orElse(null);
	}

	@Override
	public User getCurrentUser() {
		return this.currentUser;
	}

	@Override
	public void setCurrentUser(final User currentUser) {
		this.currentUser = currentUser;
		this.initializePermissions();
	}

	@Transactional
	protected void initializePermissions() {
		this.currentUser.getRoles().forEach(eachRole -> {
			final List<Acl> roleAcls = this.aclRepository.findByRoleId(eachRole.getId());
			roleAcls.forEach(eachRoleAcl -> {
				this.permissionBySecuredObjectId.put(eachRoleAcl.getSecuredObject().getId(), eachRoleAcl.getPermission());
			});
		});
		this.aclRepository.findByUserId(this.currentUser.getId()).forEach(eachUserAcl -> {
			this.permissionBySecuredObjectId.put(eachUserAcl.getSecuredObject().getId(), eachUserAcl.getPermission());
		});
	}

}