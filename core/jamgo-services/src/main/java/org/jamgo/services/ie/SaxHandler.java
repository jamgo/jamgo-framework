package org.jamgo.services.ie;

import java.util.List;

import org.jamgo.model.entity.Model;
import org.xml.sax.helpers.DefaultHandler;

public abstract class SaxHandler<MODEL extends Model> extends DefaultHandler {

	protected abstract List<MODEL> getList();

}
