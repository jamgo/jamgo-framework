package org.jamgo.services.impl;

import java.util.Optional;

import org.apache.tika.Tika;
import org.jamgo.model.entity.Downloadable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class DownloadService {

	public static final Logger logger = LoggerFactory.getLogger(DownloadService.class);

	final Tika tika = new Tika();

	public String getContentMimeType(final Downloadable resource) {
		DownloadService.logger.debug("getContentMimeType() ->");
		String mimeType = null;
		if (StringUtils.hasText(resource.getMimeType())) {
			mimeType = resource.getMimeType();
			DownloadService.logger.debug("binaryResource mimeType stored in database: {}", mimeType);
		}
		if (mimeType == null) {
			if (resource.getFileName() != null) {
				mimeType = this.tika.detect(resource.getFileName());
				DownloadService.logger.debug("binaryResource mimeType detected by Tika using fileName {}: {}", resource.getFileName(), mimeType);
			}
			if (mimeType == null) {
				mimeType = this.tika.detect(resource.getByteContents());
				DownloadService.logger.debug("binaryResource mimeType detected by Tika using byteContents: {}", mimeType);
			}
		}
		DownloadService.logger.debug("getContentMimeType() <- {}", mimeType);
		return mimeType;
	}

	public int getContentLength(final Downloadable resource) {
		int contentLength;
		if (resource.getFileLength() != null) {
			contentLength = resource.getFileLength();
			DownloadService.logger.debug("response contentLength using fileLength: {}", resource.getFileLength());
		} else {
			contentLength = resource.getByteContents().length;
			DownloadService.logger.debug("response contentLength using byteContents length: {}", resource.getByteContents().length);
		}
		return contentLength;
	}

	public boolean isImage(final Downloadable resource) {
		final String mimeType = this.getContentMimeType(resource);
		return Optional.ofNullable(mimeType).map(mt -> mt.startsWith("image/")).orElse(false);
	}

	public boolean isPdf(final Downloadable resource) {
		final String mimeType = this.getContentMimeType(resource);
		return Optional.ofNullable(mimeType).map(mt -> mt.equals("application/pdf")).orElse(false);
	}

}
