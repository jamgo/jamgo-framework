package org.jamgo.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.jamgo.model.entity.BasicModelEntity;
import org.jamgo.model.entity.Model;
import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.services.exception.ServiceForClassNotDefinedException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class ServiceManager implements InitializingBean {

	@Autowired
	private ApplicationContext applicationContext;
	private Map<Class<?>, BasicModelEntityService<?, ?>> services;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		this.services = new HashMap<>();
		final Map<String, Object> serviceBeans = this.applicationContext.getBeansWithAnnotation(Service.class);
		serviceBeans.forEach((eachKey, eachValue) -> {
			if (eachValue instanceof BasicModelEntityService) {
				final BasicModelEntityService<?, ?> basicModelEntityService = (BasicModelEntityService<?, ?>) eachValue;
				this.services.put(basicModelEntityService.getModelClass(), basicModelEntityService);
			}
		});
	}

	@SuppressWarnings("unchecked")
	public <ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE> BasicModelEntityService<ENTITY, ID_TYPE> getService(final ENTITY object) throws RepositoryForClassNotDefinedException {
		return this.getService((Class<ENTITY>) object.getClass());
	}

	@SuppressWarnings("unchecked")
	public <ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE> BasicModelEntityService<ENTITY, ID_TYPE> getService(final Class<ENTITY> clazz) {
		BasicModelEntityService<?, ?> service = this.services.get(clazz);
		if (service == null && Model.class.isAssignableFrom(clazz)) {
			service = this.applicationContext.getBean(DefaultModelService.class, clazz);
			this.services.put(clazz, service);
		}
		if (service == null) {
			throw new ServiceForClassNotDefinedException(clazz.getName());
		}
		return (BasicModelEntityService<ENTITY, ID_TYPE>) service;
	}

}
