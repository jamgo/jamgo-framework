package org.jamgo.services;

import java.util.List;

import org.jamgo.model.entity.Role;

public interface RoleService {

	List<? extends Role> findAll();
}
