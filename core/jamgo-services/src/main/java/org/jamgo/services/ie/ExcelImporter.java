package org.jamgo.services.ie;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jamgo.model.entity.Model;
import org.jamgo.model.exception.JmgImportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ExcelImporter<MODEL extends Model> extends AbstractImporter<MODEL> {

	private static final Logger logger = LoggerFactory.getLogger(ExcelImporter.class);

	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	protected Row headerRow;

	@Override
	protected void doExtraction(final InputStream inputStream) throws IOException {
		Workbook workbook;
		try {
			workbook = WorkbookFactory.create(inputStream);
			final Sheet firstSheet = workbook.getSheetAt(this.getInitSheet());
			final Iterator<Row> iterator = firstSheet.iterator();
			for (int rowCnt = 0; rowCnt < this.getInitRow(); rowCnt++) {
				this.headerRow = iterator.next();
			}
			MODEL entity = null;
			final List<Object> previusEntitiesIds = new ArrayList<>();
			Object entityId = null;
			while (iterator.hasNext()) {
				try {
					final Row nextRow = iterator.next();
					entity = this.processExtractedRow(firstSheet, nextRow, entity);
					if (entity != null) {
						entityId = this.getObjectFileId(entity);
						if (!this.isRepeatedId(previusEntitiesIds, entityId)) {
							this.getList().add(entity);
							previusEntitiesIds.add(entityId);
						}
					}
				} catch (final JmgImportException e) {
					this.getErrors().add(e.getMessage());
				} catch (final Exception e) {
					ExcelImporter.logger.error(e.getMessage(), e);
					this.getErrors().add(e.getMessage());
				}
			}
			workbook.close();
		} catch (final EncryptedDocumentException e) {
			ExcelImporter.logger.error(e.getMessage(), e);
		}
	}

	@Override
	public InputStream generateSample() {
		final ByteArrayOutputStream os = new ByteArrayOutputStream();
		final Workbook wb = new XSSFWorkbook();
		try {
			final Sheet sheet = wb.createSheet();
			final Row row = sheet.createRow(0);
			int headerIndex = 0;
			for (final String header : this.getColumnHeaders()) {
				row.createCell(headerIndex).setCellValue(header);
				headerIndex++;
			}
			wb.write(os);
		} catch (final IOException e) {
			ExcelImporter.logger.error(e.getMessage(), e);
			return null;
		} finally {
			try {
				wb.close();
			} catch (final IOException e) {
				ExcelImporter.logger.error(e.getMessage(), e);
				return null;
			}
		}
		return new ByteArrayInputStream(os.toByteArray());
	}

	protected Cell getCell(final Sheet sheet, final Row row, final int numCell) {
		Cell cell = row.getCell(numCell);
		if (cell != null) {
			for (final CellRangeAddress region : sheet.getMergedRegions()) {
				if (region.isInRange(cell.getRowIndex(), cell.getColumnIndex())) {
					final int rowNum = region.getFirstRow();
					final int colIndex = region.getFirstColumn();
					cell = sheet.getRow(rowNum).getCell(colIndex);
					break;
				}
			}
		}
		return cell;
	}

	protected String getCellStringValue(final Sheet sheet, final Row row, final int numCell) {
		final Cell cell = this.getCell(sheet, row, numCell);
		if (cell != null) {
			return cell.getStringCellValue();
		}
		return null;
	}

	protected Integer getCellNumericIntValue(final Sheet sheet, final Row row, final int numCell) {
		final Cell cell = this.getCell(sheet, row, numCell);
		if (cell != null) {
			return (int) cell.getNumericCellValue();
		}
		return null;
	}

	protected Integer getCellStringNumericValue(final Sheet sheet, final Row row, final int numCell) {
		final Cell cell = this.getCell(sheet, row, numCell);
		String value = null;
		Integer valueInt = null;
		if (cell != null) {
			value = cell.getStringCellValue();
			if ((value != null) && !value.trim().isEmpty()) {
				valueInt = Integer.valueOf(value);
			}
		}
		return valueInt;
	}

	protected String getCellNumericStringValue(final Sheet sheet, final Row row, final int numCell) {
		final Cell cell = this.getCell(sheet, row, numCell);
		if (cell != null) {
			return NumberToTextConverter.toText(cell.getNumericCellValue());
		}
		return null;
	}

	protected LocalDateTime getCellStringDateValue(final Sheet sheet, final Row row, final int numCell) {
		final Cell cell = this.getCell(sheet, row, numCell);
		if (cell != null) {
			try {
				return LocalDateTime.parse(cell.getStringCellValue(), ExcelImporter.DATE_FORMATTER);
			} catch (final DateTimeParseException e) {
				ExcelImporter.logger.error(e.getMessage(), e);
			}
		}
		return null;
	}

	protected LocalDateTime getCellDateValue(final Sheet sheet, final Row row, final int numCell) {
		final Cell cell = this.getCell(sheet, row, numCell);
		if (cell != null) {
			return LocalDateTime.from(Instant.ofEpochMilli(cell.getDateCellValue().getTime()));
		}
		return null;
	}

	private boolean isRepeatedId(final List<Object> previusEntitiesIds, final Object entityId) {
		if (!previusEntitiesIds.isEmpty() && (entityId != null)) {
			return previusEntitiesIds.contains(entityId);
		}
		return false;
	}

	protected int getInitSheet() {
		return 0;
	}

	protected int getInitRow() {
		return 1;
	}

	@Override
	public FileType getFileType() {
		return FileType.XLS;
	}

	protected abstract MODEL processExtractedRow(Sheet sheet, Row nextRow, MODEL previousEntity) throws JmgImportException;

}
