package org.jamgo.services;

import java.util.List;

import org.jamgo.model.entity.User;

public interface UserService {

	User getCurrentUser();

	User getUser(String username);

	List<? extends User> getAllUsers();
}
