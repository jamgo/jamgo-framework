package org.jamgo.services.impl;

import org.jamgo.model.entity.Model;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DefaultModelService<MODEL extends Model> extends ModelService<MODEL> {

	@Override
	public void afterPropertiesSet() throws Exception {
		// Do Nothing.
	}

	public DefaultModelService(final Class<MODEL> modelClass) {
		super();
		this.modelClass = modelClass;
	}

}
