package org.jamgo.services.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SettingsService {

	@Value("${ui.collapsedMenuOnStart:false}")
	private boolean collapsedMenuOnStart;

	@Value("${vaadin.images.path:images/}")
	private String imagesPath;
	@Value("${vaadin.menu.icons.suffix:-16-gray.png}")
	private String iconsSuffix;

	@Value("${security.service.missingHasPermission:true}")
	private boolean missingHasPermission;

	@Value("${localizedMessage.basenames:i18n/messages,i18n/base}")
	private String[] basenames;
	@Value("${localizedMessage.encoding:UTF-8}")
	private String encoding;

	@Value("${language.default:#{null}}")
	private String defaultLanguageId;

	public boolean isCollapsedMenuOnStart() {
		return this.collapsedMenuOnStart;
	}

	public void setCollapsedMenuOnStart(final boolean collapsedMenuOnStart) {
		this.collapsedMenuOnStart = collapsedMenuOnStart;
	}

	public String getImagesPath() {
		return this.imagesPath;
	}

	public void setImagesPath(final String imagesPath) {
		this.imagesPath = imagesPath;
	}

	public String getIconsSuffix() {
		return this.iconsSuffix;
	}

	public void setIconsSuffix(final String iconsSuffix) {
		this.iconsSuffix = iconsSuffix;
	}

	public boolean isMissingHasPermission() {
		return this.missingHasPermission;
	}

	public void setMissingHasPermission(final boolean missingHasPermission) {
		this.missingHasPermission = missingHasPermission;
	}

	public String[] getBasenames() {
		return this.basenames;
	}

	public void setBasenames(final String[] basenames) {
		this.basenames = basenames;
	}

	public String getEncoding() {
		return this.encoding;
	}

	public void setEncoding(final String encoding) {
		this.encoding = encoding;
	}

	public String getDefaultLanguageId() {
		return this.defaultLanguageId;
	}

	public void setDefaultLanguageId(final String defaultLanguageId) {
		this.defaultLanguageId = defaultLanguageId;
	}

}
