package org.jamgo.services.exception;

public class CrudException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CrudException() {
		super();
	}

	public CrudException(final Throwable cause) {
		super(cause);
	}

}
