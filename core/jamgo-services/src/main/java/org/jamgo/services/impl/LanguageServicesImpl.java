package org.jamgo.services.impl;

import java.util.HashSet;
import java.util.Set;

import org.jamgo.model.entity.Language;
import org.jamgo.model.repository.LanguageRepository;
import org.jamgo.services.LanguageServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LanguageServicesImpl implements LanguageServices {

	@Autowired
	protected LanguageRepository languagesRepository;

	@Override
	public Set<Language> getAll() {
		return new HashSet<>(this.languagesRepository.findAll());
	}

	@Override
	public Language get(final Long id) {
		return this.languagesRepository.findById(id);
	}

	@Override
	public Language get(final String languageCode, final String countryCode) {
		return this.languagesRepository.findByLanguageCodeAndCountryCode(languageCode, countryCode);
	}

	@Override
	public Language get(final String languageCode) {
		return this.languagesRepository.findByLanguageCode(languageCode);
	}

	@Override
	public Language getDefaultLanguage() {
		return this.languagesRepository.findDefaultLanguage();
	}

}
