package org.jamgo.services.impl;

import java.util.Locale;

import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.LocalizedMessage;
import org.jamgo.model.repository.LanguageRepository;
import org.jamgo.model.repository.LocalizedMessageRepository;
import org.jamgo.services.session.SessionContext;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;

@Component
public class LocalizedMessageService implements InitializingBean {

	public static final String MISSING_PREFIX = "#";

	@Autowired
	private SessionContext sessionContext;
	@Autowired
	private LocalizedMessageRepository localizedMessageRepository;
	@Autowired
	private LanguageRepository languageRepository;
	@Autowired
	private SettingsService settingsService;

	@Value("${language.default:#{null}}")
	private String defaultLanguageId;
	@Value("${text.repository.enabled:#{false}}")
	private boolean enableRepositoryTexts;

	private final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();

	public LocalizedMessageService() {
		super();
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		this.messageSource.setBasenames(this.settingsService.getBasenames());
		this.messageSource.setDefaultEncoding(this.settingsService.getEncoding());
	}

	public String getMessage(final String name) {
		return this.getLocalizedMessage(name).getValue().get(this.getLocale().getLanguage());
	}

	public String getMessage(final String name, final String defaultMessage) {
		final LocalizedMessage localizedMessage = this.getLocalizedMessage(name);
		if (localizedMessage.isMissing() && defaultMessage != null) {
			return defaultMessage;
		} else {
			return localizedMessage.getValue().get(this.getLocale().getLanguage());
		}
	}

	public String getMessage(final String name, final String[] args) {
		return this.getLocalizedMessage(name, args, this.getLocale()).getValue().get(this.getLocale().getLanguage());
	}

	public String getMessage(final String name, final String[] args, final Locale locale) {
		return this.getLocalizedMessage(name, args, locale).getValue().get(locale.getLanguage());
	}

	public String getMessageForField(final String messageName, final String fieldName) {
		if (fieldName == null) {
			return messageName;
		}
		String localizedfieldName = this.getMessage(fieldName);
		return this.getMessage(messageName, new String[] {localizedfieldName});
	}
	
	public String[] getMessages(final String[] names) {
		final String[] messages = new String[names.length];
		for (int i = 0; i < names.length; i++) {
			messages[i] = this.getMessage(names[i]);
		}
		return messages;
	}

	public LocalizedMessage getLocalizedMessage(final String name) {
		return this.getLocalizedMessage(name, null, this.getLocale());
	}

	public LocalizedMessage getLocalizedMessage(final String name, final String[] args, final Locale locale) {
		LocalizedMessage localizedMessage = null;
		if (this.enableRepositoryTexts) {
			localizedMessage = this.localizedMessageRepository.findByKey(name);
		}
		if (localizedMessage == null) {
			String message = null;
			try {
				message = this.messageSource.getMessage(name, args, locale);
			} catch (final NoSuchMessageException e) {
				message = String.join("", LocalizedMessageService.MISSING_PREFIX, name, " [", locale.toLanguageTag(), "]");
			}
			localizedMessage = new LocalizedMessage();
			localizedMessage.setValue(message, locale);
			localizedMessage.setMissing(message == null);
		} else if (args != null) {
			for (int i = 0; i < args.length; i++) {
				final String value = localizedMessage.getValue(locale).replace("{" + i + "}", args[i]);
				localizedMessage.setValue(value, locale);
			}
		}
		return localizedMessage;
	}

	/**
	 * Return selected locale
	 *
	 * @return
	 */
	public Locale getLocale() {
		try {
			return this.sessionContext.getCurrentLocale();
		} catch (final Exception e) {
			return this.getDefaultLocale();
		}
	}

	public Locale getDefaultLocale() {
		if (this.defaultLanguageId != null) {
			final Language language = this.languageRepository.findById(Long.valueOf(this.defaultLanguageId));
			if (language != null) {
				return language.getLocale();
			}
		}
		return new Locale("ca", "ES");
	}

}
