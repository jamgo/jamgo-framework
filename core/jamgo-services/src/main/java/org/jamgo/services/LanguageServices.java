package org.jamgo.services;

import java.util.Set;

import org.jamgo.model.entity.Language;

public interface LanguageServices {

	Set<Language> getAll();

	Language get(Long id);

	Language get(String languageCode, String countryCode);

	Language get(String languageCode);

	Language getDefaultLanguage();

}
