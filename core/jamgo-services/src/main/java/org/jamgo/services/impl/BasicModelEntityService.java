package org.jamgo.services.impl;

import java.util.Collection;
import java.util.List;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.BasicModelEntity;
import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.SecuredObject;
import org.jamgo.model.repository.BasicModelEntityRepository;
import org.jamgo.model.repository.RepositoryManager;
import org.jamgo.model.search.DummySearchSpecification;
import org.jamgo.model.util.OffsetSizePageRequest;
import org.jamgo.services.LanguageServices;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.blazebit.persistence.PagedList;

public abstract class BasicModelEntityService<ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE> implements InitializingBean {

	@Autowired
	protected RepositoryManager repositoryManager;
	@Autowired
	private SecurityService securityService;
	@Autowired
	protected LanguageServices languageService;

	protected Class<ENTITY> modelClass;

	@SuppressWarnings("unchecked")
	@Override
	public void afterPropertiesSet() throws Exception {
		this.modelClass = (Class<ENTITY>) ResolvableType.forClass(this.getClass()).getSuperType().getGeneric(0).resolve();
	}

	@Transactional
	public ENTITY save(final ENTITY item) {
		return this.basicSave(item);
	}

	@Transactional
	protected ENTITY basicSave(final ENTITY item) {
		return this.getRepository(item).persist(item);
	}

	@Transactional
	public ENTITY save(final ENTITY item, final SecuredObject securedObject) {
		final ENTITY savedItem = this.save(item);
		if (securedObject != null) {
			this.securityService.save(securedObject);
		} else {
			this.securityService.removeSecurityFromEntity(item);
		}
		return savedItem;
	}

	@Transactional
	public List<ENTITY> save(final List<ENTITY> items) {
		return this.basicSave(items);
	}

	@Transactional
	protected List<ENTITY> basicSave(final List<ENTITY> items) {
		List<ENTITY> savedItems = null;
		if (!CollectionUtils.isEmpty(items)) {
			savedItems = this.getRepository(items.get(0)).persistAll(items);
		}
		return savedItems;
	}

	private BasicModelEntityRepository<ENTITY, ?> getRepository(final ENTITY item) {
		return this.repositoryManager.getRepository(item);
	}

	private BasicModelEntityRepository<ENTITY, ID_TYPE> getRepository(final Class<ENTITY> entityClass) {
		return this.repositoryManager.getRepository(entityClass);
	}

	@Transactional
	public <V extends BasicModel<?>> void delete(final Collection<V> items) {
		this.delete(items, this.modelClass);
	}

	@Transactional
	public <V extends BasicModel<?>> void delete(final Collection<V> items, final Class<ENTITY> entityClass) {
		this.basicDelete(items, entityClass);
	}

	@SuppressWarnings("unchecked")
	@Transactional
	protected <BASIC_MODEL extends BasicModel<?>> void basicDelete(final Collection<BASIC_MODEL> items, final Class<ENTITY> entityClass) {
		for (final BASIC_MODEL eachItem : items) {
			final BasicModelEntityRepository<ENTITY, ID_TYPE> repository = this.getRepository(entityClass);
			repository.remove(entityClass, (ID_TYPE) eachItem.getId());
		}
	}

	public List<ENTITY> findAll() {
		return this.repositoryManager.getRepository(this.modelClass).findAll();
	}

	public <RESULT extends BasicModel<ID_TYPE>> List<RESULT> findAll(final Class<RESULT> resultClass) {
		return this.repositoryManager.getRepository(this.modelClass).findAll(resultClass);
	}

	public PagedList<ENTITY> findAll(final OffsetSizePageRequest pageRequest) {
		return this.repositoryManager.getRepository(this.modelClass).findAll(new DummySearchSpecification(), pageRequest);
	}

	public ENTITY findById(final ID_TYPE id) {
		return this.repositoryManager.getRepository(this.modelClass).findById(id);
	}

	public <RESULT extends BasicModel<ID_TYPE>> RESULT findById(final ID_TYPE id, final Class<RESULT> resultClass) {
		return this.repositoryManager.getRepository(this.modelClass).findById(id, resultClass);
	}

	public ENTITY createReference(final ID_TYPE id) {
		return this.repositoryManager.getRepository(this.modelClass).createReference(id);
	}

	protected LocalizedString createLocalizedString(final String value) {
		final LocalizedString localizedString = new LocalizedString();
		for (final Language eachLanguage : this.languageService.getAll()) {
			localizedString.set(eachLanguage.getLanguageCode(), value);
		}
		return localizedString;
	}

	public Class<ENTITY> getModelClass() {
		return this.modelClass;
	}

}
