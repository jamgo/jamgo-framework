package org.jamgo.services.session;

import java.util.Locale;

import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.User;
import org.jamgo.model.enums.Permission;

/**
 * Class used to group all attributes related to session.
 *
 * Can be used with different scopes depending on the application. The implementation is not
 * annotated to avoid been automatically scaned by Spring. It must be instantiated explicitly
 * in a configuration class. Eg:
 * <br><br>
 * Spring MVC application:
 *
 * <pre><code>
 *	{@literal @}Bean
 *	{@literal @}Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
 *	public SessionContext sessionContext() {
 *		return new SessionContextImpl();
 *	}
 * </code></pre>
 * Spring + Vaadin appication:
 *
 * <pre><code>
 *	{@literal @}Bean
 *	{@literal @}VaadinSessionScope
 *	{@literal @}Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
 *	public SessionContext sessionContext() {
 *		return new SessionContextImpl();
 *	}
 * </code></pre>
 *
 * In general, it should be extended in each application to add all the session attributes
 * specific to that application.<br><br>
 *
 * @author martin
 *
 */
public interface SessionContext {

	void reset();

	Language getCurrentLanguage();

	Locale getCurrentLocale();

	void setCurrentLocale(Locale currentLocale);

	void setCurrentLanguage(Language currentLanguage);

	Permission getPermission(Long securedObejctId);

	User getCurrentUser();

	void setCurrentUser(User user);
}
