package org.jamgo.services.exception;

public class NoClassParentRelationDefinedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NoClassParentRelationDefinedException(final Class<?> clazz, final Class<?> parent) {
		super("Can't find items for class " + clazz.getName() + " with parent " + parent.getName());
	}
}