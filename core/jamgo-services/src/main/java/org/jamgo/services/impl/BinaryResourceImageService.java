package org.jamgo.services.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import javax.imageio.ImageIO;

import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.BinaryResourceImage;
import org.jamgo.model.repository.BinaryResourceImageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import net.coobird.thumbnailator.Thumbnails;

@Service
public class BinaryResourceImageService extends ModelService<BinaryResourceImage> {

	public static final String PDF_IMAGE_NAME = "THUMBNAIL";

	public static final Logger logger = LoggerFactory.getLogger(BinaryResourceImageService.class);

	@Value("#{${binaryResource.images:{}}}")
	private Map<String, String> binaryResourceImagesProperties;

	@Autowired
	private BinaryResourceImageRepository repository;
	@Autowired
	private DownloadService downloadService;

	@Override
	public void afterPropertiesSet() throws Exception {
		super.afterPropertiesSet();
		BinaryResourceImageService.logger.info("Ready to generate images for all BinaryResource: {}", Optional.ofNullable(this.binaryResourceImagesProperties).map(o -> o.toString()).orElse("<none>"));
	}

	public Map<String, String> getBinaryResourceImagesProperties() {
		return this.binaryResourceImagesProperties;
	}

	public void createBinaryResourceImages(final BinaryResource binaryResource) {
		if (this.isImage(binaryResource)) {
			// Create "images" (thumbnails) for the binaryResource based on configuration
			if (this.binaryResourceImagesProperties != null) {
				this.binaryResourceImagesProperties.forEach((key, value) -> this.createBinaryResourceImage(binaryResource, key, value));
			}
		} else if (this.isPdf(binaryResource)) {
			this.createPdfThumbnail(binaryResource);
		} else {
			// TODO: create images from different object types (ods, odt,...)
		}
	}

	private void createPdfThumbnail(final BinaryResource binaryResource) {
		// Check that an image with the same name does not exist
		if (binaryResource.getImages().stream().anyMatch(i -> i.getName().equals(BinaryResourceImageService.PDF_IMAGE_NAME))) {
			return;
		}

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		String imageSize = null;
		try {
			final PDDocument document = Loader.loadPDF(binaryResource.getByteContents());
			final PDFRenderer pdfRenderer = new PDFRenderer(document);
			final BufferedImage image = pdfRenderer.renderImage(0, 0.5f, ImageType.RGB);
			imageSize = String.format("%dx%d", image.getWidth(), image.getHeight());
			ImageIO.write(image, "PNG", baos);

			final byte[] bytes = baos.toByteArray();

			final BinaryResourceImage thumbnail = new BinaryResourceImage();
			thumbnail.setSource(binaryResource);
			thumbnail.setName(BinaryResourceImageService.PDF_IMAGE_NAME);
			thumbnail.setSize(imageSize);
			thumbnail.setMimeType("image/png");
			thumbnail.setFileLength(bytes.length);
			thumbnail.setByteContents(bytes);
			this.save(thumbnail);
		} catch (final IOException e) {
			BinaryResourceImageService.logger.warn("Unable to create pdf thumbnail", e);
		}

	}

	/**
	 * Creates a binary resource image using the received parameters, if it not exists
	 *
	 * @param binaryResource source image
	 * @param name image name
	 * @param size image size in format "[width]x[height]", e.g. "300x200"
	 */
	private void createBinaryResourceImage(final BinaryResource binaryResource, final String name, final String size) {
		// Check that an image with the same name does not exist
		if (binaryResource.getImages().stream().anyMatch(i -> i.getName().equals(name))) {
			return;
		}

		final String[] parts = size.split("x");
		final int width = Integer.parseInt(parts[0]);
		final int height = Integer.parseInt(parts[1]);
		try {
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Thumbnails.of(new ByteArrayInputStream(binaryResource.getByteContents())).size(width, height).toOutputStream(baos);

			final BinaryResourceImage image = new BinaryResourceImage();
			image.setSource(binaryResource);
			image.setName(name);
			image.setSize(size);
			image.setMimeType(binaryResource.getMimeType());
			image.setFileLength(baos.size());
			image.setByteContents(baos.toByteArray());
			this.save(image);
		} catch (final IOException e) {
			throw new RuntimeException("Error generating binary resource image", e);
		}
	}

	private boolean isImage(final BinaryResource binaryResource) {
		return this.downloadService.isImage(binaryResource);
	}

	private boolean isPdf(final BinaryResource binaryResource) {
		return this.downloadService.isPdf(binaryResource);
	}

	public BinaryResourceImage findByName(final Long id, final String name) {
		return this.repository.findByName(id, name);
	}

}
