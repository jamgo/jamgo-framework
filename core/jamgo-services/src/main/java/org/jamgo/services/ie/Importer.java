package org.jamgo.services.ie;

import java.io.InputStream;
import java.util.List;

import org.jamgo.model.entity.BasicModelEntity;
import org.jamgo.model.portable.ImportStats;
import org.jamgo.services.ie.AbstractImporter.FileType;
import org.springframework.transaction.annotation.Transactional;

public interface Importer<ENTITY extends BasicModelEntity<?>> {

	ImportStats<ENTITY> extractData(InputStream is) throws Exception;

	ImportStats<ENTITY> updateImportCounts();

	String[] getColumnNames();

	String[] getColumnHeaders();

	List<String> getErrors();

	@Transactional
	ImportStats<ENTITY> doImport();

	void setDeleteAllBeforeImport(Boolean value);

	boolean deleteAllBeforeImport();

	InputStream generateSample();

	FileType getFileType();

}
