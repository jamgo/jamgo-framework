package org.jamgo.services.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.repository.BinaryResourceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BinaryResourceService extends ModelService<BinaryResource> {

	public static final Logger logger = LoggerFactory.getLogger(BinaryResourceService.class);

	@Autowired
	private BinaryResourceImageService binaryResourceImageService;
	@Autowired
	private BinaryResourceRepository binaryResourceRepository;

	@Transactional
	public BinaryResource createBinaryResource(final byte[] contents, final String mimeType, final String fileName) {
		final BinaryResource binaryResource = new BinaryResource(contents, mimeType, fileName);
		return this.createBinaryResource(binaryResource);
	}

	@Transactional
	public BinaryResource createBinaryResource(final byte[] contents, final String mimeType, final String fileName, final String description) {
		final BinaryResource binaryResource = new BinaryResource(contents, mimeType, fileName, description);
		return this.createBinaryResource(binaryResource);
	}

	@Transactional
	public BinaryResource createBinaryResource(final BinaryResource binaryResource) {
		if (binaryResource.getTimeStamp() == null) {
			binaryResource.setTimeStamp(LocalDateTime.now());
		}

		final BinaryResource saved = super.save(binaryResource);
		this.binaryResourceImageService.createBinaryResourceImages(binaryResource);
		return saved;
	}

	@Transactional
	public void recreateBinaryResourceImages() {
		final List<BinaryResource> allBinaryResources = this.binaryResourceRepository.findAll();
		allBinaryResources.forEach(binaryResource -> this.recreateBinaryResourceImages(binaryResource));
	}

	private void recreateBinaryResourceImages(final BinaryResource binaryResource) {
		this.binaryResourceImageService.createBinaryResourceImages(binaryResource);
	}

	/**
	 * Gets the list of all binaryResources not attached to any object in the application.
	 */
	public List<BinaryResource> getPurgeCandidates() {
		return this.binaryResourceRepository.getPurgeCandidates();
	}

	/**
	 * Remove all binaryResources not attached to any object in the application.
	 *
	 */
	public void purge() {
		final List<BinaryResource> entitiesToRemove = this.binaryResourceRepository.getPurgeCandidates();
		this.binaryResourceRepository.remove(entitiesToRemove);
	}

}
