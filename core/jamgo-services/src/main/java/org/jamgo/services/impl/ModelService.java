package org.jamgo.services.impl;

import org.jamgo.model.entity.Model;

public abstract class ModelService<MODEL extends Model> extends BasicModelEntityService<MODEL, Long> {

	public ModelService() {
		super();
	}

}