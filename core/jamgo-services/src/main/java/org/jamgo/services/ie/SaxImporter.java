package org.jamgo.services.ie;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.jamgo.model.entity.Model;
import org.xml.sax.SAXException;

public abstract class SaxImporter<MODEL extends Model> extends AbstractImporter<MODEL> {

	@Override
	protected void doExtraction(InputStream inputStream) throws IOException, ParserConfigurationException, SAXException {
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        SAXParser saxParser = saxParserFactory.newSAXParser();
        SaxHandler<MODEL> saxHandler = this.getSaxHandler();
        saxParser.parse(inputStream, saxHandler);
       	this.setList(saxHandler.getList());
	}
	
	protected abstract SaxHandler<MODEL> getSaxHandler ();
	
	@Override
	public FileType getFileType() {
		return FileType.XML;
	}

}
