package org.jamgo.ui.component;

import java.util.Optional;
import java.util.function.Predicate;

import org.jamgo.model.entity.Model;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.Component;

public abstract class ModelToManyAction<T extends Model, P extends Model> {

	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;

	protected abstract Component getComponent(final T relatedObject);

	protected ModelToMany<T, P> modelToMany;

	protected Predicate<T> visiblePredicate;

	public ModelToManyAction(final ModelToMany<T, P> modelToMany) {
		super();
		this.modelToMany = modelToMany;
	}

	public ModelToManyAction(final ModelToMany<T, P> modelToMany, final Predicate<T> visiblePredicate) {
		this(modelToMany);
		this.visiblePredicate = visiblePredicate;
	}

	public boolean isVisible(final T item) {
		return Optional.ofNullable(this.visiblePredicate)
			.map(o -> o.test(item))
			.orElse(true);
	}
}