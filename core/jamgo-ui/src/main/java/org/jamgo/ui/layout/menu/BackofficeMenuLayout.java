package org.jamgo.ui.layout.menu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.AppRole;
import org.jamgo.model.entity.AppUser;
import org.jamgo.model.entity.Category;
import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.LocalizedMessage;
import org.jamgo.model.view.BinaryResourceTableView;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.ui.layout.BackofficeApplicationDef;
import org.jamgo.ui.layout.BackofficeLayoutDef;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.flow.component.icon.VaadinIcon;

public class BackofficeMenuLayout implements MenuLayout, InitializingBean {

	private static final Comparator<MenuItem> MENU_ITEM_COMPARATOR = Comparator.comparingInt(o -> o.getOrder());

	public enum BackofficeMenuGroup {
		ROOT, CONFIGURATION, CATEGORIES, FUNCTIONS
	}

	@Autowired
	protected ApplicationContext applicationContext;
	@Autowired
	protected BackofficeApplicationDef backofficeApplicationDef;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired(required = false)
	@MenuItemComponent
	protected List<ModelMenuItemDef<? extends BasicModel<Long>>> menuItems;
	@Autowired(required = false)
	@MenuItemComponent
	protected List<FunctionMenuItemDef> functionItems;

	protected MenuGroup rootMenuGroup;
	protected MenuGroup configurationGroup;
	protected MenuGroup categoriesGroup;
	protected MenuGroup functionsGroup;

	@Override
	public void afterPropertiesSet() throws Exception {
		// Create menu groups
		this.createRootMenuGroup();
		this.createMenuGroups();

		// Add menu items
		this.addConfigurationMenuItems();
		this.addModelMenuItems();
		this.addFunctionItems();

		// Finally, sort menu items
		this.removeEmptyGroups();
		this.sortMenuItems();
	}

	/**
	 * Creates the root menu for the application.
	 */
	protected void createRootMenuGroup() {
		final MenuGroup menuGroup = new MenuGroup();
		menuGroup.setNameSupplier(() -> this.messageSource.getMessage("application.title"));
		this.rootMenuGroup = menuGroup;
	}

	/**
	 * Creates menu groups for the application.
	 * Override this method in subclasses to create custom menu groups for the application.
	 */
	protected void createMenuGroups() {
		this.configurationGroup = this.createConfigurationGroup();
		this.categoriesGroup = this.createCategoriesGroup();
		this.functionsGroup = this.createFunctionsGroup();
	}

	protected MenuGroup createConfigurationGroup() {
		final MenuGroup menuGroup = new MenuGroup();
		menuGroup.setNameSupplier(() -> this.messageSource.getMessage("menu.configuration"));
		menuGroup.setIcon(VaadinIcon.COG.create());
		menuGroup.setOrder(1);
		this.rootMenuGroup.addMenuItem(menuGroup);
		return menuGroup;
	}

	protected MenuGroup createCategoriesGroup() {
		final MenuGroup menuGroup = new MenuGroup();
		menuGroup.setNameSupplier(() -> this.messageSource.getMessage("menu.auxiliarTables"));
		menuGroup.setIcon(VaadinIcon.TABLE.create());
		menuGroup.setOrder(2);
		this.rootMenuGroup.addMenuItem(menuGroup);
		return menuGroup;
	}

	protected MenuGroup createFunctionsGroup() {
		final MenuGroup menuGroup = new MenuGroup();
		menuGroup.setNameSupplier(() -> this.messageSource.getMessage("menu.functions"));
		menuGroup.setIcon(VaadinIcon.FUNCTION.create());
		menuGroup.setOrder(Integer.MAX_VALUE);
		this.rootMenuGroup.addMenuItem(menuGroup);
		return menuGroup;
	}

	protected void addConfigurationMenuItems() {
		// TODO: Create a "ModelMenuItem" for both classes in Identity module
		this.configurationGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(AppUser.class), 1));
		this.configurationGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(AppRole.class), 2));
		// FIXME: Add this menu item again when necessary
		// this.configurationGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(PermissionsLayout.class)));
		// TODO: Create a "ModelMenuItem" for each of these classes
		this.configurationGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(Language.class), 3));
		this.configurationGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(LocalizedMessage.class), 4));
		this.configurationGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(BinaryResourceTableView.class), 5));
	}

	private void addModelMenuItems() {
		if (this.menuItems != null) {
			this.menuItems.forEach(item -> this.addModelMenuItem(item));
		}
	}

	protected void addModelMenuItem(final ModelMenuItemDef<? extends BasicModel<Long>> modelMenuItemDef) {
		final MenuItem menuItem = this.createMenuItemFromDef(modelMenuItemDef);
		if (menuItem != null) {
			this.addModelMenuItemToGroup(modelMenuItemDef, menuItem);
		}
	}

	protected MenuItem createMenuItemFromDef(final MenuItemDef menuItemDef) {
		if (!this.canAccess(menuItemDef)) {
			return null;
		}
		final BackofficeLayoutDef layoutDef = this.backofficeApplicationDef.getLayoutDef(menuItemDef.getLayoutDefClass());
		final MenuItem menuItem = new MenuItem(layoutDef);
		if (menuItemDef.hasIcon()) {
			menuItem.setIcon(menuItemDef.getIcon().create());
		}
		menuItem.setOrder(menuItemDef.getOrder());
		return menuItem;
	}

	/**
	 * Adds the {@link ModelMenuItemDef} to a menu group, depending on the model classes related to the menu item.
	 * By default, subclasses of {@link Category} are located to categories group, and other classes, to the main menu.
	 * Override this method in subclasses to reorganize the menu items in the application.
	 * @param modelMenuItemDef
	 * @param menuItem
	 */
	protected void addModelMenuItemToGroup(final ModelMenuItemDef<? extends BasicModel<Long>> modelMenuItemDef, final MenuItem menuItem) {
		final boolean added = this.addMenuItemToGroup(modelMenuItemDef, menuItem);
		if (!added) {
			// Use default behaviour
			if (Category.class.isAssignableFrom(modelMenuItemDef.getModelClass())) {
				this.categoriesGroup.addMenuItem(menuItem);
			} else {
				this.rootMenuGroup.addMenuItem(menuItem);
			}
		}
	}

	protected boolean addMenuItemToGroup(final MenuItemDef menuItemDef, final MenuItem menuItem) {
		if (menuItemDef.getMenuGroup() != null) {
			// Add the menu item to its group
			switch (menuItemDef.getMenuGroup()) {
				case CONFIGURATION:
					this.configurationGroup.addMenuItem(menuItem);
					break;
				case CATEGORIES:
					this.categoriesGroup.addMenuItem(menuItem);
					break;
				case FUNCTIONS:
					this.functionsGroup.addMenuItem(menuItem);
					break;
				case ROOT:
					this.getRootMenuGroup().addMenuItem(menuItem);
					break;
			}
		}
		return (menuItemDef.getMenuGroup() != null);
	}

	private void addFunctionItems() {
		if (this.functionItems != null) {
			this.functionItems.forEach(item -> this.addFunctionMenuItem(item));
		}
	}

	protected void addFunctionMenuItem(final FunctionMenuItemDef functionMenuItemDef) {
		final MenuItem menuItem = this.createMenuItemFromDef(functionMenuItemDef);
		if (menuItem != null) {
			this.addFunctionMenuItemToGroup(functionMenuItemDef, menuItem);
		}
	}

	/**
	 * Adds the {@link FunctionMenuItemDef} to the functions group.
	 * Override this method in subclasses to reorganize the menu items in the application.
	 * @param menuItem
	 */
	protected void addFunctionMenuItemToGroup(final FunctionMenuItemDef functionMenuItemDef, final MenuItem menuItem) {
		final boolean added = this.addMenuItemToGroup(functionMenuItemDef, menuItem);
		if (!added) {
			this.functionsGroup.addMenuItem(menuItem);
		}
	}

	protected boolean canAccess(final MenuItemDef menuItemDef) {
		// TODO: Consultar ACL si el usuario tiene permiso para mostrar el modelmenuitem
		return true;
	}

	/**
	 * Removes from root menu all those menu groups without menu items
	 */
	protected void removeEmptyGroups() {
		this.removeEmptyGroups(this.rootMenuGroup);
	}

	private MenuGroup removeEmptyGroups(final MenuGroup menuGroup) {
		// Remove child menu groups without menu items
		final List<MenuItem> toRemove = new ArrayList<>();
		menuGroup.getMenuItems().stream()
			.filter(menuItem -> menuItem instanceof MenuGroup)
			.map(menuItem -> (MenuGroup) menuItem)
			.forEach(group -> this.addIfNotNull(this.removeEmptyGroups(group), toRemove));
		menuGroup.getMenuItems().removeAll(toRemove);

		// Once the menu group has purged empty child menu groups, checks if it needs to be removed
		if (!menuGroup.hasItems()) {
			return menuGroup;
		}
		return null;
	}

	private <T> void addIfNotNull(final T item, final List<T> list) {
		if (item != null) {
			list.add(item);
		}
	}

	/**
	 * Sort menu items of the same group.
	 * By default, menu items are shown in the same order they are added to the group.
	 * Override this method in subclasses to sort the menu items following an specific criteria.
	 */
	protected void sortMenuItems() {
		this.sortMenuItems(this.getRootMenuGroup());
	}

	private void sortMenuItems(final MenuGroup menuGroup) {
		Collections.sort(menuGroup.getMenuItems(), BackofficeMenuLayout.MENU_ITEM_COMPARATOR);
		menuGroup.getMenuItems().stream()
			.filter(item -> item instanceof MenuGroup)
			.map(item -> (MenuGroup) item)
			.forEach(group -> this.sortMenuItems(group));
	}

	@Override
	public MenuGroup getRootMenuGroup() {
		return this.rootMenuGroup;
	}

}
