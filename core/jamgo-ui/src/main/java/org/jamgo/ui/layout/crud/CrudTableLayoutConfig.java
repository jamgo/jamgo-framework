package org.jamgo.ui.layout.crud;

public abstract class CrudTableLayoutConfig {

	public abstract void initialize(final CrudTableLayout<?, ?, ?> crudTableLayout);

	public abstract void applyDefaultTo(CrudTableLayout<?, ?, ?> crudTableLayout);

	public abstract void applyMaximizedDetailsTo(CrudTableLayout<?, ?, ?> crudTableLayout);

}
