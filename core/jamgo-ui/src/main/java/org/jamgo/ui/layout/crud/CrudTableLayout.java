package org.jamgo.ui.layout.crud;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.BasicModelEntity;
import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.model.repository.BasicModelEntityRepository;
import org.jamgo.model.repository.RepositoryManager;
import org.jamgo.model.search.SearchSpecification;
import org.jamgo.services.exception.CrudException;
import org.jamgo.services.impl.CrudServices;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.ui.component.ModelGrid;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.jamgo.ui.layout.crud.CrudDetailsLayout.LayoutMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.util.CollectionUtils;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.GridMultiSelectionModel;
import com.vaadin.flow.component.grid.GridMultiSelectionModel.SelectAllCheckboxVisibility;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.ItemClickEvent;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.data.provider.DataProvider;

@CssImport(value = "./styles/crud-table-layout.css")
@CssImport(value = "./styles/vaadin-split-layout.css", themeFor = "vaadin-split-layout")
public abstract class CrudTableLayout<BASIC_MODEL extends BasicModel<ID_TYPE>, ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE> extends VerticalLayout implements InitializingBean {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(CrudTableLayout.class);

	@Autowired
	protected ApplicationContext applicationContext;
	@Autowired
	protected RepositoryManager repositoryManager;
	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	protected CrudServices crudServices;
	@Autowired
	protected CrudManager crudManager;

	protected HorizontalLayout toolBar;
	protected ModelGrid<BASIC_MODEL> table;
	protected VerticalLayout tableWrapper;
	protected CrudSearchLayout<? extends SearchSpecification> searchLayout;
	protected CrudLayout<?> detailsLayout;

	protected List<Button> toolBarButtons;
	protected Button addButton;
	protected Button deleteButton;
	protected Button searchButton;

	protected CrudLayoutDef<BASIC_MODEL, ENTITY> crudLayoutDef;
	protected CrudTableLayoutConfig tableLayoutConfig;

	private LayoutMode initialDetailsMode;
	private Span countText;
	private HorizontalLayout countBar;
	protected HorizontalLayout buttonsLayout;

	public CrudTableLayout(final CrudLayoutDef<BASIC_MODEL, ENTITY> crudLayoutDef) {
		super();
		this.crudLayoutDef = crudLayoutDef;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init(this.crudLayoutDef);
	}

	public void init(final CrudLayoutDef<BASIC_MODEL, ENTITY> crudLayoutDef) {
		this.crudLayoutDef = crudLayoutDef;
		this.tableLayoutConfig = this.crudLayoutDef.getTableLayoutConfig();
		if (crudLayoutDef.isMaximized()) {
			this.initialDetailsMode = LayoutMode.MAXIMIZED;
		} else {
			this.initialDetailsMode = LayoutMode.NORMAL;
		}

		// ...	Initialize GridLayout.
		this.addClassName("crud-table-layout");
		this.setSizeFull();
		this.setMargin(false);
		this.setSpacing(false);

		// ...	Add components.
		this.addToolBar();
		this.addTable();
		this.addDetailsLayout();
		this.addSearchLayout();

		this.tableLayoutConfig.initialize(this);
	}

	protected void addToolBar() {
		this.createToolBar();
	}

	private HorizontalLayout createCountBar() {
		this.countBar = new HorizontalLayout();
		if (this.crudLayoutDef.isCountEnabled()) {
			this.countText = this.componentBuilderFactory.createSpanBuilder()
				.setText(this.crudLayoutDef.getCountSupplier())
				.build();
			this.countBar.add(this.countText);
		}
		return this.countBar;
	}

	protected void addTable() {
		this.tableWrapper = new VerticalLayout();
		this.tableWrapper.setPadding(false);
		this.tableWrapper.add(this.createTable());
	}

	protected void addDetailsLayout() {
		if (this.crudLayoutDef.getDetailsLayoutClass() != null) {
			this.detailsLayout = this.applicationContext.getBean(this.crudLayoutDef.getDetailsLayoutClass());
			this.detailsLayout.setClassName("details");
			this.detailsLayout.setVisible(false);
			if (CrudBasicModelLayout.class.isAssignableFrom(this.crudLayoutDef.getDetailsLayoutClass())) {
				@SuppressWarnings("unchecked")
				final CrudBasicModelLayout<BasicModel<?>> crudBasicModelLayout = (CrudBasicModelLayout<BasicModel<?>>) this.detailsLayout;
				crudBasicModelLayout.setCrudLayoutDef(this.crudLayoutDef);
				crudBasicModelLayout.setOkHandler(obj -> this.saveDetails(obj));
				crudBasicModelLayout.setCancelHandler(obj -> this.attemptLostFocusDetails(obj, () -> CrudTableLayout.this.cancelDetails(this.detailsLayout)));
				crudBasicModelLayout.setMaximizeRestoreHandler(obj -> this.maximizeRestoreDetails(obj));
			}
		}
	}

	protected void addSearchLayout() {
		if (this.crudLayoutDef.getSearchLayoutClass() != null) {
			this.searchLayout = this.applicationContext.getBean(this.crudLayoutDef.getSearchLayoutClass());
			this.searchLayout.setClassName("search");
			final SearchSpecification searchSpecification = this.crudLayoutDef.getSearchSpecification();
			if (searchSpecification != null) {
				this.searchLayout.setTargetObject(searchSpecification);
			}
			this.searchLayout.initialize();
			this.searchLayout.setApplyHandler(obj -> this.applySearch(obj));
			this.searchLayout.setCloseHandler(obj -> this.closeSearch(obj));
			this.searchLayout.setCloseButtonVisible(this.crudLayoutDef.isSearchLayoutCloseable());
			this.searchLayout.setVisible(false);
		}
	}

	protected HorizontalLayout createToolBar() {
		this.toolBar = new HorizontalLayout();
		this.toolBar.setJustifyContentMode(JustifyContentMode.BETWEEN);
		this.toolBar.setAlignItems(FlexComponent.Alignment.CENTER);
		this.toolBar.setWidth(90, Unit.PERCENTAGE);

		this.buttonsLayout = new HorizontalLayout();
		this.toolBar.setClassName("tool-bar");
		if (this.crudLayoutDef.isAddEnabled() && this.crudLayoutDef.getDetailsLayoutClass() != null) {
			this.addButton = this.componentBuilderFactory.createButtonBuilder()
				.setText(this.messageSource.getMessage("action.add"))
				.build();
			this.addButton.addClickListener(event -> this.attemptLostFocusDetails(this.detailsLayout, () -> this.doAdd()));
			this.buttonsLayout.add(this.addButton);
		}
		if (this.isRemoveEnabled()) {
			this.deleteButton = this.componentBuilderFactory.createButtonBuilder()
				.setText(this.messageSource.getMessage("action.delete"))
				.build();
			this.deleteButton.addClickListener(event -> this.attemptLostFocusDetails(this.detailsLayout, () -> this.doConfirmDelete()));
			this.buttonsLayout.add(this.deleteButton);
		}
		if (this.isSearchEnabled() && (this.crudLayoutDef.getSearchLayoutClass() != null)) {
			this.searchButton = this.componentBuilderFactory.createButtonBuilder()
				.setText(this.messageSource.getMessage("action.search"))
				.build();
			this.searchButton.addClickListener(event -> this.attemptLostFocusDetails(this.detailsLayout, () -> this.doSearch()));
			this.buttonsLayout.add(this.searchButton);
		}
		this.toolBar.add(this.buttonsLayout);
		this.toolBar.add(this.createCountBar());

		return this.toolBar;
	}

	protected boolean isRemoveEnabled() {
		return this.crudLayoutDef.isRemoveEnabled();
	}

	protected boolean isSearchEnabled() {
		return this.crudLayoutDef.isSearchEnabled();
	}

	protected boolean isImportEnabled() {
		return this.crudLayoutDef.isImportEnabled();
	}

	protected boolean isExportEnabled() {
		return this.crudLayoutDef.isExportEnabled();
	}

	protected ConfirmDialog getRemoveMessageBox() {
		final Collection<BASIC_MODEL> selectedItems = this.table.getSelectedItems();

		final ConfirmDialog dialog = new ConfirmDialog();
		dialog.setHeader(this.messageSource.getMessage("dialog.delete.caption"));
		dialog.setText(String.format(this.messageSource.getMessage("dialog.delete.message"), selectedItems.size()));
		dialog.setCancelable(true);
		dialog.setCancelText(this.messageSource.getMessage("dialog.no"));
		dialog.setConfirmText(this.messageSource.getMessage("dialog.yes"));
		dialog.addConfirmListener(event -> CrudTableLayout.this.doRemove());

		return dialog;
	}

	protected ModelGrid<BASIC_MODEL> createTable() {
		this.table = this.componentBuilderFactory.createModelGridBuilder(this.crudLayoutDef.getModelClass()).build();
		this.table.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_ROW_STRIPES);
		if (this.crudLayoutDef.isRemoveEnabled()) {
			this.table.setSelectionMode(SelectionMode.MULTI);
			((GridMultiSelectionModel<BASIC_MODEL>) this.table.getSelectionModel()).setSelectAllCheckboxVisibility(SelectAllCheckboxVisibility.VISIBLE);
		} else {
			this.table.setSelectionMode(SelectionMode.SINGLE);
		}
		if (this.crudLayoutDef.getGridClassNameGenerator() != null) {
			this.table.setClassNameGenerator(this.crudLayoutDef.getGridClassNameGenerator());
		}
		if (this.crudLayoutDef.getDetailsLayoutClass() != null) {
			this.table.addItemClickListener(event -> this.attemptLostFocusDetails(this.detailsLayout, () -> this.doOpen(event)));
		}

		this.table.createColumns(this.crudLayoutDef.getColumnDefs());

		final DataProvider<BASIC_MODEL, Void> dataProvider = this.getDefaultDataProvider();
		this.table.setItems(dataProvider);

		return this.table;
	}

	protected DataProvider<BASIC_MODEL, Void> getDefaultDataProvider() {

		DataProvider<BASIC_MODEL, Void> dataProvider = null;
		try {
			if (this.crudLayoutDef.isEmptyWithoutFilter()) {
				dataProvider = this.getEmptyDataProvider();
			} else {
				if (this.crudLayoutDef.getSearchSpecification() != null) {
					dataProvider = this.crudManager.getDataProvider(this.crudLayoutDef.getEntityClass(), this.crudLayoutDef.getModelClass(), this.crudLayoutDef.getSearchSpecification(), this.crudLayoutDef.isCountOnEntity());
				} else {
					dataProvider = this.crudManager.getDataProvider(this.crudLayoutDef.getEntityClass(), this.crudLayoutDef.getModelClass(), this.crudLayoutDef.isCountOnEntity());
				}
			}
		} catch (final CrudException e) {
			CrudTableLayout.logger.error(e.getMessage(), e);
			this.showErrorNotification(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName());
		}

		return dataProvider;
	}

	protected DataProvider<BASIC_MODEL, Void> getEmptyDataProvider() {
		return new CallbackDataProvider<>(
			q -> new ArrayList<BASIC_MODEL>().stream(),
			q -> 0);
	}

	@Override
	public void onAttach(final AttachEvent event) {
		super.onAttach(event);
		if ((this.crudLayoutDef.getSearchLayoutClass() != null) &&
			this.crudLayoutDef.isSearchEnabled() &&
			this.crudLayoutDef.isShowSearchOnStart() &&
			((this.detailsLayout == null) || !this.detailsLayout.isVisible())) {

			this.doSearch();
		}
	}

	protected void doAdd() {
		try {
			final ENTITY item = this.getNewItem();
			this.detailsLayout.updateFields(item);
			this.showDetailsLayout();
		} catch (final Exception e) {
			CrudTableLayout.logger.error(e.getMessage(), e);
			this.showErrorNotification(this.messageSource.getMessage("error.general.add"));
		}
	}

	protected void doConfirmDelete() {
		if (CollectionUtils.isEmpty(this.table.getSelectedItems())) {
			Notification.show(this.messageSource.getMessage("warning.selection.empty"));
		} else {
			this.getRemoveMessageBox().open();
		}
	}

	protected void doRemove() {
		final Collection<BASIC_MODEL> selectedItems = this.table.getSelectedItems();
		try {
			this.crudServices.delete(selectedItems, this.crudLayoutDef.getEntityClass());
			this.table.deselectAll();

			// close detailsLayout if one of the deleted items is the one showing there
			if (selectedItems.contains(this.detailsLayout.getTargetObject())) {
				this.cancelDetails(this.detailsLayout);
			}
		} catch (final Exception e) {
			CrudTableLayout.logger.error(e.getMessage(), e);
			this.showErrorNotification(this.messageSource.getMessage("error.general.remove"));
		}
		this.refreshAll();
	}

	protected void doSearch() {
		try {
			this.searchLayout.updateFields();
			this.showSearchLayout();
		} catch (final Exception e) {
			CrudTableLayout.logger.error(e.getMessage(), e);
			this.showErrorNotification(this.messageSource.getMessage("error.general.search"));
		}
	}

	private void showErrorNotification(final String message) {
		final Notification notification = new Notification();
		notification.addThemeVariants(NotificationVariant.LUMO_ERROR);

		final Span label = new Span(message);

		final Button cancelButton = new Button(
			this.messageSource.getMessage("action.cancel"),
			e -> notification.close());
		cancelButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

		notification.add(label, cancelButton);
		notification.open();
	}

	public void openDetails(final ID_TYPE id) {
		try {
			final BasicModelEntityRepository<ENTITY, ID_TYPE> entityRepository = this.repositoryManager.getRepository(this.crudLayoutDef.getEntityClass());
			final ENTITY entity = entityRepository.findById(id);
			this.doOpen(entity);
		} catch (final RepositoryForClassNotDefinedException e) {
			throw new CrudException(e);
		}
	}

	protected void doOpen(final ItemClickEvent<BASIC_MODEL> event) {
		if (event.getColumn() != null) {
			this.table.deselectAll();
			final BASIC_MODEL item = event.getItem();
			this.table.select(item);
			this.openDetails(item.getId());
		}
	}

	protected void doOpen(final ENTITY entity) {
		this.detailsLayout.updateFields(this.getDetailsItem(entity));
		this.showDetailsLayout();
	}

	protected Object getDetailsItem(final BasicModel<?> item) {
		return item;
	}

	protected void setWritePermissions(final boolean hasWritePermission) {
		if (this.addButton != null) {
			this.addButton.setVisible(hasWritePermission);
		}
		if (this.deleteButton != null) {
			this.deleteButton.setVisible(hasWritePermission);
		}
	}

	protected void saveDetails(final CrudBasicModelLayout<?> detailsLayout) {
		ENTITY savedItem = null;
		try {
			savedItem = this.saveTargetObject();
			// TODO: show message humanized style.
			Notification.show(this.messageSource.getMessage("info.action.saved.entity"));
		} catch (final CrudException e) {
			CrudTableLayout.logger.error(e.getMessage(), e);
			this.showErrorNotification(this.messageSource.getMessage("error.general.save"));
		}
		detailsLayout.updateFields(savedItem);
		// ...	Workaround: Selected items are stored in a separated Set into the Grid.
		//		The method refreshAll() does not udates thas Set so, if another item is selected
		//		after calling refreshAll() the row values of the deselected item changes to the
		//		old values!!. To avoid that behavior we must deselect all items, refresh and select
		//		again.
		final Set<BASIC_MODEL> selectedItems = this.table.getSelectedItems();
		this.table.deselectAll();
		this.refreshAll();
		selectedItems.forEach(each -> this.table.select(each));
	}

	@SuppressWarnings("unchecked")
	protected ENTITY saveTargetObject() {
		return this.crudServices.save((ENTITY) this.detailsLayout.getTargetObject());
	}

	protected void attemptLostFocusDetails(final CrudLayout<?> detailsLayout, final Runnable action) {
		if ((detailsLayout != null) && this.isDetailsVisible() && detailsLayout.hasChanges()) {
			final ConfirmDialog dialog = new ConfirmDialog();
			dialog.setHeader(this.messageSource.getMessage("dialog.close"));
			dialog.setText(this.messageSource.getMessage("dialog.areyousure.close"));
			dialog.setCancelable(true);
			dialog.setCancelText(this.messageSource.getMessage("dialog.no"));
			dialog.setConfirmText(this.messageSource.getMessage("dialog.yes"));
			dialog.addConfirmListener(event -> action.run());
			dialog.open();
		} else {
			action.run();
		}
	}

	protected void cancelDetails(final CrudLayout<?> detailsLayout) {
		this.hideDetailsLayout();
		this.toolBar.setVisible(true);
		this.tableWrapper.setVisible(true);
		if ((this.searchLayout != null) && (this.searchLayout.getTargetObject() != null)) {
			this.showSearchLayout();
		}
		this.tableLayoutConfig.applyDefaultTo(this);
	}

	protected void maximizeRestoreDetails(final LayoutMode mode) {
		if (mode == LayoutMode.MAXIMIZED) {
			this.maximizeDetailsLayout();
		} else {
			this.restoreDetailsLayout();
		}
	}

	protected void applySearch(final CrudSearchLayout<?> searchLayout) {
		final SearchSpecification searchSpecification = searchLayout.getTargetObject();
		try {
			this.table.setItems(this.crudManager.getDataProvider(this.crudLayoutDef.getEntityClass(), this.crudLayoutDef.getModelClass(), searchSpecification, this.crudLayoutDef.isCountOnEntity()));
		} catch (final CrudException e) {
			this.showErrorNotification(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName());
		}
		this.refreshAll();
	}

	protected void closeSearch(final CrudSearchLayout<?> searchLayout) {
		this.hideSearchLayout();
		this.searchLayout.setTargetObject(null);
		this.table.setItems(this.getDefaultDataProvider());
	}

	public <S> void showSearchLayout(final S searchObject) {
		if (this.searchLayout != null) {
			if (!this.isSearchVisible()) {
				this.hideDetailsLayout();
				this.searchLayout.setVisible(true);
				this.tableLayoutConfig.applyDefaultTo(this);
			}
			if (searchObject != null) {
				this.searchLayout.updateFields(searchObject);
				this.searchLayout.doApply();
			}
		}
	}

	protected void showSearchLayout() {
		this.showSearchLayout(null);
	}

	protected void hideSearchLayout() {
		if (this.isSearchVisible() && (this.searchLayout != null)) {
			this.searchLayout.setVisible(false);
			this.tableLayoutConfig.applyDefaultTo(this);
		}
	}

	protected void showDetailsLayout() {
		if (!this.isDetailsVisible() && (this.detailsLayout != null)) {
			this.hideSearchLayout();
			this.detailsLayout.setVisible(true);
			if (this.initialDetailsMode == LayoutMode.MAXIMIZED) {
				this.maximizeDetailsLayout();
			} else {
				this.restoreDetailsLayout();
			}
		}
	}

	protected void hideDetailsLayout() {
		if (this.isDetailsVisible() && (this.detailsLayout != null)) {
			this.detailsLayout.setVisible(false);
			this.tableLayoutConfig.applyDefaultTo(this);
		}
	}

	protected void maximizeDetailsLayout() {
		this.toolBar.setVisible(false);
		this.tableWrapper.setVisible(false);
		if (this.isSearchVisible()) {
			this.searchLayout.setVisible(false);
		}
		this.tableLayoutConfig.applyMaximizedDetailsTo(this);
	}

	protected void restoreDetailsLayout() {
		this.toolBar.setVisible(true);
		this.tableWrapper.setVisible(true);
		Optional.ofNullable(this.searchLayout).ifPresent(obj -> obj.setVisible(false));
		this.tableLayoutConfig.applyDefaultTo(this);
	}

	protected void refreshAll() {
		this.table.getDataProvider().refreshAll();
	}

	private ENTITY getNewItem() throws Exception {
		return this.crudLayoutDef.getEntityClass().getDeclaredConstructor().newInstance();
	}

	public HorizontalLayout getToolBar() {
		return this.toolBar;
	}

	public CrudSearchLayout<?> getSearchLayout() {
		return this.searchLayout;
	}

	public CrudLayout<?> getDetailsLayout() {
		return this.detailsLayout;
	}

	public Grid<BASIC_MODEL> getTable() {
		return this.table;
	}

	public VerticalLayout getTableWrapper() {
		return this.tableWrapper;
	}

	public Boolean isToolbarVisible() {
		return (this.toolBar != null) && this.toolBar.isVisible();
	}

	public Boolean isTableVisible() {
		return (this.tableWrapper != null) && this.tableWrapper.isVisible();
	}

	public Boolean isSearchVisible() {
		return (this.searchLayout != null) && this.searchLayout.isVisible();
	}

	public Boolean isDetailsVisible() {
		return (this.detailsLayout != null) && this.detailsLayout.isVisible();
	}

}
