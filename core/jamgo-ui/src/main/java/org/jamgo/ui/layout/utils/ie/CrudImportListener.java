package org.jamgo.ui.layout.utils.ie;

/**
 *
 * @author jamgo
 *
 * @deprecated Use {@link org.jamgo.services.ie.CrudImportListener} from jamgo-services module instead
 *
 */
@Deprecated
public interface CrudImportListener {

	void importSucceeded();

}
