package org.jamgo.ui.layout.defs;

import java.util.Optional;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.view.BinaryResourceTableView;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.details.BinaryResourceDetailsLayout;
import org.jamgo.ui.layout.search.BinaryResourceSearchLayout;
import org.jamgo.ui.layout.table.BinaryResourceTableLayout;

@LayoutDefComponent
public class BinaryResourceLayoutDef extends BasicModelLayoutDef<BinaryResourceTableView, BinaryResource> {

	public BinaryResourceLayoutDef(final LocalizedMessageService messageSource, final LocalizedObjectService localizedObjectService) {
		super(messageSource, localizedObjectService);
	}

	@SuppressWarnings("unchecked")
	@Override
	public CrudLayoutDef<BinaryResourceTableView, BinaryResource> getBackofficeLayoutDef() {
		// @formatter:off
		return (CrudLayoutDef<BinaryResourceTableView, BinaryResource>) CrudLayoutDef.<BinaryResourceTableView, BinaryResource> builder()
			.setId("language")
			.setNameSupplier(() -> this.messageSource.getMessage("table.binaryResource"))
			.modelGridDef()
				.setEntityClass(BinaryResource.class)
				.setModelClass(BinaryResourceTableView.class)
				.setCountOnEntity(true)
				.columnDef()
					.setId("id")
					.setCaptionSupplier(() -> this.messageSource.getMessage("generic.entity.id"))
					.setValueProvider(o -> o.getId())
				.end()
				.columnDef()
					.setId("fileName")
					.setCaptionSupplier(() -> this.messageSource.getMessage("binaryresource.fileName"))
					.setValueProvider(o -> o.getFileName())
				.end()
				.columnDef()
					.setId("description")
					.setCaptionSupplier(() -> this.messageSource.getMessage("binaryresource.description"))
					.setValueProvider(o -> o.getDescription())
				.end()
				.columnDef()
					.setId("fileLength")
					.setCaptionSupplier(() -> this.messageSource.getMessage("binaryresource.fileLength"))
					.setValueProvider(o -> o.getFileLength())
				.end()
				.columnDef()
					.setId("mimeType")
					.setCaptionSupplier(() -> this.messageSource.getMessage("binaryresource.mimeType"))
					.setValueProvider(o -> o.getMimeType())
				.end()
				.columnDef()
					.setId("timeStamp")
					.setCaptionSupplier(() -> this.messageSource.getMessage("binaryresource.timeStamp"))
					.setValueProvider(o -> Optional.ofNullable(o.getTimeStamp()).map(t -> AbstractLayoutDef.DATETIME_FORMATTER.format(t)).orElse(null))
				.end()
//				.columnDef()
//					.setId("images")
//					.setCaptionSupplier(() -> this.messageSource.getMessage("binaryresource.images"))
//					.setValueProvider(o -> o.getBinaryResourceImageCount())
//				.end()
				.columnDef()
					.setId("imageNames")
					.setCaptionSupplier(() -> this.messageSource.getMessage("binaryresource.images"))
					.setValueProvider(o -> Optional.ofNullable(o.getBinaryResourceImageNames()).map(names -> String.join(",", names)).orElse("-"))
				.end()
				.detailsLayoutClass(BinaryResourceDetailsLayout.class)
			.end()
			.tableLayoutVerticalConfig()
			.setAddEnabled(false)
			.setSearchEnabled(true)
			.searchLayoutClass(BinaryResourceSearchLayout.class)
			.tableLayoutClass(BinaryResourceTableLayout.class)
			.build();
		// @formatter:on
	}

	@Override
	public Class<BinaryResourceTableView> getModelClass() {
		return BinaryResourceTableView.class;
	}

}
