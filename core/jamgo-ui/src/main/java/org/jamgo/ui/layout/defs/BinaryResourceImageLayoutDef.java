package org.jamgo.ui.layout.defs;

import org.jamgo.model.entity.BinaryResourceImage;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.details.BinaryResourceImageDetailsLayout;

@LayoutDefComponent
public class BinaryResourceImageLayoutDef extends ModelLayoutDef<BinaryResourceImage> {

	public BinaryResourceImageLayoutDef(final LocalizedMessageService messageSource, final LocalizedObjectService localizedObjectService) {
		super(messageSource, localizedObjectService);
	}

	@SuppressWarnings("unchecked")
	@Override
	public CrudLayoutDef<BinaryResourceImage, BinaryResourceImage> getBackofficeLayoutDef() {
		// @formatter:off
		return (CrudLayoutDef<BinaryResourceImage, BinaryResourceImage>) CrudLayoutDef.<BinaryResourceImage> builderEntity()
			.setId("language")
			.setNameSupplier(() -> this.messageSource.getMessage("table.binaryResourceImage"))
			.modelGridDef()
				.setEntityClass(BinaryResourceImage.class)
				.columnDef()
					.setId("name")
					.setCaptionSupplier(() -> this.messageSource.getMessage("binaryresourceimage.name"))
					.setValueProvider(o -> o.getName())
				.end()
				.columnDef()
					.setId("description")
					.setCaptionSupplier(() -> this.messageSource.getMessage("binaryresourceimage.size"))
					.setValueProvider(o -> o.getSize())
				.end()
				.columnDef()
					.setId("fileLength")
					.setCaptionSupplier(() -> this.messageSource.getMessage("binaryresourceimage.fileLength"))
					.setValueProvider(o -> o.getFileLength())
				.end()
				.columnDef()
					.setId("mimeType")
					.setCaptionSupplier(() -> this.messageSource.getMessage("binaryresourceimage.mimeType"))
					.setValueProvider(o -> o.getMimeType())
				.end()
				.detailsLayoutClass(BinaryResourceImageDetailsLayout.class)
			.end()
			.tableLayoutVerticalConfig()
			.build();
		// @formatter:on
	}

	@Override
	public Class<BinaryResourceImage> getModelClass() {
		return BinaryResourceImage.class;
	}

}
