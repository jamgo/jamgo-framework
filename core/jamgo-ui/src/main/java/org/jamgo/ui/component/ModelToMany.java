package org.jamgo.ui.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.jamgo.model.entity.Model;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Focusable;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.HasTheme;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.GridMultiSelectionModel;
import com.vaadin.flow.component.grid.GridMultiSelectionModel.SelectAllCheckboxVisibility;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.dataview.GridDataView;
import com.vaadin.flow.component.grid.dataview.GridLazyDataView;
import com.vaadin.flow.component.grid.dataview.GridListDataView;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.event.SortEvent;
import com.vaadin.flow.data.event.SortEvent.SortNotifier;
import com.vaadin.flow.data.provider.BackEndDataProvider;
import com.vaadin.flow.data.provider.DataGenerator;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.HasDataGenerators;
import com.vaadin.flow.data.provider.HasDataView;
import com.vaadin.flow.data.provider.HasLazyDataView;
import com.vaadin.flow.data.provider.HasListDataView;
import com.vaadin.flow.data.provider.InMemoryDataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.selection.MultiSelect;
import com.vaadin.flow.data.selection.MultiSelectionListener;
import com.vaadin.flow.shared.Registration;

public abstract class ModelToMany<MODEL extends Model, PARENT_MODEL extends Model> extends ModelSelect<MODEL> implements
	HasStyle,
	HasSize,
	MultiSelect<ModelGrid<MODEL>, MODEL>,
	Focusable<ModelToMany<MODEL, PARENT_MODEL>>,
	SortNotifier<ModelToMany<MODEL, PARENT_MODEL>, GridSortOrder<MODEL>>,
	HasTheme,
	HasDataGenerators<MODEL>,
	HasListDataView<MODEL, GridListDataView<MODEL>>,
	HasDataView<MODEL, Void, GridDataView<MODEL>>,
	HasLazyDataView<MODEL, Void, GridLazyDataView<MODEL>> {

	protected Supplier<PARENT_MODEL> parentModelSupplier;
	protected BiConsumer<MODEL, PARENT_MODEL> addToParent;
	protected BiConsumer<MODEL, PARENT_MODEL> deleteFromParent;
	protected ModelGrid<MODEL> modelGrid;
	protected HorizontalLayout buttonsLayout;
	protected HorizontalLayout actionButtonsLayout;
	protected final List<ModelToManyAction<MODEL, PARENT_MODEL>> actions;
	private boolean deleteVisible = true;
	private Predicate<MODEL> deleteVisiblePredicate;
	protected Consumer<Collection<MODEL>> deleteValidator;
	protected Consumer<Collection<MODEL>> deleteListener;
	private final Set<ModelToMany<MODEL, PARENT_MODEL>> linked;

	public ModelToMany(final CrudLayoutDef<?, MODEL> crudLayoutDef) {
		super();
		this.crudLayoutDef = crudLayoutDef;
		this.linked = new HashSet<>();
		this.actions = new ArrayList<>();
	}

	public void init() {
		this.getContent().setWidth(100, Unit.PERCENTAGE);
		this.getContent().setPadding(false);
		this.getContent().setMargin(false);
		this.getContent().setSpacing(true);

		this.modelGrid = (ModelGrid<MODEL>) this.componentBuilderFactory.createModelGridBuilder(this.crudLayoutDef.getModelClass())
			.setWidth(100, Unit.PERCENTAGE)
			.setAllRowsVisible(true)
			.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_ROW_STRIPES)
			.build();

		if (this.isDeleteVisible()) {
			this.modelGrid.setSelectionMode(SelectionMode.MULTI);
			((GridMultiSelectionModel<MODEL>) this.modelGrid.getSelectionModel()).setSelectAllCheckboxVisibility(SelectAllCheckboxVisibility.VISIBLE);
		} else {
			this.modelGrid.setSelectionMode(SelectionMode.SINGLE);
		}

		this.createColumns();
		this.initializeButtonsLayout();

		this.getContent().add(this.modelGrid);
	}

	protected void initializeButtonsLayout() {
		this.buttonsLayout = this.componentBuilderFactory.createHorizontalLayoutBuilder()
			.build();
		this.buttonsLayout.setMargin(false);
		this.buttonsLayout.setPadding(false);
		this.buttonsLayout.setSpacing(false);

		this.createButtons().forEach(each -> this.buttonsLayout.add(each));
	}

	protected List<Component> createButtons() {
		this.createButton = this.componentBuilderFactory.createButtonBuilder()
			.addClassName("to-many-action-button")
			.setIcon(VaadinIcon.PLUS.create())
			.setVisible(this.createVisible)
			.build();
		this.createButton.addClickListener(event -> this.doCreateNew());
		return Lists.newArrayList(this.createButton);
	}

	protected void createColumns() {
		this.modelGrid.createColumns(this.crudLayoutDef.getColumnDefs());
		this.modelGrid.addComponentColumn(o -> this.createActionButtonsLayout(o)).setFlexGrow(0);
	}

	protected HorizontalLayout createActionButtonsLayout(final MODEL relatedObject) {
		this.actionButtonsLayout = new HorizontalLayout();
		this.actionButtonsLayout.setClassName("to-many-row-action-buttons");
		this.actionButtonsLayout.setSpacing(false);

		this.actions.forEach(each -> {
			if (each.isVisible(relatedObject)) {
				this.actionButtonsLayout.add(each.getComponent(relatedObject));
			}
		});

		return this.actionButtonsLayout;
	}

	protected void doDelete(final MODEL item) {
		this.doDelete(Lists.newArrayList(item));
	}

	protected void doDelete(final List<MODEL> items) {
		this.deleteContents(items);
		this.linked.forEach(each -> each.deleteContents(items));
	}

	protected void deleteContents(final Collection<MODEL> deletedItems) {
		@SuppressWarnings("unchecked")
		final Set<MODEL> items = Sets.newHashSet(((ListDataProvider<MODEL>) this.modelGrid.getDataProvider()).getItems());
		if (items.removeAll(deletedItems) || CollectionUtils.isEmpty(items)) {
			this.modelGrid.setItems(new ListDataProvider<>(items));
		}
		if (this.deleteListener != null) {
			this.deleteListener.accept(deletedItems);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void updateContents(final Set<MODEL> items) {
		final Set<MODEL> currentItems = Sets.newHashSet(((ListDataProvider<MODEL>) this.modelGrid.getDataProvider()).getItems());
		currentItems.removeAll(items);
		currentItems.addAll(items);
		this.modelGrid.setItems(new ListDataProvider<>(currentItems));
	}

	@Override
	@SuppressWarnings("unchecked")
	public void addItem(final MODEL newItem) {
		this.addItems(Sets.newHashSet(newItem));
	}

	@Override
	public void addItems(final Set<MODEL> newItems) {
		if (this.addToParent != null) {
			newItems.forEach(each -> this.addToParent.accept(each, this.getParentModel()));
		}

		this.updateContents(newItems);
		this.linked.forEach(each -> each.updateContents(newItems));
	}

	public PARENT_MODEL getParentModel() {
		return Optional.ofNullable(this.parentModelSupplier)
			.map(o -> o.get())
			.orElse(null);
	}

	public Supplier<PARENT_MODEL> getParentModelSupplier() {
		return this.parentModelSupplier;
	}

	public void setParentModelSupplier(final Supplier<PARENT_MODEL> parentModelSupplier) {
		this.parentModelSupplier = parentModelSupplier;
	}

	public BiConsumer<MODEL, PARENT_MODEL> getAddToParent() {
		return this.addToParent;
	}

	public void setAddToParent(final BiConsumer<MODEL, PARENT_MODEL> addToParent) {
		this.addToParent = addToParent;
	}

	public BiConsumer<MODEL, PARENT_MODEL> getDeleteFromParent() {
		return this.deleteFromParent;
	}

	public void setDeleteFromParent(final BiConsumer<MODEL, PARENT_MODEL> deleteFromParent) {
		this.deleteFromParent = deleteFromParent;
	}

	public HorizontalLayout getButtonsLayout() {
		return this.buttonsLayout;
	}

	public void addLinked(final ModelToMany<MODEL, PARENT_MODEL> linked) {
		this.linked.add(linked);
	}

	public Predicate<MODEL> getDeleteVisiblePredicate() {
		return this.deleteVisiblePredicate;
	}

	public void setDeleteVisiblePredicate(final Predicate<MODEL> deleteVisiblePredicate) {
		this.deleteVisiblePredicate = deleteVisiblePredicate;
	}

	// ...	Grid delegation methods.

	@Override
	public GridLazyDataView<MODEL> setItems(final BackEndDataProvider<MODEL, Void> dataProvider) {
		return this.modelGrid.setItems(dataProvider);
	}

	@Override
	public GridLazyDataView<MODEL> getLazyDataView() {
		return this.modelGrid.getLazyDataView();
	}

	@Override
	public GridDataView<MODEL> setItems(final DataProvider<MODEL, Void> dataProvider) {
		return this.modelGrid.setItems(dataProvider);
	}

	@Override
	public GridDataView<MODEL> setItems(final InMemoryDataProvider<MODEL> dataProvider) {
		return this.modelGrid.setItems(dataProvider);
	}

	@Override
	public GridDataView<MODEL> getGenericDataView() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GridListDataView<MODEL> setItems(final ListDataProvider<MODEL> dataProvider) {
		return this.modelGrid.setItems(dataProvider);
	}

	@Override
	public GridListDataView<MODEL> getListDataView() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Registration addDataGenerator(final DataGenerator<MODEL> generator) {
		return this.modelGrid.addDataGenerator(generator);
	}

	@Override
	public Registration addSortListener(final ComponentEventListener<SortEvent<ModelToMany<MODEL, PARENT_MODEL>, GridSortOrder<MODEL>>> listener) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void clearContents() {
		// TODO Auto-generated method stub
	}

	@Override
	public void setValue(final Set<MODEL> value) {
		this.modelGrid.setItems(new ListDataProvider<>(value));
	}

	@Override
	@SuppressWarnings("unchecked")
	public Set<MODEL> getSelectedItems() {
		return Sets.newHashSet(((ListDataProvider<MODEL>) this.modelGrid.getDataProvider()).getItems());
	}

	public DataProvider<MODEL, ?> getDataProvider() {
		return this.modelGrid.getDataProvider();
	}

	public boolean isDeleteVisible() {
		return this.deleteVisible;
	}

	public void setDeleteVisible(final boolean deleteVisible) {
		this.deleteVisible = deleteVisible;
	}

	public Consumer<Collection<MODEL>> getDeleteValidator() {
		return this.deleteValidator;
	}

	public void setDeleteValidator(final Consumer<Collection<MODEL>> deleteValidator) {
		this.deleteValidator = deleteValidator;
	}

	public Consumer<Collection<MODEL>> getDeleteListener() {
		return this.deleteListener;
	}

	public void setDeleteListener(final Consumer<Collection<MODEL>> deleteListener) {
		this.deleteListener = deleteListener;
	}

	@Override
	public Registration addValueChangeListener(final ValueChangeListener<? super ComponentValueChangeEvent<ModelGrid<MODEL>, Set<MODEL>>> listener) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateSelection(final Set<MODEL> addedItems, final Set<MODEL> removedItems) {
		// TODO Auto-generated method stub

	}

	@Override
	public Registration addSelectionListener(final MultiSelectionListener<ModelGrid<MODEL>, MODEL> listener) {
		// TODO Auto-generated method stub
		return null;
	}

}