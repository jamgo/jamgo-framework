package org.jamgo.ui.layout.crud;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.splitlayout.SplitLayout;

/*
 * ---------------------------------------------------
 * | toolbar                                         |
 * ---------------------------------------------------
 * | search      | table                             |
 * |             |                                   |
 * |             |                                   |
 * |             |                                   |
 * |             |                                   |
 * |             |                                   |
 * |             |                                   |
 * |             |                                   |
 * ---------------------------------------------------
 */

public class SelectTableLayoutVerticalConfig extends SelectTableLayoutConfig {

	@Override
	public void initialize(final SelectTableLayout<?, ?, ?> crudTableLayout) {
		if (crudTableLayout.getToolBar() != null) {
			crudTableLayout.add(crudTableLayout.getToolBar());
		}

		final SplitLayout tableDetailsLayout = new SplitLayout();
		tableDetailsLayout.setSizeFull();
		tableDetailsLayout.addToPrimary(crudTableLayout.getTableWrapper());

		if (crudTableLayout.getSearchLayout() != null) {
			final SplitLayout searchContentsLayout = new SplitLayout();
			searchContentsLayout.setSizeFull();
			searchContentsLayout.addToPrimary(crudTableLayout.getSearchLayout());
			searchContentsLayout.addToSecondary(tableDetailsLayout);
			crudTableLayout.addAndExpand(searchContentsLayout);
		} else {
			crudTableLayout.addAndExpand(tableDetailsLayout);
		}

		this.applyDefaultTo(crudTableLayout);
	}

	@Override
	public void applyDefaultTo(final SelectTableLayout<?, ?, ?> crudTableLayout) {
		if (crudTableLayout.getSearchLayout() != null) {
			if (crudTableLayout.isSearchVisible()) {
				crudTableLayout.getSearchLayout().setWidth(30f, Unit.PERCENTAGE);
				crudTableLayout.getSearchLayout().getParent().ifPresent(p -> ((SplitLayout) p).removeClassName("hide-splitter"));
				crudTableLayout.getTableWrapper().setWidth(70f, Unit.PERCENTAGE);
			} else {
				crudTableLayout.getSearchLayout().setWidth(0f, Unit.PERCENTAGE);
				crudTableLayout.getSearchLayout().getParent().ifPresent(p -> ((SplitLayout) p).addClassName("hide-splitter"));
				crudTableLayout.getTableWrapper().setWidth(100f, Unit.PERCENTAGE);
			}
		} else {
			crudTableLayout.getTableWrapper().setWidth(100f, Unit.PERCENTAGE);
		}
	}

}
