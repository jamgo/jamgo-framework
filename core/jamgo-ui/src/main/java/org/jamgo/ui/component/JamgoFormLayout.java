package org.jamgo.ui.component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.vaadin.components.JamgoComponentConstants;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.componentfactory.ToggleButton;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

@org.springframework.stereotype.Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@CssImport(value = "./styles/jamgo-form-layout.css")
public class JamgoFormLayout extends FormLayout {

	private static final long serialVersionUID = 1L;

	@Autowired
	protected LocalizedMessageService messageSource;

	private boolean captionsBold = false;
	private final Map<Component, FormItem> formItemsMap = new HashMap<>();
	private ComponentColspan defaultColspan;

	public JamgoFormLayout() {
		super();
		this.addClassName("jamgo-form-layout");
	}

	public ComponentColspan getDefaultColspan() {
		return this.defaultColspan;
	}

	public void setDefaultColspan(final ComponentColspan defaultColspan) {
		this.defaultColspan = defaultColspan;
	}

	public FormItem addFormComponent(final Component component, final String labelId) {
		return this.addFormComponent(component, labelId, null, this.calculateComponentColspan(component));
	}

	public FormItem addFormComponent(final Component component, final String labelId, final ComponentColspan colspan) {
		return this.addFormComponent(component, labelId, null, colspan);
	}

	public FormItem addFormComponent(final Component component, final String labelId, final String subLabelId) {
		return this.addFormComponent(component, labelId, subLabelId, this.calculateComponentColspan(component));
	}

	public FormItem addFormComponent(final Component component, final String labelId, final String subLabelId, final ComponentColspan colspan) {
		final String labelString = this.translate(labelId);
		final String sublabelString = this.translate(subLabelId);
		return this.addFormComponent(component, labelString, sublabelString, colspan.getColumns());
	}

	public FormItem addFormComponent(final Component component, final ComponentColspan colspan) {
		return this.addFormComponent(component, (Component) null, colspan);
	}

	public FormItem addFormComponent(final Component component, final Component label, final ComponentColspan colspan) {
		return this.addFormComponent(component, label, colspan.getColumns());
	}

	private FormItem addFormComponent(final Component component, final String labelString, final String subLabelString, final int colspan) {
		final Label label = this.createLabel(component, labelString, subLabelString);

		if (component instanceof ModelToMany) {
			final HorizontalLayout labelLayout = new HorizontalLayout();
			label.addClassName("to-many-label");
			labelLayout.add(label, ((ModelToMany<?, ?>) component).getButtonsLayout());
			return this.addFormComponent(component, labelLayout, colspan);
//		} else if (component instanceof ModelManyToMany) {
//			final HorizontalLayout labelLayout = new HorizontalLayout();
//			label.addClassName("to-many-label");
//			labelLayout.add(label, ((ModelManyToMany<?>) component).getButtonsLayout());
//			return this.addFormComponent(component, labelLayout, colspan);
		} else {
			return this.addFormComponent(component, label, colspan);
		}
	}

	private FormItem addFormComponent(final Component component, final Component label, final int colspan) {
		if (label == null) {
			// Examples: checkbox, details section, button... are not FormItems
			this.add(component, colspan);
			return null;
		} else {

			final FormItem formItem = this.addFormItem(component, label);
			this.setColspan(formItem, colspan);
			this.formItemsMap.put(component, formItem);
			return formItem;
		}
	}

	private Label createLabel(final Component component, final String labelString, final String subLabelString) {
		@SuppressWarnings("unchecked")
		final Consumer<String> labelSetter = (Consumer<String>) ComponentUtil.getData(component, JamgoComponentConstants.PROPERTY_LABEL_SETTER);
		if (labelSetter != null) {
			labelSetter.accept(labelString);
			return null;
		}

		Label label = null;
		if (subLabelString == null) {
			label = Optional.ofNullable(labelString).map(o -> new Label(labelString)).orElse(null);
		} else {
			final Span mainLabel = Optional.ofNullable(labelString).map(o -> new Span(labelString)).orElse(null);
			final Span subLabel = Optional.ofNullable(subLabelString).map(o -> new Span(subLabelString)).orElse(null);
			subLabel.addClassName("sub-label");
			label = new Label();
			label.add(mainLabel, new Html("<br/>"), subLabel);
		}
		return label;
	}

	private String translate(final String name) {
		return Optional.ofNullable(name).map(o -> this.messageSource.getMessage(o)).orElse(null);
	}

	private ComponentColspan calculateComponentColspan(final Component component) {
		final ComponentColspan componentColspan = (ComponentColspan) ComponentUtil.getData(component, JamgoComponentConstants.PROPERTY_DEFAULT_WIDTH);
		return Optional.ofNullable(componentColspan).orElse(this.getDefaultColspan());
	}

	/**
	 * @deprecated
	 *             <p>
	 *             Use {@link #addFormComponent(Component, String)} instead.
	 */
	@Deprecated
	public FormItem addLocalized(final Component component) {
		return this.addLocalized(component, this.calculateComponentColspan(component).getColumns());
	}

	/**
	 * @deprecated
	 *             <p>
	 *             Use
	 *             {@link #addFormComponent(Component, String, ComponentColspan)}
	 *             instead.
	 */
	@Deprecated
	public FormItem addLocalized(final Component component, final int colspan) {
		if (component instanceof Checkbox) {
			this.add(component, colspan);
			// ... FIXME:
			// When component is a CheckBok there are no FormItem.
			// It doesn't really make sense that all add* methods return
			// the FormItem, they should be void just like in Vaadin's FormLayout.
			return null;
		} else if (component instanceof ToggleButton) {
			return this.add(component, (Component) null, colspan);
		} else {
			return this.add(component, (Component) null, ComponentColspan.FULL_WIDTH.getColumns());
		}
	}

	/**
	 * @deprecated
	 *             <p>
	 *             Use {@link #addFormComponent(Component, String)} instead.
	 */
	@Deprecated
	public FormItem add(final Component component, final String labelString) {
		return this.add(component, labelString, null, this.calculateComponentColspan(component).getColumns());
	}

	/**
	 * @deprecated
	 *             <p>
	 *             Use
	 *             {@link #addFormComponent(Component, String, String, ComponentColspan)}
	 *             instead.
	 */
	@Deprecated
	public FormItem add(final Component component, final String labelString, final String subLabelString) {
		return this.add(component, labelString, subLabelString, this.calculateComponentColspan(component).getColumns());
	}

	/**
	 * @deprecated
	 *             <p>
	 *             Use {@link #addFormComponent(Component, String)} instead.
	 */
	@Deprecated
	public FormItem addLocalized(final Component component, final String labelId) {
		return this.add(component, this.messageSource.getMessage(labelId));
	}

	/**
	 * @deprecated
	 *             <p>
	 *             Use
	 *             {@link #addFormComponent(Component, String, String, ComponentColspan)}
	 *             instead.
	 */
	@Deprecated
	public FormItem addLocalized(final Component component, final String labelId, final String subLabelId) {
		return this.add(component, this.messageSource.getMessage(labelId), this.messageSource.getMessage(subLabelId));
	}

	/**
	 * @deprecated
	 *             <p>
	 *             Use {@link #addFormComponent(Component, String)} instead.
	 */
	@Deprecated
	public FormItem addLocalizedColon(final Component component, final String labelId) {
		return this.add(component, this.messageSource.getMessage(labelId) + ":");
	}

	/**
	 * @deprecated
	 *             <p>
	 *             Use
	 *             {@link #addFormComponent(Component, String, ComponentColspan)}
	 *             instead.
	 */
	@Deprecated
	public FormItem addLocalized(final Component component, final String labelId, final int colspan) {
		return this.add(component, this.messageSource.getMessage(labelId), colspan);
	}

	/**
	 * @deprecated
	 *             <p>
	 *             Use
	 *             {@link #addFormComponent(Component, String, ComponentColspan)}
	 *             instead.
	 */
	@Deprecated
	public FormItem addLocalizedColon(final Component component, final String labelId, final int colspan) {
		return this.add(component, this.messageSource.getMessage(labelId) + ":", colspan);
	}

	/**
	 * @deprecated
	 *             <p>
	 *             Use
	 *             {@link #addFormComponent(Component, String, ComponentColspan)}
	 *             instead.
	 */
	@Deprecated
	public FormItem add(final Component component, final String labelString, final int colspan) {
		return this.add(component, labelString, null, colspan);
	}

	/**
	 * @deprecated
	 *             <p>
	 *             Use
	 *             {@link #addFormComponent(Component, String, String, ComponentColspan)}
	 *             instead.
	 */
	@Deprecated
	public FormItem add(final Component component, final String labelString, final String subLabelString, final int colspan) {
		return this.addFormComponent(component, labelString, subLabelString, colspan);
	}

	/**
	 * @deprecated
	 *             <p>
	 *             Use
	 *             {@link #addFormComponent(Component, Component, ComponentColspan)}
	 *             instead.
	 */
	@Deprecated
	public FormItem add(final Component component, final Component label, final int colspan) {
		return this.addFormComponent(component, label, colspan);
	}

	public void setVisible(final Component component, final boolean visible) {
		Optional.ofNullable(this.formItemsMap.get(component)).ifPresentOrElse(o -> o.setVisible(visible), () -> component.setVisible(visible));
	}

	public boolean isCaptionsBold() {
		return this.captionsBold;
	}

	public void setCaptionsBold(final boolean captionsBold) {
		this.captionsBold = captionsBold;
	}

}
