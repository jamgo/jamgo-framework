package org.jamgo.ui.component.builders;

import java.util.Optional;
import java.util.function.Consumer;

import org.jamgo.vaadin.builder.base.HasEnabledBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.jamgo.vaadin.components.JamgoComponentConstants;
import org.jamgo.vaadin.ui.builder.JamgoComponentBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.componentfactory.ToggleButton;
import com.vaadin.flow.component.ComponentUtil;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
/**
 * Builder for ToggleButton vaadin component.
 * More information:
 * - https://vaadin.com/directory/component/togglebutton-for-flow/overview
 * - https://incubator.app.fi/togglebutton-demo/togglebutton
 *
 * @author Jamgo SCCL
 */
public class ToggleButtonBuilder extends JamgoComponentBuilder<ToggleButton, ToggleButtonBuilder> implements
	HasStyleBuilder<ToggleButtonBuilder, ToggleButton>,
	HasSizeBuilder<ToggleButtonBuilder, ToggleButton>,
	HasValueBuilder<ToggleButtonBuilder, ToggleButton>,
	HasEnabledBuilder<ToggleButtonBuilder, ToggleButton> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new ToggleButton();
		this.instance.setValue(Boolean.TRUE);
	}

	@Override
	public ToggleButton build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));

		// final BiConsumer<Component, String> labelSetter = (component, label) -> ((Checkbox) component).setLabel(label);
		final Consumer<String> labelSetter = (label) -> this.instance.setLabel(label);
		ComponentUtil.setData(this.instance, JamgoComponentConstants.PROPERTY_LABEL_SETTER, labelSetter);

		return this.instance;
	}

}