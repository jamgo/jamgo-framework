package org.jamgo.ui.layout;

public class LayoutArea {

	Integer x;
	Integer y;
	Integer width;
	Integer height;

	public LayoutArea(Integer x, Integer y, Integer width, Integer height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public Integer getX() {
		return this.x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return this.y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public Integer getWidth() {
		return this.width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return this.height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getMaxX() {
		return this.x + (this.width == null ? 0 : this.width - 1);
	}

	public Integer getMaxY() {
		return this.y + (this.height == null ? 0 : this.height - 1);
	}

}
