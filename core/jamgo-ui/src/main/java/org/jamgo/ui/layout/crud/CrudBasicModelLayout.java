package org.jamgo.ui.layout.crud;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.jamgo.model.BasicModel;
import org.jamgo.model.enums.Permission;
import org.jamgo.model.enums.SecuredObjectType;
import org.jamgo.services.UserService;
import org.jamgo.services.impl.SecurityService;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.jamgo.ui.layout.crud.CrudDetailsLayout.LayoutMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.google.common.collect.Lists;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Focusable;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.data.binder.BindingValidationStatus;
import com.vaadin.flow.data.binder.ValidationException;

@SuppressWarnings("serial")
public abstract class CrudBasicModelLayout<T extends BasicModel<?>> extends CrudLayout<T> {

	static final Logger logger = LoggerFactory.getLogger(CrudBasicModelLayout.class);

	@Autowired
	protected ApplicationContext applicationContext;
	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected SecurityService securityService;
	@Autowired
	protected CrudPermissionsLayout<T> permissionsLayout;
	@Autowired
	protected UserService userService;
	protected HorizontalLayout buttonsLayout;
	protected Button maximizeRestoreButton;
	protected Consumer<LayoutMode> maximizeRestoreHandler;
	protected Icon maximizeRestoreButtonCompressIcon = VaadinIcon.COMPRESS_SQUARE.create();
	protected Icon maximizeRestoreButtonExpandIcon = VaadinIcon.EXPAND_SQUARE.create();
	protected CrudLayoutDef<?, ?> crudLayoutDef;
	protected boolean withButtons = true;
	protected Button okButton;
	protected Button cancelButton;
	protected Consumer<CrudBasicModelLayout<T>> okHandler;
	protected Consumer<CrudBasicModelLayout<T>> cancelHandler;

	protected CrudBasicPanel mainPanel;
	protected CrudDetailsPanel crudPermissionsPanel;
	protected List<CrudBasicPanel> pluggablePanels = new ArrayList<>();

	protected Tabs tabSheet;

	protected Div content;

	protected BidiMap<Tab, CrudBasicPanel> tabsComponentMap = new DualLinkedHashBidiMap<>();

	public CrudBasicModelLayout() {
		super();
	}

	public void initialize(final CrudLayoutDef<?, ?> crudLayoutDef) {
		this.crudLayoutDef = crudLayoutDef;

		if (this.withButtons) {
			this.addClassName("crud-details-layout");
		} else {
			this.addClassName("crud-details-layout-readonly");
		}
		this.initialize();
		this.setSizeFull();

		this.addContent(crudLayoutDef);
		if (this.withButtons) {
			this.addActionButtons();
		}
	}

	@Override
	public void onAttach(final AttachEvent event) {
		if (this.content == null) {
			this.initialize(this.crudLayoutDef);
		}
		super.onAttach(event);

		if (!this.tabsComponentMap.isEmpty() && this.tabsComponentMap.values().stream().allMatch(each -> !each.isVisible())) {
			final Tab firstTab = this.tabsComponentMap.mapIterator().next();
			this.tabsComponentMap.get(firstTab).setVisible(true);
		}

		for (final CrudBasicPanel eachPanel : this.getPanels()) {
			eachPanel.setEnabled(this.securityService.hasWritePermission(SecuredObjectType.TAB, eachPanel.getSecurityId(), true));
		}

		if (this.okHandler == null && this.okButton != null) {
			this.okButton.setVisible(false);
		}
		if (this.cancelHandler == null && this.cancelButton != null) {
			this.cancelButton.setVisible(false);
		}
		if (this.maximizeRestoreHandler == null && this.maximizeRestoreButton != null) {
			this.maximizeRestoreButton.setVisible(false);
		}
	}

	protected void addContent(final CrudLayoutDef<?, ?> crudLayoutDef) {
		this.content = new Div();
		this.content.addClassName("crud-details-content");
		this.content.setWidthFull();
	}

	@Override
	protected void addActionButtons() {
		this.actionButtons = this.createActionButtons();
		this.buttonsLayout = new HorizontalLayout();
		this.buttonsLayout.setWidth(100, Unit.PERCENTAGE);
		this.buttonsLayout.setMargin(false);
		this.buttonsLayout.setSpacing(true);
		this.buttonsLayout.setJustifyContentMode(JustifyContentMode.CENTER);
		this.buttonsLayout.add(this.actionButtons.toArray(new Component[] {}));

		this.maximizeRestoreButton = this.componentBuilderFactory.createButtonBuilder()
			.setIcon(this.maximizeRestoreButtonExpandIcon)
			.addClassName("j-maximize-restore")
			.build();
		this.maximizeRestoreButton.addClickListener(event -> this.doMaximizeRestore());
		this.buttonsLayout.add(this.maximizeRestoreButton);

		this.add(this.buttonsLayout);
	}

	@Override
	protected List<? extends Component> createActionButtons() {
		final List<Button> buttons = new ArrayList<>();

		this.okButton = this.componentBuilderFactory.createButtonBuilder()
			.addThemeVariants(ButtonVariant.LUMO_PRIMARY)
			.setText(this.messageSource.getMessage("action.save"))
			.build();
		this.okButton.addClickShortcut(Key.ENTER, KeyModifier.CONTROL);
		this.okButton.addClickListener(event -> this.doOk());
		buttons.add(this.okButton);

		this.cancelButton = this.componentBuilderFactory.createButtonBuilder()
			.setText(this.messageSource.getMessage("action.cancel"))
			.build();
		this.cancelButton.addClickListener(event -> this.doCancel());
		buttons.add(this.cancelButton);

		return buttons;
	}

	protected void doOk() {
		if (this.okHandler != null) {
			try {
				this.updateTargetObject();
				this.okHandler.accept(this);
			} catch (final ValidationException e) {
				String errorMessage = null;
				if (!e.getFieldValidationErrors().isEmpty()) {
					@SuppressWarnings("unchecked")
					final BindingValidationStatus<String> validationStatus = (BindingValidationStatus<String>) e.getFieldValidationErrors().stream().findFirst().get();
					errorMessage = validationStatus.getMessage().orElse(this.messageSource.getMessage("validation.error"));
					if (validationStatus.getField() instanceof Focusable) {
						((Focusable<?>) validationStatus.getField()).focus();
					}
				}
				// TODO: notification with error style
				Notification.show(errorMessage);
			} catch (final IllegalArgumentException e) {
				CrudDetailsLayout.logger.error(e.getMessage(), e);
				// TODO: notification with error style
				Notification.show(e.getMessage());
			}
		}
	}

	protected void doCancel() {
		if (this.cancelHandler != null) {
			this.cancelHandler.accept(this);
		}
	}

	protected void doMaximizeRestore() {
		if (this.maximizeRestoreHandler != null) {
			if (this.maximizeRestoreButton.getIcon() == this.maximizeRestoreButtonExpandIcon) {
				this.maximizeRestoreButton.setIcon(this.maximizeRestoreButtonCompressIcon);
				this.maximizeRestoreHandler.accept(LayoutMode.MAXIMIZED);
			} else {
				this.maximizeRestoreButton.setIcon(this.maximizeRestoreButtonExpandIcon);
				this.maximizeRestoreHandler.accept(LayoutMode.NORMAL);
			}
		}
	}

	public Button getOkButton() {
		return this.okButton;
	}

	public Button getCancelButton() {
		return this.cancelButton;
	}

	public Button getMaximizeRestoreButton() {
		return this.maximizeRestoreButton;
	}

	public boolean withButtons() {
		return this.withButtons;
	}

	public void withButtons(final boolean withButtons) {
		this.withButtons = withButtons;
	}

	public Consumer<CrudBasicModelLayout<T>> getOkHandler() {
		return this.okHandler;
	}

	public void setOkHandler(final Consumer<CrudBasicModelLayout<T>> okHandler) {
		this.okHandler = okHandler;
	}

	public Consumer<CrudBasicModelLayout<T>> getCancelHandler() {
		return this.cancelHandler;
	}

	public void setCancelHandler(final Consumer<CrudBasicModelLayout<T>> cancelHandler) {
		this.cancelHandler = cancelHandler;
	}

	public Consumer<LayoutMode> getMaximizeRestoreHandler() {
		return this.maximizeRestoreHandler;
	}

	public void setMaximizeRestoreHandler(final Consumer<LayoutMode> maximizeRestoreHandler) {
		this.maximizeRestoreHandler = maximizeRestoreHandler;
	}

	public Icon getMaximizeRestoreButtonCompressIcon() {
		return this.maximizeRestoreButtonCompressIcon;
	}

	public void setMaximizeRestoreButtonCompressIcon(final Icon maximizeRestoreButtonCompressIcon) {
		this.maximizeRestoreButtonCompressIcon = maximizeRestoreButtonCompressIcon;
	}

	public Icon getMaximizeRestoreButtonExpandIcon() {
		return this.maximizeRestoreButtonExpandIcon;
	}

	public void setMaximizeRestoreButtonExpandIcon(final Icon maximizeRestoreButtonExpandIcon) {
		this.maximizeRestoreButtonExpandIcon = maximizeRestoreButtonExpandIcon;
	}

	@Override
	public void updateFields(final Object savedItem) {
		super.updateFields(savedItem);

		//		if (this.summaryLayout != null) {
		//			this.summaryLayout.updateFields(savedItem);
		//		}

		if (this.crudPermissionsPanel != null) {
			// TODO: Qué se supone que intenta hacer?
			//			final User user = this.userService.getCurrentUser();
			//			final String securedObjectKey = this.securityService.getSecuredObjectKey(savedItem);
			//			this.tabSheet.getTab(this.crudPermissionsPanel).setVisible(this.securityService.hasWritePermission(SecuredObjectType.ENTITY, securedObjectKey, savedItem.getClass().getName()));
			//			if (this.tabSheet.getSelectedTab() == this.crudPermissionsPanel) {
			//				this.permissionsLayout.updateFields(savedItem);
			//			}
		}

		this.pluggablePanels.forEach(pluggablePanel -> pluggablePanel.updateFields());

		if (this.okButton != null) {
			this.okButton.setVisible(this.securityService.hasPermission(this.targetObject, Permission.WRITE));
		}

		this.updateElementsVisibility();
	}

	protected void updateElementsVisibility() {
		final boolean isCreation = this.isCreation();
		CrudDetailsLayout.logger.trace("isCreation: " + isCreation);

		this.pluggablePanels.forEach(pluggablePanel -> this.updatePluggablePanelTabVisibility(pluggablePanel, isCreation));

		// Show first tab if all tabs are not visible
		if (!this.tabsComponentMap.isEmpty() && this.tabsComponentMap.values().stream().allMatch(each -> !each.isVisible())) {
			final Tab firstTab = this.tabsComponentMap.mapIterator().next();
			this.tabSheet.setSelectedTab(firstTab);
			this.tabsComponentMap.get(firstTab).setVisible(true);
		}
	}

	private void updatePluggablePanelTabVisibility(final CrudBasicPanel pluggablePanel, final boolean isCreation) {
		final boolean visible = pluggablePanel.isVisible(isCreation);
		this.setTabVisible(pluggablePanel, visible);
	}

	protected void setSelectedTab(final CrudBasicPanel panel) {
		final Tab tab = this.tabsComponentMap.inverseBidiMap().get(panel);
		this.tabSheet.setSelectedTab(tab);
	}

	protected void setTabVisible(final CrudBasicPanel panel, final boolean visible) {
		final Tab tab = this.tabsComponentMap.inverseBidiMap().get(panel);
		tab.setVisible(visible);
	}

	protected List<CrudBasicPanel> getPanels() {
		return Lists.newArrayList(this.tabsComponentMap.values());
	}

	protected boolean isCreation() {
		return this.targetObject == null || this.targetObject.getId() == null;
	}

	public CrudLayoutDef<?, ?> getCrudLayoutDef() {
		return this.crudLayoutDef;
	}

	public void setCrudLayoutDef(final CrudLayoutDef<?, ?> crudLayoutDef) {
		this.crudLayoutDef = crudLayoutDef;
	}

}