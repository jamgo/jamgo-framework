package org.jamgo.ui.layout.crud;

import org.jamgo.ui.component.JamgoFormLayout;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.FormItem;
import com.vaadin.flow.spring.annotation.SpringComponent;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CrudDetailsPanel extends CrudBasicPanel {

	private static final long serialVersionUID = 1L;

	public enum Format {
		OneColumn, TwoColumn, Custom, Vertical, Horizontal, Css
	}

	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;

	protected JamgoFormLayout formLayout;

	public void initialize(final Format format) {
		this.setSizeFull();
		this.formLayout = this.getLayout(format);
		this.addComponentAsFirst(this.formLayout);
	}

	private JamgoFormLayout getLayout(final Format format) {
		JamgoFormLayout layout = null;
		switch (format) {
//			case Custom:
//				layout = this.componentFactory.newGridLayout();
//				((GridLayout) layout).setColumns(10);
//				((GridLayout) layout).setRows(10);
//				break;
//			case TwoColumn:
//				layout = this.componentFactory.newJmgTwoColumnFormLayout();
//				break;
//			case Vertical:
//				layout = this.componentFactory.newVerticalLayout();
//				break;
//			case Horizontal:
//				layout = this.componentFactory.newHorizontalLayout();
//				break;
//			case Css:
//				return this.componentFactory.newCssLayout();
			default:
				layout = this.componentBuilderFactory.createJamgoFormLayoutBuilder().build();
		}
		layout.setSizeUndefined();
		layout.setWidth(100, Unit.PERCENTAGE);

		return layout;
	}

	public FormItem addFormComponent(final Component component, final String labelId) {
		return this.formLayout.addFormComponent(component, labelId);
	}

	public FormItem addFormComponent(final Component component, final ComponentColspan colspan) {
		return this.formLayout.addFormComponent(component, colspan);
	}

	public FormItem addFormComponent(final Component component, final String labelId, final ComponentColspan colspan) {
		return this.formLayout.addFormComponent(component, labelId, colspan);
	}

	public FormItem addFormComponent(final Component component, final String labelId, final String subLabelId) {
		return this.formLayout.addFormComponent(component, labelId, subLabelId);
	}

	public FormItem addFormComponent(final Component component, final String labelId, final String subLabelId, final ComponentColspan colspan) {
		return this.formLayout.addFormComponent(component, labelId, subLabelId, colspan);
	}

	/**
	 * @deprecated
	 * Use {@link #addFormComponent(Component, String)} instead.
	 */
	@Deprecated
	public void add(final Component component) {
		this.formLayout.add(component);
	}

	/**
	 * @deprecated
	 * Use {@link #addFormComponent(Component, String)} instead.
	 */
	@Deprecated
	public FormItem add(final Component component, final String label) {
		return this.formLayout.add(component, label);
	}

	/**
	 * @deprecated
	 * Use {@link #addFormComponent(Component, String, ComponentColspan)} instead.
	 */
	@Deprecated
	public FormItem addLocalized(final Component component, final int colspan) {
		return this.formLayout.addLocalized(component, colspan);
	}

	/**
	 * @deprecated
	 * Use {@link #addFormComponent(Component, String)} instead.
	 */
	@Deprecated
	public FormItem addLocalized(final Component component, final String label) {
		return this.formLayout.addLocalized(component, label);
	}

	/**
	 * @deprecated
	 * Use {@link #addFormComponent(Component, String, ComponentColspan)} instead.
	 */
	@Deprecated
	public FormItem addLocalized(final Component component, final String label, final int colspan) {
		return this.formLayout.addLocalized(component, label, colspan);
	}

	/**
	 * @deprecated
	 * Use {@link #addFormComponent(Component, String)} instead.
	 */
	@Deprecated
	public FormItem addLocalizedColon(final Component component, final String label) {
		return this.formLayout.addLocalizedColon(component, label);
	}

	/**
	 * @deprecated
	 * Use {@link #addFormComponent(Component, String, ComponentColspan)} instead.
	 */
	@Deprecated
	public FormItem addLocalizedColon(final Component component, final String label, final int colspan) {
		return this.formLayout.addLocalizedColon(component, label, colspan);
	}

	public void add(final Component component, final int colspan) {
		this.formLayout.add(component, colspan);
	}

	@Override
	public void setVisible(final Component component, final boolean visible) {
		this.formLayout.setVisible(component, visible);
	}

	public void replaceComponent(final Component oldComponent, final Component newComponent) {
		// TODO
//		this.formLayout.replaceComponent(oldComponent, newComponent);
	}

	public void remove(final Component component) {
		this.formLayout.remove(component);
	}

	public FormLayout getFormLayout() {
		return this.formLayout;
	}

	public void setCaptionsBold(final boolean captionsBold) {
		if (this.formLayout instanceof JamgoFormLayout) {
			this.formLayout.setCaptionsBold(captionsBold);
		}
	}

	/*
	 *  Implementation of abstract methods from CrudBasePanel superclass.
	 *  These are the default implementations:
	 *  - do not store parent details layout, as it is not needed for this class
	 *  - panel always visible
	 *  - do nothing after updateFields() method is called
	 *	Overwrite these methods in subclasses if you need a different behaviour.
	 */

	@Override
	public void setParentDetailsLayout(final CrudDetailsLayout<?> parentDetailsLayout) {
		// Do nothing
	}

	@Override
	public boolean isVisible(final boolean isCreation) {
		return true;
	}

	@Override
	public void updateFields() {
		// Do nothing
	}
}
