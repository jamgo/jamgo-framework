package org.jamgo.ui.layout.details;

import org.jamgo.model.entity.Language;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.textfield.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LanguageDetailsLayout extends CrudDetailsLayout<Language> {

	private static final long serialVersionUID = 1L;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		final CrudDetailsPanel panel = this.componentBuilderFactory.createCrudDetailsPanelBuilder().build();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		final TextField nameField = this.componentBuilderFactory.createTextFieldBuilder()
			.build();
		panel.addFormComponent(nameField, "language.name");
		this.binder.bind(nameField, Language::getName, Language::setName);

		final TextField languageCodeField = this.componentBuilderFactory.createTextFieldBuilder()
			.build();
		panel.addFormComponent(languageCodeField, "language.code");
		this.binder.bind(languageCodeField, Language::getLanguageCode, Language::setLanguageCode);

		final TextField countryCodeField = this.componentBuilderFactory.createTextFieldBuilder()
			.build();
		panel.addFormComponent(countryCodeField, "language.countryCode");
		this.binder.bind(countryCodeField, Language::getCountryCode, Language::setCountryCode);

		final Checkbox activeCheck = this.componentBuilderFactory.createCheckBoxBuilder()
			.build();
		panel.addFormComponent(activeCheck, "language.active");
		this.binder.bind(activeCheck, Language::isActive, Language::setActive);

		final Checkbox defaultCheck = this.componentBuilderFactory.createCheckBoxBuilder()
			.build();
		panel.addFormComponent(defaultCheck, "language.default");
		this.binder.bind(defaultCheck, Language::isDefaultLanguage, Language::setDefaultLanguage);

		return panel;
	}

	@Override
	protected Class<Language> getTargetObjectClass() {
		return Language.class;
	}
}
