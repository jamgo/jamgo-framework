package org.jamgo.ui.component;

import org.jamgo.vaadin.components.JamgoWysiwygE;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LocalizedTextEditor extends LocalizedStringField<JamgoWysiwygE> {

	private static final long serialVersionUID = 1L;

	@Override
	protected JamgoWysiwygE createBasicField() {
		final JamgoWysiwygE textEditor = this.componentBuilderFactory.createTextEditorBuilder()
			// .setSizeFull() // <- Do not set size full, the component will not appear (height=100% = 0px)
			.build();

		return textEditor;
	}

}
