package org.jamgo.ui.layout.menu;

public interface MenuLayout {

	MenuGroup getRootMenuGroup();
	default MenuGroup getBottomMenuGroup() {return new MenuGroup();}

}
