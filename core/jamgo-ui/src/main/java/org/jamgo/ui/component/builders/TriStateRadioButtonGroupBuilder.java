package org.jamgo.ui.component.builders;

import org.jamgo.ui.component.TriStateRadioButtonGroup;
import org.jamgo.ui.component.TriStateRadioButtonGroup.Status;
import org.jamgo.vaadin.ui.builder.JamgoComponentBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.data.renderer.ComponentRenderer;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TriStateRadioButtonGroupBuilder extends JamgoComponentBuilder<TriStateRadioButtonGroup, TriStateRadioButtonGroupBuilder> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = this.applicationContext.getBean(TriStateRadioButtonGroup.class);
	}

	public TriStateRadioButtonGroupBuilder setRenderer(final ComponentRenderer<? extends Component, Status> renderer) {
		this.getComponent().setRenderer(renderer);
		return this;
	}

}