package org.jamgo.ui.layout.utils.ie;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.jamgo.model.entity.Model;
import org.jamgo.model.portable.ImportStats;

/**
 * 
 * @author jamgo
 *
 * @deprecated Use {@link org.jamgo.services.ie.AbstractImporter} from jamgo-services module instead
 * 
 * @param <T>
 */
@Deprecated
public abstract class AbstractImporter<T extends Model> implements Importer<T> {

	protected boolean deleteAllBeforeImport;

	private List<T> list = new ArrayList<T>();
	private List<String> errors = new ArrayList<String>();

	protected int numberOfInserts;
	protected int numberOfUpdates;
	protected int numberOfDeletes;
	
	protected String fileErrorMessage = "S'ha produït un error en realitzar la importació"; // import.stats.error
	
	@Override
	public List<String> getErrors() {
		return this.errors;
	}

	protected List<T> getList() {
		return this.list;
	}
	
	public void setList(List<T> list) {
		this.list = list;
	}

	protected void clearImportCounts() {
		this.numberOfInserts = 0;
	
		this.numberOfUpdates = 0;
		this.numberOfDeletes = 0;
	}

	@Override
	public ImportStats<T> extractData(InputStream inputStream) throws Exception {
		this.clearImportCounts();
		this.getList().clear();
		try {
			this.doExtraction(inputStream);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO check how this error collecting works and if they are used afterwards
			this.getErrors().add(e.getMessage());
			throw e;
		}
		return this.getExtractedStats();
	}

	protected abstract void doExtraction(InputStream inputStream) throws Exception;

	protected ImportStats<T> getExtractedStats() {
		ImportStats<T> stats = new ImportStats<>();
		stats.setData(this.getList());
		stats.setNumberOfInserts(this.numberOfInserts);
		stats.setNumberOfUpdates(this.numberOfUpdates);
		stats.setNumberOfDeletes(this.numberOfDeletes);
		stats.setErrors(this.getErrors());
		return stats;
	}
	
	protected T getInstance(Object... value) {
		T entity = null;
		if (this.deleteAllBeforeImport) {
			entity = this.getNewInstance();
			this.numberOfInserts++;
			this.numberOfDeletes = this.getEntitiesCount();
		} else {
			entity = this.findInstance(value);
			// if entity already exist it will be updated
			if (entity == null) {
				entity = this.getNewInstance();
				this.numberOfInserts++;
			} else {
				this.numberOfUpdates++;
			}
		}
		return entity;
	}

	@Override
	public void setDeleteAllBeforeImport(Boolean deleteAllBeforeImport) {
		this.deleteAllBeforeImport = deleteAllBeforeImport;
	}

	@Override
	public boolean deleteAllBeforeImport() {
		return this.deleteAllBeforeImport;
	}

	public abstract T getNewInstance();

	public abstract T findInstance(Object... value);

	public abstract int getEntitiesCount();
	
	protected abstract Object getObjectFileId(T entity);

}
