package org.jamgo.ui.component;

import java.util.Collection;
import java.util.Set;

import org.jamgo.model.entity.Model;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.HasHelper;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasValidation;
import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.binder.HasFilterableDataProvider;
import com.vaadin.flow.data.binder.HasItemComponents;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.data.selection.MultiSelect;
import com.vaadin.flow.data.selection.MultiSelectionListener;
import com.vaadin.flow.function.SerializableFunction;
import com.vaadin.flow.function.SerializablePredicate;
import com.vaadin.flow.shared.Registration;

@org.springframework.stereotype.Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
@CssImport(value = "./styles/model-combo-box.css")
public class ModelMultiComboBox<T extends Model> extends ModelSelect<T>
	implements
	MultiSelect<MultiSelectComboBox<T>, T>,
	HasFilterableDataProvider<T, String>,
	HasItemComponents<T>, HasSize, HasValidation, HasHelper,
	InitializingBean {

	private static final long serialVersionUID = 1L;

//	private MultipleSelect<T> modelComboBox;
	private MultiSelectComboBox<T> modelComboBox;
	private Button createButton;

	public ModelMultiComboBox(final CrudLayoutDef<T, T> crudLayoutDef) {
		this.crudLayoutDef = crudLayoutDef;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		this.getContent().addClassName("model-combo-box");
		this.getContent().setPadding(false);
		final HorizontalLayout comboBoxLayout = new HorizontalLayout();
		comboBoxLayout.setWidth(100, Unit.PERCENTAGE);
		comboBoxLayout.setSpacing(false);

		this.modelComboBox = this.componentBuilderFactory.<T> createMultiSelectComboBoxBuilder()
			.setWidth(100, Unit.PERCENTAGE)
			.build();

		this.createButton = this.componentBuilderFactory.createButtonBuilder()
			.addClassName("model-combo-box-last-button")
			.setIcon(VaadinIcon.FILE_O.create())
			.setVisible(this.createVisible)
			.build();
		this.createButton.addClickListener(event -> this.doCreateNew());

		comboBoxLayout.add(this.modelComboBox, this.createButton);
		comboBoxLayout.setFlexGrow(1, this.modelComboBox);

		this.getContent().add(comboBoxLayout);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void updateContents(final Set<T> items) {
		this.modelComboBox.setItems(this.serviceManager.getService(this.crudLayoutDef.getEntityClass()).findAll());
		this.modelComboBox.select(items.stream().findFirst().get());
	}

	// ...	MultiselectComboBox delegation methods.

	public void setItemLabelGenerator(final ItemLabelGenerator<T> itemLabelGenerator) {
		this.modelComboBox.setItemLabelGenerator(itemLabelGenerator);
	}

	@Override
	public void setItems(final Collection<T> items) {
		this.modelComboBox.setItems(items);
	}

	@Override
	public Set<T> getValue() {
		return this.modelComboBox.getValue();
	}

	@Override
	public void setErrorMessage(final String errorMessage) {
		this.modelComboBox.setErrorMessage(errorMessage);
	}

	@Override
	public String getErrorMessage() {
		return this.modelComboBox.getErrorMessage();
	}

	@Override
	public void setInvalid(final boolean invalid) {
		this.modelComboBox.setInvalid(invalid);
	}

	@Override
	public boolean isInvalid() {
		return this.modelComboBox.isInvalid();
	}

	@Override
	public void setDataProvider(final DataProvider<T, String> dataProvider) {
		this.modelComboBox.setDataProvider(dataProvider);
	}

	@Override
	public <C> void setDataProvider(final DataProvider<T, C> dataProvider, final SerializableFunction<String, C> filterConverter) {
		this.modelComboBox.setDataProvider(dataProvider, filterConverter);

	}

	@Override
	public void setValue(final Set<T> value) {
		this.modelComboBox.setValue(value);
	}

	@Override
	public Registration addValueChangeListener(final ValueChangeListener<? super ComponentValueChangeEvent<MultiSelectComboBox<T>, Set<T>>> listener) {
		return this.modelComboBox.addValueChangeListener(listener);
	}

	@Override
	public void updateSelection(final Set<T> addedItems, final Set<T> removedItems) {
		this.modelComboBox.updateSelection(addedItems, removedItems);
	}

	@Override
	public Set<T> getSelectedItems() {
		return this.modelComboBox.getSelectedItems();
	}

	@Override
	public Registration addSelectionListener(final MultiSelectionListener<MultiSelectComboBox<T>, T> listener) {
		return this.modelComboBox.addSelectionListener(listener);
	}

	public void hideActions() {
		this.createButton.setVisible(false);
	}

	@Override
	protected void clearContents() {
		this.modelComboBox.clear();
	}

	@SuppressWarnings("unchecked")
	public void setFilter(final SerializablePredicate<T> filter) {
		((ListDataProvider<T>) this.modelComboBox.getDataProvider()).setFilter(filter);
	}

}
