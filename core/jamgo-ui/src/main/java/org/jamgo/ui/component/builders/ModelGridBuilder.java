package org.jamgo.ui.component.builders;

import org.jamgo.model.BasicModel;
import org.jamgo.ui.component.ModelGrid;
import org.jamgo.vaadin.ui.builder.GridBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ModelGridBuilder<BASIC_MODEL extends BasicModel<?>> extends GridBuilder<BASIC_MODEL> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new ModelGrid<>();
	}

	@Override
	public ModelGrid<BASIC_MODEL> build() {
		return (ModelGrid<BASIC_MODEL>) super.build();
	}

}