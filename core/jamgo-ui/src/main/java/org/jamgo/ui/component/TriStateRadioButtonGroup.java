package org.jamgo.ui.component;

import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.TextRenderer;

@org.springframework.stereotype.Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class TriStateRadioButtonGroup extends CustomField<Boolean> implements InitializingBean {

	private static final long serialVersionUID = 1L;

	public enum Status {
		YES,
		NO,
		UNDEFINED
	}

	@Autowired
	protected ApplicationContext applicationContext;
	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected LocalizedMessageService messageSource;

	private RadioButtonGroup<Status> statusField;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		final HorizontalLayout statusLayout = this.componentBuilderFactory.createHorizontalLayoutBuilder()
			.setWidth(100, Unit.PERCENTAGE)
			.build();

		this.statusField = this.componentBuilderFactory.<Status> createRadioButtonGroupBuilder()
			.build();
		this.statusField.setItems(Status.values());
		this.statusField.setValue(Status.UNDEFINED);
		this.statusField.setRenderer(new TextRenderer<>(o -> this.messageSource.getMessage("boolean." + o.name().toLowerCase())));

		statusLayout.add(this.statusField);

		this.add(statusLayout);
	}

	@Override
	protected Boolean generateModelValue() {
		switch (this.statusField.getValue()) {
			case YES:
				return true;
			case NO:
				return false;
			default:
				return null;
		}
	}

	@Override
	protected void setPresentationValue(final Boolean value) {
		if (value != null) {
			this.statusField.setValue(value ? Status.YES : Status.NO);
		} else {
			this.statusField.setValue(Status.UNDEFINED);
		}
	}

	/**
	 * Sets the item renderer for this radio button group. The renderer is
	 * applied to each item to create a component which represents the item.
	 * <p>
	 * Note: Component acts as a label to the button and clicks on it trigger
	 * the radio button. Hence interactive components like DatePicker or
	 * ComboBox cannot be used.
	 *
	 * @param renderer
	 *            the item renderer, not {@code null}
	 */
	public void setRenderer(final ComponentRenderer<? extends Component, Status> renderer) {
		this.statusField.setRenderer(renderer);
	}

}
