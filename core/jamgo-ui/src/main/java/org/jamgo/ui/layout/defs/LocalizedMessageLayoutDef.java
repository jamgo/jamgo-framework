package org.jamgo.ui.layout.defs;

import org.jamgo.model.entity.LocalizedMessage;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.details.LocalizedMessageDetailsLayout;

@LayoutDefComponent
public class LocalizedMessageLayoutDef extends ModelLayoutDef<LocalizedMessage> {

	public LocalizedMessageLayoutDef(final LocalizedMessageService messageSource, final LocalizedObjectService localizedObjectService) {
		super(messageSource, localizedObjectService);
	}

	@SuppressWarnings("unchecked")
	@Override
	public CrudLayoutDef<LocalizedMessage, LocalizedMessage> getBackofficeLayoutDef() {
		// @formatter:off
		return (CrudLayoutDef<LocalizedMessage, LocalizedMessage>) CrudLayoutDef.<LocalizedMessage> builderEntity()
			.setNameSupplier(() -> this.messageSource.getMessage("table.localizedMessages"))
			.modelGridDef()
				.setEntityClass(LocalizedMessage.class)
				.columnDef()
					.setId("key")
					.setCaptionSupplier(() -> "Key")
					.setValueProvider(o -> o.getKey())
					.setOrderAsc()
				.end()
				.columnDef()
					.setId("value")
					.setCaptionSupplier(() -> "Value")
					.setValueProvider(o -> o.getValue().get(this.messageSource.getLocale()))
					.setOrderAsc()
				.end()
				.detailsLayoutClass(LocalizedMessageDetailsLayout.class)
			.end()
			.tableLayoutVerticalConfig()
			.build();
		// @formatter:on
	}

	@Override
	public Class<LocalizedMessage> getModelClass() {
		return LocalizedMessage.class;
	}

}
