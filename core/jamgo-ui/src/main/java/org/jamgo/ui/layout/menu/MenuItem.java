package org.jamgo.ui.layout.menu;

import java.text.Normalizer;
import java.util.Comparator;
import java.util.function.Supplier;

import org.jamgo.ui.layout.BackofficeLayoutDef;

import com.vaadin.flow.component.icon.Icon;

public class MenuItem {

	public static final Comparator<MenuItem> NAME_ORDER = new Comparator<>() {
		@Override
		public int compare(final MenuItem objectA, final MenuItem objectB) {
			final String normalizedNameA = Normalizer.normalize(objectA.getName(), Normalizer.Form.NFD);
			final String normalizedNameB = Normalizer.normalize(objectB.getName(), Normalizer.Form.NFD);
			return normalizedNameA.compareTo(normalizedNameB);
		}
	};

	private String id;
	private String name;
	private String description;
	private Supplier<Object> nameSupplier;
	private Supplier<Object> descriptionSupplier;
	private BackofficeLayoutDef layoutDef;
	private Icon icon;
	private int order;

	public MenuItem() {
	}

	public MenuItem(final BackofficeLayoutDef layoutDef) {
		this(layoutDef, Integer.MAX_VALUE);
	}

	public MenuItem(final BackofficeLayoutDef layoutDef, final int order) {
		this.name = layoutDef.getName();
		this.description = layoutDef.getDescription();
		this.nameSupplier = layoutDef.getNameSupplier();
		this.descriptionSupplier = layoutDef.getDescriptionSupplier();
		this.layoutDef = layoutDef;
		this.order = order;
	}

	public MenuItem(final Supplier<Object> nameSupplier) {
		this.setNameSupplier(nameSupplier);
	}

	public String getId() {
		return this.id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		if (this.getNameSupplier() != null) {
			return this.getNameSupplier().get().toString();
		} else {
			return this.name;
		}
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Supplier<Object> getNameSupplier() {
		return this.nameSupplier;
	}

	public void setNameSupplier(final Supplier<Object> nameSupplier) {
		this.nameSupplier = nameSupplier;
	}

	public String getDescription() {
		if (this.getDescriptionSupplier() != null) {
			return this.getDescriptionSupplier().get().toString();
		} else {
			return this.description;
		}
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public Supplier<Object> getDescriptionSupplier() {
		return this.descriptionSupplier;
	}

	public void setDescriptionSupplier(final Supplier<Object> descriptionSupplier) {
		this.descriptionSupplier = descriptionSupplier;
	}

	public Icon getIcon() {
		return this.icon;
	}

	public void setIcon(final Icon icon) {
		this.icon = icon;
	}

	public BackofficeLayoutDef getLayoutDef() {
		return this.layoutDef;
	}

	public void setLayoutDef(final BackofficeLayoutDef layoutDef) {
		this.layoutDef = layoutDef;
	}

	public int getOrder() {
		return this.order;
	}

	public void setOrder(final int order) {
		this.order = order;
	}

}
