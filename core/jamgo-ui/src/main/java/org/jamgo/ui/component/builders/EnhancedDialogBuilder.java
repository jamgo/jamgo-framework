package org.jamgo.ui.component.builders;

import org.jamgo.vaadin.builder.base.HasEnabledBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.ui.builder.JamgoComponentBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.componentfactory.EnhancedDialog;
import com.vaadin.componentfactory.theme.EnhancedDialogVariant;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.dialog.Dialog;

@org.springframework.stereotype.Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EnhancedDialogBuilder extends JamgoComponentBuilder<EnhancedDialog, EnhancedDialogBuilder> implements
	HasSizeBuilder<EnhancedDialogBuilder, EnhancedDialog>,
	HasEnabledBuilder<EnhancedDialogBuilder, EnhancedDialog> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new EnhancedDialog();
	}

	@Override
	public EnhancedDialog build() {
		return this.instance;
	}

	/**
	 * Creates a {@code<span>} element with given text, and sets this component
	 * as dialog header
	 *
	 * @param headerText
	 *            dialog title
	 */
	public EnhancedDialogBuilder setHeader(final String headerText) {
		this.getComponent().setHeader(headerText);
		return this;
	}

	/**
	 * Replaces the header section with given components
	 *
	 * @param components
	 *            components for header section
	 */
	public EnhancedDialogBuilder setHeader(final Component... components) {
		this.getComponent().setHeader(components);
		return this;
	}

	/**
	 * Replaces the footer section with given components
	 *
	 * @param components
	 *            components for footer section
	 */
	public EnhancedDialogBuilder setFooter(final Component... components) {
		this.getComponent().setFooter(components);
		return this;
	}

	/**
	 * Sets the theme variants of this component. This method overwrites any
	 * previous set theme variants.
	 *
	 * @param variants
	 */
	public EnhancedDialogBuilder setThemeVariants(final EnhancedDialogVariant... variants) {
		this.getComponent().setThemeVariants(variants);
		return this;
	}

	/**
	 * Adds the theme variants of this component.
	 *
	 * @param variants
	 */
	public EnhancedDialogBuilder addThemeVariants(final EnhancedDialogVariant... variants) {
		this.getComponent().addThemeVariants(variants);
		return this;
	}

	/**
	 * Sets whether this dialog can be closed by hitting the esc-key or not.
	 * <p>
	 * By default, the dialog is closable with esc.
	 *
	 * @param closeOnEsc
	 *            {@code true} to enable closing this dialog with the esc-key,
	 *            {@code false} to disable it
	 */
	public EnhancedDialogBuilder setCloseOnEsc(final boolean closeOnEsc) {
		this.getComponent().setCloseOnEsc(closeOnEsc);
		return this;
	}

	/**
	 * Sets whether this dialog can be closed by clicking outside of it or not.
	 * <p>
	 * By default, the dialog is closable with an outside click.
	 *
	 * @param closeOnOutsideClick
	 *            {@code true} to enable closing this dialog with an outside
	 *            click, {@code false} to disable it
	 */
	public EnhancedDialogBuilder setCloseOnOutsideClick(final boolean closeOnOutsideClick) {
		this.getComponent().setCloseOnOutsideClick(closeOnOutsideClick);
		return this;
	}

	/**
	 * Sets whether component will open modal or modeless dialog.
	 * <p>
	 * Note: When dialog is set to be modeless, then it's up to you to provide
	 * means for it to be closed (eg. a button that calls {@link Dialog#close()}).
	 * The reason being that a modeless dialog allows user to interact with the
	 * interface under it and won't be closed by clicking outside or the ESC key.
	 * 
	 * @param modal 
	 *          {@code false} to enable dialog to open as modeless modal,
	 *          {@code true} otherwise.
	 */
	public EnhancedDialogBuilder setModal(final boolean modal) {
		this.getComponent().setModal(modal);
		return this;
	}

	/**
	 * Sets whether dialog is enabled to be dragged by the user or not.
	 * <p>
	 * To allow an element inside the dialog to be dragged by the user
	 * (for instance, a header inside the dialog), a class {@code "draggable"}
	 * can be added to it (see {@link HasStyle#addClassName(String)}).
	 * <p>
	 * Note: If draggable is enabled and dialog is opened without first
	 * being explicitly attached to a parent, then it won't restore its
	 * last position in the case the user closes and opens it again.
	 * Reason being that a self attached dialog is removed from the DOM
	 * when it's closed and position is not synched.
	 * 
	 * @param draggable 
	 *          {@code true} to enable dragging of the dialog,
	 *          {@code false} otherwise
	 */
	public EnhancedDialogBuilder setDraggable(final boolean draggable) {
		this.getComponent().setDraggable(draggable);
		return this;
	}

	/**
	 * Sets whether dialog can be resized by user or not.
	 * 
	 * @param resizable 
	 *          {@code true} to enabled resizing of the dialog,
	 *          {@code false} otherwise. 
	 */
	public EnhancedDialogBuilder setResizable(final boolean resizable) {
		this.getComponent().setResizable(resizable);
		return this;
	}

}