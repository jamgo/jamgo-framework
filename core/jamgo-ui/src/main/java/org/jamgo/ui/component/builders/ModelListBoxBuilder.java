package org.jamgo.ui.component.builders;

import org.jamgo.model.entity.Model;
import org.jamgo.services.impl.ServiceManager;
import org.jamgo.ui.layout.BackofficeApplicationDef;
import org.jamgo.vaadin.ui.builder.JamgoComponentBuilder;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.listbox.MultiSelectListBox;
import com.vaadin.flow.data.renderer.TextRenderer;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ModelListBoxBuilder<T extends Model> extends JamgoComponentBuilder<MultiSelectListBox<T>, ModelListBoxBuilder<T>> implements InitializingBean {

	@Autowired
	private ServiceManager serviceManager;
	@Autowired
	protected BackofficeApplicationDef backofficeApplicationDef;

	private Class<T> modelClass;

	public ModelListBoxBuilder(final Class<T> modelClass) {
		super();
		this.modelClass = modelClass;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new MultiSelectListBox<>();
	}

	@Override
	public MultiSelectListBox<T> build() {
		super.build();

//		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));
		this.instance.setRenderer(new TextRenderer<>(o -> o.toListString()));
		this.instance.setItems(this.serviceManager.getService(this.modelClass).findAll());

		return this.instance;
	}

	public Class<T> getModelClass() {
		return this.modelClass;
	}

	public void setModelClass(final Class<T> modelClass) {
		this.modelClass = modelClass;
	}

}