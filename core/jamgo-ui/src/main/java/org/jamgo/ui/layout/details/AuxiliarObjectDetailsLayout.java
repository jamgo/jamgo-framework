package org.jamgo.ui.layout.details;

import org.jamgo.model.entity.AuxiliarObject;
import org.jamgo.ui.component.JamgoFormLayout;
import org.jamgo.ui.component.LocalizedTextField;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;

/**
 * @deprecated Use {@link CategoryDetailsLayout} instead.
 */
@Deprecated
public abstract class AuxiliarObjectDetailsLayout<T extends AuxiliarObject> extends CrudDetailsLayout<T> {

	private static final long serialVersionUID = 1L;

	private LocalizedTextField nameField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		final CrudDetailsPanel panel = this.componentBuilderFactory.createCrudDetailsPanelBuilder()
			.setName(this.messageSource.getMessage("form.basic.info"))
			.build();

		final JamgoFormLayout mainFormLayout = (JamgoFormLayout) panel.getFormLayout();

		this.nameField = this.componentBuilderFactory.createLocalizedTextFieldBuilder()
			.build();
		this.binder.bind(this.nameField, T::getName, T::setName);

		mainFormLayout.addFormComponent(this.nameField, "auxiliarObject.name");

		return panel;
	}

	@Override
	protected abstract Class<T> getTargetObjectClass();
}
