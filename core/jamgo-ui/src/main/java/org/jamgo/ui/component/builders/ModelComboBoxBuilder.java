package org.jamgo.ui.component.builders;

import java.util.List;

import org.jamgo.model.entity.Model;
import org.jamgo.services.impl.ServiceManager;
import org.jamgo.ui.component.ModelComboBox;
import org.jamgo.ui.layout.BackofficeApplicationDef;
import org.jamgo.vaadin.builder.base.HasEnabledBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.jamgo.vaadin.ui.builder.JamgoComponentBuilder;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.ItemLabelGenerator;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ModelComboBoxBuilder<T extends Model> extends JamgoComponentBuilder<ModelComboBox<T>, ModelComboBoxBuilder<T>> implements
	InitializingBean,
	HasSizeBuilder<ModelComboBoxBuilder<T>, ModelComboBox<T>>,
	HasValueBuilder<ModelComboBoxBuilder<T>, ModelComboBox<T>>,
	HasEnabledBuilder<ModelComboBoxBuilder<T>, ModelComboBox<T>> {

	@Autowired
	private ServiceManager serviceManager;
	@Autowired
	protected BackofficeApplicationDef backofficeApplicationDef;

	private Class<T> modelClass;

	public ModelComboBoxBuilder(final Class<T> modelClass) {
		super();
		this.modelClass = modelClass;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = this.applicationContext.getBean(ModelComboBox.class, this.backofficeApplicationDef.getCrudLayoutDef(this.modelClass));
	}

	@Override
	public ModelComboBox<T> build() {
		super.build();

		this.instance.setItemLabelGenerator(o -> o.toListString());
		final List<T> allItems = this.serviceManager.getService(this.modelClass).findAll();
		allItems.sort((a, b) -> a.toListString().compareTo(b.toListString()));
		this.instance.setItems(allItems);

		return this.instance;
	}

	public Class<T> getModelClass() {
		return this.modelClass;
	}

	public void setModelClass(final Class<T> modelClass) {
		this.modelClass = modelClass;
	}

	public ModelComboBoxBuilder<T> hideActions() {
		this.getComponent().hideActions();
		return this;
	}

	public ModelComboBoxBuilder<T> setCreateVisible(final boolean createVisible) {
		this.getComponent().setCreateVisible(createVisible);
		return this;
	}

	public ModelComboBoxBuilder<T> setEditVisible(final boolean editVisible) {
		this.getComponent().setEditVisible(editVisible);
		return this;
	}

	public ModelComboBoxBuilder<T> setViewVisible(final boolean viewVisible) {
		this.getComponent().setViewVisible(viewVisible);
		return this;
	}

	public ModelComboBoxBuilder<T> setViewEmbedded(final boolean viewEmbedded) {
		this.getComponent().setViewEmbedded(viewEmbedded);
		return this;
	}

	public ModelComboBoxBuilder<T> setClearButtonVisible(final boolean clearButtonVisible) {
		this.getComponent().setClearButtonVisible(clearButtonVisible);
		return this;
	}

	public ModelComboBoxBuilder<T> setItemLabelGenerator(final ItemLabelGenerator<T> itemLabelGenerator) {
		this.getComponent().setItemLabelGenerator(itemLabelGenerator);
		return this;
	}

}