package org.jamgo.ui.component.builders;

import org.jamgo.ui.component.JamgoFormLayout;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;
import org.jamgo.vaadin.ui.builder.JamgoComponentBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep.LabelsPosition;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class JamgoFormLayoutBuilder extends JamgoComponentBuilder<JamgoFormLayout, JamgoFormLayoutBuilder> {

	private ComponentColspan defaultColspan = ComponentColspan.SIX_COLUMNS;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = this.applicationContext.getBean(JamgoFormLayout.class);
	}

	public JamgoFormLayoutBuilder withDefaultColspan(final ComponentColspan defaultColspan) {
		this.defaultColspan = defaultColspan;
		return this;
	}

	@Override
	public JamgoFormLayout build() {
		super.build();
		this.instance.setDefaultColspan(this.defaultColspan);
		this.instance.setResponsiveSteps(new FormLayout.ResponsiveStep("0px", 12, LabelsPosition.TOP));
		return this.instance;
	}
}