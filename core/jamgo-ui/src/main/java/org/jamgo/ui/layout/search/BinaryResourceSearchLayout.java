package org.jamgo.ui.layout.search;

import org.jamgo.model.search.BinaryResourceSearchSpecification;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.ui.layout.crud.CrudSearchLayout;
import org.jamgo.vaadin.components.JamgoComponentConstants;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.textfield.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BinaryResourceSearchLayout extends CrudSearchLayout<BinaryResourceSearchSpecification> {
	private static final long serialVersionUID = 1L;

	private TextField nameField;
	private TextField mimeTypeField;

	@Override
	public CrudDetailsPanel createPanel() {
		final CrudDetailsPanel panel = this.componentBuilderFactory.createCrudDetailsPanelBuilder()
			.setName(this.messageSource.getMessage("form.basic.info")).build();

		this.addFileNameField(panel, "binaryresource.fileName", JamgoComponentConstants.ComponentColspan.TWELVE_COLUMNS);
		this.addMimeTypeField(panel, "binaryresource.mimeType", JamgoComponentConstants.ComponentColspan.TWELVE_COLUMNS);

		return panel;
	}

	private void addFileNameField(final CrudDetailsPanel panel, final String fieldKey, final ComponentColspan colspan) {
		this.nameField = this.componentBuilderFactory.createTextFieldBuilder().build();
		panel.addFormComponent(this.nameField, fieldKey, colspan);
		this.binder.forField(this.nameField)
			.bind(o -> o.getFileName(), (o, v) -> o.setFileName(v));
	}

	private void addMimeTypeField(final CrudDetailsPanel panel, final String fieldKey, final ComponentColspan colspan) {
		this.mimeTypeField = this.componentBuilderFactory.createTextFieldBuilder().build();
		panel.addFormComponent(this.mimeTypeField, fieldKey, colspan);
		this.binder.forField(this.mimeTypeField)
			.bind(o -> o.getMimeType(), (o, v) -> o.setMimeType(v));
	}

	@Override
	protected Class<BinaryResourceSearchSpecification> getTargetObjectClass() {
		return BinaryResourceSearchSpecification.class;
	}
}
