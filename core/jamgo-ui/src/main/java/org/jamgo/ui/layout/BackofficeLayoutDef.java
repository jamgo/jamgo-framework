package org.jamgo.ui.layout;

import java.util.Optional;
import java.util.function.Supplier;

public abstract class BackofficeLayoutDef {

	protected String id;
	protected Supplier<Object> nameSupplier;
	protected Supplier<Object> descriptionSupplier;
	protected Supplier<Object> countSupplier;
	protected String imageName;
	protected boolean openOnStart;

	public BackofficeLayoutDef() {
		super();
		this.openOnStart = false;
	}

	public String getId() {
		return this.id;
	}

	public String getName() {
		return Optional.ofNullable(this.nameSupplier).map(f -> f.get().toString()).orElse(null);
	}

	public String getDescription() {
		return Optional.ofNullable(this.descriptionSupplier).map(f -> f.get().toString()).orElse(null);
	}

	public String getCountSupplier() {
		return Optional.ofNullable(this.countSupplier).map(f -> f.get().toString()).orElse(null);
	}

	public Supplier<Object> getNameSupplier() {
		return this.nameSupplier;
	}

	public Supplier<Object> getDescriptionSupplier() { return this.descriptionSupplier; }

	public String getImageName() {
		return this.imageName;
	}

	public boolean isOpenOnStart() {
		return this.openOnStart;
	}

}