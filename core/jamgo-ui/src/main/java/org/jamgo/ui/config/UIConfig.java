package org.jamgo.ui.config;

import java.text.DecimalFormat;

import org.jamgo.services.config.ServicesConfig;
import org.jamgo.services.session.SessionContext;
import org.jamgo.services.session.SessionContextImpl;
import org.jamgo.ui.layout.menu.BackofficeMenuLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

@Configuration
@Import({ ServicesConfig.class })
@ComponentScan(
	basePackageClasses = {
		org.jamgo.ui.component.PackageMarker.class,
		org.jamgo.ui.config.PackageMarker.class,
		org.jamgo.ui.layout.PackageMarker.class,
		org.jamgo.ui.security.PackageMarker.class,
		org.jamgo.ui.view.PackageMarker.class,
		org.jamgo.vaadin.ui.builder.PackageMarker.class
	})
public class UIConfig {

	public static final Logger logger = LoggerFactory.getLogger(UIConfig.class);

	@Bean
	@Scope(scopeName = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
	public SessionContext sessionContext() {
		return new SessionContextImpl();
	}

	@Bean
	public DecimalFormat df() {
		return new DecimalFormat("###.##");
	}

	@Bean
	@Scope(BeanDefinition.SCOPE_PROTOTYPE)
	@ConditionalOnMissingBean
	public BackofficeMenuLayout backofficeMenuLayout() {
		UIConfig.logger.debug("Using default BackofficeMenuLayout class");
		return new BackofficeMenuLayout();
	}
}
