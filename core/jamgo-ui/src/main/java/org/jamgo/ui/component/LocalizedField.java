package org.jamgo.ui.component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.jamgo.model.entity.Language;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.componentfactory.EnhancedDialog;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.HasValidation;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public abstract class LocalizedField<FIELD_TYPE extends AbstractField<?, VALUE>, VALUE extends Object, TARGET> extends CustomField<TARGET> {

	private static final long serialVersionUID = 1L;

	public enum DisplayMode {
		ONE_LANGUAGE, ALL_LANGUAGES
	}

	@Autowired
	protected ApplicationContext applicationContext;

	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;

	@Autowired
	protected LocalizedObjectService localizedObjectService;

	private List<Language> languages;
	private Language currentLanguage;
	private FIELD_TYPE field;
	private Map<Language, FIELD_TYPE> fieldMap = new HashMap<>();
	private DisplayMode displayMode;

	private EnhancedDialog dialog;

	public LocalizedField() {
		this.displayMode = DisplayMode.ALL_LANGUAGES;
		this.currentLanguage = null;
		this.setWidth(100, Unit.PERCENTAGE);
	}

	private Map<Language, FIELD_TYPE> getFieldMap() {
		if (this.fieldMap.isEmpty()) {
			this.languages.forEach(language -> {
				final FIELD_TYPE field = this.createBasicField(language);
				this.fieldMap.put(language, field);
			});
		}
		return this.fieldMap;
	}

	private FIELD_TYPE createBasicField(final Language language) {
		final FIELD_TYPE field = this.createBasicField();
		field.addValueChangeListener(event -> this.updateFieldValue(language, field));
		return field;
	}

	protected abstract FIELD_TYPE createBasicField();

	protected abstract void updateFieldValue(final Language language, final FIELD_TYPE field);

	public void initContent() {
		if (this.displayMode == DisplayMode.ONE_LANGUAGE) {
			this.field = this.createBasicField(this.getCurrentLanguageOrDefault());

			// Show button next to the field -> onclick show popup with all language fields
			final HorizontalLayout layout = new HorizontalLayout();
			layout.setWidth(100, Unit.PERCENTAGE);
			layout.setSpacing(false);

			final Button viewButton = this.componentBuilderFactory.createButtonBuilder()
				.addClassName("model-combo-box-button")
				.setIcon(VaadinIcon.GLOBE.create())
				.setVisible(true)
				.setEnabled(true)
				.build();
			viewButton.addClickListener(event -> this.doView());

			layout.add(this.field, viewButton);
			layout.setFlexGrow(1, this.field);

			this.add(layout);
		} else {
			this.add(this.createGrid());
		}
	}

	private void doView() {
		this.dialog = new EnhancedDialog();
		this.dialog.setModal(true);
		this.dialog.setCloseOnEsc(false);
		this.dialog.setCloseOnOutsideClick(false);
		this.dialog.setMinWidth(50, Unit.PERCENTAGE);
		this.dialog.setHeader("Localized field"); // FIXME Change title

		final JamgoFormLayout layout = this.createFormLayout();
		final Button closeButton = this.componentBuilderFactory.createButtonBuilder()
			.setLabel("Tancar") // FIXME: I18N
			.build();
		closeButton.addClickListener(event -> this.updateValueAndCloseDialog());

		final VerticalLayout content = new VerticalLayout();
		content.setWidth(100, Unit.PERCENTAGE);
		content.setSpacing(true);
		content.add(layout, closeButton);
		content.setAlignItems(Alignment.CENTER);

		this.dialog.setContent(content);
		this.dialog.open();
	}

	private JamgoFormLayout createFormLayout() {
		final JamgoFormLayout layout = this.componentBuilderFactory.createJamgoFormLayoutBuilder()
			.build();

		this.languages.forEach(language -> this.createFormLayoutField(layout, language));

		return layout;
	}

	private void createFormLayoutField(final JamgoFormLayout layout, final Language language) {
		final FIELD_TYPE field = this.createBasicField(language);
//		LocalizedField.logger.debug("creating basic field for language {} (localeCode = {})", language, language.getLocaleCode());
		field.setValue(this.getFieldValue(this.getValue(), language));
		layout.addFormComponent(field, language.getName(), ComponentColspan.FULL_WIDTH);
	}

	protected abstract VALUE getFieldValue(final TARGET value, final Language language);

	private void updateValueAndCloseDialog() {
		if (this.displayMode == DisplayMode.ONE_LANGUAGE) {
			// Update ONE_LANGUAGE field outside the dialog
			final Language language = this.getCurrentLanguageOrDefault();
			if (this.getValue() != null) {
				this.field.setValue(this.getFieldValue(this.getValue(), language));
			}
		}

		// Close dialog
		this.dialog.close();
		this.dialog.removeAll();
		this.dialog = null;
	}

	private Language getCurrentLanguageOrDefault() {
		return Optional.ofNullable(this.currentLanguage).orElse(this.languages.get(0));
	}

	private Grid<Language> createGrid() {
		final Grid<Language> grid = new Grid<>();
		grid.setWidth(100, Unit.PERCENTAGE);
		grid.setSelectionMode(SelectionMode.NONE);
		grid.addColumn(language -> language.getName()).setWidth("120px").setResizable(false).setFlexGrow(0);
		grid.addComponentColumn(language -> {
			final FIELD_TYPE field = this.getFieldMap().get(language);
//			LocalizedField.logger.debug("creating basic field for language {} (localeCode = {})", language, language.getLocaleCode());
			field.setValue(this.getFieldValue(this.getValue(), language));
			return field;
		})
			.setResizable(false)
			.setFlexGrow(1);
		grid.setItems(this.languages);
		grid.setAllRowsVisible(true);

		return grid;
	}

	@Override
	protected TARGET generateModelValue() {
		return this.getValue();
	}

	@Override
	protected void setPresentationValue(final TARGET value) {
		if (this.displayMode == DisplayMode.ONE_LANGUAGE) {
			final Language language = this.getCurrentLanguageOrDefault();
			final VALUE fieldValue = this.getFieldValue(value, language);
			this.field.setValue(fieldValue);
		} else { // ALL_LANGUAGES
			this.getFieldMap().entrySet().forEach(entry -> {
				final Language language = entry.getKey();
				final VALUE fieldValue = this.getFieldValue(value, language);
				final FIELD_TYPE field = entry.getValue();
				field.setValue(fieldValue);
			});
		}
	}

	public List<Language> getLanguages() {
		return this.languages;
	}

	public void setLanguages(final List<Language> languages) {
		this.languages = languages;
	}

	public Language getCurrentLanguage() {
		return this.currentLanguage;
	}

	public void setCurrentLanguage(final Language currentLanguage) {
		this.currentLanguage = currentLanguage;
	}

	public DisplayMode getDisplayMode() {
		return this.displayMode;
	}

	public void setDisplayMode(final DisplayMode displayMode) {
		this.displayMode = displayMode;
	}

	public void setFieldMap(final Map<Language, FIELD_TYPE> fieldMap) {
		this.fieldMap = fieldMap;
	}

	@Override
	public void setErrorMessage(final String errorMessage) {
		if (this.field instanceof HasValidation) {
			((HasValidation) this.field).setErrorMessage(errorMessage);
		}
	}

	@Override
	public String getErrorMessage() {
		if (this.field instanceof HasValidation) {
			return ((HasValidation) this.field).getErrorMessage();
		}
		return null;
	}

	@Override
	public void setInvalid(final boolean invalid) {
		if (this.field instanceof HasValidation) {
			((HasValidation) this.field).setInvalid(invalid);
		}
	}

	@Override
	public boolean isInvalid() {
		if (this.field instanceof HasValidation) {
			return ((HasValidation) this.field).isInvalid();
		}
		return false;
	}

}
