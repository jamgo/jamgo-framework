package org.jamgo.ui.layout.details;

import org.jamgo.model.entity.Category;
import org.jamgo.ui.component.JamgoFormLayout;
import org.jamgo.ui.component.LocalizedTextField;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;

public abstract class CategoryDetailsLayout<T extends Category> extends CrudDetailsLayout<T> {

	private static final long serialVersionUID = 1L;

	/**
	 * Main panel where all fields are created.
	 * Use this field in subclasses if you need to modify its default attributes.
	 */
	protected CrudDetailsPanel mainPanel;

	/**
	 * Use this field in subclasses to add more fields to the main panel form layout
	 */
	protected JamgoFormLayout mainFormLayout;

	protected LocalizedTextField nameField;
	protected LocalizedTextField descriptionField;
	protected boolean useDescriptionField = true;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		this.mainPanel = this.componentBuilderFactory.createCrudDetailsPanelBuilder()
			.setName(this.messageSource.getMessage("form.basic.info"))
			.build();

		this.mainFormLayout = (JamgoFormLayout) this.mainPanel.getFormLayout();

		this.addNameField(this.mainFormLayout);
		if (this.useDescriptionField) {
			this.addDescriptionField(this.mainFormLayout);
		}

		return this.mainPanel;
	}

	private void addNameField(final JamgoFormLayout formLayout) {
		this.nameField = this.componentBuilderFactory.createLocalizedTextFieldBuilder()
			.build();
		this.nameField.setRequiredIndicatorVisible(true);
		this.binder.forField(this.nameField)
			.asRequired()
			.bind(T::getName, T::setName);
		formLayout.addFormComponent(this.nameField, "category.name");
	}

	private void addDescriptionField(final JamgoFormLayout formLayout) {
		this.descriptionField = this.componentBuilderFactory.createLocalizedTextFieldBuilder()
			.build();
		this.binder.forField(this.descriptionField)
			.bind(T::getDescription, T::setDescription);
		formLayout.addFormComponent(this.descriptionField, "category.description");
	}

	@Override
	protected abstract Class<T> getTargetObjectClass();

	/**
	 * By default, a {@link Category} has name (required) and description.
	 * If your organization doesn't care about description field in this entity,
	 * you can hide the field using this method after initialization.
	 */
	protected void hideDescriptionField() {
		this.useDescriptionField = false;
	}

}
