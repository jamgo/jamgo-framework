package org.jamgo.ui.layout.utils.ie;

import java.io.InputStream;
import java.util.List;

import org.jamgo.model.entity.BasicModelEntity;
import org.jamgo.model.portable.ImportStats;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author jamgo
 *
 * @deprecated Use {@link org.jamgo.services.ie.Importer} from jamgo-services module instead
 * 
 * @param <T>
 */
@Deprecated
public interface Importer<T extends BasicModelEntity<?>> {

	ImportStats<T> extractData(InputStream is) throws Exception;

	ImportStats<T> refreshImport();

	ImportStats<T> updateImportCounts();

	String[] getColumnNames();

	String[] getColumnHeaders();

	List<String> getErrors();

	@Transactional
	ImportStats<T> doImport();

	void setDeleteAllBeforeImport(Boolean value);

	boolean deleteAllBeforeImport();

}
