package org.jamgo.ui;

import org.springframework.security.core.context.SecurityContextHolder;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinRequest;

public class MainUI extends UI {
	private static final long serialVersionUID = 1L;

	@Override
	protected void init(final VaadinRequest vaadinRequest) {
//		Responsive.makeResponsive(this);

//		UI.getCurrent().getNavigator().navigateTo(MainView.NAME);
	}

	public void logout() {
		this.getSession().close();
		SecurityContextHolder.clearContext();
		UI.getCurrent().getPage().setLocation("");
	}

}