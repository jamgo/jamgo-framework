package org.jamgo.ui.config;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jamgo.ui.view.AbstractLoginView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import com.vaadin.flow.server.VaadinServletRequest;
import com.vaadin.flow.server.VaadinServletResponse;

/**
 * HttpSessionRequestCache that avoids saving internal framework requests.
 */
public class VaadinRequestCache extends HttpSessionRequestCache {

	private static final Logger logger = LoggerFactory.getLogger(VaadinRequestCache.class);

	@Autowired
	private ServletContext servletContext;

	/**
	 * {@inheritDoc}
	 *
	 * If the method is considered an internal request from the framework, we skip
	 * saving it.
	 *
	 * @see VaadinSecurityUtils#isFrameworkInternalRequest(HttpServletRequest)
	 */
	@Override
	public void saveRequest(final HttpServletRequest request, final HttpServletResponse response) {
		if (!VaadinSecurityUtils.isFrameworkInternalRequest(request) && !request.getRequestURI().equals("/error")) {
			super.saveRequest(request, response);
		}
	}

	public String resolveRedirectUrl() {
		final SavedRequest savedRequest = this.getRequest(
			VaadinServletRequest.getCurrent().getHttpServletRequest(),
			VaadinServletResponse.getCurrent().getHttpServletResponse());
		if (savedRequest instanceof DefaultSavedRequest) {
			final String requestURI = ((DefaultSavedRequest) savedRequest).getRequestURI();
			VaadinRequestCache.logger.debug(String.format("VaadinRequestCache saved request: %s", requestURI));

			final String contextPath = this.servletContext.getContextPath();

			// check for valid URI and prevent redirecting to the login view
			if (requestURI != null && !requestURI.isEmpty() && !requestURI.contains(AbstractLoginView.NAME)) {
				return requestURI.startsWith(contextPath) ? requestURI.substring(contextPath.length()) : requestURI;
			}
		}

		// if everything fails, redirect to the main view
		return "";
	}
}