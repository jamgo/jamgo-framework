package org.jamgo.ui.layout;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.Model;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.defs.BasicModelLayoutDef;
import org.jamgo.ui.layout.defs.FunctionLayoutDef;
import org.jamgo.ui.layout.defs.LayoutDefComponent;
import org.jamgo.ui.layout.details.SystemInfoLayout;
import org.jamgo.ui.security.layout.PermissionsLayout;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BackofficeApplicationDef {

	protected static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	protected static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	protected LocalizedObjectService localizedObjectService;
	@Autowired(required = false)
	@LayoutDefComponent
	protected List<BasicModelLayoutDef<? extends BasicModel<Long>, ? extends Model>> modelLayoutDefs;
	@Autowired(required = false)
	@LayoutDefComponent
	protected List<FunctionLayoutDef> customLayoutDefs;

	protected Map<Class<?>, BackofficeLayoutDef> layoutDefs;
	protected Map<Class<?>, List<BackofficeLayoutDef>> filteredLayoutDefs;

	@PostConstruct
	public void init() {
		this.layoutDefs = new LinkedHashMap<>();

		// Add automatically all model layoutDefs defined in the application
		if (this.modelLayoutDefs != null) {
			this.modelLayoutDefs.forEach(layoutDef -> {
				this.layoutDefs.put(layoutDef.getModelClass(), layoutDef.getBackofficeLayoutDef());
			});
		}

		// Add automatically all custom layoutDefs defined in the application
		if (this.customLayoutDefs != null) {
			this.customLayoutDefs.forEach(layoutDef -> {
				this.layoutDefs.put(layoutDef.getLayoutDefClass(), layoutDef.getBackofficeLayoutDef());
			});
		}

		// Add manually other layoutDefs for the application
		this.layoutDefs.put(PermissionsLayout.class, CustomLayoutDef.builder()
			.setId("permission")
			.setNameSupplier(() -> this.messageSource.getMessage("permissions.title"))
			.setLayoutClass(PermissionsLayout.class)
			.build());

		this.layoutDefs.put(SystemInfoLayout.class, CustomLayoutDef.builder()
			.setId("systemInfo")
			.setNameSupplier(() -> this.messageSource.getMessage("table.systemInfo"))
			.setLayoutClass(SystemInfoLayout.class)
			.build());
	}

	public Map<Class<?>, BackofficeLayoutDef> getLayoutDefs() {
		return this.layoutDefs;
	}

	public void setLayoutDefs(final Map<Class<?>, BackofficeLayoutDef> layoutDefs) {
		this.layoutDefs = layoutDefs;
	}

	public BackofficeLayoutDef getLayoutDef(final Class<?> referenceClass) {
		return Optional.ofNullable(this.layoutDefs.get(referenceClass))
			.orElse(this.createMissingLayoutDef(referenceClass));
	}

	public Map<Class<?>, List<BackofficeLayoutDef>> getFilteredLayoutDefs() {
		return this.filteredLayoutDefs;
	}

	public void setFilteredLayoutDefs(final Map<Class<?>, List<BackofficeLayoutDef>> filteredLayoutDefs) {
		this.filteredLayoutDefs = filteredLayoutDefs;
	}

	public List<BackofficeLayoutDef> getFilteredLayoutDef(final Class<?> model) {
		return this.filteredLayoutDefs.get(model);
	}

	public CrudLayoutDef<?, ?> getCrudLayoutDef(final Class<?> referenceClass) {
		final BackofficeLayoutDef layoutDef = this.layoutDefs.get(referenceClass);
		if (layoutDef != null && layoutDef instanceof CrudLayoutDef) {
			return (CrudLayoutDef<?, ?>) layoutDef;
		} else {
			return null;
		}
	}

	public List<CrudLayoutDef<?, ?>> getCrudLayoutDefs() {
		final List<CrudLayoutDef<?, ?>> crudDefLayouts = new ArrayList<>();
		this.layoutDefs.values().forEach(each -> {
			if (each instanceof CrudLayoutDef) {
				crudDefLayouts.add((CrudLayoutDef<?, ?>) each);
			}
		});
		return crudDefLayouts;
	}

	public List<CrudLayoutDef<?, ?>> getCrudFilteredLayoutDefs() {
		final List<CrudLayoutDef<?, ?>> crudDefLayouts = new ArrayList<>();
		this.filteredLayoutDefs.values().forEach(each -> {
			if (each instanceof CrudLayoutDef) {
				crudDefLayouts.add((CrudLayoutDef<?, ?>) each);
			}
		});
		return crudDefLayouts;
	}

	public List<CustomLayoutDef> getCustomLayoutDefs() {
		final List<CustomLayoutDef> crudDefLayouts = new ArrayList<>();
		this.layoutDefs.values().forEach(each -> {
			if (each instanceof CustomLayoutDef) {
				crudDefLayouts.add((CustomLayoutDef) each);
			}
		});
		return crudDefLayouts;
	}

	public BackofficeLayoutDef getOpenOnStartLayoutDef() {
		return this.layoutDefs.values().stream().filter(each -> each.isOpenOnStart()).findFirst().orElse(null);
	}

	protected BackofficeLayoutDef createMissingLayoutDef(final Class<?> referenceClass) {
		return CustomLayoutDef.builder()
			.setId(referenceClass.getName())
			.setNameSupplier(() -> String.format("Missing layout def: %s", referenceClass.getName()))
			.build();
	}
}
