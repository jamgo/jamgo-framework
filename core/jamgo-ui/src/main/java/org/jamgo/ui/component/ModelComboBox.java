package org.jamgo.ui.component;

import java.util.Set;

import org.jamgo.model.entity.Model;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.HasEnabled;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasValidation;
import com.vaadin.flow.component.HasValueAndElement;
import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.combobox.dataview.ComboBoxDataView;
import com.vaadin.flow.component.combobox.dataview.ComboBoxLazyDataView;
import com.vaadin.flow.component.combobox.dataview.ComboBoxListDataView;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.provider.BackEndDataProvider;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.HasDataView;
import com.vaadin.flow.data.provider.HasLazyDataView;
import com.vaadin.flow.data.provider.HasListDataView;
import com.vaadin.flow.data.provider.InMemoryDataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.function.SerializablePredicate;
import com.vaadin.flow.shared.Registration;

@org.springframework.stereotype.Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
@CssImport(value = "./styles/model-combo-box.css")
public class ModelComboBox<MODEL extends Model> extends ModelSelect<MODEL> implements
	HasSize,
	HasEnabled,
	HasValidation,
	HasValueAndElement<ComponentValueChangeEvent<ComboBox<MODEL>, MODEL>, MODEL>,
	HasDataView<MODEL, String, ComboBoxDataView<MODEL>>,
	HasListDataView<MODEL, ComboBoxListDataView<MODEL>>,
	HasLazyDataView<MODEL, String, ComboBoxLazyDataView<MODEL>>,
	InitializingBean {

	private static final long serialVersionUID = 1L;

	HorizontalLayout comboBoxLayout;
	ComboBox<MODEL> modelComboBox;
	private Button editButton;
	private Button viewButton;

	private boolean editVisible = true;
	private boolean viewVisible = true;
	private boolean viewEmbedded = false;

	public ModelComboBox(final CrudLayoutDef<MODEL, MODEL> crudLayoutDef) {
		super();
		this.crudLayoutDef = crudLayoutDef;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		this.getContent().addClassName("model-combo-box");
		this.getContent().setPadding(false);
		this.comboBoxLayout = new HorizontalLayout();
		this.comboBoxLayout.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxLayout.setSpacing(false);

		this.modelComboBox = this.componentBuilderFactory.<MODEL> createComboBoxBuilder()
			.setWidth(100, Unit.PERCENTAGE)
			.build();
		this.modelComboBox.addValueChangeListener(event -> this.doValueChanged(event.getValue()));

		this.viewButton = this.componentBuilderFactory.createButtonBuilder()
			.addClassName("model-combo-box-button")
			.setIcon(VaadinIcon.EYE.create())
			.setVisible(this.viewVisible)
			.setEnabled(false)
			.build();
		this.viewButton.addClickListener(event -> this.doView());

		this.editButton = this.componentBuilderFactory.createButtonBuilder()
			.addClassName("model-combo-box-button")
			.setIcon(VaadinIcon.PENCIL.create())
			.setVisible(this.editVisible)
			.setEnabled(false)
			.build();
		this.editButton.addClickListener(event -> this.doEdit());

		this.createButton = this.componentBuilderFactory.createButtonBuilder()
			.addClassName("model-combo-box-last-button")
			.setIcon(VaadinIcon.FILE_O.create())
			.setVisible(this.createVisible)
			.build();
		this.createButton.addClickListener(event -> this.doCreateNew());

		this.comboBoxLayout.add(this.modelComboBox, this.viewButton, this.editButton, this.createButton);
		this.comboBoxLayout.setFlexGrow(1, this.modelComboBox);

		this.getContent().add(this.comboBoxLayout);
	}

	private void doValueChanged(final MODEL item) {
		if (item == null) {
			this.hideDetails();
			this.refreshButtonsVisibility();
		} else {
			if (this.detailsLayout != null && this.detailsLayout.isVisible()) {
				this.refreshDetails(item);
			}
			this.refreshButtonsVisibility();
		}
	}

	private void doView() {
		if (this.viewEmbedded) {
			this.switchDetails(this.getValue());
		} else {
			this.openDetailsDialog(this.getValue(), false);
		}
	}

	private void doEdit() {
		this.openDetailsDialog(this.getValue(), true);
	}

	void hideDetails() {
		if (this.detailsLayout != null) {
			this.refreshDetails(null);
			this.detailsLayout.setVisible(false);
		}
		this.viewButton.setIcon(VaadinIcon.EYE.create());
	}

	private void showDetails(final MODEL item) {
		if (this.detailsLayout == null) {
			this.detailsLayout = this.createDetailsLayout(item.getClass(), false);
			this.getContent().add(this.detailsLayout);
		}
		this.refreshDetails(this.modelComboBox.getValue());
		this.detailsLayout.setReadOnly(true);
		this.detailsLayout.setVisible(true);
		this.viewButton.setIcon(VaadinIcon.EYE_SLASH.create());
	}

	private void refreshDetails(final MODEL item) {
		this.detailsLayout.updateFields(item);
	}

	protected void switchDetails(final MODEL item) {
		if (this.detailsLayout != null && this.detailsLayout.isVisible()) {
			this.hideDetails();
		} else {
			this.showDetails(item);
		}
	}

	private void refreshButtonsVisibility() {
		if (!this.modelComboBox.isEnabled()) {
			this.viewButton.setEnabled(false);
			this.editButton.setEnabled(false);
			this.createButton.setEnabled(false);
		} else if (this.modelComboBox.isReadOnly()) {
			if (this.modelComboBox.isEmpty()) {
				this.viewButton.setEnabled(false);
			} else {
				this.viewButton.setEnabled(true);
			}
			this.editButton.setEnabled(false);
			this.createButton.setEnabled(false);
		} else {
			if (this.modelComboBox.isEmpty()) {
				this.viewButton.setEnabled(false);
				this.editButton.setEnabled(false);
				this.createButton.setEnabled(true);
			} else {
				this.viewButton.setEnabled(true);
				this.editButton.setEnabled(true);
				this.createButton.setEnabled(true);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void setFilter(final SerializablePredicate<MODEL> filter) {
		((ListDataProvider<MODEL>) this.modelComboBox.getDataProvider()).setFilter(filter);
	}

	public Registration addViewModelClickListener(final ComponentEventListener<ClickEvent<Button>> listener) {
		return this.viewButton.addClickListener(listener);
	}

	public boolean isEditVisible() {
		return this.editVisible;
	}

	public void setEditVisible(final boolean editVisible) {
		this.editVisible = editVisible;
		this.editButton.setVisible(editVisible);
	}

	public boolean isViewVisible() {
		return this.viewVisible;
	}

	public void setViewVisible(final boolean viewVisible) {
		this.viewVisible = viewVisible;
		this.viewButton.setVisible(viewVisible);
	}

	public boolean isViewEmbedded() {
		return this.viewEmbedded;
	}

	public void setViewEmbedded(final boolean viewEmbedded) {
		this.viewEmbedded = viewEmbedded;
	}

	public void hideActions() {
		this.setViewVisible(false);
		this.setEditVisible(false);
		this.setCreateVisible(false);
	}

	@Override
	protected void clearContents() {
		this.modelComboBox.clear();
	}

	@Override
	protected void updateContents(final Set<MODEL> items) {
		this.modelComboBox.setItems(new ListDataProvider<>(this.serviceManager.getService(this.crudLayoutDef.getEntityClass()).findAll()));
		this.modelComboBox.setValue(items.stream().findFirst().get());
		this.hideDetails();
	}

	// ...	ComboBox delegation methods.

	@Override
	public ComboBoxLazyDataView<MODEL> setItems(final BackEndDataProvider<MODEL, String> dataProvider) {
		return this.modelComboBox.setItems(dataProvider);
	}

	@Override
	public ComboBoxLazyDataView<MODEL> getLazyDataView() {
		return this.modelComboBox.getLazyDataView();
	}

	@Override
	public ComboBoxListDataView<MODEL> setItems(final ListDataProvider<MODEL> dataProvider) {
		return this.modelComboBox.setItems(dataProvider);
	}

	@Override
	public ComboBoxListDataView<MODEL> getListDataView() {
		return this.modelComboBox.getListDataView();
	}

	@Override
	public ComboBoxDataView<MODEL> setItems(final DataProvider<MODEL, String> dataProvider) {
		return this.modelComboBox.setItems(dataProvider);
	}

	/**
	 * @see #ComboBox.setItems(InMemoryDataProvider)
	 * @deprecated does not work so don't use
	 */
	@Deprecated
	@Override
	public ComboBoxDataView<MODEL> setItems(final InMemoryDataProvider<MODEL> dataProvider) {
		return this.modelComboBox.setItems(dataProvider);
	}

	@Override
	public ComboBoxDataView<MODEL> getGenericDataView() {
		return this.modelComboBox.getGenericDataView();
	}

	@Override
	public void setValue(final MODEL value) {
		this.modelComboBox.setValue(value);
	}

	@Override
	public MODEL getValue() {
		return this.modelComboBox.getValue();
	}

	@Override
	public MODEL getEmptyValue() {
		return this.modelComboBox.getEmptyValue();
	}

	public void setClearButtonVisible(final boolean clearButtonVisible) {
		this.modelComboBox.setClearButtonVisible(clearButtonVisible);
	}

	public void setItemLabelGenerator(final ItemLabelGenerator<MODEL> itemLabelGenerator) {
		this.modelComboBox.setItemLabelGenerator(itemLabelGenerator);
	}

	protected void setPresentationValue(final MODEL newPresentationValue) {
		this.modelComboBox.setValue(newPresentationValue);
	}

	@Override
	public void setRequiredIndicatorVisible(final boolean required) {
		this.modelComboBox.setRequiredIndicatorVisible(required);
	}

	@Override
	public boolean isRequiredIndicatorVisible() {
		return this.modelComboBox.isRequiredIndicatorVisible();
	}

	@Override
	public void setReadOnly(final boolean readOnly) {
		this.modelComboBox.setReadOnly(readOnly);
	}

	@Override
	public boolean isReadOnly() {
		return this.modelComboBox.isReadOnly();
	}

	@Override
	public Registration addValueChangeListener(final ValueChangeListener<? super ComponentValueChangeEvent<ComboBox<MODEL>, MODEL>> listener) {
		// TODO Auto-generated method stub
		return this.modelComboBox.addValueChangeListener(listener);
	}

	public void setRenderer(final Renderer<MODEL> renderer) {
		this.modelComboBox.setRenderer(renderer);
	}

	@Override
	public void setErrorMessage(final String errorMessage) {
		this.modelComboBox.setErrorMessage(errorMessage);
	}

	@Override
	public String getErrorMessage() {
		return this.modelComboBox.getErrorMessage();
	}

	@Override
	public void setInvalid(final boolean invalid) {
		this.modelComboBox.setInvalid(invalid);

	}

	@Override
	public boolean isInvalid() {
		return this.modelComboBox.isInvalid();
	}

}
