package org.jamgo.ui.layout.defs;

import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.ui.layout.CustomLayoutDef;

public abstract class FunctionLayoutDef extends AbstractLayoutDef {

	public FunctionLayoutDef(final LocalizedMessageService messageSource, final LocalizedObjectService localizedObjectService) {
		super(messageSource, localizedObjectService);
	}

	@Override
	public abstract CustomLayoutDef getBackofficeLayoutDef();

}
