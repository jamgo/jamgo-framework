package org.jamgo.ui.component.builders;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.jamgo.model.entity.Language;
import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.model.repository.LanguageRepository;
import org.jamgo.ui.component.LocalizedField;
import org.jamgo.ui.component.LocalizedField.DisplayMode;
import org.jamgo.vaadin.ui.builder.JamgoComponentBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class LocalizedFieldBuilder<T extends LocalizedField<?, ?, ?>> extends JamgoComponentBuilder<T, LocalizedFieldBuilder<T>> {

	private static final Logger logger = LoggerFactory.getLogger(LocalizedFieldBuilder.class);

	@Autowired
	private LanguageRepository languageRepository;

	protected List<Language> languages;
	protected DisplayMode displayMode = DisplayMode.ALL_LANGUAGES;

	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			this.languages = this.languageRepository.findByActiveTrue();
			// Set "default language" in first position
			Collections.sort(this.languages, Language.DEFAULT_FIRST);
		} catch (final RepositoryForClassNotDefinedException e) {
			LocalizedFieldBuilder.logger.info(e.getMessage());
		}
	}

	public LocalizedFieldBuilder<T> setDisplayMode(final DisplayMode displayMode) {
		this.displayMode = displayMode;
		return this;
	}

	protected Language getLanguage(final Locale locale) {
		return this.languages.stream().filter(each -> each.getLanguageCode().equals(locale.getLanguage())).findFirst().get();
	}

	@Override
	public T build() {
		super.build();
		Optional.ofNullable(this.label).ifPresent(label -> this.instance.setLabel(label));
		this.instance.setLanguages(this.languages);
		this.instance.setCurrentLanguage(this.getLanguage(this.messageSource.getLocale()));
		this.instance.setDisplayMode(this.displayMode);
		this.instance.initContent();
		return this.instance;
	}

}
