package org.jamgo.ui.layout.details;

import org.jamgo.model.entity.LocalizedMessage;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.textfield.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LocalizedMessageDetailsLayout extends CrudDetailsLayout<LocalizedMessage> {

	private static final long serialVersionUID = 1L;

	private TextField keyField;
	private Span parametersIntroField;
	private Span parametersInfoField;

//	TODO
//	private TextField valueField;

//	private JmgLocalizedTextArea descriptionField;

//	TODO
//	private TextArea descriptionField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		final CrudDetailsPanel panel = this.componentBuilderFactory.createCrudDetailsPanelBuilder().build();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.keyField = this.componentBuilderFactory.createTextFieldBuilder()
			.setWidth(100, Unit.PERCENTAGE)
			.build();
		panel.addFormComponent(this.keyField, "localizedMessage.key");
		this.binder.bind(this.keyField, LocalizedMessage::getKey, LocalizedMessage::setKey);

		this.parametersIntroField = this.componentBuilderFactory.createLabelBuilder()
			.setWidth(100, Unit.PERCENTAGE)
			.build();
//		this.parametersIntroField.setContentMode(ContentMode.HTML);
		panel.addFormComponent(this.parametersIntroField, "localizedMessage.parameters.intro");

		this.parametersInfoField = this.componentBuilderFactory.createLabelBuilder()
			.setWidth(100, Unit.PERCENTAGE)
			.build();
//		this.parametersInfoField.setContentMode(ContentMode.HTML);
		panel.addFormComponent(this.parametersInfoField, "localizedMessage.parameters");

		// TODO
//		this.valueField = this.componentBuilderFactory.createTextFieldBuilder().setLabelId("localizedMessage.value").build();
//		this.valueField.setWidth(100, Unit.PERCENTAGE);
//		panel.add(this.valueField);
//		this.binder.bind(this.valueField, o -> o.getValue().getDefaultText(), o -> o.setValue(this.valueField));

		// TODO
//		this.descriptionField = (JmgLocalizedRichTextArea) this.componentBuilderFactory.createLocalizedRichTextAreaBuilder().setCaption("localizedMessage.description").setWidth(100, Unit.PERCENTAGE).build();
//		this.descriptionField.setMaxLength(2048);
//		panel.addComponent(this.descriptionField);
//		this.binder.bind(this.descriptionField, LocalizedMessage::getDescription, LocalizedMessage::setDescription);

		return panel;
	}

	@Override
	public void updateFields(final Object savedItem) {
		super.updateFields(savedItem);

		this.parametersIntroField.setVisible(((LocalizedMessage) savedItem).getParametersInfo() != null);
		this.parametersInfoField.setVisible(((LocalizedMessage) savedItem).getParametersInfo() != null);
		// TODO
//		if (((LocalizedMessage) savedItem).getParametersInfo() != null) {
//			this.parametersInfoField.setValue(((LocalizedMessage) savedItem).getParametersInfo().get(this.locale));
//		}
	}

	@Override
	protected Class<LocalizedMessage> getTargetObjectClass() {
		return LocalizedMessage.class;
	}
}
