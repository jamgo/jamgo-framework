package org.jamgo.ui.layout.menu;

import org.jamgo.ui.layout.menu.BackofficeMenuLayout.BackofficeMenuGroup;

import com.vaadin.flow.component.icon.IconFactory;

public class MenuItemDef {

	private final Class<?> layoutDefClass;
	private IconFactory icon;
	private BackofficeMenuGroup menuGroup;
	private int order = Integer.MAX_VALUE;

	public MenuItemDef(final Class<?> layoutDefClass) {
		this.layoutDefClass = layoutDefClass;
	}

	public Class<?> getLayoutDefClass() {
		return this.layoutDefClass;
	}

	public IconFactory getIcon() {
		return this.icon;
	}

	public void setIcon(final IconFactory icon) {
		this.icon = icon;
	}

	public boolean hasIcon() {
		return this.icon != null;
	}

	public BackofficeMenuGroup getMenuGroup() {
		return this.menuGroup;
	}

	public void setMenuGroup(final BackofficeMenuGroup menuGroup) {
		this.menuGroup = menuGroup;
	}

	public int getOrder() {
		return this.order;
	}

	public void setOrder(final int order) {
		this.order = order;
	}

}
