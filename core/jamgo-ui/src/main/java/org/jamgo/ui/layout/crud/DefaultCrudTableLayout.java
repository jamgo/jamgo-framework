package org.jamgo.ui.layout.crud;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.BasicModelEntity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.spring.annotation.SpringComponent;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DefaultCrudTableLayout<T extends BasicModel<ID_TYPE>, E extends BasicModelEntity<ID_TYPE>, ID_TYPE> extends CrudTableLayout<T, E, ID_TYPE> {

	public DefaultCrudTableLayout(final CrudLayoutDef<T, E> crudLayoutDef) {
		super(crudLayoutDef);
	}

}
