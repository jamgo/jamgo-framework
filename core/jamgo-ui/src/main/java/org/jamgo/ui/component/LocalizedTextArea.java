package org.jamgo.ui.component;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.textfield.TextArea;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LocalizedTextArea extends LocalizedStringField<TextArea> {

	private static final long serialVersionUID = 1L;

	@Override
	protected TextArea createBasicField() {
		return this.componentBuilderFactory.createTextAreaBuilder()
			.setSizeFull()
			.build();
	}

}
