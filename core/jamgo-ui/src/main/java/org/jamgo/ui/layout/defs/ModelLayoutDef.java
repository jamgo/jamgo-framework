package org.jamgo.ui.layout.defs;

import org.jamgo.model.entity.Model;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.ui.layout.crud.CrudLayoutDef;

public abstract class ModelLayoutDef<MODEL extends Model> extends BasicModelLayoutDef<MODEL, MODEL> {

	public ModelLayoutDef(final LocalizedMessageService messageSource, final LocalizedObjectService localizedObjectService) {
		super(messageSource, localizedObjectService);
	}

	@Override
	public Class<MODEL> getLayoutDefClass() {
		return this.getModelClass();
	}

	@Override
	public abstract CrudLayoutDef<MODEL, MODEL> getBackofficeLayoutDef();

	@Override
	public abstract Class<MODEL> getModelClass();

}
