package org.jamgo.ui.layout.crud;

import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.jamgo.model.enums.Permission;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.SecurityService;
import org.jamgo.services.session.SessionContext;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;

public abstract class CrudLayout<T> extends VerticalLayout {

	private static final long serialVersionUID = 1L;

	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	protected SecurityService securityService;
	@Autowired
	@Qualifier("permissionProperties")
	protected Properties permissionProperties;
	@Autowired
	protected SessionContext sessionContext;

	protected Locale locale;
	protected Binder<T> binder;
	protected boolean binderReading = false;
	protected List<? extends Component> actionButtons;

	protected T targetObject;

	public void initialize() {
		this.setMargin(false);
		this.locale = this.sessionContext.getCurrentLocale();
		this.binder = new Binder<>(this.getTargetObjectClass());
	}

	protected void addActionButtons() {
		this.actionButtons = this.createActionButtons();
		final HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setMargin(false);
		buttonsLayout.setSpacing(false);
		for (final Component eachButton : this.actionButtons) {
			buttonsLayout.add(eachButton);
		}
		this.add(buttonsLayout);
		this.setHorizontalComponentAlignment(Alignment.CENTER, buttonsLayout);
	}

	public Binder<T> getBinder() {
		return this.binder;
	}

	public void setBinder(final Binder<T> binder) {
		this.binder = binder;
	}

	protected T getNewTargetObject() throws Exception {
		return this.getTargetObjectClass().getDeclaredConstructor().newInstance();
	}

	public T getTargetObject() {
		return this.targetObject;
	}

	public void setTargetObject(final T targetObject) {
		this.targetObject = targetObject;
	}

	public void updateFields() throws Exception {
		this.updateFields(this.getNewTargetObject());
	}

	@SuppressWarnings("unchecked")
	public void updateFields(final Object savedItem) {
		this.targetObject = (T) savedItem;
		this.binderReading = true;
		this.binder.readBean(this.targetObject);
		this.binderReading = false;
		this.setReadOnly(!this.securityService.hasPermission(this.targetObject, Permission.WRITE));
	}

	public boolean isBinderReading() {
		return this.binderReading;
	}

	public void setReadOnly(final boolean readOnly) {
		this.binder.setReadOnly(readOnly);
	}

	public void updateTargetObject() throws ValidationException {
		this.validateForm();
		this.binder.writeBean(this.targetObject);
	}

	protected void validateForm() throws ValidationException {
		if (this.binder.validate().hasErrors()) {
			throw new ValidationException(this.binder.validate().getFieldValidationErrors(),
				this.binder.validate().getBeanValidationErrors());
		}
	}

	public boolean hasChanges() {
		return this.binder.hasChanges();
	}

	protected abstract Class<T> getTargetObjectClass();

	protected abstract List<? extends Component> createActionButtons();

}
