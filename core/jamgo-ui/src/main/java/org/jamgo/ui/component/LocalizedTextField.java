package org.jamgo.ui.component;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.HasValidation;
import com.vaadin.flow.component.textfield.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LocalizedTextField extends LocalizedStringField<TextField> implements HasValidation {

	private static final long serialVersionUID = 1L;

	@Override
	protected TextField createBasicField() {
		return this.componentBuilderFactory.createTextFieldBuilder()
			.setSizeFull()
			.build();
	}

}
