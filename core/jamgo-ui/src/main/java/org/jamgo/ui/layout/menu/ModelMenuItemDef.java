package org.jamgo.ui.layout.menu;

import org.jamgo.model.BasicModel;

public abstract class ModelMenuItemDef<MODEL extends BasicModel<Long>> extends MenuItemDef {

	public ModelMenuItemDef(final Class<MODEL> modelClass) {
		super(modelClass);
	}

	@SuppressWarnings("unchecked")
	public Class<MODEL> getModelClass() {
		return (Class<MODEL>) this.getLayoutDefClass();
	}

}
