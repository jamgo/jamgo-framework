package org.jamgo.ui.component.builders;

import java.util.function.Predicate;

import org.jamgo.model.entity.Model;
import org.jamgo.ui.component.ModelManyToMany;
import org.jamgo.ui.component.ModelToMany;
import org.jamgo.ui.layout.BackofficeApplicationDef;
import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasEnabledBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.components.JamgoComponentConstants;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;
import org.jamgo.vaadin.ui.builder.JamgoComponentBuilder;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.ComponentUtil;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ModelManyToManyBuilder<MODEL extends Model, PARENT_MODEL extends Model>
	extends JamgoComponentBuilder<ModelManyToMany<MODEL, PARENT_MODEL>, ModelManyToManyBuilder<MODEL, PARENT_MODEL>>
	implements
	InitializingBean,
	HasSizeBuilder<ModelManyToManyBuilder<MODEL, PARENT_MODEL>, ModelManyToMany<MODEL, PARENT_MODEL>>,
	HasStyleBuilder<ModelManyToManyBuilder<MODEL, PARENT_MODEL>, ModelManyToMany<MODEL, PARENT_MODEL>>,
	FocusableBuilder<ModelManyToManyBuilder<MODEL, PARENT_MODEL>, ModelManyToMany<MODEL, PARENT_MODEL>, ModelToMany<MODEL, PARENT_MODEL>>,
	HasThemeBuilder<ModelManyToManyBuilder<MODEL, PARENT_MODEL>, ModelManyToMany<MODEL, PARENT_MODEL>>,
	HasEnabledBuilder<ModelManyToManyBuilder<MODEL, PARENT_MODEL>, ModelManyToMany<MODEL, PARENT_MODEL>> {

	@Autowired
	protected BackofficeApplicationDef backofficeApplicationDef;

	private Class<MODEL> modelClass;

	public ModelManyToManyBuilder(final Class<MODEL> modelClass) {
		super();
		this.modelClass = modelClass;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = this.applicationContext.getBean(ModelManyToMany.class, this.backofficeApplicationDef.getCrudLayoutDef(this.modelClass));
		ComponentUtil.setData(this.instance, JamgoComponentConstants.PROPERTY_DEFAULT_WIDTH, ComponentColspan.FULL_WIDTH);
	}

	@Override
	public ModelManyToMany<MODEL, PARENT_MODEL> build() {
		this.instance.init();
		super.build();

		return this.instance;
	}

	public Class<MODEL> getModelClass() {
		return this.modelClass;
	}

	public void setModelClass(final Class<MODEL> modelClass) {
		this.modelClass = modelClass;
	}

	public ModelManyToManyBuilder<MODEL, PARENT_MODEL> setCreateVisible(final boolean createVisible) {
		this.getComponent().setCreateVisible(createVisible);
		return this;
	}

	public ModelManyToManyBuilder<MODEL, PARENT_MODEL> setDeleteVisible(final boolean deleteVisible) {
		this.getComponent().setDeleteVisible(deleteVisible);
		return this;
	}

	public ModelManyToManyBuilder<MODEL, PARENT_MODEL> setDeleteVisiblePredicate(final Predicate<MODEL> deleteVisiblePredicate) {
		this.getComponent().setDeleteVisiblePredicate(deleteVisiblePredicate);
		return this;
	}

}