package org.jamgo.ui.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;

import com.vaadin.flow.spring.security.VaadinWebSecurityConfigurerAdapter;

public abstract class VaadinSecurityConfiguration extends VaadinWebSecurityConfigurerAdapter {

	private static final String LOGIN_PROCESSING_URL = "/login";
	private static final String LOGIN_FAILURE_URL = "/login";
	private static final String LOGIN_URL = "/login";
	private static final String LOGOUT_SUCCESS_URL = "/login";

	@Autowired
	protected DataSource dataSource;

	@Bean(name = "authenticationManager")
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public VaadinRequestCache getVaadinRequestCache() {
		return new VaadinRequestCache();
	}
	// @formatter:off

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http
			.csrf().disable()
			.cors()
				.and()

			// ...	Register our VaadinRequestCache, that saves unauthorized access attempts, so
			// 		the user is redirected after login.
			.requestCache()
				.requestCache(this.getVaadinRequestCache());

		this.configurePermittedRequests(http);

		http
			// ...	Authorize vaadin intelnal requests.
			.authorizeRequests()
				.requestMatchers(o -> VaadinSecurityUtils.isFrameworkInternalRequest(o)).permitAll()
				.anyRequest().authenticated()
				.and()

			.formLogin()
				.loginPage(VaadinSecurityConfiguration.LOGIN_URL).permitAll()
				.loginProcessingUrl(VaadinSecurityConfiguration.LOGIN_PROCESSING_URL)
				.failureUrl(VaadinSecurityConfiguration.LOGIN_FAILURE_URL)
				.and()

			.logout()
				.logoutSuccessUrl(VaadinSecurityConfiguration.LOGOUT_SUCCESS_URL)
				.and()

			.headers()
				.frameOptions().disable()
				.and()

			.httpBasic();
	}

	// @formatter:on

	protected void configurePermittedRequests(final HttpSecurity http) throws Exception {
		// ... Do nothing by default.
	}

	@Override
	public void configure(final WebSecurity web) throws Exception {
		web.ignoring().antMatchers(
			"/css/*",

			// Vaadin Flow static resources
			"/VAADIN/**",

			// the standard favicon URI
			"/favicon.ico",

			// the robots exclusion standard
			"/robots.txt",

			// web application manifest
			"/manifest.webmanifest",
			"/sw.js",
			"/offline-page.html",

			// icons and images
			"/icons/**",
			"/images/**",

			// (development mode) static resources
			"/frontend/**",

			// (development mode) webjars
			"/webjars/**",

			// (development mode) H2 debugging console
			"/h2-console/**",

			// (production mode) static resources
			"/frontend-es5/**", "/frontend-es6/**",

			"/sw-runtime-resources-precache.js");
	}

}
