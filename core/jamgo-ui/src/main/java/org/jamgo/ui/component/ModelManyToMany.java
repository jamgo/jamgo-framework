package org.jamgo.ui.component;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.Model;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.crud.DefaultTableLayout;
import org.jamgo.ui.layout.crud.SelectTableLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.componentfactory.EnhancedDialog;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;

@org.springframework.stereotype.Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ModelManyToMany<MODEL extends Model, PARENT_MODEL extends Model> extends ModelToMany<MODEL, PARENT_MODEL> {

	private static final Logger logger = LoggerFactory.getLogger(ModelManyToMany.class);

	private Button selectButton;
	private Consumer<Void> doSelectHandler;
	private Button unlinkButton;
	protected SelectTableLayout<? extends BasicModel<?>, ?, ?> selectTableLayout;

	public ModelManyToMany(final CrudLayoutDef<?, MODEL> crudLayoutDef) {
		super(crudLayoutDef);
		this.createVisible = false;
	}

	@Override
	public void init() {
		this.getContent().addClassName("model-many-to-many");
		super.init();
		this.modelGrid.addSelectionListener(event -> this.selectionChanged());
	}

	private void selectionChanged() {
		if (this.unlinkButton != null) {
			this.unlinkButton.setEnabled(!this.modelGrid.getSelectedItems().isEmpty());
		}
	}

	public void addAction(final ModelToManyAction<MODEL, PARENT_MODEL> modelOneToManyAction) {
		this.actions.add(modelOneToManyAction);
	}

	@Override
	protected List<Component> createButtons() {
		final List<Component> buttons = super.createButtons();
		this.selectButton = this.componentBuilderFactory.createButtonBuilder()
			.addClassName("to-many-action-button")
			.setIcon(VaadinIcon.SEARCH.create())
			.setVisible(true)
			.build();
		this.selectButton.addClickListener(event -> this.doSelect());
		buttons.add(0, this.selectButton);

		if (this.isDeleteVisible()) {
			this.unlinkButton = this.componentBuilderFactory.createButtonBuilder()
				.addClassName("to-many-action-button")
				.setIcon(VaadinIcon.UNLINK.create())
				.setVisible(true)
				.setEnabled(false)
				.build();
			this.unlinkButton.addClickListener(event -> this.getUnlinkMessageBox().open());
			buttons.add(this.unlinkButton);
		}

		return buttons;
	}

	protected void doSelect() {
		if (this.doSelectHandler == null) {
			try {
				this.openSelectDialog();
			} catch (final Exception e) {
				ModelManyToMany.logger.error(e.getMessage(), e);
				// TODO: show message error style.
				Notification.show(this.messageSource.getMessage("error.general.select"));
				throw new RuntimeException(e);
			}
		} else {
			this.doSelectHandler.accept(null);
		}
	}

	private ConfirmDialog getUnlinkMessageBox() {
		final ConfirmDialog dialog = new ConfirmDialog();
		dialog.setHeader(this.messageSource.getMessage("dialog.unlink.caption"));
		dialog.setText(this.messageSource.getMessage("dialog.unlink.message"));
		dialog.setCancelable(true);
		dialog.setCancelText(this.messageSource.getMessage("dialog.no"));
		dialog.setConfirmText(this.messageSource.getMessage("dialog.yes"));
		dialog.addConfirmListener(event -> ModelManyToMany.this.doUnlink());

		return dialog;
	}

	protected void doUnlink() {
		this.deleteContents(this.modelGrid.getSelectedItems());
	}

	protected void openSelectDialog() {
		final EnhancedDialog dialog = new EnhancedDialog();
		dialog.setModal(true);
		dialog.setCloseOnEsc(false);
		dialog.setCloseOnOutsideClick(false);
		dialog.setMinWidth(50, Unit.PERCENTAGE);
		dialog.setHeader(this.crudLayoutDef.getName());

		final CrudLayoutDef<?, MODEL> selectionCrudLayoutDef = this.crudLayoutDef.clone();
		selectionCrudLayoutDef.setAddEnabled(false);
		selectionCrudLayoutDef.setExportEnabled(false);
		selectionCrudLayoutDef.setImportEnabled(false);
		selectionCrudLayoutDef.setAddEnabled(false);
		selectionCrudLayoutDef.setAddEnabled(false);
		selectionCrudLayoutDef.setAddEnabled(false);

		this.selectTableLayout = this.applicationContext.getBean(DefaultTableLayout.class, this.crudLayoutDef);

		this.selectTableLayout.setOkHandler(obj -> {
			this.addItems((Set<MODEL>) obj.getSelectedItems());
			dialog.close();
		});
		this.selectTableLayout.setCancelHandler(obj -> dialog.close());

		dialog.setContent(this.selectTableLayout);
		dialog.open();
	}
}
