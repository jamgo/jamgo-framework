package org.jamgo.ui.component.builders;

import java.io.File;

import org.jamgo.vaadin.builder.base.HasEnabledBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.jamgo.vaadin.ui.builder.JamgoComponentBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.vaadin.filesystemdataprovider.FileSelect;

@org.springframework.stereotype.Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FileSelectBuilder extends JamgoComponentBuilder<FileSelect, FileSelectBuilder> implements
	HasSizeBuilder<FileSelectBuilder, FileSelect>,
	HasValueBuilder<FileSelectBuilder, FileSelect>,
	HasValidationBuilder<FileSelectBuilder, FileSelect>,
	HasEnabledBuilder<FileSelectBuilder, FileSelect> {

	private final File rootFile;

	public FileSelectBuilder(final File rootFile) {
		super();
		this.rootFile = rootFile;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new FileSelect(this.rootFile);
	}

	@Override
	public FileSelect build() {
		return this.instance;
	}

}