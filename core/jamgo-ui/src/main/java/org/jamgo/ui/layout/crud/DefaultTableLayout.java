package org.jamgo.ui.layout.crud;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.BasicModelEntity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.spring.annotation.SpringComponent;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DefaultTableLayout<BASIC_MODEL extends BasicModel<ID_TYPE>, ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE>
	extends SelectTableLayout<BASIC_MODEL, ENTITY, ID_TYPE> {

	public DefaultTableLayout(final CrudLayoutDef<BASIC_MODEL, ENTITY> crudLayoutDef) {
		super(crudLayoutDef);
	}

}
