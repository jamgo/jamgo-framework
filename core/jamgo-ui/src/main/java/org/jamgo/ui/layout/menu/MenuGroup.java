package org.jamgo.ui.layout.menu;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class MenuGroup extends MenuItem {
	private List<MenuItem> menuItems;

	public MenuGroup() {
		this.menuItems = new ArrayList<>();
	}

	public List<MenuItem> getMenuItems() {
		return this.menuItems;
	}

	public void setMenuItems(final List<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	public void addMenuItem(final MenuItem menuItem) {
		if (menuItem != null) {
			this.menuItems.add(menuItem);
		}
	}

	public boolean hasItems() {
		return !CollectionUtils.isEmpty(this.menuItems);
	}

}
