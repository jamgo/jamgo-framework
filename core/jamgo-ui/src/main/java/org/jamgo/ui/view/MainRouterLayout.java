package org.jamgo.ui.view;

import java.util.ArrayList;
import java.util.List;

import org.jamgo.model.entity.User;
import org.jamgo.model.enums.SecuredObjectType;
import org.jamgo.services.UserService;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.SecurityService;
import org.jamgo.services.session.SessionContext;
import org.jamgo.ui.layout.ContentLayout;
import org.jamgo.ui.layout.menu.MenuGroup;
import org.jamgo.ui.layout.menu.MenuItem;
import org.jamgo.ui.layout.menu.MenuLayout;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import com.github.appreciated.app.layout.addons.notification.DefaultNotificationHolder;
import com.github.appreciated.app.layout.component.appbar.AppBarBuilder;
import com.github.appreciated.app.layout.component.applayout.LeftLayouts;
import com.github.appreciated.app.layout.component.applayout.LeftLayouts.LeftHybrid;
import com.github.appreciated.app.layout.component.builder.AppLayoutBuilder;
import com.github.appreciated.app.layout.component.menu.left.builder.LeftAppMenuBuilder;
import com.github.appreciated.app.layout.component.menu.left.builder.LeftSubMenuBuilder;
import com.github.appreciated.app.layout.component.menu.left.items.LeftClickableItem;
import com.github.appreciated.app.layout.component.menu.left.items.LeftHeaderItem;
import com.github.appreciated.app.layout.component.router.AppLayoutRouterLayout;
import com.github.appreciated.app.layout.entity.Section;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.server.VaadinServletRequest;
import com.vaadin.flow.spring.annotation.UIScope;

@Push
@org.springframework.stereotype.Component
@UIScope
@CssImport(value = "./styles/app-layout.css", themeFor = LeftLayouts.LeftHybrid.TAG)
public class MainRouterLayout extends AppLayoutRouterLayout<LeftLayouts.LeftHybrid> implements InitializingBean {

	private static final long serialVersionUID = 1L;

	private static final String LOGOUT_SUCCESS_URL = "/";

	@Autowired
	private SecurityService securityService;
	@Autowired
	private MenuLayout menuLayout;
	@Autowired
	private ContentLayout contentLayout;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	protected UserService userService;
	@Autowired
	private SessionContext sessionContext;

//	TODO
	protected DefaultNotificationHolder notifications;

//	private DefaultBadgeHolder badge;

	@Override
	public void afterPropertiesSet() throws Exception {

//		TODO
		this.notifications = new DefaultNotificationHolder(newStatus -> {
		});

		final LeftHybrid appLayout = this.createAppLayout();
		this.init(appLayout);

//		this.notifications = new DefaultNotificationHolder(newStatus -> {
//		});
//        badge = new DefaultBadgeHolder(5);
//        for (int i = 1; i < 6; i++) {
//            notifications.add(new DefaultNotification("Test title" + i, "A rather long test description ..............." + i));
//        }
//        LeftNavigationItem menuEntry = new LeftNavigationItem("Menu", VaadinIcon.MENU.create(), View6.class);
//        badge.bind(menuEntry.getBadge());

//                        .add(new LeftNavigationItem("Home", VaadinIcon.HOME.create(), View1.class),
//                                LeftSubMenuBuilder.get("My Submenu", VaadinIcon.PLUS.create())
//                                        .add(LeftSubMenuBuilder.get("My Submenu", VaadinIcon.PLUS.create())
//                                                        .add(new LeftNavigationItem("Charts", VaadinIcon.SPLINE_CHART.create(), View2.class),
//                                                                new LeftNavigationItem("Contact", VaadinIcon.CONNECT.create(), View3.class),
//                                                                new LeftNavigationItem("More", VaadinIcon.COG.create(), View4.class))
//                                                        .build(),
//                                                new LeftNavigationItem("Contact1", VaadinIcon.CONNECT.create(), View3.class),
//                                                new LeftNavigationItem("More1", VaadinIcon.COG.create(), View5.class))
//                                        .build(),
//                                menuEntry)
	}

	protected LeftHybrid createAppLayout() {
		final LeftHybrid appLayout = AppLayoutBuilder.get(LeftLayouts.LeftHybrid.class)
			.withTitle(this.menuLayout.getRootMenuGroup().getName())
			.withIconComponent(new Image("images/logo-s.png", "Icon"))
			.withAppMenu(LeftAppMenuBuilder.get()
				.addToSection(Section.HEADER, new LeftHeaderItem(null, null, null))
				.add(this.getTopGroup())
				.add(this.getBottomGroup())
				.build())
			.build();

		if (this.userService.getCurrentUser() != null) {
			final Span userNameSpan = new Span(this.getDisplayName(this.userService.getCurrentUser()));
			userNameSpan.getStyle().set("white-space", "nowrap");
			final Button logoutButton = new Button(VaadinIcon.EXIT.create());
			logoutButton.addClickListener(event -> this.logoutButtonClicked(event));
			logoutButton.getElement().setProperty("title", this.messageSource.getMessage("action.logout"));
			logoutButton.getStyle().set("background-color", "transparent");
			final FlexLayout appBar = AppBarBuilder.get()
				.add(userNameSpan)
				.add(logoutButton)
				.build();
			appLayout.setAppBar(appBar);
		}

//		if (this.securityService.getCurrentUser() != null) {
//			final FlexLayout appBar = AppBarBuilder.get()
//				.add(new Span(this.securityService.getCurrentUser().getUsername()))
//				.add(new NotificationButton<>(VaadinIcon.BELL, this.notifications))
//				.build();
//			appLayout.setAppBar(appBar);
//		}

		return appLayout;
	}


	protected String getDisplayName(final User currentUser) {
		return currentUser.getUsername();
	}

	protected void logoutButtonClicked(final ClickEvent<Button> event) {
		UI.getCurrent().getPage().setLocation(MainRouterLayout.LOGOUT_SUCCESS_URL);
		final SecurityContextLogoutHandler logoutHandler = new SecurityContextLogoutHandler();
		logoutHandler.logout(VaadinServletRequest.getCurrent().getHttpServletRequest(), null, null);
		this.sessionContext.reset();
	}

	public Component[] createMenuComponents() {
		return this.createMenuComponents(this.menuLayout.getRootMenuGroup());
	}

	public Component[] createMenuComponents(final MenuGroup menuGroup) {
		final List<Component> menuComponents = new ArrayList<>();
		menuGroup.getMenuItems().stream().forEach(eachMenuItem -> {
			if (this.securityService.hasReadPermission(SecuredObjectType.MENU, eachMenuItem.getId(), false)) {
				menuComponents.add(this.createMenuComponent(eachMenuItem));
			}
		});
		return menuComponents.toArray(new Component[menuComponents.size()]);
	}

	protected Component createMenuComponent(final MenuItem menuItem) {
		Component menuComponent;
		if (menuItem instanceof MenuGroup) {
			menuComponent = LeftSubMenuBuilder.get(menuItem.getName(), menuItem.getIcon())
				.add(this.createMenuComponents((MenuGroup) menuItem))
				.build();
		} else {
			menuComponent = new LeftClickableItem(
				menuItem.getName(),
				menuItem.getIcon(),
				clickEvent -> this.contentLayout.openView(menuItem.getLayoutDef()));
		}
		return menuComponent;
	}

	private Div getTopGroup() {
		Div topGroup = new Div();
		topGroup.setWidthFull();
		topGroup.add(this.createMenuComponents());
		return topGroup;
	}

	private Div getBottomGroup() {
		Div bottomGroup = new Div();
		bottomGroup.setWidthFull();
		bottomGroup.addClassNames("margin-top-auto");
		bottomGroup.add(this.createBottomMenuComponents());
		return bottomGroup;
	}

	public Component[] createBottomMenuComponents() {
		return this.createMenuComponents(this.menuLayout.getBottomMenuGroup());
	}

}
