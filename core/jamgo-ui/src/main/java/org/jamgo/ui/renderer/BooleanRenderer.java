package org.jamgo.ui.renderer;

import org.jamgo.model.BasicModel;

import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.ValueProvider;

public class BooleanRenderer<T extends BasicModel<?>> extends ComponentRenderer<Span, T> {

	private static final long serialVersionUID = 1L;

	public BooleanRenderer(final ValueProvider<T, Boolean> valueProvider) {
		super(o -> {
			final Span span = new Span();
			span.setClassName("rendered-boolean");
			if (valueProvider.apply(o)) {
				span.add(new Icon(VaadinIcon.CHECK));
			}
			return span;
		});
	}
	
}
