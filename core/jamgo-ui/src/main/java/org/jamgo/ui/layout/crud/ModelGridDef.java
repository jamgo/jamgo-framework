package org.jamgo.ui.layout.crud;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.BasicModelEntity;

import com.vaadin.flow.function.SerializableFunction;

public class ModelGridDef<BASIC_MODEL extends BasicModel<?>, ENTITY extends BasicModelEntity<?>> {

	private Class<BASIC_MODEL> modelClass;
	private Class<ENTITY> entityClass;
	private Class<? extends CrudLayout<?>> detailsLayoutClass;
	private final List<CrudLayoutColumnDef<?, ?>> columnDefs;
	private SerializableFunction<BASIC_MODEL, String> gridClassNameGenerator;
	private boolean countOnEntity;
	private final List<Class<? extends CrudBasicPanel>> pluggablePanels = new ArrayList<>();

	public ModelGridDef() {
		this.columnDefs = new ArrayList<>();
	}

	public static Builder<?, ?> builder() {
		return new ModelGridDef.Builder<>();
	}

	public static class Builder<BASIC_MODEL extends BasicModel<?>, ENTITY extends BasicModelEntity<?>> {
		protected final ModelGridDef<BASIC_MODEL, ENTITY> instance = new ModelGridDef<>();
		private CrudLayoutDef.Builder<BASIC_MODEL, ?> parentBuilder;
		private Consumer<ModelGridDef<BASIC_MODEL, ENTITY>> callback;

		public Builder() {
		}

		public Builder(final CrudLayoutDef.Builder<BASIC_MODEL, ?> parentBuilder, final Consumer<ModelGridDef<BASIC_MODEL, ENTITY>> callback) {
			this.parentBuilder = parentBuilder;
			this.callback = callback;
		}

		public Builder<BASIC_MODEL, ENTITY> setModelClass(final Class<BASIC_MODEL> modelClass) {
			this.instance.modelClass = modelClass;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setEntityClass(final Class<ENTITY> entityClass) {
			this.instance.entityClass = entityClass;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setGridClassNameGenerator(final SerializableFunction<BASIC_MODEL, String> gridClassNameGenerator) {
			this.instance.gridClassNameGenerator = gridClassNameGenerator;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> detailsLayoutClass(final Class<? extends CrudBasicModelLayout<?>> detailsLayoutClass) {
			this.instance.detailsLayoutClass = detailsLayoutClass;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setCountOnEntity(final boolean countOnEntity) {
			this.instance.countOnEntity = countOnEntity;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> addPluggablePanel(final Class<? extends CrudBasicPanel> clazz) {
			this.instance.pluggablePanels.add(clazz);
			return this;
		}

		public <S> CrudLayoutColumnDef.Builder<BASIC_MODEL, S> columnDef() {
			final Consumer<CrudLayoutColumnDef<BASIC_MODEL, S>> f = obj -> this.instance.columnDefs.add(obj);
			return new CrudLayoutColumnDef.Builder<>(this, f);
		}

		public ModelGridDef<BASIC_MODEL, ENTITY> build() {
			return this.instance;
		}

		public CrudLayoutDef.Builder<BASIC_MODEL, ?> end() {
			this.callback.accept(this.instance);
			return this.parentBuilder;
		}

	}

	public static class BuilderEntity<ENTITY extends BasicModelEntity<?>> extends Builder<ENTITY, ENTITY> {

		public BuilderEntity(final CrudLayoutDef.Builder<ENTITY, ?> parentBuilder, final Consumer<ModelGridDef<ENTITY, ENTITY>> callback) {
			super(parentBuilder, callback);
		}

		@Override
		public Builder<ENTITY, ENTITY> setEntityClass(final Class<ENTITY> entityClass) {
			this.instance.entityClass = entityClass;
			if (this.instance.modelClass == null) {
				this.instance.modelClass = entityClass;
			}
			return this;
		}

	}

	@SuppressWarnings("unchecked")
	public Class<BASIC_MODEL> getModelClass() {
		if (this.modelClass == null) {
			return (Class<BASIC_MODEL>) this.entityClass;
		}
		return this.modelClass;
	}

	public Class<ENTITY> getEntityClass() {
		return this.entityClass;
	}

	public SerializableFunction<BASIC_MODEL, String> getGridClassNameGenerator() {
		return this.gridClassNameGenerator;
	}

	public Class<? extends CrudLayout<?>> getDetailsLayoutClass() {
		return this.detailsLayoutClass;
	}

	public List<CrudLayoutColumnDef<?, ?>> getColumnDefs() {
		return this.columnDefs;
	}

	public boolean isCountOnEntity() {
		return this.countOnEntity;
	}

	public List<Class<? extends CrudBasicPanel>> getPluggablePanels() {
		return this.pluggablePanels;
	}

}
