package org.jamgo.ui.layout.crud;

import java.util.Optional;

import com.vaadin.flow.component.Unit;

/*
 * ---------------------------------------------------
 * | toolbar                                         |
 * ---------------------------------------------------
 * | search                                          |
 * ---------------------------------------------------
 * | table                                           |
 * |                                                 |
 * |                                                 |
 * |                                                 |
 * ---------------------------------------------------
 * | details                                         |
 * |                                                 |
 * |                                                 |
 * |                                                 |
 * ---------------------------------------------------
 */

public class CrudTableLayoutHorizontalConfig extends CrudTableLayoutConfig {

	@Override
	public void initialize(final CrudTableLayout<?, ?, ?> crudTableLayout) {
		crudTableLayout.add(crudTableLayout.getToolBar());
		crudTableLayout.add(crudTableLayout.getSearchLayout());
		crudTableLayout.add(crudTableLayout.getTableWrapper());
		Optional.ofNullable(crudTableLayout.getDetailsLayout()).ifPresent(o -> crudTableLayout.add(o));
	}

	@Override
	public void applyDefaultTo(final CrudTableLayout<?, ?, ?> crudTableLayout) {

		if (crudTableLayout.isSearchVisible()) {
			crudTableLayout.getSearchLayout().setHeight(40f, Unit.PERCENTAGE);
			crudTableLayout.getTableWrapper().setHeight(60f, Unit.PERCENTAGE);
			Optional.ofNullable(crudTableLayout.getDetailsLayout()).ifPresent(o -> o.setHeight(0f, Unit.PERCENTAGE));
		} else if (crudTableLayout.isDetailsVisible()) {
			crudTableLayout.getSearchLayout().setHeight(0f, Unit.PERCENTAGE);
			crudTableLayout.getTableWrapper().setHeight(40f, Unit.PERCENTAGE);
			crudTableLayout.getDetailsLayout().setHeight(60f, Unit.PERCENTAGE);
			Optional.ofNullable(crudTableLayout.getDetailsLayout()).ifPresent(o -> o.setHeight(60f, Unit.PERCENTAGE));
		} else {
			crudTableLayout.getSearchLayout().setHeight(0f, Unit.PERCENTAGE);
			crudTableLayout.getTableWrapper().setHeight(100f, Unit.PERCENTAGE);
			Optional.ofNullable(crudTableLayout.getDetailsLayout()).ifPresent(o -> o.setHeight(0f, Unit.PERCENTAGE));
		}

	}

	@Override
	public void applyMaximizedDetailsTo(final CrudTableLayout<?, ?, ?> crudTableLayout) {
		Optional.ofNullable(crudTableLayout.getDetailsLayout()).ifPresent(o -> o.setHeight(100f, Unit.PERCENTAGE));
	}

}
