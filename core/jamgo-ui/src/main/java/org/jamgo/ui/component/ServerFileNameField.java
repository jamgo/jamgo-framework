package org.jamgo.ui.component;

import java.io.File;

import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import com.vaadin.componentfactory.EnhancedDialog;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;

@org.springframework.stereotype.Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ServerFileNameField extends CustomField<String> implements InitializingBean {

	private static final long serialVersionUID = 1L;

	@Autowired
	protected ApplicationContext applicationContext;
	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected LocalizedMessageService messageSource;

	private TextField nameField;
	private Button selectFileButton;

	private String rootFilePath = "/tmp";

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		final HorizontalLayout fileNameLayout = this.componentBuilderFactory.createHorizontalLayoutBuilder()
			.setWidth(100, Unit.PERCENTAGE)
			.build();

		this.nameField = this.componentBuilderFactory.createTextFieldBuilder()
			.build();

		this.selectFileButton = this.componentBuilderFactory.createButtonBuilder()
			.setText(this.messageSource.getMessage("action.selectFile"))
			.build();
		this.selectFileButton.addClickListener(event -> this.doSelectFile());

		fileNameLayout.add(this.nameField, this.selectFileButton);
		fileNameLayout.setFlexGrow(1, this.nameField);

		this.add(fileNameLayout);
	}

	private void doSelectFile() {
		final EnhancedDialog dialog = this.componentBuilderFactory.createEnhancedDialogBuilder()
			.setHeader("Select file")
			.setModal(true)
			.setCloseOnEsc(false)
			.setCloseOnOutsideClick(false)
			.build();
		final ServerFileSelectLayout addLayout = this.applicationContext.getBean(ServerFileSelectLayout.class, this.rootFilePath);
		addLayout.setCloseHandler(layout -> this.addSelectdFile(layout, dialog));
		dialog.setContent(addLayout);
		dialog.open();
	}

	private void addSelectdFile(final ServerFileSelectLayout serverFileSelectLayout, final EnhancedDialog dialog) {
		dialog.close();
		final File selectedFile = serverFileSelectLayout.getSelectedFile();
		if (selectedFile != null) {
			String relativePath = selectedFile.getAbsolutePath().replace(this.rootFilePath, "");
			if (relativePath.startsWith(File.separator)) {
				relativePath = relativePath.substring(1);
			}
			this.nameField.setValue(relativePath);
			this.updateValue();
		}
	}

	@Override
	protected String generateModelValue() {
		return this.nameField.getValue();
	}

	@Override
	protected void setPresentationValue(final String fileName) {
		this.nameField.setValue(fileName);
	}

	public String getRootFilePath() {
		return this.rootFilePath;
	}

	public void setRootFilePath(final String rootFilePath) {
		this.rootFilePath = rootFilePath;
	}

}
