package org.jamgo.ui.view;

import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.jamgo.ui.config.VaadinRequestCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.server.VaadinService;

public abstract class AbstractLoginView extends VerticalLayout implements InitializingBean, LoginView {

	private static final Logger logger = LoggerFactory.getLogger(AbstractLoginView.class.getName());

	private static final long serialVersionUID = 1L;
	public static final String NAME = "login";

	@Autowired
	protected AuthenticationManager authenticationManager;
	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected VaadinRequestCache requestCache;

	@Value("${login.default.username:#{null}}")
	protected String defaultUsername;
	@Value("${login.default.password:#{null}}")
	protected String defaultPassword;

	protected TextField username;
	protected PasswordField password;
	// TODO private CheckBox rememberMe = new CheckBox("Recorda'm", true);

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		this.setSizeFull();
		this.setMargin(false);
		this.setSpacing(false);
		final Component layout = this.buildLayout();
		this.add(layout);

		this.setHorizontalComponentAlignment(Alignment.CENTER, layout);
		this.username.focus();
	}

	@Override
	public void onAttach(final AttachEvent event) {
		super.onAttach(event);
		if ((this.defaultUsername != null) && (this.defaultPassword != null)) {
			try {
				this.login(this.defaultUsername, this.defaultPassword);
			} catch (final Exception e) {
				AbstractLoginView.logger.info(e.getMessage());
			}
		}
	}

	private Component buildLayout() {
		final HorizontalLayout layout = new HorizontalLayout();
		layout.setMargin(false);
		layout.setSpacing(false);
		layout.setClassName("login-layout");
		layout.setSizeFull();
		final Component loginForm = this.buildLoginForm();
		layout.add(loginForm);
		return layout;
	}

	private Component buildLoginForm() {

		final VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		layout.setMargin(false);
		layout.setSpacing(false);
		final HorizontalLayout topBar = this.createLoginHeaderLayout();
		final VerticalLayout loginFormLayout = this.createLoginFormLayout();

		final HorizontalLayout bottomBar = new HorizontalLayout();
		bottomBar.setWidth(100, Unit.PERCENTAGE);
		bottomBar.setClassName("login-footer");

		layout.add(topBar, loginFormLayout, bottomBar);
		layout.setHorizontalComponentAlignment(Alignment.CENTER, loginFormLayout);

		return layout;
	}

	protected HorizontalLayout createLoginHeaderLayout() {
		final HorizontalLayout topBar = new HorizontalLayout();
		topBar.setWidth(100, Unit.PERCENTAGE);
		topBar.setClassName("login-header");
		return topBar;
	}

	protected VerticalLayout createLoginFormLayout() {
		final FormLayout loginFormLayout = new FormLayout();
		loginFormLayout.setClassName("login-form");
		loginFormLayout.setWidth(30, Unit.PERCENTAGE);
//		final Panel loginFormPanel = this.componentFactory.newPanel();
//		loginFormPanel.setContent(loginLayout);
//		loginFormPanel.setSizeFull();

		// FIXME: Change default to english.
		this.username = this.componentBuilderFactory.createTextFieldBuilder()
			.setLabelId("login.user", "Usuari")
			.build();
		this.password = this.componentBuilderFactory.createPasswordFieldBuilder()
			.setLabelId("login.password", "Contrasenya")
			.build();

		final Button accedir = this.componentBuilderFactory.createButtonBuilder()
			.setLabelId("action.login", "Accedir")
			.build();
		accedir.addClickListener(this::clickLogin);
		accedir.addClickShortcut(Key.ENTER);

		final HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.add(accedir);
		buttonsLayout.setSpacing(true);

		loginFormLayout.add(this.username, this.password/*, rememberMe*/, buttonsLayout);

		final VerticalLayout loginLayout = new VerticalLayout();
		loginLayout.add(loginFormLayout);
		loginLayout.setSizeFull();
		loginLayout.setMargin(false);
		loginLayout.setHorizontalComponentAlignment(Alignment.CENTER, loginFormLayout);

		return loginLayout;
	}

	protected void clickLogin(final ClickEvent<?> event) {
		try {
			final String user = this.username.getValue();
			final String pass = this.password.getValue();
			// TODO boolean check = rememberMe.getValue();
			this.login(user, pass/*, check*/);
			UI.getCurrent().navigate(this.requestCache.resolveRedirectUrl());
		} catch (final AuthenticationException e) {
			// TODO show message error style
			Notification.show("Usuari o password incorrectes");
			AbstractLoginView.logger.error(e.getMessage());
		} catch (final Exception e) {
			AbstractLoginView.logger.error(e.getMessage(), e);
		}
		// TODO Register Remember me Token

		/*
		 * Redirect is handled by the VaadinRedirectStrategy. User is
		 * redirected to either always the default or the URL the user
		 * request before authentication.
		 *
		 * Strategy is configured within SecurityConfiguration Defaults
		 * to User request URL.
		 */
	}

	protected void login(final String username, final String password) throws Exception {
		final Authentication token = this.authenticationManager
			.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		// Reinitialize the session to protect against session fixation attacks. This does not work
		// with websocket communication.
		VaadinService.reinitializeSession(VaadinService.getCurrentRequest());
		SecurityContextHolder.getContext().setAuthentication(token);
	}

}
