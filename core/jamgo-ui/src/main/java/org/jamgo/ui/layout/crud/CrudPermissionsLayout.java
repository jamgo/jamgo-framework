package org.jamgo.ui.layout.crud;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jamgo.model.entity.Acl;
import org.jamgo.model.entity.AppRole;
import org.jamgo.model.entity.Model;
import org.jamgo.model.entity.Role;
import org.jamgo.model.entity.SecuredObject;
import org.jamgo.model.entity.User;
import org.jamgo.model.enums.Permission;
import org.jamgo.model.enums.SecuredObjectType;
import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.services.UserService;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.SecurityService;
import org.jamgo.services.impl.ServiceManager;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.ListDataProvider;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CrudPermissionsLayout<T> extends VerticalLayout {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(CrudPermissionsLayout.class);

	@Autowired
	private SecurityService securityService;
	@Autowired
	private ServiceManager serviceManager;
	@Autowired
	private LocalizedMessageService messageSource;
	@Autowired
	private JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	private UserService userService;

	private SecuredObject securedObject;
	private final List<Acl> aclList = new ArrayList<>();
	private final ListDataProvider<Acl> aclDataProvider = new ListDataProvider<>(this.aclList);

	private final Map<Long, Acl> roleAclMap = new HashMap<>();

	private Grid<Acl> rolesGridLayout;
	private Button addPermissionsButton;
	private Button removePermissionsButton;

	protected void init(final Class<?> clazz) {
		final String fieldName = this.securityService.getSecurizedField(clazz.getName());
		if (fieldName != null) {
			this.add(new Span("Entitat securitzada pel camp: <i>" + fieldName + "</i>"));
		}

		this.initializeGridAcls();
		this.addButtons(clazz);
		this.addGrid();

		this.add(this.addPermissionsButton, this.removePermissionsButton, this.rolesGridLayout);
	}

	private void addButtons(final Class<?> clazz) {
		this.addPermissionsButton = new Button(this.messageSource.getMessage("permission.add"));
		this.removePermissionsButton = new Button(this.messageSource.getMessage("permission.remove"));

		this.addPermissionsButton.addClickListener(listener -> {
			this.rolesGridLayout.setVisible(true);
			this.addPermissionsButton.setVisible(false);
			this.removePermissionsButton.setVisible(true);

			final SecuredObject parentSecuredObject = this.securityService.getSecuredObject(SecuredObjectType.ENTITY, clazz.getName());
			this.securedObject = new SecuredObject();
			this.securedObject.setParent(parentSecuredObject);
			this.securedObject.setAcl(new ArrayList<>());
			final User user = this.userService.getCurrentUser();
			user.getRoles().stream().forEach(role -> {
				final Acl acl = new Acl();
				acl.setSecuredObject(this.securedObject);
				acl.setRoleId(role.getId());
				acl.setPermission(Permission.ALL);
//				acl.setReadPermission(true);
//				acl.setWritePermission(true);
				this.securedObject.getAcl().add(acl);
			});
		});

		this.removePermissionsButton.addClickListener(listener -> {
			this.rolesGridLayout.setVisible(false);
			this.addPermissionsButton.setVisible(true);
			this.removePermissionsButton.setVisible(false);

			this.roleAclMap.clear();
			this.securedObject = null;
		});
	}

	private void addGrid() {
		this.rolesGridLayout = new Grid<>();
		this.rolesGridLayout.setItems(this.aclDataProvider);
		this.rolesGridLayout.setSelectionMode(SelectionMode.NONE);
		this.rolesGridLayout.addColumn(acl -> {
			try {
				if (!this.roleAclMap.containsKey(acl.getRoleId())) {
					this.roleAclMap.put(acl.getRoleId(), acl);
				}
				final Role role = this.serviceManager.getService(AppRole.class).findById(acl.getRoleId());
				return role.getRolename();
			} catch (final RepositoryForClassNotDefinedException e) {
				CrudPermissionsLayout.logger.error(e.getMessage(), e);
			}
			return "";
		}).setHeader(this.messageSource.getMessage("permission.role"));
		this.rolesGridLayout.addComponentColumn(acl -> {
			final Checkbox checkBox = this.componentBuilderFactory.createCheckBoxBuilder()
				.build();
			checkBox.addValueChangeListener(listener -> {
				this.roleAclMap.get(acl.getRoleId()).setReadPermission(listener.getValue());
			});
			if (this.securedObject != null) {
				checkBox.setValue(this.securedObject.getAcl().stream().anyMatch(each -> acl.getRoleId().equals(each.getRoleId()) && each.canRead()));
			}
			return checkBox;
		}).setHeader(this.messageSource.getMessage("permission.read"));
		this.rolesGridLayout.addComponentColumn(acl -> {
			final Checkbox checkBox = this.componentBuilderFactory.createCheckBoxBuilder()
				.build();
			checkBox.addValueChangeListener(listener -> {
				this.roleAclMap.get(acl.getRoleId()).setWritePermission(listener.getValue());
			});
			if (this.securedObject != null) {
				checkBox.setValue(this.securedObject.getAcl().stream().anyMatch(each -> acl.getRoleId().equals(each.getRoleId()) && each.canWrite()));
			}
			return checkBox;
		}).setHeader(this.messageSource.getMessage("permission.write"));
		this.rolesGridLayout.setHeightFull();

		this.add(this.rolesGridLayout);
	}

	private void initializeGridAcls() {
		try {
			this.aclList.clear();
			final List<AppRole> roles = this.serviceManager.getService(AppRole.class).findAll();
			roles.stream().forEach(role -> {
				final Acl acl = new Acl();
				acl.setSecuredObject(new SecuredObject());
				acl.setRoleId(role.getId());
				acl.setReadPermission(false);
				acl.setWritePermission(false);
				this.aclList.add(acl);
			});
		} catch (final RepositoryForClassNotDefinedException e) {
			CrudPermissionsLayout.logger.error(e.getMessage(), e);
		}
	}

	public void writeSecuredObject(final Object targetObject) {
		if (this.securedObject != null) {
			this.roleAclMap.values().stream().forEach(acl -> {
				acl.setSecuredObject(this.securedObject);
			});
			this.securedObject.setAcl(new ArrayList<>(this.roleAclMap.values()));

			final String securedObjectKey = this.securityService.getSecuredObjectKey(targetObject);
			this.securedObject.setSecuredObjectKey(securedObjectKey);
			if (StringUtils.isBlank(securedObjectKey)) {
				throw new IllegalArgumentException(this.messageSource.getMessage("error.permissions.securedObjectKey.empty"));
			}
		}
	}

	public SecuredObject getSecuredObject(final Object targetObject) {
		this.writeSecuredObject(targetObject);
		return this.securedObject;
	}

	public void updateFields(final Object savedItem) {
		this.initializeGridAcls();
		this.roleAclMap.clear();
		this.securedObject = null;

		if (((Model) savedItem).getId() != null) {
			final String securedObjectKey = this.securityService.getSecuredObjectKey(savedItem);
			this.securedObject = this.securityService.getSecuredObject(SecuredObjectType.ENTITY, savedItem.getClass().getName(), securedObjectKey);
		}

		if (this.securedObject != null) {
			this.securedObject.getAcl().stream().forEach(acl -> {
				this.roleAclMap.put(acl.getRoleId(), acl);
			});
		}

		this.rolesGridLayout.setVisible((this.securedObject != null) && (((Model) savedItem).getId() != null));
		this.removePermissionsButton.setVisible((this.securedObject != null) && (((Model) savedItem).getId() != null));
		this.addPermissionsButton.setVisible((this.securedObject == null));

		this.aclDataProvider.refreshAll();
	}

}
