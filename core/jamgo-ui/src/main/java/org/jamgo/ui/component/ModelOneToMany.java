package org.jamgo.ui.component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import org.apache.commons.collections4.CollectionUtils;
import org.jamgo.model.entity.Model;
import org.jamgo.services.exception.CrudException;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.data.binder.Setter;

@org.springframework.stereotype.Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ModelOneToMany<MODEL extends Model, PARENT_MODEL extends Model> extends ModelToMany<MODEL, PARENT_MODEL> {

	private static final Logger logger = LoggerFactory.getLogger(ModelOneToMany.class);

	private Setter<MODEL, PARENT_MODEL> parentSetter;
	private boolean editVisible = true;
	private Predicate<MODEL> editVisiblePredicate;
	private Button deleteButton;
	private boolean earlyPersist = false;

	public ModelOneToMany(final CrudLayoutDef<?, MODEL> crudLayoutDef) {
		super(crudLayoutDef);
	}

	@Override
	public void init() {
		this.getContent().addClassName("model-one-to-many");
		super.init();
		this.modelGrid.addSelectionListener(event -> this.selectionChanged());
	}

	@Override
	@SuppressWarnings("unchecked")
	protected void createColumns() {
		if (this.editVisible) {
			this.actions.add(this.applicationContext.getBean(ModelOneToManyEditAction.class, this, this.editVisiblePredicate));
		}
		super.createColumns();
	}

	@Override
	protected List<Component> createButtons() {
		final List<Component> buttons = super.createButtons();
		if (this.isDeleteVisible()) {
			this.deleteButton = this.componentBuilderFactory.createButtonBuilder()
				.addClassName("to-many-action-button")
				.setIcon(VaadinIcon.TRASH.create())
				.setVisible(true)
				.setEnabled(false)
				.build();
			this.deleteButton.addClickListener(event -> this.deleteButtonClicked());
			buttons.add(this.deleteButton);
		}

		return buttons;
	}

	private void deleteButtonClicked() {
		if (this.deleteValidator != null) {
			try {
				this.deleteValidator.accept(new ArrayList<>(this.modelGrid.getSelectedItems()));
			} catch (final Exception e) {
				this.showErrorNotification(e.getMessage());
				return;
			}
		}

		this.getDeleteMessageBox().open();
	}

	private ConfirmDialog getDeleteMessageBox() {
		final Set<MODEL> selectedItems = this.modelGrid.getSelectedItems();

		final ConfirmDialog dialog = new ConfirmDialog();
		dialog.setHeader(this.messageSource.getMessage("dialog.delete.caption"));
		dialog.setText(String.format(this.messageSource.getMessage("dialog.delete.message"), selectedItems.size()));
		dialog.setCancelable(true);
		dialog.setCancelText(this.messageSource.getMessage("dialog.no"));
		dialog.setConfirmText(this.messageSource.getMessage("dialog.yes"));
		dialog.addConfirmListener(event -> ModelOneToMany.this.doDelete());

		return dialog;
	}

	private void selectionChanged() {
		if (this.deleteButton != null) {
			this.deleteButton.setEnabled(!this.modelGrid.getSelectedItems().isEmpty());
		}
	}

	public void addAction(final ModelToManyAction<MODEL, PARENT_MODEL> modelOneToManyAction) {
		this.actions.add(modelOneToManyAction);
	}

	@Override
	protected MODEL getNewItem() throws Exception {
		final MODEL newInstance = super.getNewItem();
		this.parentSetter.accept(newInstance, this.getParentModel());
		return newInstance;
	}

	@Override
	public void addItems(final Set<MODEL> newItems) {
		final Set<MODEL> savedItems = new HashSet<>();
		final boolean parentPersisted = this.isParentPersisted();

		if (this.addToParent == null && !parentPersisted) {
			// TODO: show message error style.
			Notification.show(this.messageSource.getMessage("manyToMany.error.addWithoutParent"));
			return;
		}

		if (this.addToParent == null || (parentPersisted && this.earlyPersist)) {
			try {
				newItems.forEach(each -> {
					savedItems.add(this.crudServices.save(each));
				});
				// TODO: show message humanized style.
				Notification.show(this.messageSource.getMessage("info.action.saved.entity"));
			} catch (final CrudException e) {
				ModelOneToMany.logger.error(e.getMessage(), e);
				// TODO: show message error style.
				Notification.show(this.messageSource.getMessage("error.general.save"));
			}
		} else {
			savedItems.addAll(newItems);
		}

		super.addItems(savedItems);
	}

	protected void doDelete() {
		this.doDelete(new ArrayList<>(this.modelGrid.getSelectedItems()));
	}

	@Override
	protected void doDelete(final List<MODEL> items) {
		final boolean parentPersisted = this.isParentPersisted();

		if (CollectionUtils.isNotEmpty(items)) {
			if (this.deleteFromParent == null || (parentPersisted && this.earlyPersist)) {
				this.crudServices.delete(items, items.get(0).getClass());
			} else {
				items.forEach(each -> this.deleteFromParent.accept(each, this.getParentModel()));
			}
		}

		super.doDelete(items);
	}

	protected boolean isParentPersisted() {
		/*
		 * ... FIXME
		 *
		 * The parent model was never used (and was always null) up to and including version 8.0.2.
		 * Now we replaced parentModel by a parentModelSupplier (wich was the correct implementation).
		 * But for backward compatibility we can't assume that parent is not persisted if parentModel is
		 * null, because parentModel can be null when parentModelSupplier is null (as it will be in all
		 * applications using the framework before this version).
		 * So, until we implement the parentModelSupplier as required we have to assume that it can
		 * be persisted if parentModel is null.
		 */
		return Optional.ofNullable(this.getParentModel())
			.map(o -> o.getId() != null)
			.orElse(true);
	}

	public Setter<MODEL, PARENT_MODEL> getParentSetter() {
		return this.parentSetter;
	}

	public void setParentSetter(final Setter<MODEL, PARENT_MODEL> parentSetter) {
		this.parentSetter = parentSetter;
	}

	public Predicate<MODEL> getEditVisiblePredicate() {
		return this.editVisiblePredicate;
	}

	public void setEditVisiblePredicate(final Predicate<MODEL> editVisiblePredicate) {
		this.editVisiblePredicate = editVisiblePredicate;
	}

	public boolean isEditVisible() {
		return this.editVisible;
	}

	public void setEditVisible(final boolean editVisible) {
		this.editVisible = editVisible;
	}

	public boolean isEarlyPersist() {
		return this.earlyPersist;
	}

	public void setEarlyPersist(final boolean earlyPersist) {
		this.earlyPersist = earlyPersist;
	}

}
