package org.jamgo.ui.layout.defs;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.Model;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.ui.layout.crud.CrudLayoutDef;

public abstract class BasicModelLayoutDef<MODEL extends BasicModel<Long>, ENTITY extends Model> extends AbstractLayoutDef {

	public BasicModelLayoutDef(final LocalizedMessageService messageSource, final LocalizedObjectService localizedObjectService) {
		super(messageSource, localizedObjectService);
	}

	@Override
	public Class<MODEL> getLayoutDefClass() {
		return this.getModelClass();
	}

	@Override
	public abstract CrudLayoutDef<MODEL, ENTITY> getBackofficeLayoutDef();

	public abstract Class<MODEL> getModelClass();

}
