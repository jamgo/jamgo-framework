package org.jamgo.ui.security.layout;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.jamgo.model.entity.Acl;
import org.jamgo.model.entity.Role;
import org.jamgo.model.entity.SecuredObject;
import org.jamgo.model.enums.SecuredObjectType;
import org.jamgo.model.repository.AclRepository;
import org.jamgo.model.repository.SecuredObjectRepository;
import org.jamgo.services.RoleService;
import org.jamgo.services.exception.CrudException;
import org.jamgo.services.impl.CrudServices;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.jamgo.ui.layout.CustomLayout;
import org.jamgo.ui.layout.CustomLayoutDef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.renderer.TextRenderer;
import com.vaadin.flow.spring.annotation.SpringComponent;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PermissionsLayout extends CustomLayout {

	private static final Logger logger = LoggerFactory.getLogger(PermissionsLayout.class);

	private static final long serialVersionUID = 1L;

	@Autowired
	private JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected RoleService roleService;
	@Autowired
	protected SecuredObjectRepository securedObjectRepository;
	@Autowired
	protected AclRepository aclRepository;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	protected CrudServices crudServices;

	private VerticalLayout mainLayout;

	private ComboBox<Role> roleField;
	private TreeGrid<AppliedAcl> appliedAclTreeField;

	private class AppliedAcl {

		private final SecuredObject securedObject;
		private Acl acl;

		public AppliedAcl(final SecuredObject securedObject, final Acl acl) {
			this.securedObject = securedObject;
			this.acl = acl;
		}

		public SecuredObject getSecuredObject() {
			return this.securedObject;
		}

		public Acl getAcl() {
			return this.acl;
		}

		public void setAcl(final Acl acl) {
			this.acl = acl;
		}

		public Boolean getReadPermission() {
			return this.acl.canRead();
		}

		public void setReadPermission(final Boolean readPermission) {
			this.acl.setReadPermission(readPermission);
		}

		public Boolean getWritePermission() {
			return this.acl.canWrite();
		}

		public void setWritePermission(final Boolean writePermission) {
			this.acl.setWritePermission(writePermission);
		}
	}

	public PermissionsLayout(final CustomLayoutDef customLayoutDef) {
		super(customLayoutDef);
	}

	@Override
	public void init(final CustomLayoutDef customLayoutDef) {
		this.setSizeFull();
//		this.setMargin(false);
//		this.setSpacing(false);

		this.mainLayout = new VerticalLayout();
		this.mainLayout.setSizeFull();
		this.mainLayout.setMargin(true);
		this.mainLayout.setSpacing(true);

		this.addRoleField();
		this.addAppliedAclTreeField();

		this.add(this.mainLayout);
	}

//	@Override
//	public void initialize() {
//		this.setSizeFull();
//		this.setMargin(false);
//		this.setSpacing(false);
//
//		this.mainLayout = new VerticalLayout();
//		this.mainLayout.setSizeFull();
//		this.mainLayout.setMargin(true);
//		this.mainLayout.setSpacing(true);
//
//		this.addRoleField();
//		this.addAppliedAclTreeField();
//
//		this.add(this.mainLayout);
//	}

	private void addRoleField() {
		this.roleField = this.componentBuilderFactory.<Role> createComboBoxBuilder()
			.setLabel(this.messageSource.getMessage("user.roles"))
			.setWidth(25f, Unit.PERCENTAGE)
			.setRenderer(new TextRenderer<>(obj -> obj.getRolename()))
			.build();
		this.roleField.setItems(this.roleService.findAll().stream().filter(each -> !each.isAdmin()).collect(Collectors.toList()));
		this.roleField.addValueChangeListener(event -> {
			final Role selectedRole = event.getValue();
			this.appliedAclTreeField.setItems(this.getAppliedAcls(null, selectedRole), item -> this.getAppliedAcls(item, selectedRole));
		});

		this.mainLayout.add(this.roleField);
	}

	private void addAppliedAclTreeField() {
		// FIXME: Replace with builder.
		this.appliedAclTreeField = new TreeGrid<>();
//		this.appliedAclTreeField.setCaption(this.messageSource.getMessage("permissions.permissions"));
		this.appliedAclTreeField.setSizeFull();
		this.appliedAclTreeField.setItems(this.getAppliedAcls(null, null), item -> this.getAppliedAcls(item, this.roleField.getValue()));

		this.appliedAclTreeField
			.addColumn(item -> this.getIcon(item.getSecuredObject().getSecuredObjectType()) + " " + item.getSecuredObject().getName())
			.setHeader(this.messageSource.getMessage("permissions.securedObject"));

		this.appliedAclTreeField
			.addComponentColumn(item -> {
				final Checkbox canReadField = new Checkbox();
				canReadField.setValue(Optional.ofNullable(item.getReadPermission()).orElse(false));
				canReadField.addValueChangeListener(event -> {
					item.setReadPermission(event.getValue());
					this.saveAcl(item);
				});
				return canReadField;
			})
			.setHeader(this.messageSource.getMessage("permissions.read"));

		this.appliedAclTreeField
			.addComponentColumn(item -> {
				final Checkbox canUpdateField = new Checkbox();
				canUpdateField.setValue(Optional.ofNullable(item.getWritePermission()).orElse(false));
				canUpdateField.addValueChangeListener(event -> {
					item.setWritePermission(event.getValue());
					this.saveAcl(item);
				});
				return canUpdateField;
			})
			.setHeader(this.messageSource.getMessage("permissions.write"));

		this.mainLayout.addAndExpand(this.appliedAclTreeField);
	}

	private Icon getIcon(final SecuredObjectType securedObjectType) {
		switch (securedObjectType) {
			case MENU:
				return VaadinIcon.MENU.create();
			case TAB:
				return VaadinIcon.PANEL.create();
			case ENTITY:
				return VaadinIcon.DATABASE.create();
			default:
				return VaadinIcon.CIRCLE_THIN.create();
		}
	}

	private Collection<AppliedAcl> getAppliedAcls(final AppliedAcl parentAppliedAcl, final Role role) {
		List<SecuredObject> securedObjects = null;
		if (parentAppliedAcl == null) {
			securedObjects = this.securedObjectRepository.findByParentIsNull();
		} else {
			securedObjects = this.securedObjectRepository.findByParent(parentAppliedAcl.getSecuredObject());
		}
		List<Acl> relatedAcls = null;
		if (role != null && !securedObjects.isEmpty()) {
			relatedAcls = this.aclRepository.findByRoleIdAndSecuredObjectIn(role.getId(), securedObjects);
		} else {
			relatedAcls = new ArrayList<>();
		}
//		Map<SecuredObject, Acl> relatedAclsMap = relatedAcls.stream()
//			.collect(Collectors.toMap(each -> each.getSecuredObject(), each -> each));
		final Map<SecuredObject, List<Acl>> relatedAclsMap = new HashMap<>();
		relatedAcls.stream().forEach(relatedAcl -> {
			if (!relatedAclsMap.containsKey(relatedAcl.getSecuredObject())) {
				relatedAclsMap.put(relatedAcl.getSecuredObject(), new ArrayList<>());
			}
			relatedAclsMap.get(relatedAcl.getSecuredObject()).add(relatedAcl);
		});

//		List<AppliedAcl> appliedAcls = securedObjects.stream()
//			.map(each -> new AppliedAcl(role, each, Optional.ofNullable(relatedAclsMap.get(each)).orElse(new Acl())))
//			.collect(Collectors.toList());
		final List<AppliedAcl> appliedAcls = new ArrayList<>();
		securedObjects.stream().forEach(securedObject -> {
			if (relatedAclsMap.containsKey(securedObject)) {
				relatedAclsMap.get(securedObject).stream().forEach(acl -> appliedAcls.add(new AppliedAcl(securedObject, acl)));
			}
		});

		return appliedAcls;
	}

	private void saveAcl(final AppliedAcl appliedAcl) {
		final Acl acl = appliedAcl.getAcl();
		if (acl.getSecuredObject() == null) {
			acl.setSecuredObject(appliedAcl.getSecuredObject());
		}
		try {
			appliedAcl.setAcl(this.crudServices.save(acl));
		} catch (final CrudException e) {
			PermissionsLayout.logger.error(e.getMessage(), e);
		}
	}

}
