package org.jamgo.ui.layout.crud;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.BasicModelEntity;
import org.jamgo.model.entity.User;
import org.jamgo.model.exception.RepositoryForClassNotDefinedException;
import org.jamgo.model.repository.BasicModelEntityRepository;
import org.jamgo.model.repository.RepositoryManager;
import org.jamgo.model.repository.SecuredObjectRepository;
import org.jamgo.model.search.DummySearchSpecification;
import org.jamgo.model.search.SearchSpecification;
import org.jamgo.model.util.OffsetSizePageRequest;
import org.jamgo.model.util.Order;
import org.jamgo.services.UserService;
import org.jamgo.services.exception.CrudException;
import org.jamgo.services.impl.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.blazebit.persistence.PagedList;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.data.provider.CallbackDataProvider.FetchCallback;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.function.SerializableSupplier;

//@Component
//@IgnoreDuringScan
public class CrudManager {

	@Autowired
	protected RepositoryManager repositoryManager;
	@Autowired
	protected UserService userService;
	@Autowired
	protected SecurityService securityService;
	@Autowired
	protected SecuredObjectRepository securedObjectRepository;
	@Autowired
	@Qualifier("permissionProperties")
	protected Properties permissionProperties;

	public <ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE> DataProvider<ENTITY, Void> getDataProvider(
		final Class<ENTITY> entityClass)
		throws CrudException {

		return this.getDataProvider(entityClass, entityClass, null, true);
	}

	public <ENTITY extends BasicModelEntity<ID_TYPE>, RESULT extends BasicModel<ID_TYPE>, ID_TYPE> DataProvider<RESULT, Void> getDataProvider(
		final Class<ENTITY> entityClass,
		final Class<RESULT> resultClass,
		final boolean countOnEntity)
		throws CrudException {

		return this.getDataProvider(entityClass, resultClass, null, countOnEntity);
	}

	public <ENTITY extends BasicModelEntity<ID_TYPE>, RESULT extends BasicModel<ID_TYPE>, ID_TYPE> DataProvider<RESULT, Void> getDataProvider(
		final Class<ENTITY> entityClass,
		final Class<RESULT> resultClass,
		final SearchSpecification searchSpecification,
		final boolean countOnEntity)
		throws CrudException {

		CallbackDataProvider<RESULT, Void> dataProvider = null;
		try {
			final BasicModelEntityRepository<ENTITY, ID_TYPE> repository = this.repositoryManager.getRepository(entityClass);
			final FetchCallback<RESULT, Void> fetchCallback = this.createFetchCallback(repository, resultClass, searchSpecification);
			final SerializableSupplier<Integer> sizeCallback = this.createSizeCallback(repository, resultClass, searchSpecification, countOnEntity);
			dataProvider = new CallbackDataProvider<>(
				q -> fetchCallback.fetch(q),
				q -> sizeCallback.get(),
				o -> o.getId());
		} catch (RepositoryForClassNotDefinedException | IllegalArgumentException | SecurityException e) {
			throw new CrudException(e);
		}

		return dataProvider;
	}

	private <ENTITY extends BasicModelEntity<ID_TYPE>, RESULT extends BasicModel<ID_TYPE>, ID_TYPE> FetchCallback<RESULT, Void> createFetchCallback(
		final BasicModelEntityRepository<ENTITY, ID_TYPE> repository,
		final Class<RESULT> resultClass,
		final SearchSpecification searchSpecification) {

		return query -> {
			final User user = this.userService.getCurrentUser();
			List<Order> pageOrders = new ArrayList<>();
			if (!query.getSortOrders().isEmpty()) {
				pageOrders = query.getSortOrders().stream().map(each -> {
					return new Order(each.getSorted(), each.getDirection() == SortDirection.ASCENDING, false);
				}).collect(Collectors.toList());
			}
			pageOrders.add(new Order("id", true, false));
			final OffsetSizePageRequest pageRequest = new OffsetSizePageRequest(query.getOffset(), query.getLimit(), pageOrders);
			PagedList<RESULT> page = null;
			if (searchSpecification != null) {
				searchSpecification.setUser(user);
				searchSpecification.setProperties(this.permissionProperties);
				page = repository.findAll(searchSpecification, pageRequest, resultClass);
			} else {
				final DummySearchSpecification dummySearchSpecification = new DummySearchSpecification(user, this.permissionProperties);
				page = repository.findAll(dummySearchSpecification, pageRequest, resultClass);
			}
			return page.stream();
		};
	}

	private <ENTITY extends BasicModelEntity<ID_TYPE>, RESULT extends BasicModel<ID_TYPE>, ID_TYPE> SerializableSupplier<Integer> createSizeCallback(
		final BasicModelEntityRepository<ENTITY, ID_TYPE> repository,
		final Class<RESULT> resultClass,
		final SearchSpecification searchSpecification,
		final boolean countOnEntity) {

		return () -> {
			final User user = this.userService.getCurrentUser();
			Long count = 0L;

			final SearchSpecification specification = Optional.ofNullable(searchSpecification).orElse(new DummySearchSpecification());
			specification.setUser(user);
			specification.setProperties(this.permissionProperties);

			if (countOnEntity) {
				count = repository.countAll(specification);
			} else {
				count = repository.countAll(specification, resultClass);
			}
			return count.intValue();
		};
	}

//	private <T extends BasicModel<?>, S> SerializableSupplier<Integer> createSizeCallback(
//		final BasicModelEntityRepository<T, ?> repository,
//		final SearchSpecification<T, S, ?> searchSpecification) {
//		return () -> {
//			final User user = this.userService.getCurrentUser();
//			Long count = 0L;
//			if (JpaSpecificationExecutor.class.isAssignableFrom(repository.getClass())) {
//				@SuppressWarnings("unchecked")
//				final JpaSpecificationExecutor<T> jpaSpecificationRepository = (JpaSpecificationExecutor<T>) repository;
//				if (searchSpecification != null) {
//					searchSpecification.setUser(user);
//					searchSpecification.setProperties(this.permissionProperties);
//					count = jpaSpecificationRepository.count(searchSpecification);
//				} else {
//					final DummySearchSpecification dummySearchSpecification = new DummySearchSpecification(user, this.permissionProperties);
//					count = jpaSpecificationRepository.count((SearchSpecification<T, S>) dummySearchSpecification);
//				}
//			} else {
//				count = repository.count();
//			}
//
//			return count.intValue();
//		};
//	}

//	private <T extends BasicModel<?>, S> FetchItemsCallback<T> createFetchCallback(final BasicModelEntityRepository<T, ?> repository, final SearchSpecification<T, S> searchSpecification) {
//		return (sortOrders, offset, limit) -> {
//			final User user = this.userService.getCurrentUser();
//			Sort pageSort = null;
//			if (!sortOrders.isEmpty()) {
//				List<Order> pageOrders = new ArrayList<>();
//				pageOrders = sortOrders.stream().map(each -> {
//					final Sort.Direction direction = each.getDirection() == SortDirection.ASCENDING ? Sort.Direction.ASC : Sort.Direction.DESC;
//					return new Order(direction, each.getSorted());
//				}).collect(Collectors.toList());
//				pageSort = Sort.by(pageOrders);
//			}
//			final OffsetSizePageRequest pageRequest = new OffsetSizePageRequest(offset, limit, pageSort);
//			Page<T> page = null;
//			if (JpaSpecificationExecutor.class.isAssignableFrom(repository.getClass())) {
//				@SuppressWarnings("unchecked")
//				final JpaSpecificationExecutor<T> jpaSpecificationRepository = (JpaSpecificationExecutor<T>) repository;
//				if (searchSpecification != null) {
//					searchSpecification.setUser(user);
//					searchSpecification.setProperties(this.permissionProperties);
//					page = jpaSpecificationRepository.findAll(searchSpecification, pageRequest);
//				} else {
//					final DummySearchSpecification dummySearchSpecification = new DummySearchSpecification(user, this.permissionProperties);
//					page = jpaSpecificationRepository.findAll((SearchSpecification<T, S>) dummySearchSpecification, pageRequest);
//				}
//			} else {
//				page = repository.findAll(pageRequest);
//			}
//			return page.getContent().stream();
//		};
//	}

}
