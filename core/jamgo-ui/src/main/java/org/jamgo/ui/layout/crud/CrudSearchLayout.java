package org.jamgo.ui.layout.crud;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;

import org.jamgo.model.search.SearchSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.data.binder.ValidationException;

@CssImport(value = "./styles/crud-search-layout.css")
public abstract class CrudSearchLayout<T extends SearchSpecification> extends CrudLayout<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(CrudSearchLayout.class);

	protected Button applyButton;
	protected Button closeButton;
	protected Consumer<CrudSearchLayout<T>> applyHandler;
	protected Consumer<CrudSearchLayout<T>> closeHandler;

	@Override
	public void initialize() {
		this.addClassName("crud-search-layout");

		super.initialize();
		this.setSizeFull();

		this.addPanel();
		this.addActionButtons();
	}

	@Override
	protected List<? extends Component> createActionButtons() {
		final List<Button> buttons = new ArrayList<>();

		this.applyButton = this.componentBuilderFactory.createButtonBuilder()
			.setText(this.messageSource.getMessage("action.apply"))
			.build();
		this.applyButton.addClickListener(event -> this.doApply());
		buttons.add(this.applyButton);

		this.closeButton = this.componentBuilderFactory.createButtonBuilder()
			.setText(this.messageSource.getMessage("action.close"))
			.build();
		this.closeButton.addClickListener(event -> this.doClose());
		buttons.add(this.closeButton);

		return buttons;
	}

	protected void addPanel() {
		final CrudDetailsPanel panel = this.createPanel();
		panel.addClassName("crud-search-content");
		this.addAndExpand(panel);
	}

	protected void doApply() {
		if (this.applyHandler != null) {
			try {
				this.updateTargetObject();
				this.applyHandler.accept(this);
			} catch (final ValidationException e) {
				// TODO: show error message style
				Notification.show(this.messageSource.getMessage("validation.error"));
			}
		}
	}

	protected void doClose() {
		if (this.closeHandler != null) {
			this.closeHandler.accept(this);
		}
	}

	public Consumer<CrudSearchLayout<T>> getApplyHandler() {
		return this.applyHandler;
	}

	public void setApplyHandler(final Consumer<CrudSearchLayout<T>> applyHandler) {
		this.applyHandler = applyHandler;
	}

	public Consumer<CrudSearchLayout<T>> getCloseHandler() {
		return this.closeHandler;
	}

	public void setCloseHandler(final Consumer<CrudSearchLayout<T>> closeHandler) {
		this.closeHandler = closeHandler;
	}

	public Button getApplyButton() {
		return this.applyButton;
	}

	public Button getCloseButton() {
		return this.closeButton;
	}

	public void setCloseButtonVisible(final boolean visible) {
		this.closeButton.setVisible(visible);
	}

	protected abstract CrudDetailsPanel createPanel();

	@Override
	public void updateFields(final Object searchObject) {
		super.updateFields(searchObject);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setTargetObject(final SearchSpecification searchSpecification) {
		super.setTargetObject((T) searchSpecification);
	}

	@Override
	protected T getNewTargetObject() throws Exception {
		try {
			final Constructor<T> declaredConstructor = this.getTargetObjectClass().getDeclaredConstructor(Locale.class);
			CrudSearchLayout.logger.debug("Using declared constructor with locale: {}", this.locale);
			return declaredConstructor.newInstance(this.locale);
		} catch (final NoSuchMethodException e) {
			final Constructor<T> declaredConstructor = this.getTargetObjectClass().getDeclaredConstructor();
			CrudSearchLayout.logger.debug("Using declared constructor without arguments");
			return declaredConstructor.newInstance();
		}
	}

}
