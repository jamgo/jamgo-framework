package org.jamgo.ui.layout.defs;

import org.jamgo.model.entity.Language;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.details.LanguageDetailsLayout;

@LayoutDefComponent
public class LanguageLayoutDef extends ModelLayoutDef<Language> {

	public LanguageLayoutDef(final LocalizedMessageService messageSource, final LocalizedObjectService localizedObjectService) {
		super(messageSource, localizedObjectService);
	}


	@SuppressWarnings("unchecked")
	@Override
	public CrudLayoutDef<Language, Language> getBackofficeLayoutDef() {
		// @formatter:off
		return (CrudLayoutDef<Language, Language>) CrudLayoutDef.<Language> builderEntity()
			.setId("language")
			.setNameSupplier(() -> this.messageSource.getMessage("table.languages"))
			.modelGridDef()
				.setEntityClass(Language.class)
				.columnDef()
					.setId("name")
					.setCaptionSupplier(() -> "Name")
					.setValueProvider(o -> o.getName())
				.end()
				.columnDef()
					.setId("languageCode")
					.setCaptionSupplier(() -> "Language Code")
					.setValueProvider(o -> o.getLanguageCode())
				.end()
				.columnDef()
					.setId("countryCode")
					.setCaptionSupplier(() -> "Country Code")
					.setValueProvider(o -> o.getCountryCode())
				.end()
				.detailsLayoutClass(LanguageDetailsLayout.class)
			.end()
			.tableLayoutVerticalConfig()
			.build();
		// @formatter:on
	}

	@Override
	public Class<Language> getModelClass() {
		return Language.class;
	}

}
