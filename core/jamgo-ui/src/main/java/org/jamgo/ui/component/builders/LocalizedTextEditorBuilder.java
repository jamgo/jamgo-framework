package org.jamgo.ui.component.builders;

import org.jamgo.ui.component.LocalizedTextEditor;
import org.jamgo.vaadin.components.JamgoComponentConstants;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.ComponentUtil;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class LocalizedTextEditorBuilder extends LocalizedFieldBuilder<LocalizedTextEditor> {

	@Override
	public void afterPropertiesSet() throws Exception {
		super.afterPropertiesSet();
		this.instance = this.applicationContext.getBean(LocalizedTextEditor.class);
		ComponentUtil.setData(this.instance, JamgoComponentConstants.PROPERTY_DEFAULT_WIDTH, ComponentColspan.FULL_WIDTH);
	}

}
