package org.jamgo.ui.component;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import org.jamgo.model.entity.Model;
import org.jamgo.services.exception.CrudException;
import org.jamgo.services.impl.CrudServices;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.ServiceManager;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.jamgo.ui.layout.BackofficeApplicationDef;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.google.common.collect.Sets;
import com.vaadin.componentfactory.EnhancedDialog;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.shared.Registration;

public abstract class ModelSelect<MODEL extends Model> extends Composite<VerticalLayout> {

	private static final Logger logger = LoggerFactory.getLogger(ModelSelect.class);

	@Autowired
	protected ApplicationContext applicationContext;
	@Autowired
	protected BackofficeApplicationDef backofficeApplicationDef;
	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	protected CrudServices crudServices;
	@Autowired
	protected ServiceManager serviceManager;

	protected Button createButton;

	private Class<? extends CrudDetailsLayout<MODEL>> detailsLayoutClass;
	protected CrudLayoutDef<?, MODEL> crudLayoutDef;
	protected CrudDetailsLayout<MODEL> detailsLayout;

	protected boolean createVisible = true;

	private Consumer<Void> doCreateNewHandler;

	public ModelSelect() {
		super();
	}

	protected void doCreateNew() {
		if (this.doCreateNewHandler == null) {
			try {
				final MODEL item = this.getNewItem();
				this.doCreateNew(item);
			} catch (final Exception e) {
				ModelSelect.logger.error(e.getMessage(), e);
				// TODO: show message error style.
				Notification.show(this.messageSource.getMessage("error.general.add"));
				throw new RuntimeException(e);
			}
		} else {
			this.doCreateNewHandler.accept(null);
		}
	}

	public void doCreateNew(final MODEL item) {
		this.openDetailsDialog(item, true);
	}

	protected abstract void clearContents();

	protected void openDetailsDialog(final MODEL relatedObject, final boolean editable) {
		final EnhancedDialog dialog = new EnhancedDialog();
		dialog.setModal(true);
		dialog.setCloseOnEsc(false);
		dialog.setCloseOnOutsideClick(false);
		dialog.setMinWidth(50, Unit.PERCENTAGE);
		dialog.setHeader(this.crudLayoutDef.getName());

		final CrudDetailsLayout<? extends MODEL> detailsLayout = this.createDetailsLayout(relatedObject.getClass(), true);

		if (editable) {
			detailsLayout.setOkHandler(obj -> {
				this.addItem(obj.getTargetObject());
				dialog.close();
			});
		}

		detailsLayout.setCancelHandler(obj -> dialog.close());
		detailsLayout.setCrudLayoutDef(this.crudLayoutDef);
		detailsLayout.updateFields(relatedObject);

		if (!editable) {
			detailsLayout.setReadOnly(true);
		}

		dialog.setContent(detailsLayout);
		dialog.open();
	}

	protected CrudDetailsLayout<MODEL> createDetailsLayout(final boolean withButtons) {
		return this.createDetailsLayout(this.crudLayoutDef.getEntityClass(), withButtons);
	}

	protected CrudDetailsLayout<MODEL> createDetailsLayout(final Class<? extends Model> itemModelClass, final boolean withButtons) {
		final CrudLayoutDef<?, ?> itemCrudLayoutDef = this.backofficeApplicationDef.getCrudLayoutDef(itemModelClass);
		final CrudDetailsLayout<MODEL> editDetailsLayout = (CrudDetailsLayout<MODEL>) this.applicationContext.getBean(itemCrudLayoutDef.getDetailsLayoutClass());
		editDetailsLayout.withButtons(withButtons);
		editDetailsLayout.initialize(itemCrudLayoutDef);
		return editDetailsLayout;
	}

	protected MODEL getNewItem() throws Exception {
		return this.crudLayoutDef.getEntityClass().getDeclaredConstructor().newInstance();
	}

	protected void updateDetails(final MODEL targetObject) {
		if (this.detailsLayout != null) {
			this.detailsLayout.updateFields(targetObject);
		}
	}

	@SuppressWarnings("unchecked")
	public void addItem(final MODEL newItem) {
		this.addItems(Sets.newHashSet(newItem));
	}

	public void addItems(final Set<MODEL> newItems) {
		final Set<MODEL> savedItems = new HashSet<>();
		try {
			newItems.forEach(each -> {
				savedItems.add(this.crudServices.save(each));
			});
			// TODO: show message humanized style.
			Notification.show(this.messageSource.getMessage("info.action.saved.entity"));
		} catch (final CrudException e) {
			ModelSelect.logger.error(e.getMessage(), e);
			// TODO: show message error style.
			Notification.show(this.messageSource.getMessage("error.general.save"));
		}

		this.updateDetails(savedItems.stream().findFirst().get());
		this.updateContents(savedItems);
	}

	protected abstract void updateContents(Set<MODEL> items);

	public CrudLayoutDef<?, MODEL> getCrudLayoutDef() {
		return this.crudLayoutDef;
	}

	public void setCrudLayoutDef(final CrudLayoutDef<MODEL, MODEL> crudLayoutDef) {
		this.crudLayoutDef = crudLayoutDef;
	}

	public Class<? extends CrudDetailsLayout<?>> getDetailsLayoutClass() {
		return this.detailsLayoutClass;
	}

	public void setDetailsLayoutClass(final Class<? extends CrudDetailsLayout<MODEL>> detailsLayoutClass) {
		this.detailsLayoutClass = detailsLayoutClass;
	}

	public boolean isCreateVisible() {
		return this.createVisible;
	}

	public void setCreateVisible(final boolean createVisible) {
		this.createVisible = createVisible;
		this.createButton.setVisible(createVisible);
	}

	public Registration addCreateModelClickListener(final ComponentEventListener<ClickEvent<Button>> listener) {
		return this.createButton.addClickListener(listener);
	}

	public Consumer<Void> getDoCreateNewHandler() {
		return this.doCreateNewHandler;
	}

	public void setDoCreateNewHandler(final Consumer<Void> handler) {
		this.doCreateNewHandler = handler;
	}

	// ... FIXME: Multiple implementations, create notifications service

	protected void showErrorNotification(final String message) {
		final Notification notification = new Notification();
		notification.addThemeVariants(NotificationVariant.LUMO_ERROR);

		final Span label = new Span(message);

		final Button cancelButton = new Button(
			this.messageSource.getMessage("action.cancel"),
			e -> notification.close());
		cancelButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

		notification.add(label, cancelButton);
		notification.open();
	}

}