package org.jamgo.ui.layout.defs;

import java.time.format.DateTimeFormatter;

import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.ui.layout.BackofficeLayoutDef;

public abstract class AbstractLayoutDef {

	public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

	protected final LocalizedMessageService messageSource;
	protected final LocalizedObjectService localizedObjectService;

	public AbstractLayoutDef(final LocalizedMessageService messageSource, final LocalizedObjectService localizedObjectService) {
		this.messageSource = messageSource;
		this.localizedObjectService = localizedObjectService;
	}

	public abstract Class<?> getLayoutDefClass();

	public abstract BackofficeLayoutDef getBackofficeLayoutDef();

}
