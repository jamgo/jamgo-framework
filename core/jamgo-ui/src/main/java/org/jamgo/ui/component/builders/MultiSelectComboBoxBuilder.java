package org.jamgo.ui.component.builders;

import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import org.jamgo.vaadin.builder.base.HasEnabledBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.jamgo.vaadin.ui.builder.JamgoComponentBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MultiSelectComboBoxBuilder<T> extends JamgoComponentBuilder<MultiSelectComboBox<T>, MultiSelectComboBoxBuilder<T>> implements
	HasSizeBuilder<MultiSelectComboBoxBuilder<T>, MultiSelectComboBox<T>>,
	HasStyleBuilder<MultiSelectComboBoxBuilder<T>, MultiSelectComboBox<T>>,
	HasValidationBuilder<MultiSelectComboBoxBuilder<T>, MultiSelectComboBox<T>>,
	HasEnabledBuilder<MultiSelectComboBoxBuilder<T>, MultiSelectComboBox<T>>,
	HasThemeBuilder<MultiSelectComboBoxBuilder<T>, MultiSelectComboBox<T>>,
	HasValueBuilder<MultiSelectComboBoxBuilder<T>, MultiSelectComboBox<T>> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new MultiSelectComboBox<>();
	}

}