package org.jamgo.ui.component.builders;

import java.io.File;

import com.vaadin.flow.component.tabs.TabSheet;
import org.apache.commons.lang3.StringUtils;
import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.Model;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.vaadin.ui.builder.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class JamgoComponentBuilderFactory {

	@Autowired
	protected ApplicationContext applicationContext;
	@Autowired
	protected LocalizedMessageService messageService;

	public TextFieldBuilder createTextFieldBuilder() {
		return this.getBean(TextFieldBuilder.class);
	}

	public PasswordFieldBuilder createPasswordFieldBuilder() {
		return this.getBean(PasswordFieldBuilder.class);
	}

	public TextAreaBuilder createTextAreaBuilder() {
		return this.getBean(TextAreaBuilder.class);
	}

	public TextEditorBuilder createTextEditorBuilder() {
		return this.getBean(TextEditorBuilder.class);
	}

	public NumberFieldBuilder createNumberFieldBuilder() {
		return this.getBean(NumberFieldBuilder.class);
	}

	public IntegerFieldBuilder createIntegerFieldBuilder() {
		return this.getBean(IntegerFieldBuilder.class);
	}

	public BigDecimalFieldBuilder createBigDecimalFieldBuilder() {
		return this.getBean(BigDecimalFieldBuilder.class);
	}

	public EmailFieldBuilder createEmailFieldBuilder() {
		return this.getBean(EmailFieldBuilder.class);
	}

	@SuppressWarnings("unchecked")
	public <T> CheckBoxGroupBuilder<T> createCheckBoxGroupBuilder() {
		return this.getBean(CheckBoxGroupBuilder.class);
	}

	public CheckBoxBuilder createCheckBoxBuilder() {
		return this.getBean(CheckBoxBuilder.class);
	}

	@SuppressWarnings("unchecked")
	public <T> ComboBoxBuilder<T> createComboBoxBuilder() {
		return this.getBean(ComboBoxBuilder.class);
	}

	@SuppressWarnings("unchecked")
	public <T extends Enum<?>> EnumComboBoxBuilder<T> createEnumComboBoxBuilder(final Class<T> enumClass) {
		return this.getBean(EnumComboBoxBuilder.class, enumClass);
	}

	@SuppressWarnings("unchecked")
	public <MODEL extends Model> ModelComboBoxBuilder<MODEL> createModelComboBoxBuilder(final Class<MODEL> modelClass) {
		return this.getBean(ModelComboBoxBuilder.class, modelClass);
	}

	@SuppressWarnings("unchecked")
	public <MODEL extends Model> ModelMultiComboBoxBuilder<MODEL> createModelMultiComboBoxBuilder(final Class<MODEL> modelClass) {
		return this.getBean(ModelMultiComboBoxBuilder.class, modelClass);
	}

	@SuppressWarnings("unchecked")
	public <MODEL extends Model, PARENT_MODEL extends Model> ModelOneToManyBuilder<MODEL, PARENT_MODEL> createModelOneToManyBuilder(final Class<MODEL> modelClass) {
		return this.getBean(ModelOneToManyBuilder.class, modelClass);
	}

	@SuppressWarnings("unchecked")
	public <MODEL extends Model, PARENT_MODEL extends Model> ModelManyToManyBuilder<MODEL, PARENT_MODEL> createModelManyToManyBuilder(final Class<MODEL> modelClass) {
		return this.getBean(ModelManyToManyBuilder.class, modelClass);
	}

	@SuppressWarnings("unchecked")
	public <BASIC_MODEL extends BasicModel<?>> ModelGridBuilder<BASIC_MODEL> createModelGridBuilder(final Class<BASIC_MODEL> modelClass) {
		return this.getBean(ModelGridBuilder.class);
	}

	@SuppressWarnings("unchecked")
	public <MODEL extends Model> ModelListBoxBuilder<MODEL> createModelListBoxBuilder(final Class<MODEL> modelClass) {
		return this.getBean(ModelListBoxBuilder.class, modelClass);
	}

	public LabelBuilder createLabelBuilder() {
		return this.getBean(LabelBuilder.class);
	}

	public H3Builder createH3Builder() {
		return this.getBean(H3Builder.class);
	}

	public ParagraphBuilder createParagraphBuilder() {
		return this.getBean(ParagraphBuilder.class);
	}

	public ButtonBuilder createButtonBuilder() {
		return this.getBean(ButtonBuilder.class);
	}

	public DateTimeFieldBuilder createDateTimeFieldBuilder() {
		return this.getBean(DateTimeFieldBuilder.class);
	}

	public DateFieldBuilder createDateFieldBuilder() {
		return this.getBean(DateFieldBuilder.class);
	}

	public TimeFieldBuilder createTimeFieldBuilder() {
		return this.getBean(TimeFieldBuilder.class);
	}

	@SuppressWarnings("unchecked")
	public <T> RadioButtonGroupBuilder<T> createRadioButtonGroupBuilder() {
		return this.getBean(RadioButtonGroupBuilder.class);
	}

	public TriStateRadioButtonGroupBuilder createTriStateRadioButtonGroupBuilder() {
		return this.getBean(TriStateRadioButtonGroupBuilder.class);
	}

	@SuppressWarnings("unchecked")
	public <T> GridBuilder<T> createGridBuilder() {
		return this.getBean(GridBuilder.class);
	}

	@SuppressWarnings("unchecked")
	public <T> ListBoxBuilder<T> createListBoxBuilder() {
		return this.getBean(ListBoxBuilder.class);
	}

	@SuppressWarnings("unchecked")
	public <T> MultiSelectListBoxBuilder<T> createMultiSelectListBoxBuilder() {
		return this.getBean(MultiSelectListBoxBuilder.class);
	}

	@SuppressWarnings("unchecked")
	public <T> MultiSelectComboBoxBuilder<T> createMultiSelectComboBoxBuilder() {
		return this.getBean(MultiSelectComboBoxBuilder.class);
	}

	public UploadBuilder createUploadBuilder() {
		return this.getBean(UploadBuilder.class);
	}

	public JamgoFormLayoutBuilder createJamgoFormLayoutBuilder() {
		return this.getBean(JamgoFormLayoutBuilder.class);
	}

	public CrudDetailsPanelBuilder createCrudDetailsPanelBuilder() {
		return this.getBean(CrudDetailsPanelBuilder.class);
	}

	public HorizontalLayoutBuilder createHorizontalLayoutBuilder() {
		return this.getBean(HorizontalLayoutBuilder.class);
	}

	public VerticalLayoutBuilder createVerticalLayoutBuilder() {
		return this.getBean(VerticalLayoutBuilder.class);
	}

	public EnhancedDialogBuilder createEnhancedDialogBuilder() {
		return this.getBean(EnhancedDialogBuilder.class);
	}

	public AnchorBuilder createAnchorBuilder() {
		return this.getBean(AnchorBuilder.class);
	}

	public ImageBuilder createImageBuilder() {
		return this.getBean(ImageBuilder.class);
	}

	public SpanBuilder createSpanBuilder() {
		return this.getBean(SpanBuilder.class);
	}

	public DetailsBuilder createDetailsBuilder() {
		return this.getBean(DetailsBuilder.class);
	}

	public FileSelectBuilder createFileSelectBuilder(final File rootFile) {
		return this.getBean(FileSelectBuilder.class, rootFile);
	}

	public BinaryResourceFieldBuilder createBinaryResourceFieldBuilder() {
		return this.getBean(BinaryResourceFieldBuilder.class);
	}

	public LocalizedTextFieldBuilder createLocalizedTextFieldBuilder() {
		return this.getBean(LocalizedTextFieldBuilder.class);
	}

	public LocalizedTextEditorBuilder createLocalizedTextEditorBuilder() {
		return this.getBean(LocalizedTextEditorBuilder.class);
	}

	public LocalizedTextAreaBuilder createLocalizedTextAreaBuilder() {
		return this.getBean(LocalizedTextAreaBuilder.class);
	}

	public LocalizedBinaryResourceFieldBuilder createLocalizedBinaryResourceFieldBuilder() {
		return this.getBean(LocalizedBinaryResourceFieldBuilder.class);
	}

	public ServerFileNameFieldBuilder createServerFileNameFieldBuilder() {
		return this.getBean(ServerFileNameFieldBuilder.class);
	}

//	public ColorPickerFieldBuilder createColorPickerFieldBuilder() {
//		return this.getBean(ColorPickerFieldBuilder.class);
//	}

	public ToggleButtonBuilder createToggleButtonBuilder() {
		return this.getBean(ToggleButtonBuilder.class);
	}
	public TabSheetBuilder createTabSheetBuilder() {
		return this.getBean(TabSheetBuilder.class);
	}

	protected <T> T getBean(final Class<T> beanClass) {
		return this.getBean(beanClass, true, (Object[]) null);
	}

	protected <T> T getBean(final Class<T> beanClass, final Object... args) {
		return this.getBean(beanClass, true, args);
	}

	/*
	 * ...	If any builder is extended in a project, an ambiguity of beans would be generated
	 * 		since both the original builder and its subclass would match the requested bean.
	 * 		To avoid this we try to get the beans using their default qualifier (the
	 * 		uncapitalized name of the class).
	 */

	@SuppressWarnings("unchecked")
	protected <T> T getBean(final Class<T> beanClass, final boolean ignoreSubclasses, final Object... args) {
		T bean = null;
		if (ignoreSubclasses) {
			if (args == null) {
				bean = this.applicationContext.getBean(StringUtils.uncapitalize(beanClass.getSimpleName()), beanClass);
			} else {
				bean = (T) this.applicationContext.getBean(StringUtils.uncapitalize(beanClass.getSimpleName()), args);
			}
		} else {
			if (args == null) {
				bean = this.applicationContext.getBean(beanClass);
			} else {
				bean = this.applicationContext.getBean(beanClass, args);
			}
		}
		return bean;
	}
}
