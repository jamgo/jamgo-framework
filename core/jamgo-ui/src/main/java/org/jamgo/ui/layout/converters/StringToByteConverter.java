package org.jamgo.ui.layout.converters;

import java.io.UnsupportedEncodingException;

import com.vaadin.flow.data.binder.Result;
import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.data.converter.Converter;

public class StringToByteConverter implements Converter<String, byte[]> {

	private static final long serialVersionUID = -5775962504859167749L;

	@Override
	public Result<byte[]> convertToModel(final String value, final ValueContext context) {

		try {
			return Result.ok(value.getBytes("UTF-8"));
		} catch (final UnsupportedEncodingException e) {
			return Result.error(e.getMessage());
		}
	}

	@Override
	public String convertToPresentation(final byte[] value, final ValueContext context) {
		if (value == null) {
			return "";
		}
		return new String(value);

	}

}
