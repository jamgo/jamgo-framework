package org.jamgo.ui.config;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.vaadin.flow.di.ResourceProvider;

/*
 * ...	WORKAROUND!!!
 * 		Resources from app-layout-addon-4.0.0.jar are not in the directory where vaadin search.
 */

@Component
public class VaadinResourceProvider implements ResourceProvider {

	private final Map<String, CachedStreamData> cache = new ConcurrentHashMap<>();

	/**
	 * Creates a new instance.
	 */
	public VaadinResourceProvider() {
	}

	@Override
	public URL getApplicationResource(final String path) {
		String realPath = path;
		if (path.contains("appreciated")) {
			realPath = "META-INF/resources/frontend" + StringUtils.removeStart(path, ".");
		}
		return VaadinResourceProvider.class.getClassLoader().getResource(realPath);
	}

	@Override
	public List<URL> getApplicationResources(final String path)
		throws IOException {
		return Collections.list(VaadinResourceProvider.class.getClassLoader().getResources(path));
	}

	@Override
	public URL getClientResource(final String path) {
		return this.getApplicationResource(path);
	}

	@Override
	public InputStream getClientResourceAsStream(final String path)
		throws IOException {
		// the client resource should be available in the classpath, so
		// its content is cached once. If an exception is thrown then
		// something is broken and it's also cached and will be rethrown on
		// every subsequent access
		final CachedStreamData cached = this.cache.computeIfAbsent(path, key -> {
			final URL url = this.getClientResource(key);
			try (InputStream stream = url.openStream()) {
				final ByteArrayOutputStream tempBuffer = new ByteArrayOutputStream();
				IOUtils.copy(stream, tempBuffer);
				return new CachedStreamData(tempBuffer.toByteArray(), null);
			} catch (final IOException e) {
				return new CachedStreamData(null, e);
			}
		});

		final IOException exception = cached.exception;
		if (exception == null) {
			return new ByteArrayInputStream(cached.data);
		}
		throw exception;
	}

	private static class CachedStreamData {

		private final byte[] data;
		private final IOException exception;

		private CachedStreamData(final byte[] data, final IOException exception) {
			this.data = data;
			this.exception = exception;
		}
	}

}
