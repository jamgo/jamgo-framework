package org.jamgo.ui.component;

import java.util.function.Predicate;

import org.jamgo.model.entity.Model;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;

@org.springframework.stereotype.Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ModelOneToManyEditAction<T extends Model, P extends Model> extends ModelToManyAction<T, P> {

	public ModelOneToManyEditAction(final ModelOneToMany<T, P> modelOneToMany) {
		super(modelOneToMany);
	}

	public ModelOneToManyEditAction(final ModelOneToMany<T, P> modelOneToMany, final Predicate<T> visiblePredicate) {
		super(modelOneToMany, visiblePredicate);
	}

	@Override
	protected Component getComponent(final T relatedObject) {
		final Button editButton = this.componentBuilderFactory.createButtonBuilder()
			.addClassName("to-many-row-action-button")
			.setIcon(VaadinIcon.PENCIL.create())
			.build();
		editButton.addClickListener(event -> this.doEdit(relatedObject));
		return editButton;
	}

	private void doEdit(final T targetObject) {
		this.modelToMany.openDetailsDialog(targetObject, true);
	}

}
