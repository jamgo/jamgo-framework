package org.jamgo.ui.layout.menu;

import java.util.HashMap;
import java.util.Map;

import org.jamgo.model.enums.SecuredObjectType;
import org.jamgo.model.repository.SecuredObjectRepository;
import org.jamgo.services.LanguageServices;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.SecurityService;
import org.jamgo.services.impl.SettingsService;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.jamgo.ui.layout.BackofficeApplicationDef;
import org.jamgo.ui.layout.BackofficeLayoutDef;
//import org.jamgo.ui.layout.utils.JamgoComponentFactory;
//import org.jamgo.vaadin.ui.engine.MultiLanguageEngine;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.VaadinService;

@Component
public abstract class MenuLayoutOld extends VerticalLayout implements InitializingBean {

	private static final long serialVersionUID = 1L;

	protected Map<Button, VerticalLayout> menuGroupLayoutMap = new HashMap<>();

//	@Autowired
//	private ContentLayout contentLayout;
	@Autowired
	protected LocalizedMessageService messageSource;
//	@Autowired
//	protected JamgoComponentFactory componentFactory;
	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected SecurityService securityService;
	@Autowired
	protected LanguageServices languageServices;
//	@Autowired
//	protected MultiLanguageEngine multilangeEngine;
	@Autowired
	protected SecuredObjectRepository securedObjectRepository;
	@Autowired
	private SettingsService settingsService;
	@Autowired
	protected BackofficeApplicationDef backofficeApplicationDef;

	@Value("${language.backoffice.enabled:#{false}}")
	private String multiLangEnabled;

	protected Label title;
	protected HorizontalLayout languageLayout = new HorizontalLayout();
	protected VerticalLayout menuItemsLayout = new VerticalLayout();

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		this.setWidth(100, Unit.PERCENTAGE);
		this.setMargin(false);
		this.setSpacing(false);

		final VerticalLayout top = new VerticalLayout();
		top.setWidth(100, Unit.PERCENTAGE);
		top.setDefaultHorizontalComponentAlignment(Alignment.START);
		top.addClassName("valo-menu-title");
		this.add(top);

		this.title = new Label(this.messageSource.getMessage("application.title"));
//		this.title.setContentMode(ContentMode.HTML);
		this.title.setSizeUndefined();
		top.add(this.title);
//		top.setComponentAlignment(this.title, Alignment.MIDDLE_CENTER);

//		if (Boolean.valueOf(this.multiLangEnabled)) {
//			this.languageServices.getAll().stream().forEach(language -> {
//				if (language.isActive()) {
//					final Button languageButton = new Button();
//					languageButton.setText(language.getName().substring(0, 3).toLowerCase());
//					languageButton.addClassName("menu");
//					languageButton.addStyleName(ValoTheme.BUTTON_LINK);
//					languageButton.addClickListener(listener -> this.multilangeEngine.setLanguage(language.getId()));
//					this.languageLayout.addComponent(languageButton);
//				}
//			});
//			top.addComponent(this.languageLayout);
//			top.setComponentAlignment(this.languageLayout, Alignment.MIDDLE_RIGHT);
//		}

		final HorizontalLayout logoutLayout = new HorizontalLayout();
		logoutLayout.setSpacing(true);
		final Button logoutButton = new Button(this.messageSource.getMessage("action.logout"));
		final String userName = VaadinService.getCurrentRequest().getUserPrincipal().getName();
		final Label userNameLabel = new Label(userName);
		logoutButton.setClassName("link");
		logoutButton.addClickListener(event -> this.doLogout(event));
		logoutLayout.add(userNameLabel, logoutButton);
		this.add(logoutLayout);
		this.setHorizontalComponentAlignment(Alignment.END, logoutLayout);

		this.add(this.menuItemsLayout);
		this.addMenuItems();

		this.addClassName("valo-menu-part");
	}

	protected MenuItem createMenuItem(final BackofficeLayoutDef layoutDef) {
		MenuItem newMenuItem = null;
		if (this.securityService.hasReadPermission(SecuredObjectType.MENU, layoutDef.getId(), false)) {
			newMenuItem = new MenuItem(layoutDef);
		}
		return newMenuItem;
	}

	protected MenuGroup createMenuGroup(final String id) {
		MenuGroup newMenuGroup = null;
		if (this.securityService.hasReadPermission(SecuredObjectType.MENU, id, false)) {
			newMenuGroup = new MenuGroup();
			newMenuGroup.setId(id);
		}
		return newMenuGroup;
	}

	protected void doLogout(final ClickEvent<?> event) {
		// TODO
//		((MainUI) MenuLayout.this.getUI()).logout();
	}

	protected void doMenuGroupButtonClick(final ClickEvent<Button> event) {
		final Button clickedButton = event.getSource();
		for (final Map.Entry<Button, VerticalLayout> eachEntry : this.menuGroupLayoutMap.entrySet()) {
			final Button button = eachEntry.getKey();
			final VerticalLayout layout = eachEntry.getValue();
			if (button == clickedButton) {
				layout.setVisible(!layout.isVisible());
				this.setMenuGroupButtonStyle(button, layout.isVisible());
			} else {
				layout.setVisible(false);
				this.setMenuGroupButtonStyle(button, false);
			}
		}
	}

	private void setMenuGroupButtonStyle(final Button button, final boolean opened) {
		if (opened) {
			button.removeClassName("jamgo-menu-group-closed");
			button.addClassName("jamgo-menu-group-opened");
		} else {
			button.removeClassName("jamgo-menu-group-opened");
			button.addClassName("jamgo-menu-group-closed");
		}
	}

	protected void doMenuItemButtonClick(final ClickEvent<Button> event) {
//		final MenuItem menuItem = (MenuItem) event.getSource();
//		this.contentLayout.activateTab(menuItem.getLayoutDef());
	}

	protected void registerMenuGroup(final Button menuGroupButton, final VerticalLayout menuGroupItemsLayout) {
		this.menuGroupLayoutMap.put(menuGroupButton, menuGroupItemsLayout);
	}

	public abstract MenuGroup getRootMenuGroup();

	public LocalizedMessageService getMessageService() {
		return this.messageSource;
	}

	public String getImagesPath() {
		return this.settingsService.getImagesPath();
	}

	public String getIconsSuffix() {
		return this.settingsService.getIconsSuffix();
	}

	public void addMenuItems() {
		this.getRootMenuGroup().getMenuItems().stream().forEach(eachMenuItem -> {
//			if (this.securityService.hasReadPermission(SecuredObjectType.MENU, eachMenuItem.getId().get(), false)) {
//				this.menuItemsLayout.add(eachMenuItem);
//			}
		});
	}

	public void refreshMenuItems() {
		this.backofficeApplicationDef.init();
		this.menuItemsLayout.removeAll();
		this.addMenuItems();
	}
}
