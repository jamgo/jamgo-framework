package org.jamgo.ui.layout;

import java.util.function.Supplier;

public class CustomLayoutDef extends BackofficeLayoutDef {

	private Class<? extends CustomLayout> layoutClass;

	public CustomLayoutDef() {
		super();
	}

	public static Builder builder() {
		return new CustomLayoutDef.Builder();
	}

	public static class Builder {

		private CustomLayoutDef instance = new CustomLayoutDef();

		public Builder setId(String id) {
			this.instance.id = id;
			return this;
		}

		public Builder setNameSupplier(Supplier<Object> nameSupplier) {
			this.instance.nameSupplier = nameSupplier;
			return this;
		}

		public Builder setDescriptionSupplier(Supplier<Object> descriptionSupplier) {
			this.instance.descriptionSupplier = descriptionSupplier;
			return this;
		}

		public Builder setCountSupplier(Supplier<Object> countSupplier) {
			this.instance.countSupplier = countSupplier;
			return this;
		}

		/*
		 * @Deprecated
		 *
		 * ...	Use setNameSupplier.
		 */
		@Deprecated
		public Builder setName(String name) {
			this.instance.nameSupplier = () -> name;
			return this;
		}

		public Builder setImageName(String imageName) {
			this.instance.imageName = imageName;
			return this;
		}

		public Builder setOpenOnStart(boolean openOnStart) {
			this.instance.openOnStart = openOnStart;
			return this;
		}

		public Builder setLayoutClass(Class<? extends CustomLayout> layoutClass) {
			this.instance.layoutClass = layoutClass;
			return this;
		}

		public CustomLayoutDef build() {
			return this.instance;
		}

	}

	public Class<? extends CustomLayout> getLayoutClass() {
		return this.layoutClass;
	}

}
