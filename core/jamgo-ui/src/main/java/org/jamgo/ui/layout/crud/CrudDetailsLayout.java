package org.jamgo.ui.layout.crud;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.SecuredObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.Tabs.SelectedChangeEvent;

@SuppressWarnings("serial")
@CssImport(value = "./styles/crud-details-layout.css")
public abstract class CrudDetailsLayout<T extends BasicModel<?>> extends CrudBasicModelLayout<T> {

	static final Logger logger = LoggerFactory.getLogger(CrudDetailsLayout.class);

	public enum LayoutMode {
		NORMAL,
		MAXIMIZED
	}

	protected CrudDetailsPanel summaryLayout;

	@Override
	public void initialize(final CrudLayoutDef<?, ?> crudLayoutDef) {
		super.initialize(crudLayoutDef);
		this.addTabs();
	}

	protected List<CrudBasicPanel> createPanels() {
		final List<CrudBasicPanel> panels = new ArrayList<>();
		this.mainPanel = this.createMainPanel();
		this.mainPanel.setSecurityId("main");
		panels.add(this.mainPanel);
		return panels;
	}

	protected abstract CrudBasicPanel createMainPanel();

	@Override
	protected void addContent(final CrudLayoutDef<?, ?> crudLayoutDef) {
		super.addContent(crudLayoutDef);
		final Component tabSheet = this.createTabSheet();
		if (crudLayoutDef.getSummaryLayoutClass() != null) {
			final SplitLayout summaryAndContentsPanel = new SplitLayout();
			summaryAndContentsPanel.setSizeFull();

			final CrudDetailsPanel summary = this.createSummary(crudLayoutDef.getSummaryLayoutClass());
			final VerticalLayout contentsPanel = this.componentBuilderFactory.createVerticalLayoutBuilder()
				.build();
			contentsPanel.add(tabSheet);
			contentsPanel.add(this.content);
			summaryAndContentsPanel.addToPrimary(summary);
			summaryAndContentsPanel.addToSecondary(contentsPanel);
			summary.setWidth(15f, Unit.PERCENTAGE);
			contentsPanel.setWidth(85f, Unit.PERCENTAGE);
			this.add(summaryAndContentsPanel);
		} else {
			this.add(tabSheet);
			this.addAndExpand(this.content);
		}
	}

	protected CrudDetailsPanel createSummary(final Class<? extends CrudDetailsPanel> summaryLayoutClass) {
		this.summaryLayout = this.applicationContext.getBean(summaryLayoutClass);
		return this.summaryLayout;
	}

	protected Component createTabSheet() {
		this.tabSheet = new Tabs();
		this.tabSheet.setWidthFull();
		return this.tabSheet;
	}

	protected void selectedTabChanged(final SelectedChangeEvent event) {
		this.tabsComponentMap.values().forEach(each -> each.setVisible(false));
		final Component selectedComponent = this.tabsComponentMap.get(event.getSelectedTab());
		Optional.ofNullable(selectedComponent).ifPresent(o -> o.setVisible(true));
		if (selectedComponent == this.crudPermissionsPanel) {
			this.permissionsLayout.updateFields(this.targetObject);
		}
	}

	protected void addTabs() {
		// Create all panels for this details layout - createPanels() method can be overriden in subprojects
		final List<CrudBasicPanel> panels = this.createPanels();

		// Create all pluggable panels defined in layout def
		this.crudLayoutDef.getPluggablePanels().forEach(clazz -> this.addPluggablePanel(clazz, panels));

		// Create a tab for each panel created before, and add tab to tabsheet
		// Saves a map to associate each tab with a panel.
		// Adds the panel to a single div "content", and each panel will be visible only when its related tab is selected
		// By default, first tab will be selected, so the first panel added to the map will be visible
		for (final CrudBasicPanel eachPanel : panels) {
			final Tab tab = new Tab(eachPanel.getName());
			this.tabSheet.add(tab);
			this.tabsComponentMap.put(tab, eachPanel);
			if (this.tabsComponentMap.isEmpty()) {
				eachPanel.setVisible(true);
			} else {
				eachPanel.setVisible(false);
			}
			this.content.add(eachPanel);
		}

		// Do not show tab sheet if there is only 1 panel for this details layout
		if (panels.size() == 1) {
			this.tabSheet.setVisible(false);
		}

		// Create a selected listener to tabsheet to hide the previous tab content and show the selected tab's panel
		this.tabSheet.addSelectedChangeListener(event -> this.selectedTabChanged(event));

		// Do security permissions things...
		if (!this.securityService.isPermissionEnabled()) {
			return;
		}

		if (this.securityService.hasPermission(this.crudLayoutDef.getEntityClass())) {
			this.crudPermissionsPanel = this.createPermissionsPanel();
			final Tab tab = new Tab(this.crudPermissionsPanel.getName());
			this.tabSheet.add(tab);
			this.tabsComponentMap.put(tab, this.crudPermissionsPanel);
			this.crudPermissionsPanel.setVisible(false);
			this.content.add(this.crudPermissionsPanel);
		}
	}

	private void addPluggablePanel(final Class<? extends CrudBasicPanel> pluggablePanelClass, final List<CrudBasicPanel> panels) {
		final CrudBasicPanel pluggablePanel = this.applicationContext.getBean(pluggablePanelClass, this);
		pluggablePanel.setParentDetailsLayout(this);
		this.pluggablePanels.add(pluggablePanel);
		panels.add(pluggablePanel);
	}

	@Override
	public void updateFields(final Object savedItem) {
		super.updateFields(savedItem);
		this.updateElementsVisibility();
	}

	@Override
	public void setEnabled(final boolean enabled) {
		this.content.setEnabled(enabled);
		if (this.okButton != null) {
			this.okButton.setEnabled(enabled);
		}
	}

	protected CrudDetailsPanel createPermissionsPanel() {
		this.permissionsLayout.init(this.crudLayoutDef.getEntityClass());

		final CrudDetailsPanel permissionsPanel = this.componentBuilderFactory.createCrudDetailsPanelBuilder().build();
		permissionsPanel.setName(this.messageSource.getMessage("form.permissions"));
		permissionsPanel.addFormComponent(this.permissionsLayout, (String) null);
		return permissionsPanel;
	}

	public SecuredObject getSecuredObject() {
		return this.permissionsLayout.getSecuredObject(this.getTargetObject());
	}

}
