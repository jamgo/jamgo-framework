package org.jamgo.ui.component.builders;

import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.ui.layout.crud.CrudDetailsPanel.Format;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasTextBuilder;
import org.jamgo.vaadin.ui.builder.JamgoComponentBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.Component;

@org.springframework.stereotype.Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class CrudDetailsPanelBuilder extends JamgoComponentBuilder<CrudDetailsPanel, CrudDetailsPanelBuilder> implements
	HasSizeBuilder<CrudDetailsPanelBuilder, CrudDetailsPanel>,
	HasStyleBuilder<CrudDetailsPanelBuilder, CrudDetailsPanel>,
	HasTextBuilder<CrudDetailsPanelBuilder, CrudDetailsPanel> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = this.applicationContext.getBean("crudDetailsPanel", CrudDetailsPanel.class);
		this.instance.initialize(Format.OneColumn);
	}

	public CrudDetailsPanelBuilder setName(final String name) {
		this.getComponent().setName(name);
		return this;
	}

	public CrudDetailsPanelBuilder setSecurityId(final String securityId) {
		this.getComponent().setSecurityId(securityId);
		return this;
	}

	public CrudDetailsPanelBuilder setVisible(final Component component, final boolean visible) {
		this.getComponent().setVisible(component, visible);
		return this;
	}

	public CrudDetailsPanelBuilder setCaptionsBold(final boolean captionsBold) {
		this.getComponent().setCaptionsBold(captionsBold);
		return this;
	}

}