package org.jamgo.ui.view;

import org.jamgo.ui.layout.ContentLayout;
//import org.jamgo.ui.layout.utils.JamgoComponentFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;

@Route(value = "", layout = MainRouterLayout.class)
public abstract class MainView extends HorizontalLayout implements InitializingBean {

	private static final long serialVersionUID = 1L;
	public static final String NAME = "";

	private Div contentArea;
	@Autowired
	private ContentLayout contentLayout;
	
//	TODO
//	@Autowired
//	private SettingsService settingsService;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		this.contentArea = new Div();
		this.setSizeFull();

		this.contentArea.add(this.contentLayout);
		this.contentArea.setClassName("valo-content");
		this.contentArea.setSizeFull();

		this.add(this.contentArea);
		this.contentArea.setWidth(100, Unit.PERCENTAGE);
	}

}
