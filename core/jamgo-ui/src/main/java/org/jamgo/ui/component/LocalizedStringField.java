package org.jamgo.ui.component;

import java.util.Optional;

import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.LocalizedString;

import com.vaadin.flow.component.AbstractField;

public abstract class LocalizedStringField<FIELD_TYPE extends AbstractField<?, String>> extends LocalizedField<FIELD_TYPE, String, LocalizedString> {

	private static final long serialVersionUID = 1L;

	@Override
	protected void updateFieldValue(final Language language, final FIELD_TYPE field) {
		if (this.getValue() == null) {
			final LocalizedString newValue = new LocalizedString();
			newValue.set(language, field.getValue());
			this.setModelValue(newValue, false);
		} else {
			this.getValue().set(language, field.getValue());
		}
	}

	@Override
	protected String getFieldValue(final LocalizedString value, final Language language) {
		return Optional.ofNullable(value).map(o -> this.localizedObjectService.getValue(o, language)).orElse("");
	}

}
