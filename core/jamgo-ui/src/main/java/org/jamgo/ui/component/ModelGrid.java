package org.jamgo.ui.component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import org.jamgo.model.BasicModel;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.ui.layout.crud.CrudLayoutColumnDef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.function.ValueProvider;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ModelGrid<BASIC_MODEL extends BasicModel<?>> extends Grid<BASIC_MODEL> {

	@Autowired
	protected LocalizedMessageService messageSource;

	private static final long serialVersionUID = -8418406135307527518L;

	public void createColumns(final List<CrudLayoutColumnDef<?, ?>> columnDefs) {
		final List<GridSortOrder<BASIC_MODEL>> order = new ArrayList<>();
		final Map<String, Supplier<String>> columnHeaderLabelKeys = new HashMap<>();
		for (final CrudLayoutColumnDef<?, ?> eachColumnDef : columnDefs) {
			Column<BASIC_MODEL> column;
			@SuppressWarnings("unchecked")
			final CrudLayoutColumnDef<BASIC_MODEL, ?> columnDef = (CrudLayoutColumnDef<BASIC_MODEL, ?>) eachColumnDef;

			if (columnDef.getRenderer() != null) {
				column = this.addColumn(columnDef.getRenderer());
			} else {
				final ValueProvider<BASIC_MODEL, ?> valueProvider = columnDef.getValueProvider();
				column = this.addColumn(valueProvider);
			}

			if (columnDef.getCaptionSupplier() != null) {
				column.setHeader(columnDef.getCaptionSupplier().get());
			} else {
				column.setHeader(this.messageSource.getMessage(columnDef.getCaption()));
			}

			// FIXME Check else if...
			// - seems like sort properties are only applied if no id is defined for the columnDef
			if (columnDef.getId() != null) {
				column.setId(columnDef.getId());
				column.setKey(columnDef.getId());
			} else if ((columnDef.getSortProperties() != null) && (columnDef.getSortProperties().length > 0)) {
				column.setSortProperty(columnDef.getSortProperties());
			}

			if (columnDef.isOrderAsc()) {
				order.add(new GridSortOrder<>(column, SortDirection.ASCENDING));
			} else if (columnDef.isOrderDesc()) {
				order.add(new GridSortOrder<>(column, SortDirection.DESCENDING));
			}

			if (columnDef.getTooltipGenerator() != null) {
				column.setTooltipGenerator(columnDef.getTooltipGenerator());
			}

			// FIXME Try to understand this method logic, deprecate unused
			// - sort must be done in the data provider, not in the grid
//			if (columnDef.getLocalizedValueProvider() != null) {
//				column.setComparator((ls1, ls2) -> {
//					final String lang = this.messageSource.getLocale().getLanguage();
//					final Function<BASIC_MODEL, LocalizedString> localizedValueProvider = columnDef.getLocalizedValueProvider();
//					final LocalizedString c1 = localizedValueProvider.apply(ls1);
//					final LocalizedString c2 = localizedValueProvider.apply(ls2);
//					if ((c1 != null) && (c2 != null)) {
//						final String s1 = StringUtils.stripAccents(c1.get(lang)).toLowerCase();
//						final String s2 = StringUtils.stripAccents(c2.get(lang)).toLowerCase();
//						return s1.compareTo(s2);
//					} else if ((c1 == null) && (c2 != null)) {
//						return 1;
//					} else if ((c1 != null) && (c2 == null)) {
//						return -1;
//					} else {
//						return 0;
//					}
//				});
//			}

			// FIXME Try to understand this method logic, deprecate unused
			// - sort must be done in the data provider, not in the grid
			if (columnDef.getComparator() != null) {
				column.setComparator(columnDef.getComparator());
			}

			// Text alignment
			column.setTextAlign(columnDef.getTextAlign());
			// Column width automatic
			if (columnDef.getFlexGrow() != null) {
				column.setFlexGrow(columnDef.getFlexGrow());
			}

			// add multilang header captions info
			columnHeaderLabelKeys.put(columnDef.getId(), columnDef.getCaptionSupplier());
		}

		if (!order.isEmpty()) {
			this.sort(order);
			this.setMultiSort(order.size() > 1);
		}

	}

}
