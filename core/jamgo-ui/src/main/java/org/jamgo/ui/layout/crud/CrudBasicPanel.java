package org.jamgo.ui.layout.crud;

import org.jamgo.services.impl.LocalizedMessageService;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;

@SuppressWarnings("serial")
public abstract class CrudBasicPanel extends Div {

	@Autowired
	protected LocalizedMessageService messageSource;

	private String name;
	private String securityId;

	public CrudBasicPanel() {
		super();
	}

	public CrudBasicPanel(final Component... components) {
		super(components);
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getSecurityId() {
		return this.securityId;
	}

	public void setSecurityId(final String securityId) {
		this.securityId = securityId;
	}

	public void setVisible(final Component component, final boolean visible) {
		component.setVisible(visible);
	}

	public abstract void setParentDetailsLayout(final CrudDetailsLayout<?> parentDetailsLayout);

	public abstract boolean isVisible(boolean isCreation);

	public abstract void updateFields();

}