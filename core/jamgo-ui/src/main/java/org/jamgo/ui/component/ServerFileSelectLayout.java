package org.jamgo.ui.component;

import java.io.File;
import java.util.function.Consumer;

import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vaadin.filesystemdataprovider.FileSelect;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ServerFileSelectLayout extends VerticalLayout implements InitializingBean {

	private static final long serialVersionUID = 1L;

	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected LocalizedMessageService messageSource;

	private FileSelect fileSelectField;

	private String rootFilePath;
	private File selectedFile;

	private Button closeButton;
	private Consumer<ServerFileSelectLayout> closeHandler;

	public ServerFileSelectLayout() {
		super();
	}

	public ServerFileSelectLayout(final String rootFilePath) {
		super();
		this.rootFilePath = rootFilePath;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	public void init() {
		this.setSizeFull();

		this.addPanel();
	}

	private void addPanel() {
		final File root = new File(this.rootFilePath);
		this.fileSelectField = this.componentBuilderFactory.createFileSelectBuilder(root)
			.setWidth(50, Unit.EM)
			.setHeight(40, Unit.EM)
			.build();
		this.fileSelectField.addValueChangeListener(event -> {
			this.setSelectedFile(this.fileSelectField.getValue());
			this.doClose();
		});
		this.addAndExpand(this.fileSelectField);

		this.closeButton = this.componentBuilderFactory.createButtonBuilder()
			.setText(this.messageSource.getMessage("action.close"))
			.build();
		this.closeButton.addClickListener(event -> this.doClose());
		this.add(this.closeButton);
	}

	public File getSelectedFile() {
		return this.selectedFile;
	}

	public void setSelectedFile(final File selectedFile) {
		this.selectedFile = selectedFile;
	}

	private void doClose() {
		if (this.closeHandler != null) {
			this.closeHandler.accept(this);
		}
	}

	public Consumer<ServerFileSelectLayout> getCloseHandler() {
		return this.closeHandler;
	}

	public void setCloseHandler(final Consumer<ServerFileSelectLayout> closeHandler) {
		this.closeHandler = closeHandler;
	}

}
