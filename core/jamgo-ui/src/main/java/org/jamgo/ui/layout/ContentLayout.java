package org.jamgo.ui.layout;

import java.util.Optional;
import java.util.function.Supplier;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.BasicModelEntity;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.crud.CrudTableLayout;
import org.jamgo.ui.layout.crud.CrudTableLayoutConfig;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

@CssImport(value = "./styles/content-layout.css")
public class ContentLayout extends VerticalLayout implements InitializingBean {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
//	@Autowired
//	private SettingsService settingsService;
	@Autowired
	protected BackofficeApplicationDef backofficeApplicationDef;
//	@Autowired
//	protected MultiLanguageEngine multilangEngine;

	private CrudTableLayoutConfig tableLayoutConfig;

	private H3 viewTitle;
	private Span viewDescription;
	private Component currentView;
//	private final Map<BackofficeLayoutDef, Component> viewsMap = new HashMap<>();
//	private final Map<CrudLayoutDef<?, ?>, CrudTableLayout<?, ?>> tableLayoutsMap = new HashMap<>();
//	private final BiMap<Component, BackofficeLayoutDef> layoutDefsMap = HashBiMap.create();

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		this.addClassName("content-layout");
		this.setSizeFull();
		this.setPadding(false);

		this.viewTitle = this.componentBuilderFactory.createH3Builder()
			.setLabel("")
			.build();
		this.add(this.viewTitle);

		this.viewDescription = this.componentBuilderFactory.createSpanBuilder()
			.setLabel("")
			.setVisible(false)
			.addClassName("layout-description")
			.build();

		this.add(this.viewDescription);

		if (this.backofficeApplicationDef.getOpenOnStartLayoutDef() != null) {
			this.openView(this.backofficeApplicationDef.getOpenOnStartLayoutDef());
		}
	}

	public void openView(final BackofficeLayoutDef layoutDef) {
//		Component view = this.viewsMap.get(layoutDef);
//		if (view == null) {
//			view = this.addView(layoutDef);
//		}
//		this.viewsMap.values().forEach(each -> each.setVisible(false));
		this.viewTitle.setText((String) layoutDef.getNameSupplier().get());

		final String descriptionText = (String) Optional.ofNullable(layoutDef.getDescriptionSupplier())
			.map(Supplier::get)
			.orElse("");

		this.viewDescription.setText(descriptionText);
		this.viewDescription.setVisible(!descriptionText.isEmpty());

//		view.setVisible(true);

		if (this.currentView != null) {
			this.remove(this.currentView);
		}

		this.currentView = this.addView(layoutDef);
		this.add(this.currentView);
		this.currentView.setVisible(true);
	}

	@SuppressWarnings("unchecked")
	public void openView(final BackofficeLayoutDef layoutDef, final Long id) {
		this.openView(layoutDef);
		if (CrudTableLayout.class.isAssignableFrom(this.currentView.getClass())) {
			((CrudTableLayout<?, ?, Long>) this.currentView).openDetails(id);
		}
	}

//	public void activateCrudTab(final CrudLayoutDef<?, ?> layoutDef, final Object searchObject) {
//		this.activateTab(layoutDef);
//		this.getTableLayout(layoutDef).showSearchLayout(searchObject);
//	}

//	public Component getView(final BackofficeLayoutDef layoutDef) {
//		return this.viewsMap.get(layoutDef);
//	}

	private <T extends BasicModel<?>, E extends BasicModelEntity<?>> Component addView(final BackofficeLayoutDef layoutDef) {
		Component component = null;
		if (layoutDef instanceof CrudLayoutDef) {
			@SuppressWarnings("unchecked")
			final CrudLayoutDef<T, E> crudLayoutDef = (CrudLayoutDef<T, E>) layoutDef;
//			final String tableLayoutBeanName = StringUtils.uncapitalize(crudLayoutDef.getTableLayoutClass().getSimpleName());
			@SuppressWarnings("unchecked")
			final CrudTableLayout<T, E, ?> tableLayout = (CrudTableLayout<T, E, ?>) this.applicationContext.getBean(crudLayoutDef.getTableLayoutClass(), crudLayoutDef);
//			tableLayout.initialize(crudLayoutDef);
			component = tableLayout;
		} else if (layoutDef instanceof CustomLayoutDef) {
			final CustomLayoutDef customLayoutDef = (CustomLayoutDef) layoutDef;
			final CustomLayout customLayout = this.applicationContext.getBean(customLayoutDef.getLayoutClass(), layoutDef);
//			customLayout.initialize();
			component = customLayout;
		}

//		this.viewsMap.put(layoutDef, component);
		component.setVisible(false);
//		this.add(component);
		return component;
	}

	public CrudTableLayoutConfig getTableLayoutConfig() {
		return this.tableLayoutConfig;
	}

	public void setTableLayoutConfig(final CrudTableLayoutConfig tableLayoutConfig) {
		this.tableLayoutConfig = tableLayoutConfig;
	}

//	public CrudTableLayout<?, ?> getTableLayout(final CrudLayoutDef<?, ?> crudLayoutDef) {
//		return this.tableLayoutsMap.get(crudLayoutDef);
//	}
}
