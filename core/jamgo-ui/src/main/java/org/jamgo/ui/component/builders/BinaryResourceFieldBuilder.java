package org.jamgo.ui.component.builders;

import org.jamgo.ui.component.BinaryResourceField;
import org.jamgo.vaadin.ui.builder.JamgoComponentBuilder;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class BinaryResourceFieldBuilder extends JamgoComponentBuilder<BinaryResourceField, BinaryResourceFieldBuilder> implements
	InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = this.applicationContext.getBean(BinaryResourceField.class);
	}

	@Override
	public BinaryResourceField build() {
		super.build();

		return this.instance;
	}

}