package org.jamgo.ui.layout.utils.ie;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.jamgo.model.entity.Model;
import org.xml.sax.SAXException;

/**
 * 
 * @author jamgo
 *
 * @deprecated Use {@link org.jamgo.services.ie.SaxImporter} from jamgo-services module instead
 * 
 * @param <T>
 */
@Deprecated
public abstract class SaxImporter<T extends Model> extends AbstractImporter<T> {

	@Override
	protected void doExtraction(InputStream inputStream) throws IOException, ParserConfigurationException, SAXException {
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        SAXParser saxParser = saxParserFactory.newSAXParser();
        SaxHandler<T> saxHandler = this.getSaxHandler();
        saxParser.parse(inputStream, saxHandler);
       	this.setList(saxHandler.getList());
	}
	
	protected abstract SaxHandler<T> getSaxHandler ();

}
