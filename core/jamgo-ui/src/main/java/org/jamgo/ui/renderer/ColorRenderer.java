package org.jamgo.ui.renderer;

import org.jamgo.model.BasicModel;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.ValueProvider;

public class ColorRenderer<T extends BasicModel<?>> extends ComponentRenderer<Div, T> {

	private static final long serialVersionUID = 1L;

	public ColorRenderer(final ValueProvider<T, String> valueProvider) {
		super(o -> {
			final Span span = new Span();
			span.getStyle().set("background-color", valueProvider.apply(o));
			span.getStyle().set("padding-left", "1em");
			span.getStyle().set("margin-right", "1em");

			final Div div = new Div();
			div.setClassName("rendered-color");
			div.setWidth(100.0f, Unit.PERCENTAGE);
			div.add(span);
			return div;
		});
	}
	
}
