package org.jamgo.ui.layout.details;

import org.jamgo.model.entity.BinaryResourceImage;
import org.jamgo.ui.component.JamgoFormLayout;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BinaryResourceImageDetailsLayout extends CrudDetailsLayout<BinaryResourceImage> {

	private static final long serialVersionUID = 1L;

	private CrudDetailsPanel mainPanel;
	private JamgoFormLayout mainFormLayout;

	private TextField nameField;
	private TextField sizeField;
	private TextField mimeTypeField;
	private IntegerField fileLengthField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		this.mainPanel = this.componentBuilderFactory.createCrudDetailsPanelBuilder()
			.setName(this.messageSource.getMessage("form.basic.info"))
			.build();

		this.mainFormLayout = (JamgoFormLayout) this.mainPanel.getFormLayout();

		this.addFileNameField(this.mainFormLayout, "binaryresourceimage.name", ComponentColspan.SIX_COLUMNS);
		this.addSizeField(this.mainFormLayout, "binaryresourceimage.size", ComponentColspan.SIX_COLUMNS);
		this.addMimeTypeField(this.mainFormLayout, "binaryresourceimage.mimeType", ComponentColspan.SIX_COLUMNS);
		this.addFileLengthField(this.mainFormLayout, "binaryresourceimage.fileLength", ComponentColspan.SIX_COLUMNS);

		return this.mainPanel;
	}

	private void addFileNameField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.nameField = this.componentBuilderFactory.createTextFieldBuilder().build();
		formLayout.addFormComponent(this.nameField, fieldKey, colspan);
		this.binder.forField(this.nameField)
			.bind(o -> o.getName(), null);
	}

	private void addSizeField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.sizeField = this.componentBuilderFactory.createTextFieldBuilder().build();
		formLayout.addFormComponent(this.sizeField, fieldKey, colspan);
		this.binder.forField(this.sizeField)
			.bind(o -> o.getSize(), null);
	}

	private void addFileLengthField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.fileLengthField = this.componentBuilderFactory.createIntegerFieldBuilder().build();
		formLayout.addFormComponent(this.fileLengthField, fieldKey, colspan);
		this.binder.forField(this.fileLengthField)
			.bind(o -> o.getFileLength(), null);
	}

	private void addMimeTypeField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.mimeTypeField = this.componentBuilderFactory.createTextFieldBuilder().build();
		formLayout.addFormComponent(this.mimeTypeField, fieldKey, colspan);
		this.binder.forField(this.mimeTypeField)
			.bind(o -> o.getMimeType(), null);
	}

	@Override
	protected Class<BinaryResourceImage> getTargetObjectClass() {
		return BinaryResourceImage.class;
	}

}
