package org.jamgo.ui.layout.crud;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.ui.renderer.BooleanRenderer;

import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.function.SerializableComparator;
import com.vaadin.flow.function.SerializableFunction;
import com.vaadin.flow.function.ValueProvider;

public class CrudLayoutColumnDef<BASIC_MODEL extends BasicModel<?>, COLUMN_TYPE> {

	public enum Order {
		ASC, DESC
	}

	private String id;
	private ValueProvider<BASIC_MODEL, COLUMN_TYPE> valueProvider;
	private Supplier<String> captionSupplier;

	@Deprecated
	/**
	 * Return type for the column.
	 *
	 * @deprecated This attribute was used only to render a boolean column in
	 *             CRUD tables. Now you can use renderer attribute to specify a
	 *             renderer for the column.
	 */
	private Class<?> returnType;

	/**
	 * Renders the column using this renderer. If not defined, a default text
	 * rendere
	 */
	private Renderer<BASIC_MODEL> renderer;

	private Order order;
	private String[] sortProperties;
	private SerializableComparator<BASIC_MODEL> comparator;
	private Function<BASIC_MODEL, LocalizedString> localizedValueProvider;
	private ColumnTextAlign textAlign;
	private SerializableFunction<BASIC_MODEL, String> tooltipGenerator;

	/**
	 * Increases column width according to flex growth value. Set a value > 1 to
	 * increase the column width. If not set, vaadin uses a default value 1.
	 */
	private Integer flexGrow;

	public static Builder<?, ?> builder() {
		return new CrudLayoutColumnDef.Builder<>();
	}

	public static class Builder<BT extends BasicModel<?>, BS> {

		private final CrudLayoutColumnDef<BT, BS> instance = new CrudLayoutColumnDef<>();
		private ModelGridDef.Builder<BT, ?> parentBuilder;
		private Consumer<CrudLayoutColumnDef<BT, BS>> callback;

		public Builder() {
		}

		public Builder(final ModelGridDef.Builder<BT, ?> parentBuilder, final Consumer<CrudLayoutColumnDef<BT, BS>> callback) {
			this.parentBuilder = parentBuilder;
			this.callback = callback;
		}

		public Builder<BT, BS> setId(final String id) {
			this.instance.id = id;
			return this;
		}

		public Builder<BT, BS> setValueProvider(final ValueProvider<BT, BS> valueProvider) {
			this.instance.valueProvider = valueProvider;
			return this;
		}

		public Builder<BT, BS> setCaptionSupplier(final Supplier<String> captionSupplier) {
			this.instance.captionSupplier = captionSupplier;
			return this;
		}

		/*
		 * @Deprecated
		 *
		 * ...	Use setCaptionSupplier.
		 */
		@Deprecated
		public Builder<BT, BS> setCaption(final String caption) {
			this.instance.captionSupplier = () -> caption;
			return this;
		}

		/*
		 * @Deprecated
		 *
		 * ...	Use setRenderer.
		 */
		@SuppressWarnings("unchecked")
		@Deprecated
		public Builder<BT, BS> setReturnType(final Class<?> returnType) {
			if (returnType.isAssignableFrom(Boolean.class)) {
				this.instance.renderer = new BooleanRenderer<>((ValueProvider<BT, Boolean>) this.instance.getValueProvider());
			}
			this.instance.returnType = returnType;
			return this;
		}

		public Builder<BT, BS> setRenderer(final Renderer<BT> renderer) {
			this.instance.renderer = renderer;
			return this;
		}

		public Builder<BT, BS> setOrderAsc() {
			this.instance.order = Order.ASC;
			return this;
		}

		public Builder<BT, BS> setOrderDesc() {
			this.instance.order = Order.DESC;
			return this;
		}

		public Builder<BT, BS> setSortProperties(final String[] sortProperties) {
			this.instance.sortProperties = sortProperties;
			return this;
		}

		public Builder<BT, BS> setComparator(final SerializableComparator<BT> comparator) {
			this.instance.comparator = comparator;
			return this;
		}

		public Builder<BT, BS> setLocalizedValueProvider(final Function<BT, LocalizedString> localizedValueProvider) {
			this.instance.localizedValueProvider = localizedValueProvider;
			return this;
		}

		public Builder<BT, BS> setTooltipGenerator(final SerializableFunction<BT, String> tooltipGenerator) {
			this.instance.tooltipGenerator = tooltipGenerator;
			return this;
		}

		public Builder<BT, BS> setTextAlign(final ColumnTextAlign textAlign) {
			this.instance.textAlign = textAlign;
			return this;
		}

		public Builder<BT, BS> setFlexGrow(final int flexGrow) {
			this.instance.flexGrow = flexGrow;
			return this;
		}

		public CrudLayoutColumnDef<BT, BS> build() {
			return this.instance;
		}

		public ModelGridDef.Builder<BT, ?> end() {
			this.callback.accept(this.instance);
			return this.parentBuilder;
		}
	}

	public String getId() {
		return this.id;
	}

	public ValueProvider<BASIC_MODEL, ?> getValueProvider() {
		return this.valueProvider;
	}

	public String getCaption() {
		return Optional.ofNullable(this.captionSupplier).map(f -> f.get().toString()).orElse(null);
	}

	public Supplier<String> getCaptionSupplier() {
		return this.captionSupplier;
	}

	@Deprecated
	public Class<?> getReturnType() {
		return this.returnType;
	}

	public Renderer<BASIC_MODEL> getRenderer() {
		return this.renderer;
	}

	public Order getOrder() {
		return this.order;
	}

	public boolean isOrderAsc() {
		return Order.ASC == this.order;
	}

	public boolean isOrderDesc() {
		return Order.DESC == this.order;
	}

	public String[] getSortProperties() {
		return this.sortProperties;
	}

	public SerializableComparator<BASIC_MODEL> getComparator() {
		return this.comparator;
	}

	public Function<BASIC_MODEL, LocalizedString> getLocalizedValueProvider() {
		return this.localizedValueProvider;
	}

	public SerializableFunction<BASIC_MODEL, String> getTooltipGenerator() {
		return this.tooltipGenerator;
	}

	public ColumnTextAlign getTextAlign() {
		return this.textAlign;
	}

	public Integer getFlexGrow() {
		return this.flexGrow;
	}

}
