package org.jamgo.ui.component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.io.IOUtils;
import org.jamgo.model.entity.BinaryResource;
import org.jamgo.services.impl.BinaryResourceService;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.server.StreamResource;

@org.springframework.stereotype.Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
@CssImport(value = "./styles/binary-resource-field.css")
@CssImport(value = "./styles/binary-resource-field-upload.css", themeFor = "vaadin-upload")
public class BinaryResourceField extends CustomField<BinaryResource> implements
	InitializingBean {

	private static final String EMPTY_IMAGE = "images/image-placeholder.png";

	private MemoryBuffer uploadBuffer;
	private Upload upload;
	private Image uploadedImage;
	private Button clearImageButton;

	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	private BinaryResourceService binaryResourceService;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		final Div imageDiv = new Div();

		this.uploadBuffer = new MemoryBuffer();
		this.upload = new Upload(this.uploadBuffer);
		this.upload.getElement().getThemeList().add("binary-resource-upload");
		this.upload.addClassName("binary-resource-upload");
		this.upload.setMaxFiles(Integer.MAX_VALUE);

		this.uploadedImage = new Image();
		this.uploadedImage.addClassName("binary-resource-image");
		this.uploadedImage.setWidth(200, Unit.PIXELS);
		this.uploadedImage.setHeight(200, Unit.PIXELS);
		this.uploadedImage.setSrc(BinaryResourceField.EMPTY_IMAGE);

		this.clearImageButton = this.componentBuilderFactory.createButtonBuilder()
			.build();
		this.clearImageButton.setIcon(VaadinIcon.CLOSE_CIRCLE_O.create());
		this.clearImageButton.addClassName("binary-resource-clear-button");
		this.clearImageButton.addClickListener(event -> this.clearImage());
		this.clearImageButton.setVisible(false);

		final Div overlay = new Div();
		overlay.addClassName("binary-resource-image-overlay");

		imageDiv.add(this.uploadedImage, overlay);

		this.upload.setUploadButton(imageDiv);
		this.upload.setDropLabel(null);
		this.upload.setDropLabelIcon(null);

		this.upload.addSucceededListener(event -> {
			this.updateImage(event.getMIMEType(), event.getFileName(), this.uploadBuffer.getInputStream());
		});
		this.upload.getElement().addEventListener("file-remove", event -> {
			this.setValue(null);
		});

		this.add(this.upload, this.clearImageButton);
	}

	private void clearImage() {
		this.setValue(null);
		this.uploadedImage.setSrc(BinaryResourceField.EMPTY_IMAGE);
		this.clearImageButton.setVisible(false);
	}

	private void updateImage(final String mimeType, final String fileName, final InputStream stream) {
		if (mimeType.startsWith("image")) {
			try {
				final byte[] bytes = IOUtils.toByteArray(stream);
				this.setValue(this.binaryResourceService.createBinaryResource(bytes, mimeType, fileName));
				this.uploadedImage.getElement().setAttribute("src", new StreamResource(fileName, () -> new ByteArrayInputStream(bytes)));
				try (ImageInputStream in = ImageIO.createImageInputStream(new ByteArrayInputStream(bytes))) {
					final Iterator<ImageReader> readers = ImageIO.getImageReaders(in);
					if (readers.hasNext()) {
						final ImageReader reader = readers.next();
						try {
							reader.setInput(in);
						} finally {
							reader.dispose();
						}
					}
				}
			} catch (final IOException e) {
				e.printStackTrace();
			}
			this.clearImageButton.setVisible(true);
		}
	}

	@Override
	protected BinaryResource generateModelValue() {
		return this.getValue();
	}

	@Override
	protected void setPresentationValue(final BinaryResource binaryResource) {
		this.setValue(binaryResource);
		if (binaryResource != null) {
			try {
				final byte[] bytes = IOUtils.toByteArray(binaryResource.getContents().getBinaryStream());
				this.uploadedImage.getElement().setAttribute("src", new StreamResource(binaryResource.getFileName(), () -> new ByteArrayInputStream(bytes)));
			} catch (IOException | SQLException e) {
				e.printStackTrace();
			}
			this.clearImageButton.setVisible(true);
		} else {
			this.uploadedImage.setSrc(BinaryResourceField.EMPTY_IMAGE);
			this.clearImageButton.setVisible(false);
		}
	}

}
