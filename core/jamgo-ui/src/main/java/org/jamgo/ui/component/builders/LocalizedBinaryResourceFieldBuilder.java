package org.jamgo.ui.component.builders;

import org.jamgo.ui.component.LocalizedBinaryResourceField;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class LocalizedBinaryResourceFieldBuilder extends LocalizedFieldBuilder<LocalizedBinaryResourceField> {

	@Override
	public void afterPropertiesSet() throws Exception {
		super.afterPropertiesSet();
		this.instance = this.applicationContext.getBean(LocalizedBinaryResourceField.class);
	}

}
