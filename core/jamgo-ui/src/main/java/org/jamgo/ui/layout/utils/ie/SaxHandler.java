package org.jamgo.ui.layout.utils.ie;

import java.util.List;

import org.jamgo.model.entity.Model;
import org.xml.sax.helpers.DefaultHandler;

/**
 * 
 * @author jamgo
 *
 * @deprecated Use {@link org.jamgo.services.ie.SaxHandler} from jamgo-services module instead
 * @see org.jamgo.services.ie.SaxHandler
 * 
 * @param <T>
 */
@Deprecated
public abstract class SaxHandler<T extends Model> extends DefaultHandler {

	protected abstract List<T> getList();

}
