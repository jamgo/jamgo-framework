package org.jamgo.ui.layout.details;

import java.io.ByteArrayInputStream;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.BinaryResourceImage;
import org.jamgo.services.impl.BinaryResourceImageService;
import org.jamgo.services.impl.DownloadService;
import org.jamgo.ui.component.JamgoFormLayout;
import org.jamgo.ui.component.ModelOneToMany;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.server.StreamResource;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BinaryResourceDetailsLayout extends CrudDetailsLayout<BinaryResource> {

	private static final long serialVersionUID = 1L;

	private final DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

	@Autowired
	private DownloadService downloadService;

	private CrudDetailsPanel mainPanel;

	private JamgoFormLayout mainFormLayout;

	private TextField idField;
	private Image imagePreviewField;
	private TextField nameField;
	private TextField descriptionField;
	private IntegerField fileLengthField;
	private TextField mimeTypeField;
	private TextField timeStampField;
	private ModelOneToMany<BinaryResourceImage, BinaryResource> imagesField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		this.mainPanel = this.componentBuilderFactory.createCrudDetailsPanelBuilder()
			.setName(this.messageSource.getMessage("form.basic.info"))
			.build();

		this.mainFormLayout = (JamgoFormLayout) this.mainPanel.getFormLayout();

		// TODO: Add upload field for new BinaryResources (this will require re-enable ADD button in BinaryResourceLayoutDef class)
		this.addImagePreviewField(this.mainFormLayout, null, ComponentColspan.FULL_WIDTH);
		this.addIdField(this.mainFormLayout, "generic.entity.id", ComponentColspan.TWO_COLUMNS);
		this.addFileNameField(this.mainFormLayout, "binaryresource.fileName", ComponentColspan.FOUR_COLUMNS);
		this.addDescriptionField(this.mainFormLayout, "binaryresource.description", ComponentColspan.SIX_COLUMNS);
		this.addFileLengthField(this.mainFormLayout, "binaryresource.fileLength", ComponentColspan.SIX_COLUMNS);
		this.addMimeTypeField(this.mainFormLayout, "binaryresource.mimeType", ComponentColspan.SIX_COLUMNS);
		this.addTimeStampField(this.mainFormLayout, "binaryresource.timeStamp", ComponentColspan.SIX_COLUMNS);
		this.addImagesField(this.mainFormLayout, "binaryresource.images", ComponentColspan.FULL_WIDTH);

		return this.mainPanel;
	}

	private void addImagePreviewField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		final HorizontalLayout layout = this.componentBuilderFactory.createHorizontalLayoutBuilder().setWidthFull().build();
		this.imagePreviewField = this.componentBuilderFactory.createImageBuilder().setAlt("image preview").build();
		this.imagePreviewField.setHeight(400.0f, Unit.PIXELS);
		this.imagePreviewField.setWidth(null);
		layout.add(this.imagePreviewField);
		layout.setJustifyContentMode(JustifyContentMode.CENTER);
		formLayout.addFormComponent(layout, fieldKey, colspan);
		// Field not binded, updated in updateFields() method
	}

	private void addIdField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.idField = this.componentBuilderFactory.createTextFieldBuilder().build();
		this.idField.addThemeVariants(TextFieldVariant.LUMO_ALIGN_RIGHT);
		formLayout.addFormComponent(this.idField, fieldKey, colspan);
		this.binder.forField(this.idField)
			.bind(o -> o.getId().toString(), null);
	}

	private void addFileNameField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.nameField = this.componentBuilderFactory.createTextFieldBuilder().build();
		formLayout.addFormComponent(this.nameField, fieldKey, colspan);
		this.binder.forField(this.nameField)
			.bind(o -> o.getFileName(), null);
	}

	private void addDescriptionField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.descriptionField = this.componentBuilderFactory.createTextFieldBuilder().build();
		formLayout.addFormComponent(this.descriptionField, fieldKey, colspan);
		this.binder.forField(this.descriptionField)
			.bind(o -> o.getDescription(), (o, v) -> o.setDescription(v));
	}

	private void addFileLengthField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.fileLengthField = this.componentBuilderFactory.createIntegerFieldBuilder().build();
		formLayout.addFormComponent(this.fileLengthField, fieldKey, colspan);
		this.binder.forField(this.fileLengthField)
			.bind(o -> o.getFileLength(), null);
	}

	private void addMimeTypeField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.mimeTypeField = this.componentBuilderFactory.createTextFieldBuilder().build();
		formLayout.addFormComponent(this.mimeTypeField, fieldKey, colspan);
		this.binder.forField(this.mimeTypeField)
			.bind(o -> o.getMimeType(), null);
	}

	private void addTimeStampField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.timeStampField = this.componentBuilderFactory.createTextFieldBuilder().build();
		formLayout.addFormComponent(this.timeStampField, fieldKey, colspan);
		this.binder.forField(this.timeStampField)
			.bind(o -> Optional.ofNullable(o.getTimeStamp()).map(dateTime -> dateTime.format(this.format)).orElse(null), null);
	}

	private void addImagesField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.imagesField = this.componentBuilderFactory.<BinaryResourceImage, BinaryResource> createModelOneToManyBuilder(BinaryResourceImage.class).build();
		this.imagesField.setCreateVisible(false);
		this.imagesField.setEditVisible(false);
		this.imagesField.setParentSetter((o, v) -> o.setSource(this.getTargetObject()));
		this.imagesField.setAddToParent((o, v) -> this.targetObject.addImage(o));
		formLayout.addFormComponent(this.imagesField, fieldKey, colspan);
		this.binder.forField(this.imagesField)
			.bind(o -> o.getImages(), (o, v) -> o.setImages(v));
	}

	@Override
	protected Class<BinaryResource> getTargetObjectClass() {
		return BinaryResource.class;
	}

	@Override
	public void updateFields(final Object savedItem) {
		final Long previousId = Optional.ofNullable(this.targetObject).map(o -> o.getId()).orElse(null);
		super.updateFields(savedItem);

		// Update image field if new object is charged
		if (this.targetObject != null && this.targetObject.getId() != null && !this.targetObject.getId().equals(previousId)) {
			StreamResource imageResource = null;
			if (this.downloadService.isImage(this.targetObject)) {
				final byte[] bytes = this.targetObject.getByteContents();
				imageResource = new StreamResource(this.targetObject.getFileName(), () -> new ByteArrayInputStream(bytes));
			} else if (this.downloadService.isPdf(this.targetObject)) {
				final BinaryResourceImage thumbnail = this.targetObject.getImages().stream().filter(image -> image.getName().equals(BinaryResourceImageService.PDF_IMAGE_NAME)).findFirst().orElse(null);
				if (thumbnail != null) {
					imageResource = new StreamResource(this.getTargetObject().getFileName(), () -> new ByteArrayInputStream(thumbnail.getByteContents()));
				}
			}
			if (imageResource == null) {
				imageResource = new StreamResource(this.targetObject.getFileName(), () -> Thread.currentThread().getContextClassLoader().getResourceAsStream("images/image-not-available.png"));
			}
			this.imagePreviewField.setSrc(imageResource);
		}
	}
}
