package org.jamgo.ui.layout.details;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.jamgo.services.impl.SystemInfoService;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.jamgo.ui.layout.CustomLayout;
import org.jamgo.ui.layout.CustomLayoutDef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.internal.Pair;
import com.vaadin.flow.spring.annotation.SpringComponent;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SystemInfoLayout extends CustomLayout {

	private static final long serialVersionUID = 1L;

	@Autowired
	private JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	private SystemInfoService systemInfoService;

	private VerticalLayout mainLayout;

	public SystemInfoLayout(final CustomLayoutDef customLayoutDef) {
		super(customLayoutDef);
	}

	@Override
	public void init(final CustomLayoutDef customLayoutDef) {
		this.setSizeFull();

		this.mainLayout = this.componentBuilderFactory.createVerticalLayoutBuilder()
			.build();
		this.mainLayout.setSizeFull();

		this.systemInfoService.getSystemPropertiesTable().rowMap().forEach((group, groupProperties) -> {
			final H3 groupHeader = this.componentBuilderFactory.createH3Builder()
				.setText(group)
				.build();
			this.mainLayout.add(groupHeader);
			this.mainLayout.add(this.createGroupGrid(groupProperties));
		});

//		final Span defaultZoneField = this.componentBuilderFactory.createSpanBuilder()
//			.build();
//		defaultZoneField.setText(ZoneId.systemDefault().toString());
//		this.mainLayout.addLocalizedColon(defaultZoneField, "system.defaultZone");
//
//		final Span defaultClockZoneField = this.componentBuilderFactory.createSpanBuilder()
//			.build();
//		defaultClockZoneField.setText(Clock.systemDefaultZone().toString());
//		this.mainLayout.addLocalizedColon(defaultClockZoneField, "system.defaultClockZone");
//
//		final Span localDateTimeNowField = this.componentBuilderFactory.createSpanBuilder()
//			.build();
//		localDateTimeNowField.setText(LocalDateTime.now().toString());
//		this.mainLayout.addLocalizedColon(localDateTimeNowField, "system.localDateTimeNow");

		this.add(this.mainLayout);
	}

	private Grid<Pair<String, String>> createGroupGrid(final Map<String, String> properties) {
		final Grid<Pair<String, String>> groupGrid = this.componentBuilderFactory.<Pair<String, String>> createGridBuilder()
			.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_ROW_STRIPES, GridVariant.LUMO_WRAP_CELL_CONTENT, GridVariant.LUMO_COLUMN_BORDERS)
			.setSelectionMode(SelectionMode.NONE)
			.build();
		groupGrid.setAllRowsVisible(true);
		final Column<Pair<String, String>> keyColumn = groupGrid.addColumn(Pair::getFirst);
		keyColumn.setFlexGrow(1);
		final Column<Pair<String, String>> valueColumn = groupGrid.addColumn(Pair::getSecond);
		valueColumn.setFlexGrow(2);
		final List<Pair<String, String>> groupProperties = properties.entrySet().stream()
			.sorted((a, b) -> a.getKey().compareTo(b.getKey()))
			.map(eachEntry -> new Pair<>(eachEntry.getKey(), eachEntry.getValue()))
			.collect(Collectors.toList());
		groupGrid.setItems(groupProperties);

		return groupGrid;
	}
}
