package org.jamgo.ui.layout;

import org.springframework.beans.factory.InitializingBean;

import com.vaadin.flow.component.html.Div;

public abstract class CustomLayout extends Div implements InitializingBean {

	private static final long serialVersionUID = 1L;

	protected CustomLayoutDef customLayoutDef;

	public CustomLayout(final CustomLayoutDef customLayoutDef) {
		this.customLayoutDef = customLayoutDef;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init(this.customLayoutDef);
	}

	public abstract void init(final CustomLayoutDef customLayoutDef);

}
