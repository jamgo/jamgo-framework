package org.jamgo.ui.component;

import java.util.Optional;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.LocalizedBinaryResource;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LocalizedBinaryResourceField extends LocalizedField<BinaryResourceField, BinaryResource, LocalizedBinaryResource> {

	private static final long serialVersionUID = 1L;

	@Override
	protected BinaryResourceField createBasicField() {
		return this.componentBuilderFactory.createBinaryResourceFieldBuilder().build();
	}

	@Override
	protected void updateFieldValue(final Language language, final BinaryResourceField field) {
		if (this.getValue() == null) {
			final LocalizedBinaryResource newValue = new LocalizedBinaryResource();
			newValue.set(language, field.getValue());
			this.setModelValue(newValue, false);
		} else {
			this.getValue().set(language, field.getValue());
		}
	}

	@Override
	protected BinaryResource getFieldValue(final LocalizedBinaryResource value, final Language language) {
		return Optional.ofNullable(value).map(o -> this.localizedObjectService.getValue(o, language)).orElse(null);
	}

}
