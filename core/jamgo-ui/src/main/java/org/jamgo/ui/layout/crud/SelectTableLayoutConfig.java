package org.jamgo.ui.layout.crud;

public abstract class SelectTableLayoutConfig {

	public abstract void initialize(final SelectTableLayout<?, ?, ?> crudTableLayout);

	public abstract void applyDefaultTo(SelectTableLayout<?, ?, ?> crudTableLayout);

}
