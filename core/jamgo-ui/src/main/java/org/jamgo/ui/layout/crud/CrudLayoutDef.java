package org.jamgo.ui.layout.crud;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.BasicModelEntity;
import org.jamgo.model.search.SearchSpecification;
import org.jamgo.ui.layout.BackofficeLayoutDef;

import com.vaadin.flow.function.SerializableFunction;

public class CrudLayoutDef<BASIC_MODEL extends BasicModel<?>, ENTITY extends BasicModelEntity<?>> extends BackofficeLayoutDef implements Cloneable {

	private SearchSpecification searchSpecification;
	private boolean addEnabled;
	private boolean removeEnabled;
	private boolean viewDetailsEnabled;
	private boolean searchEnabled;
	private boolean importEnabled;
	private boolean exportEnabled;
	private boolean maximized;
	private boolean countEnabled;
	private boolean emptyWithoutFilter;
	private boolean showSearchOnStart;
	private boolean searchLayoutCloseable = true; // default behaivour is to show close button
	private CrudTableLayoutConfig tableLayoutConfig;
	private Class<? extends CrudTableLayout<?, ?, ?>> tableLayoutClass;
	private Class<? extends CrudDetailsPanel> summaryLayoutClass;
	private Class<? extends CrudSearchLayout<? extends SearchSpecification>> searchLayoutClass;
//	private Class<? extends CrudImportLayout<ENTITY>> importLayoutClass;
//	private Class<? extends CrudExportLayout<BASIC_MODEL>> exportLayoutClass;
	private ModelGridDef<BASIC_MODEL, ENTITY> modelGridDef;
	private String exportFileNameKey;
	private Class<ENTITY> importBeanClass;

	@SuppressWarnings("unchecked")
	public CrudLayoutDef() {
		super();
		this.addEnabled = true;
		this.removeEnabled = true;
		this.viewDetailsEnabled = true;
		this.searchEnabled = true;
		this.importEnabled = true;
		this.countEnabled = true;
		this.maximized = false;
		this.emptyWithoutFilter = false;
		this.showSearchOnStart = false;
		this.tableLayoutClass = (Class<? extends CrudTableLayout<?, ?, ?>>) (Class<?>) DefaultCrudTableLayout.class;
		this.tableLayoutConfig = new CrudTableLayoutHorizontalConfig();
		this.modelGridDef = new ModelGridDef<>();
	}

	public static <BASIC_MODEL extends BasicModel<?>, ENTITY extends BasicModelEntity<?>> Builder<BASIC_MODEL, ENTITY> builder() {
		return new CrudLayoutDef.Builder<>();
	}

	public static class Builder<BASIC_MODEL extends BasicModel<?>, ENTITY extends BasicModelEntity<?>> {

		protected final CrudLayoutDef<BASIC_MODEL, ENTITY> instance = new CrudLayoutDef<>();

		public Builder<BASIC_MODEL, ENTITY> setId(final String id) {
			this.instance.id = id;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setNameSupplier(final Supplier<Object> nameSupplier) {
			this.instance.nameSupplier = nameSupplier;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setDescriptionSupplier(final Supplier<Object> descriptionSupplier) {
			this.instance.descriptionSupplier = descriptionSupplier;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setCountSupplier(final Supplier<Object> countSupplier) {
			this.instance.countSupplier = countSupplier;
			return this;
		}

		/*
		 * @Deprecated
		 *
		 * ...	Use setNameSupplier.
		 */
		@Deprecated
		public Builder<BASIC_MODEL, ENTITY> setName(final String name) {
			this.instance.nameSupplier = () -> name;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setImageName(final String imageName) {
			this.instance.imageName = imageName;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setDescription(final String description) {
			this.instance.descriptionSupplier = () -> description;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setCountText(final String countText) {
			this.instance.countSupplier = () -> countText;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setSearchSpecification(final SearchSpecification searchSpecification) {
			this.instance.searchSpecification = searchSpecification;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setOpenOnStart(final boolean openOnStart) {
			this.instance.openOnStart = openOnStart;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> tableLayoutHorizontalConfig() {
			this.instance.tableLayoutConfig = new CrudTableLayoutHorizontalConfig();
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> tableLayoutVerticalConfig() {
			this.instance.tableLayoutConfig = new CrudTableLayoutVerticalConfig();
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> tableLayoutConfig(final CrudTableLayoutConfig tableLayoutConfig) {
			this.instance.tableLayoutConfig = tableLayoutConfig;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setAddEnabled(final boolean flag) {
			this.instance.addEnabled = flag;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setRemoveEnabled(final boolean flag) {
			this.instance.removeEnabled = flag;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setViewDetailsEnabled(final boolean flag) {
			this.instance.viewDetailsEnabled = flag;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setSearchEnabled(final boolean flag) {
			this.instance.searchEnabled = flag;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setImportEnabled(final boolean flag) {
			this.instance.importEnabled = flag;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setCountEnabled(final boolean flag) {
			this.instance.countEnabled = flag;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setExportEnabled(final boolean flag) {
			this.instance.exportEnabled = flag;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setMaximized(final boolean flag) {
			this.instance.maximized = flag;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setEmptyWithoutFilter(final boolean flag) {
			this.instance.emptyWithoutFilter = flag;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setShowSearchOnStart(final boolean flag) {
			this.instance.showSearchOnStart = flag;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> setSearchLayoutCloseable(final boolean closeable) {
			this.instance.searchLayoutCloseable = closeable;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> tableLayoutClass(final Class<? extends CrudTableLayout<?, ?, ?>> tableLayoutClass) {
			this.instance.tableLayoutClass = tableLayoutClass;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> summaryLayoutClass(final Class<? extends CrudDetailsPanel> summaryLayoutClass) {
			this.instance.summaryLayoutClass = summaryLayoutClass;
			return this;
		}

		public Builder<BASIC_MODEL, ENTITY> searchLayoutClass(final Class<? extends CrudSearchLayout<? extends SearchSpecification>> searchLayoutClass) {
			this.instance.searchLayoutClass = searchLayoutClass;
			return this;
		}

//		public <S> Builder<BASIC_MODEL, ENTITY> importLayoutClass(final Class<? extends CrudImportLayout<ENTITY>> importLayoutClass) {
//			this.instance.importLayoutClass = importLayoutClass;
//			return this;
//		}
//
//		public <S> Builder<BASIC_MODEL, ENTITY> exportLayoutClass(final Class<? extends CrudExportLayout<BASIC_MODEL>> exportLayoutClass) {
//			this.instance.exportLayoutClass = exportLayoutClass;
//			return this;
//		}

		public <S> Builder<BASIC_MODEL, ENTITY> exportFileNameKey(final String exportFileNameKey) {
			this.instance.exportFileNameKey = exportFileNameKey;
			return this;
		}

		public <S> Builder<BASIC_MODEL, ENTITY> importBeanClass(final Class<ENTITY> importBeanClass) {
			this.instance.importBeanClass = importBeanClass;
			return this;
		}

		public <S> ModelGridDef.Builder<BASIC_MODEL, ENTITY> modelGridDef() {
			final Consumer<ModelGridDef<BASIC_MODEL, ENTITY>> f = obj -> this.instance.modelGridDef = obj;
			return new ModelGridDef.Builder<>(this, f);
		}

		public CrudLayoutDef<BASIC_MODEL, ENTITY> build() {
			return this.instance;
		}

	}

	public static <ENTITY extends BasicModelEntity<?>> BuilderEntity<ENTITY> builderEntity() {
		return new CrudLayoutDef.BuilderEntity<>();
	}

	public static class BuilderEntity<ENTITY extends BasicModelEntity<?>> extends Builder<ENTITY, ENTITY> {

		@Override
		public <S> ModelGridDef.Builder<ENTITY, ENTITY> modelGridDef() {
			final Consumer<ModelGridDef<ENTITY, ENTITY>> f = obj -> this.instance.modelGridDef = obj;
			return new ModelGridDef.BuilderEntity<>(this, f);
		}

	}

	public CrudTableLayoutConfig getTableLayoutConfig() {
		return this.tableLayoutConfig;
	}

	public boolean isAddEnabled() {
		return this.addEnabled;
	}

	public boolean isRemoveEnabled() {
		return this.removeEnabled;
	}

	public boolean isViewDetailsEnabled() {
		return this.viewDetailsEnabled;
	}

	public boolean isSearchEnabled() {
		return this.searchEnabled;
	}

	public boolean isImportEnabled() {
		return this.importEnabled;
	}

	public boolean isExportEnabled() {
		return this.exportEnabled;
	}

	public boolean isCountEnabled() {
		return this.countEnabled;
	}

	public boolean isMaximized() {
		return this.maximized;
	}

	public boolean isEmptyWithoutFilter() {
		return this.emptyWithoutFilter;
	}

	public boolean isShowSearchOnStart() {
		return this.showSearchOnStart;
	}

	public boolean isSearchLayoutCloseable() {
		return this.searchLayoutCloseable;
	}

	public Class<? extends CrudTableLayout<?, ?, ?>> getTableLayoutClass() {
		return this.tableLayoutClass;
	}

	public Class<? extends CrudDetailsPanel> getSummaryLayoutClass() {
		return this.summaryLayoutClass;
	}

	public Class<? extends CrudSearchLayout<? extends SearchSpecification>> getSearchLayoutClass() {
		return this.searchLayoutClass;
	}

//	public Class<? extends CrudImportLayout<ENTITY>> getImportLayoutClass() {
//		return this.importLayoutClass;
//	}
//
//	public Class<? extends CrudExportLayout<BASIC_MODEL>> getExportLayoutClass() {
//		return this.exportLayoutClass;
//	}

	public Class<ENTITY> getImportBeanClass() {
		if (this.importBeanClass != null) {
			return this.importBeanClass;
		}
		return this.getEntityClass();
	}

	public String getExportFileNameKey() {
		return this.exportFileNameKey;
	}

	public SearchSpecification getSearchSpecification() {
		return this.searchSpecification;
	}

	public void setSearchSpecification(final SearchSpecification searchSpecification) {
		this.searchSpecification = searchSpecification;
	}

	// Delegate methods for modelGridDef

	public Class<BASIC_MODEL> getModelClass() {
		return this.modelGridDef.getModelClass();
	}

	public Class<ENTITY> getEntityClass() {
		return this.modelGridDef.getEntityClass();
	}

	public SerializableFunction<BASIC_MODEL, String> getGridClassNameGenerator() {
		return this.modelGridDef.getGridClassNameGenerator();
	}

	public Class<? extends CrudLayout<?>> getDetailsLayoutClass() {
		return this.modelGridDef.getDetailsLayoutClass();
	}

	public List<CrudLayoutColumnDef<?, ?>> getColumnDefs() {
		return this.modelGridDef.getColumnDefs();
	}

	public boolean isCountOnEntity() {
		return this.modelGridDef.isCountOnEntity();
	}

	public List<Class<? extends CrudBasicPanel>> getPluggablePanels() {
		return this.modelGridDef.getPluggablePanels();
	}

	@SuppressWarnings("unchecked")
	@Override
	public CrudLayoutDef<BASIC_MODEL, ENTITY> clone() {
		CrudLayoutDef<BASIC_MODEL, ENTITY> clon = null;
		try {
			clon = (CrudLayoutDef<BASIC_MODEL, ENTITY>) super.clone();
		} catch (final CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return clon;
	}

	public void setAddEnabled(final boolean addEnabled) {
		this.addEnabled = addEnabled;
	}

	public void setRemoveEnabled(final boolean removeEnabled) {
		this.removeEnabled = removeEnabled;
	}

	public void setViewDetailsEnabled(final boolean viewDetailsEnabled) {
		this.viewDetailsEnabled = viewDetailsEnabled;
	}

	public void setSearchEnabled(final boolean searchEnabled) {
		this.searchEnabled = searchEnabled;
	}

	public void setImportEnabled(final boolean importEnabled) {
		this.importEnabled = importEnabled;
	}

	public void setCountEnabled(final boolean addEnabled) {
		this.countEnabled = addEnabled;
	}

	public void setExportEnabled(final boolean exportEnabled) {
		this.exportEnabled = exportEnabled;
	}

	public void setEmptyWithoutFilter(final boolean emptyWithoutFilter) {
		this.emptyWithoutFilter = emptyWithoutFilter;
	}

	public void setShowSearchOnStart(final boolean showSearchOnStart) {
		this.showSearchOnStart = showSearchOnStart;
	}

}
