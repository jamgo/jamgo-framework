package org.jamgo.ui.component;

import java.util.function.Predicate;

import org.jamgo.model.entity.Model;
import org.jamgo.services.impl.CrudServices;
import org.jamgo.services.impl.LocalizedMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.icon.VaadinIcon;

@org.springframework.stereotype.Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ModelToManyDeleteAction<T extends Model, P extends Model> extends ModelToManyAction<T, P> {

	@Autowired
	protected CrudServices crudServices;
	@Autowired
	protected LocalizedMessageService messageSource;

	public ModelToManyDeleteAction(final ModelToMany<T, P> modelToMany) {
		super(modelToMany);
	}

	public ModelToManyDeleteAction(final ModelToMany<T, P> modelToMany, final Predicate<T> visiblePredicate) {
		super(modelToMany, visiblePredicate);
	}

	@Override
	protected Component getComponent(final T relatedObject) {
		final Button deleteButton = this.componentBuilderFactory.createButtonBuilder()
			.addClassName("to-many-row-action-button")
			.setIcon(VaadinIcon.TRASH.create())
			.build();
		deleteButton.addClickListener(event -> this.getDeleteMessageBox(relatedObject).open());
		return deleteButton;
	}

	private ConfirmDialog getDeleteMessageBox(final T targetObject) {
		final ConfirmDialog dialog = new ConfirmDialog();
		dialog.setHeader(this.messageSource.getMessage("dialog.delete.caption"));
		dialog.setText(this.messageSource.getMessage("dialog.delete.message"));
		dialog.setCancelable(true);
		dialog.setCancelText(this.messageSource.getMessage("dialog.no"));
		dialog.setConfirmText(this.messageSource.getMessage("dialog.yes"));
		dialog.addConfirmListener(event -> ModelToManyDeleteAction.this.doDelete(targetObject));

		return dialog;
	}

	protected void doDelete(final T targetObject) {
		this.modelToMany.doDelete(targetObject);
	}

}
