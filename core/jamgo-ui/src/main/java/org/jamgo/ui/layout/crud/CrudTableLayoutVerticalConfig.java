package org.jamgo.ui.layout.crud;

import java.util.Optional;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.splitlayout.SplitLayout;

/*
 * ---------------------------------------------------
 * | toolbar                                         |
 * ---------------------------------------------------
 * | search      | table             | details       |
 * |             |                   |               |
 * |             |                   |               |
 * |             |                   |               |
 * |             |                   |               |
 * |             |                   |               |
 * |             |                   |               |
 * |             |                   |               |
 * ---------------------------------------------------
 */

public class CrudTableLayoutVerticalConfig extends CrudTableLayoutConfig {

	@Override
	public void initialize(final CrudTableLayout<?, ?, ?> crudTableLayout) {
		if (crudTableLayout.getToolBar() != null) {
			crudTableLayout.add(crudTableLayout.getToolBar());
		}

		final SplitLayout tableDetailsLayout = new SplitLayout();
		tableDetailsLayout.setSizeFull();
		tableDetailsLayout.addToPrimary(crudTableLayout.getTableWrapper());
		Optional.ofNullable(crudTableLayout.getDetailsLayout())
			.ifPresent(o -> tableDetailsLayout.addToSecondary(o));

		if (crudTableLayout.getSearchLayout() != null) {
			final SplitLayout searchContentsLayout = new SplitLayout();
			searchContentsLayout.setSizeFull();
			searchContentsLayout.addToPrimary(crudTableLayout.getSearchLayout());
			searchContentsLayout.addToSecondary(tableDetailsLayout);
			crudTableLayout.addAndExpand(searchContentsLayout);
		} else {
			crudTableLayout.addAndExpand(tableDetailsLayout);
		}

		this.applyDefaultTo(crudTableLayout);
	}

	@Override
	public void applyDefaultTo(final CrudTableLayout<?, ?, ?> crudTableLayout) {
		if (crudTableLayout.getSearchLayout() != null) {
			if (crudTableLayout.isSearchVisible()) {
				crudTableLayout.getSearchLayout().setWidth(30f, Unit.PERCENTAGE);
				crudTableLayout.getSearchLayout().getParent().ifPresent(p -> ((SplitLayout) p).removeClassName("hide-splitter"));
				crudTableLayout.getTableWrapper().setWidth(70f, Unit.PERCENTAGE);
				Optional.ofNullable(crudTableLayout.getDetailsLayout())
					.ifPresent(o -> o.setWidth(0f, Unit.PERCENTAGE));
				Optional.ofNullable(crudTableLayout.getDetailsLayout())
					.map(o -> o.getParent())
					.ifPresent(o -> ((SplitLayout) o.get()).addClassName("hide-splitter"));
			} else if (crudTableLayout.isDetailsVisible()) {
				crudTableLayout.getSearchLayout().setWidth(0f, Unit.PERCENTAGE);
				crudTableLayout.getSearchLayout().getParent().ifPresent(p -> ((SplitLayout) p).addClassName("hide-splitter"));
				crudTableLayout.getTableWrapper().setWidth(20f, Unit.PERCENTAGE);
				Optional.ofNullable(crudTableLayout.getDetailsLayout())
					.ifPresent(o -> o.setWidth(80f, Unit.PERCENTAGE));
				Optional.ofNullable(crudTableLayout.getDetailsLayout())
					.map(o -> o.getParent())
					.ifPresent(o -> ((SplitLayout) o.get()).removeClassName("hide-splitter"));
			} else {
				crudTableLayout.getSearchLayout().setWidth(0f, Unit.PERCENTAGE);
				crudTableLayout.getSearchLayout().getParent().ifPresent(p -> ((SplitLayout) p).addClassName("hide-splitter"));
				crudTableLayout.getTableWrapper().setWidth(100f, Unit.PERCENTAGE);
				Optional.ofNullable(crudTableLayout.getDetailsLayout())
					.ifPresent(o -> o.setWidth(0f, Unit.PERCENTAGE));
				Optional.ofNullable(crudTableLayout.getDetailsLayout())
					.map(o -> o.getParent())
					.ifPresent(o -> ((SplitLayout) o.get()).addClassName("hide-splitter"));
			}
		} else {
			if (crudTableLayout.isDetailsVisible()) {
				crudTableLayout.getTableWrapper().setWidth(20f, Unit.PERCENTAGE);
				Optional.ofNullable(crudTableLayout.getDetailsLayout())
					.ifPresent(o -> o.setWidth(80f, Unit.PERCENTAGE));
				Optional.ofNullable(crudTableLayout.getDetailsLayout())
					.map(o -> o.getParent())
					.ifPresent(o -> ((SplitLayout) o.get()).removeClassName("hide-splitter"));
			} else {
				crudTableLayout.getTableWrapper().setWidth(100f, Unit.PERCENTAGE);
				Optional.ofNullable(crudTableLayout.getDetailsLayout())
					.ifPresent(o -> o.setWidth(0f, Unit.PERCENTAGE));
				Optional.ofNullable(crudTableLayout.getDetailsLayout())
					.map(o -> o.getParent())
					.ifPresent(o -> ((SplitLayout) o.get()).addClassName("hide-splitter"));
			}
		}
	}

	@Override
	public void applyMaximizedDetailsTo(final CrudTableLayout<?, ?, ?> crudTableLayout) {
		Optional.ofNullable(crudTableLayout.getDetailsLayout())
			.ifPresent(o -> o.setWidth(100f, Unit.PERCENTAGE));
		Optional.ofNullable(crudTableLayout.getSearchLayout())
			.map(o -> o.getParent())
			.ifPresent(o -> ((SplitLayout) o.get()).addClassName("hide-splitter"));
		Optional.ofNullable(crudTableLayout.getDetailsLayout())
			.map(o -> o.getParent())
			.ifPresent(o -> ((SplitLayout) o.get()).addClassName("hide-splitter"));
	}

}
