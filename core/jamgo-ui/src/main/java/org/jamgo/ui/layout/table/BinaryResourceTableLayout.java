package org.jamgo.ui.layout.table;

import java.util.List;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.services.impl.BinaryResourceService;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.crud.CrudTableLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BinaryResourceTableLayout extends CrudTableLayout<BinaryResource, BinaryResource, Long> {
	private static final long serialVersionUID = 1L;

	@Autowired
	private BinaryResourceService binaryResourceService;

	public BinaryResourceTableLayout(final CrudLayoutDef<BinaryResource, BinaryResource> crudLayoutDef) {
		super(crudLayoutDef);
	}

	@Override
	protected HorizontalLayout createToolBar() {
		final HorizontalLayout toolBar = super.createToolBar();
		this.addRecreateButton();
		this.addPurgeButton();
		// TODO: this.addSwitchViewButton();
		return toolBar;
	}

	/**
	 * Adds RECREATE button to generate remaining BinaryResourceImages
	 */
	private void addRecreateButton() {
		final Button recreateButton = this.componentBuilderFactory.createButtonBuilder()
			.setText(this.messageSource.getMessage("action.recreate"))
			.build();
		recreateButton.addClickListener(event -> this.attemptLostFocusDetails(this.detailsLayout, () -> this.doConfirmRecreate()));
		this.buttonsLayout.add(recreateButton);
	}

	/**
	 * Adds PURGE button to delete unused BinaryResource objects
	 */
	private void addPurgeButton() {
		final Button purgeButton = this.componentBuilderFactory.createButtonBuilder()
			.setText(this.messageSource.getMessage("action.purge"))
			.build();
		purgeButton.addClickListener(event -> this.attemptLostFocusDetails(this.detailsLayout, () -> this.doConfirmPurge()));
		this.buttonsLayout.add(purgeButton);
	}

	/**
	 * Adds LIST/MOSAIC button to switch view of BinaryResources
	 */
	private void addSwitchViewButton() {
		final Button switchViewButton = this.componentBuilderFactory.createButtonBuilder()
			.setText(this.messageSource.getMessage("action.switchView"))
			.build();
		switchViewButton.addClickListener(event -> this.attemptLostFocusDetails(this.detailsLayout, () -> this.doSwitchView()));
		this.buttonsLayout.add(switchViewButton);
	}

	private void doConfirmRecreate() {
		final ConfirmDialog dialog = this.getMessageBox("dialog.recreate.caption", "dialog.recreate.message", null, () -> this.doRecreate());
		dialog.open();
	}

	private void doRecreate() {
		// TODO: show progress bar and log
		this.binaryResourceService.recreateBinaryResourceImages();
		Notification.show(this.messageSource.getMessage("dialog.recreate.finished"));
		this.refreshAll();
	}

	private void doConfirmPurge() {
		final List<BinaryResource> candidates = this.binaryResourceService.getPurgeCandidates();
		if (candidates.size() == 0) {
			Notification.show(this.messageSource.getMessage("dialog.purge.empty"));
			return;
		}
		final ConfirmDialog dialog = this.getMessageBox("dialog.purge.caption", "dialog.purge.message", new String[] { Integer.toString(candidates.size()) }, () -> this.doPurge(candidates));
		dialog.open();
	}

	private void doPurge(final List<BinaryResource> candidates) {
		this.binaryResourceService.delete(candidates);
		Notification.show(this.messageSource.getMessage("dialog.purge.finished"));
		this.refreshAll();
	}

	private void doSwitchView() {
		// TODO Auto-generated method stub
	}

	protected ConfirmDialog getMessageBox(final String headerKey, final String textKey, final String[] args, final Runnable action) {
		final ConfirmDialog dialog = new ConfirmDialog();
		dialog.setHeader(this.messageSource.getMessage(headerKey));
		dialog.setText(String.format(this.messageSource.getMessage(textKey, args)));
		dialog.setCancelable(true);
		dialog.setCancelText(this.messageSource.getMessage("dialog.no"));
		dialog.setConfirmText(this.messageSource.getMessage("dialog.yes"));
		dialog.addConfirmListener(event -> action.run());
		return dialog;
	}

}
