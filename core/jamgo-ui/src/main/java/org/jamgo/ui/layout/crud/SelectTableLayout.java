package org.jamgo.ui.layout.crud;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.BasicModelEntity;
import org.jamgo.model.repository.RepositoryManager;
import org.jamgo.model.search.SearchSpecification;
import org.jamgo.services.exception.CrudException;
import org.jamgo.services.impl.CrudServices;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.ui.component.ModelGrid;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.GridMultiSelectionModel;
import com.vaadin.flow.component.grid.GridMultiSelectionModel.SelectAllCheckboxVisibility;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.data.provider.DataProvider;

@CssImport(value = "./styles/select-table-layout.css")
@CssImport(value = "./styles/vaadin-split-layout.css", themeFor = "vaadin-split-layout")
public abstract class SelectTableLayout<BASIC_MODEL extends BasicModel<ID_TYPE>, ENTITY extends BasicModelEntity<ID_TYPE>, ID_TYPE>
	extends VerticalLayout
	implements InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(SelectTableLayout.class);

	@Autowired
	protected ApplicationContext applicationContext;
	@Autowired
	protected RepositoryManager repositoryManager;
	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	protected CrudServices crudServices;
	@Autowired
	protected CrudManager crudManager;

	protected HorizontalLayout toolBar;
	protected ModelGrid<BASIC_MODEL> table;
	protected VerticalLayout tableWrapper;
	protected CrudSearchLayout<? extends SearchSpecification> searchLayout;

	protected List<Button> toolBarButtons;
	private Button searchButton;
	protected HorizontalLayout buttonsLayout;
	protected Button okButton;
	protected Button cancelButton;
	protected Consumer<SelectTableLayout<BASIC_MODEL, ENTITY, ID_TYPE>> okHandler;
	protected Consumer<SelectTableLayout<BASIC_MODEL, ENTITY, ID_TYPE>> cancelHandler;

	protected CrudLayoutDef<BASIC_MODEL, ENTITY> crudLayoutDef;
	protected SelectTableLayoutConfig selectTableLayoutConfig;

	public SelectTableLayout(final CrudLayoutDef<BASIC_MODEL, ENTITY> crudLayoutDef) {
		super();
		this.crudLayoutDef = crudLayoutDef;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init(this.crudLayoutDef);
	}

	public void init(final CrudLayoutDef<BASIC_MODEL, ENTITY> crudLayoutDef) {
		this.crudLayoutDef = crudLayoutDef;
		this.selectTableLayoutConfig = new SelectTableLayoutVerticalConfig();

		this.addClassName("select-table-layout");
		this.setSizeFull();
		this.setMargin(false);
		this.setSpacing(false);

		// ...	Add components.
		this.addToolBar();
		this.addTable();
		this.addSearchLayout();

		this.selectTableLayoutConfig.initialize(this);
		this.addActionButtons();
	}

	protected void addToolBar() {
		this.createToolBar();
	}

	protected void addTable() {
		this.tableWrapper = new VerticalLayout();
		this.tableWrapper.setPadding(false);
		this.tableWrapper.add(this.createTable());
	}

	protected void addSearchLayout() {
		if (this.crudLayoutDef.getSearchLayoutClass() != null) {
			this.searchLayout = this.applicationContext.getBean(this.crudLayoutDef.getSearchLayoutClass());
			this.searchLayout.setClassName("search");
			final SearchSpecification searchSpecification = this.crudLayoutDef.getSearchSpecification();
			if (searchSpecification != null) {
				this.searchLayout.setTargetObject(searchSpecification);
			}
			this.searchLayout.initialize();
			this.searchLayout.setApplyHandler(obj -> this.applySearch(obj));
			this.searchLayout.setCloseHandler(obj -> this.closeSearch(obj));
			this.searchLayout.setCloseButtonVisible(this.crudLayoutDef.isSearchLayoutCloseable());
			this.searchLayout.setVisible(false);
		}
	}

	protected void addActionButtons() {
		this.buttonsLayout = new HorizontalLayout();
		this.buttonsLayout.setWidth(100, Unit.PERCENTAGE);
		this.buttonsLayout.setMargin(false);
		this.buttonsLayout.setSpacing(true);
		this.buttonsLayout.setJustifyContentMode(JustifyContentMode.CENTER);
		this.buttonsLayout.add(this.createActionButtons().toArray(new Component[] {}));
		this.add(this.buttonsLayout);
	}

	protected List<? extends Component> createActionButtons() {
		final List<Button> buttons = new ArrayList<>();

		this.okButton = this.componentBuilderFactory.createButtonBuilder()
			.addThemeVariants(ButtonVariant.LUMO_PRIMARY)
			.setText(this.messageSource.getMessage("action.save"))
			.build();
		this.okButton.addClickShortcut(Key.ENTER, KeyModifier.CONTROL);
		this.okButton.addClickListener(event -> this.doOk());
		buttons.add(this.okButton);

		this.cancelButton = this.componentBuilderFactory.createButtonBuilder()
			.setText(this.messageSource.getMessage("action.cancel"))
			.build();
		this.cancelButton.addClickListener(event -> this.doCancel());
		buttons.add(this.cancelButton);

		return buttons;
	}

	protected void doOk() {
		if (this.okHandler != null) {
			this.okHandler.accept(this);
		}
	}

	protected void doCancel() {
		if (this.cancelHandler != null) {
			this.cancelHandler.accept(this);
		}
	}

	protected HorizontalLayout createToolBar() {
		this.toolBar = new HorizontalLayout();
		this.toolBar.setClassName("tool-bar");
		if (this.crudLayoutDef.getSearchLayoutClass() != null) {
			this.searchButton = this.componentBuilderFactory.createButtonBuilder()
				.setText(this.messageSource.getMessage("action.search"))
				.build();
			this.searchButton.addClickListener(event -> this.doSearch());
			this.toolBar.add(this.searchButton);
		}
		return this.toolBar;
	}

	protected ModelGrid<BASIC_MODEL> createTable() {
		this.table = this.componentBuilderFactory.createModelGridBuilder(this.crudLayoutDef.getModelClass()).build();
		this.table.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_ROW_STRIPES);

		this.table.setSelectionMode(SelectionMode.MULTI);
		((GridMultiSelectionModel<BASIC_MODEL>) this.table.getSelectionModel()).setSelectAllCheckboxVisibility(SelectAllCheckboxVisibility.VISIBLE);

		if (this.crudLayoutDef.getGridClassNameGenerator() != null) {
			this.table.setClassNameGenerator(this.crudLayoutDef.getGridClassNameGenerator());
		}

		this.table.createColumns(this.crudLayoutDef.getColumnDefs());

		final DataProvider<BASIC_MODEL, Void> dataProvider = this.getDefaultDataProvider();
		this.table.setItems(dataProvider);

		return this.table;
	}

	protected DataProvider<BASIC_MODEL, Void> getDefaultDataProvider() {

		DataProvider<BASIC_MODEL, Void> dataProvider = null;
		try {
			if (this.crudLayoutDef.isEmptyWithoutFilter()) {
				dataProvider = this.getEmptyDataProvider();
			} else {
				if (this.crudLayoutDef.getSearchSpecification() != null) {
					dataProvider = this.crudManager.getDataProvider(this.crudLayoutDef.getEntityClass(), this.crudLayoutDef.getModelClass(), this.crudLayoutDef.getSearchSpecification(), this.crudLayoutDef.isCountOnEntity());
				} else {
					dataProvider = this.crudManager.getDataProvider(this.crudLayoutDef.getEntityClass(), this.crudLayoutDef.getModelClass(), this.crudLayoutDef.isCountOnEntity());
				}
			}
		} catch (final CrudException e) {
			SelectTableLayout.logger.error(e.getMessage(), e);
			this.showErrorNotification(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName());
		}

		return dataProvider;
	}

	protected DataProvider<BASIC_MODEL, Void> getEmptyDataProvider() {
		return new CallbackDataProvider<>(
			q -> new ArrayList<BASIC_MODEL>().stream(),
			q -> 0);
	}

	@Override
	public void onAttach(final AttachEvent event) {
		super.onAttach(event);
		if ((this.crudLayoutDef.getSearchLayoutClass() != null) && this.crudLayoutDef.isShowSearchOnStart()) {
			this.doSearch();
		}
	}

	protected void doSearch() {
		try {
			this.searchLayout.updateFields();
			this.showSearchLayout();
		} catch (final Exception e) {
			SelectTableLayout.logger.error(e.getMessage(), e);
			this.showErrorNotification(this.messageSource.getMessage("error.general.search"));
		}
	}

	private void showErrorNotification(final String message) {
		final Notification notification = new Notification();
		notification.addThemeVariants(NotificationVariant.LUMO_ERROR);

		final Span label = new Span(message);

		final Button cancelButton = new Button(
			this.messageSource.getMessage("action.cancel"),
			e -> notification.close());
		cancelButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

		notification.add(label, cancelButton);
		notification.open();
	}

	protected void applySearch(final CrudSearchLayout<?> searchLayout) {
		final SearchSpecification searchSpecification = searchLayout.getTargetObject();
		try {
			this.table.setItems(this.crudManager.getDataProvider(this.crudLayoutDef.getEntityClass(), this.crudLayoutDef.getModelClass(), searchSpecification, this.crudLayoutDef.isCountOnEntity()));
		} catch (final CrudException e) {
			this.showErrorNotification(Optional.ofNullable(e.getCause()).orElse(e).getClass().getName());
		}
		this.refreshAll();
	}

	protected void closeSearch(final CrudSearchLayout<?> searchLayout) {
		this.hideSearchLayout();
		this.searchLayout.setTargetObject(null);
		this.table.setItems(this.getDefaultDataProvider());
	}

	public <S> void showSearchLayout(final S searchObject) {
		if (this.searchLayout != null) {
			if (!this.isSearchVisible()) {
				this.searchLayout.setVisible(true);
				this.selectTableLayoutConfig.applyDefaultTo(this);
			}
			if (searchObject != null) {
				this.searchLayout.updateFields(searchObject);
				this.searchLayout.doApply();
			}
		}
	}

	protected void showSearchLayout() {
		this.showSearchLayout(null);
	}

	protected void hideSearchLayout() {
		if (this.isSearchVisible() && (this.searchLayout != null)) {
			this.searchLayout.setVisible(false);
			this.selectTableLayoutConfig.applyDefaultTo(this);
		}
	}

	protected void refreshAll() {
		this.table.getDataProvider().refreshAll();
	}

	public HorizontalLayout getToolBar() {
		return this.toolBar;
	}

	public CrudSearchLayout<?> getSearchLayout() {
		return this.searchLayout;
	}

	public Grid<BASIC_MODEL> getTable() {
		return this.table;
	}

	public VerticalLayout getTableWrapper() {
		return this.tableWrapper;
	}

	public Boolean isToolbarVisible() {
		return (this.toolBar != null) && this.toolBar.isVisible();
	}

	public Boolean isTableVisible() {
		return (this.tableWrapper != null) && this.tableWrapper.isVisible();
	}

	public Boolean isSearchVisible() {
		return (this.searchLayout != null) && this.searchLayout.isVisible();
	}

	public Consumer<SelectTableLayout<BASIC_MODEL, ENTITY, ID_TYPE>> getOkHandler() {
		return this.okHandler;
	}

	public void setOkHandler(final Consumer<SelectTableLayout<BASIC_MODEL, ENTITY, ID_TYPE>> okHandler) {
		this.okHandler = okHandler;
	}

	public Consumer<SelectTableLayout<BASIC_MODEL, ENTITY, ID_TYPE>> getCancelHandler() {
		return this.cancelHandler;
	}

	public void setCancelHandler(final Consumer<SelectTableLayout<BASIC_MODEL, ENTITY, ID_TYPE>> cancelHandler) {
		this.cancelHandler = cancelHandler;
	}

	public Set<BASIC_MODEL> getSelectedItems() {
		return this.table.getSelectedItems();
	}
}