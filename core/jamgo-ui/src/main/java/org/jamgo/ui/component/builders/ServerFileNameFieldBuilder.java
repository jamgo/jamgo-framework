package org.jamgo.ui.component.builders;

import org.jamgo.ui.component.ServerFileNameField;
import org.jamgo.vaadin.ui.builder.JamgoComponentBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ServerFileNameFieldBuilder extends JamgoComponentBuilder<ServerFileNameField, ServerFileNameFieldBuilder> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = this.applicationContext.getBean(ServerFileNameField.class);
	}

}