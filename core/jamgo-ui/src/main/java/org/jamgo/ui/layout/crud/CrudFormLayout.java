package org.jamgo.ui.layout.crud;

/**
 * @deprecated: Renamed to CrudLayout. Not all crud layouts are form based.
 * Use {@link CrudLayout}
 */

@SuppressWarnings("serial")
@Deprecated(forRemoval = true)
public abstract class CrudFormLayout<T> extends CrudLayout<T> {

}
