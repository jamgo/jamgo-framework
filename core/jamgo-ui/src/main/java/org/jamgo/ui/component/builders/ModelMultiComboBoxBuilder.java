package org.jamgo.ui.component.builders;

import org.jamgo.model.entity.Model;
import org.jamgo.services.impl.ServiceManager;
import org.jamgo.ui.component.ModelMultiComboBox;
import org.jamgo.ui.layout.BackofficeApplicationDef;
import org.jamgo.vaadin.builder.base.HasEnabledBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.jamgo.vaadin.ui.builder.JamgoComponentBuilder;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.ItemLabelGenerator;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ModelMultiComboBoxBuilder<T extends Model> extends JamgoComponentBuilder<ModelMultiComboBox<T>, ModelMultiComboBoxBuilder<T>> implements
	InitializingBean,
	HasValueBuilder<ModelMultiComboBoxBuilder<T>, ModelMultiComboBox<T>>,
	HasEnabledBuilder<ModelMultiComboBoxBuilder<T>, ModelMultiComboBox<T>> {

	@Autowired
	private ServiceManager serviceManager;
	@Autowired
	protected BackofficeApplicationDef backofficeApplicationDef;

	private Class<T> modelClass;

	public ModelMultiComboBoxBuilder(final Class<T> modelClass) {
		super();
		this.modelClass = modelClass;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = this.applicationContext.getBean(ModelMultiComboBox.class, this.backofficeApplicationDef.getCrudLayoutDef(this.modelClass));
	}

	@Override
	public ModelMultiComboBox<T> build() {
		super.build();

		this.instance.setItemLabelGenerator(o -> o.toListString());
		this.instance.setItems(this.serviceManager.getService(this.modelClass).findAll());

		return this.instance;
	}

	public Class<T> getModelClass() {
		return this.modelClass;
	}

	public void setModelClass(final Class<T> modelClass) {
		this.modelClass = modelClass;
	}

	public ModelMultiComboBoxBuilder<T> setCreateVisible(final boolean createVisible) {
		this.getComponent().setCreateVisible(createVisible);
		return this;
	}

	public ModelMultiComboBoxBuilder<T> setItemLabelGenerator(final ItemLabelGenerator<T> itemLabelGenerator) {
		this.getComponent().setItemLabelGenerator(itemLabelGenerator);
		return this;
	}

}