package org.jamgo.app.support;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.sql.DataSource;

import org.apache.commons.lang3.ArrayUtils;
import org.h2.server.web.WebServlet;
import org.hibernate.cfg.AvailableSettings;
import org.jamgo.model.filter.IgnoreDuringScan;
import org.jamgo.services.config.ServicesConfig;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate5.SpringBeanContainer;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.filter.HiddenHttpMethodFilter;

import com.blazebit.persistence.Criteria;
import com.blazebit.persistence.CriteriaBuilderFactory;
import com.blazebit.persistence.spi.CriteriaBuilderConfiguration;
import com.blazebit.persistence.view.EntityViewManager;
import com.blazebit.persistence.view.spi.EntityViewConfiguration;

@Configuration
@Import({ ServicesConfig.class })
@EnableTransactionManagement
@ComponentScan(
	basePackageClasses = {
		org.jamgo.app.config.PackageMarker.class
	},
	excludeFilters = @ComponentScan.Filter(IgnoreDuringScan.class))
@org.springframework.context.annotation.PropertySource(name = "jpaVendorProperties", value = "classpath:jpa-vendor.properties")
public abstract class JamgoServletInitializer extends SpringBootServletInitializer {

	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;

	@Bean
	public EntityManager entityManager() {
		return this.entityManagerFactory.createEntityManager();
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(final ConfigurableListableBeanFactory beanFactory, final DataSource dataSource, final Environment environment) {
		final LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(false);
		vendorAdapter.setShowSql(false);
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan(this.getPackagesToScan());
		factory.setDataSource(dataSource);
		factory.setJpaPropertyMap(this.getVendorProperties(beanFactory, environment));
		return factory;
	}

	protected String[] getPackagesToScan() {
		return ArrayUtils.toArray(org.jamgo.model.PackageMarker.class.getPackage().getName());
	}

	@Bean
	public PlatformTransactionManager transactionManager(final ConfigurableListableBeanFactory beanFactory, final DataSource dataSource, final Environment environment) {
		final JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(this.entityManagerFactory(beanFactory, dataSource, environment).getObject());
		transactionManager.setDataSource(dataSource);
		return transactionManager;
	}

	/**
	 * Create vendor properties map.
	 *
	 * By default, JPA-created injected class beans (for example {@link javax.persistence.AttributeConverter AttributeConverter})
	 * are out of the Spring beans context and it's very difficult to get references
	 * and interact with those singletons.
	 * Starting with Hibernate version 5.3, it is possible to configure the internal
	 * context of beans to associate it with Spring. To do this you have to create a
	 * {@link org.springframework.orm.hibernate5.SpringBeanContainer SpringBeanContainer} and associate it with the hibernate.resource.beans.container property.
	 */

	protected Map<String, Object> getVendorProperties(final ConfigurableListableBeanFactory beanFactory, final Environment environment) {
		MapPropertySource jpaVendorProperties = null;
		for (final PropertySource<?> eachPropertySource : ((AbstractEnvironment) environment).getPropertySources()) {
			if ("jpaVendorProperties".equals(eachPropertySource.getName())) {
				jpaVendorProperties = (MapPropertySource) eachPropertySource;
				break;
			}
		}

		final Map<String, Object> map = new HashMap<>();
		if (jpaVendorProperties != null) {
			for (final String eachPropertyKey : jpaVendorProperties.getPropertyNames()) {
				map.put(eachPropertyKey, jpaVendorProperties.getProperty(eachPropertyKey));
			}
		}

		map.put(AvailableSettings.BEAN_CONTAINER, this.springBeanContainer(beanFactory));

		return map;
	}

	@Bean
	public SpringBeanContainer springBeanContainer(final ConfigurableListableBeanFactory beanFactory) {
		return new SpringBeanContainer(beanFactory);
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
	@Lazy(false)
	public CriteriaBuilderFactory createCriteriaBuilderFactory() {
		final CriteriaBuilderConfiguration config = Criteria.getDefault();
		return config.createCriteriaBuilderFactory(this.entityManagerFactory);
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
	@Lazy(false)
	public EntityViewManager createEntityViewManager(final CriteriaBuilderFactory cbf, final EntityViewConfiguration entityViewConfiguration) {
		entityViewConfiguration.setTypeTestValue(LocalDate.class, LocalDate.ofEpochDay(1));
		return entityViewConfiguration.createEntityViewManager(cbf);
	}

	@Bean
	public FilterRegistrationBean<HiddenHttpMethodFilter> hiddenHttpMethodFilter() {
		final HiddenHttpMethodFilter hiddenHttpMethodFilter = new HiddenHttpMethodFilter();
		final FilterRegistrationBean<HiddenHttpMethodFilter> registrationBean = new FilterRegistrationBean<>();
		registrationBean.setFilter(hiddenHttpMethodFilter);
		return registrationBean;
	}

	@Bean
	@ConditionalOnProperty(value = "jamgo.h2.console", havingValue = "true")
	public ServletRegistrationBean<WebServlet> h2servletRegistration() {
		final ServletRegistrationBean<WebServlet> registrationBean = new ServletRegistrationBean<>(new WebServlet());
		registrationBean.addUrlMappings("/console/*");
		return registrationBean;
	}

	@Bean(name = "permissionProperties")
	public Properties permissionProperties() {
		final Properties properties = new Properties();
		try {
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("permission.properties"));
		} catch (final IOException e) {
			this.logger.error(e.getMessage(), e);
		}
		return properties;
	}

	@Bean
	public SimpleDateFormat getDateFormat() {
		return new SimpleDateFormat("dd/MM/yyyy");
	}

}
