package org.jamgo.vaadin.ui.builder;

import java.util.Optional;

import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasAutocapitalizeBuilder;
import org.jamgo.vaadin.builder.base.HasAutocompleteBuilder;
import org.jamgo.vaadin.builder.base.HasAutocorrectBuilder;
import org.jamgo.vaadin.builder.base.HasHelperBuilder;
import org.jamgo.vaadin.builder.base.HasPrefixAndSuffixBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.jamgo.vaadin.builder.base.HasValueChangeModeBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.textfield.IntegerField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class IntegerFieldBuilder extends JamgoComponentBuilder<IntegerField, IntegerFieldBuilder> implements
	HasSizeBuilder<IntegerFieldBuilder, IntegerField>,
	HasValueBuilder<IntegerFieldBuilder, IntegerField>,
	HasValidationBuilder<IntegerFieldBuilder, IntegerField>,
	HasValueChangeModeBuilder<IntegerFieldBuilder, IntegerField>,
	HasPrefixAndSuffixBuilder<IntegerFieldBuilder, IntegerField>,
	HasAutocompleteBuilder<IntegerFieldBuilder, IntegerField>,
	HasAutocapitalizeBuilder<IntegerFieldBuilder, IntegerField>,
	HasAutocorrectBuilder<IntegerFieldBuilder, IntegerField>,
	HasHelperBuilder<IntegerFieldBuilder, IntegerField>,
	HasStyleBuilder<IntegerFieldBuilder, IntegerField>,
	HasThemeBuilder<IntegerFieldBuilder, IntegerField>,
	FocusableBuilder<IntegerFieldBuilder, IntegerField, IntegerField> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new IntegerField();
		this.instance.setWidth(100, Unit.PERCENTAGE);
	}

	@Override
	public IntegerField build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));

		return this.instance;
	}
}