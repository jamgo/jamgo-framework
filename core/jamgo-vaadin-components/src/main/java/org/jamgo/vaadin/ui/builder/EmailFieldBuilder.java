package org.jamgo.vaadin.ui.builder;

import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasAutocapitalizeBuilder;
import org.jamgo.vaadin.builder.base.HasAutocompleteBuilder;
import org.jamgo.vaadin.builder.base.HasAutocorrectBuilder;
import org.jamgo.vaadin.builder.base.HasPrefixAndSuffixBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.jamgo.vaadin.builder.base.HasValueChangeModeBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.textfield.EmailField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EmailFieldBuilder extends JamgoComponentBuilder<EmailField, EmailFieldBuilder> implements
	HasSizeBuilder<EmailFieldBuilder, EmailField>,
	HasValueBuilder<EmailFieldBuilder, EmailField>,
	HasValidationBuilder<EmailFieldBuilder, EmailField>,
	HasValueChangeModeBuilder<EmailFieldBuilder, EmailField>,
	HasPrefixAndSuffixBuilder<EmailFieldBuilder, EmailField>,
	HasAutocompleteBuilder<EmailFieldBuilder, EmailField>,
	HasAutocapitalizeBuilder<EmailFieldBuilder, EmailField>,
	HasAutocorrectBuilder<EmailFieldBuilder, EmailField>,
	HasStyleBuilder<EmailFieldBuilder, EmailField>,
	HasThemeBuilder<EmailFieldBuilder, EmailField>,
	FocusableBuilder<EmailFieldBuilder, EmailField, EmailField> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new EmailField();
	}

}