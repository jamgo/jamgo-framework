package org.jamgo.vaadin.ui.builder;

import java.util.Optional;
import java.util.function.Consumer;

import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasTextBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.components.JamgoComponentConstants;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Image;

@org.springframework.stereotype.Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ButtonBuilder extends JamgoComponentBuilder<Button, ButtonBuilder> implements
	HasSizeBuilder<ButtonBuilder, Button>,
	HasStyleBuilder<ButtonBuilder, Button>,
	HasTextBuilder<ButtonBuilder, Button>,
	HasThemeBuilder<ButtonBuilder, Button>,
	FocusableBuilder<ButtonBuilder, Button, Button> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new Button();
	}

	@Override
	public Button build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setText(o));

		final Consumer<String> labelSetter = (label) -> this.instance.setText(label);
		ComponentUtil.setData(this.instance, JamgoComponentConstants.PROPERTY_LABEL_SETTER, labelSetter);

		return this.instance;
	}

	/**
	 * Set the button to be input focused when the page loads.
	 *
	 * @param autofocus
	 *            the boolean value to set
	 */
	public ButtonBuilder setAutofocus(final boolean autofocus) {
		this.getComponent().setAutofocus(autofocus);
		return this;
	}

	/**
	 * Set the button so that it is disabled on click.
	 * <p>
	 * Enabling the button needs to happen from the server.
	 *
	 * @param disableOnClick
	 *            true to disable button immediately when clicked
	 */
	public ButtonBuilder setDisableOnClick(final boolean disableOnClick) {
		this.getComponent().setDisableOnClick(disableOnClick);
		return this;
	}

	/**
	 * Sets the given component as the icon of this button.
	 * <p>
	 * Even though you can use almost any component as an icon, some good
	 * options are {@code Icon} and {@link Image}.
	 * <p>
	 * Use {@link #setIconAfterText(boolean)} to change the icon's position
	 * relative to the button's text content.
	 * <p>
	 * This method also sets or removes this button's <code>theme=icon</code>
	 * attribute based on whether this button contains only an icon after this
	 * operation or not.
	 *
	 * @param icon
	 *            component to be used as an icon, may be <code>null</code> to
	 *            only remove the current icon, can't be a text-node
	 */
	public ButtonBuilder setIcon(final Component icon) {
		this.getComponent().setIcon(icon);
		return this;
	}

	/**
	 * Sets whether this button's icon should be positioned after it's text
	 * content or the other way around.
	 * <p>
	 * At the element-level, this method determines whether to set
	 * {@code slot="prefix"} or {@code slot="suffix"} attribute to the icon.
	 *
	 * @param iconAfterText
	 *            whether the icon should be positioned after the text content
	 *            or not
	 */
	public ButtonBuilder setIconAfterText(final boolean iconAfterText) {
		this.getComponent().setIconAfterText(iconAfterText);
		return this;
	}

	/**
	 * Adds theme variants to the component.
	 *
	 * @param variants
	 *            theme variants to add
	 */
	public ButtonBuilder addThemeVariants(final ButtonVariant... variants) {
		this.getComponent().addThemeVariants(variants);
		return this;
	}

}