package org.jamgo.vaadin.components;

public class JamgoComponentConstants {

	public static final String PROPERTY_DEFAULT_WIDTH = "jmg-default-width";
	public static final String PROPERTY_LABEL_SETTER = "jmg-label-setter";

	public enum ComponentColspan {
		ONE_COLUMN(1),
		TWO_COLUMNS(2),
		THREE_COLUMNS(3),
		FOUR_COLUMNS(4),
		FIVE_COLUMNS(5),
		SIX_COLUMNS(6),
		SEVEN_COLUMNS(7),
		EIGHT_COLUMNS(8),
		NINE_COLUMNS(9),
		TEN_COLUMNS(10),
		ELEVEN_COLUMNS(11),
		TWELVE_COLUMNS(12),
		FULL_WIDTH(12);

		private final int columns;

		ComponentColspan(final int columns) {
			this.columns = columns;
		}

		public int getColumns() {
			return this.columns;
		}
	}

}
