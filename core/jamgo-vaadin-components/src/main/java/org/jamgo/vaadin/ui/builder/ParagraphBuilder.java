package org.jamgo.vaadin.ui.builder;

import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasTextBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.html.Paragraph;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ParagraphBuilder extends JamgoComponentBuilder<Paragraph, ParagraphBuilder> implements
	HasSizeBuilder<ParagraphBuilder, Paragraph>,
	HasStyleBuilder<ParagraphBuilder, Paragraph>,
	HasTextBuilder<ParagraphBuilder, Paragraph> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new Paragraph();
	}

}