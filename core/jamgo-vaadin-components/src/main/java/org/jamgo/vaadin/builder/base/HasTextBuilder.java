package org.jamgo.vaadin.builder.base;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasText;
import com.vaadin.flow.component.Text;

@SuppressWarnings("rawtypes")
public interface HasTextBuilder<T extends HasTextBuilder, S extends HasText> extends BaseComponentBuilder<T, S> {

	/**
	 * Sets the given string as the content of this component. This removes any
	 * existing child components and child elements. To mix text and child
	 * components in a component that also supports child components, use
	 * {@link HasComponents#add(Component...)} with the {@link Text} component
	 * for the textual parts.
	 *
	 * @param text
	 *            the text content to set
	 */
	@SuppressWarnings("unchecked")
	default T setText(final String text) {
		this.getComponent().setText(text);
		return (T) this;
	}

}
