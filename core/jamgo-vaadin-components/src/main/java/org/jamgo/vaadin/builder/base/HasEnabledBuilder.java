package org.jamgo.vaadin.builder.base;

import com.vaadin.flow.component.HasEnabled;

@SuppressWarnings("rawtypes")
public interface HasEnabledBuilder<T extends HasEnabledBuilder, S extends HasEnabled> extends BaseComponentBuilder<T, S> {

	/**
	 * Sets the UI object explicitly disabled or enabled.
	 *
	 * @param enabled
	 *            if {@code false} then explicitly disables the object, if
	 *            {@code true} then enables the object so that its state depends
	 *            on parent
	 */
	@SuppressWarnings("unchecked")
	default T setEnabled(final boolean enabled) {
		this.getComponent().setEnabled(enabled);
		return (T) this;
	}

}
