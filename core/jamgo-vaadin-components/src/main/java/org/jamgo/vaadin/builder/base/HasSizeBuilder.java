package org.jamgo.vaadin.builder.base;

import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.Unit;

@SuppressWarnings("rawtypes")
public interface HasSizeBuilder<T extends HasSizeBuilder, S extends HasSize> extends BaseComponentBuilder<T, S> {

	/**
	 * Sets the width of the component.
	 * <p>
	 * The width should be in a format understood by the browser, e.g. "100px"
	 * or "2.5em".
	 * <p>
	 * If the provided {@code width} value is {@literal null} then width is
	 * removed.
	 *
	 * @param width
	 *            the width to set, may be {@code null}
	 */
	@SuppressWarnings("unchecked")
	default T setWidth(final String width) {
		this.getComponent().setWidth(width);
		return (T) this;
	}

	/**
	 * Sets the width of the component. Negative number implies unspecified size
	 * (terminal is free to set the size).
	 *
	 * @param width
	 *            the width of the object.
	 * @param unit
	 *            the unit used for the width.
	 */
	@SuppressWarnings("unchecked")
	default T setWidth(final float width, final Unit unit) {
		this.getComponent().setWidth(width, unit);
		return (T) this;
	}

	/**
	 * Sets the min-width of the component.
	 * <p>
	 * The width should be in a format understood by the browser, e.g. "100px"
	 * or "2.5em".
	 * <p>
	 * If the provided {@code minWidth} value is {@literal null} then min-width is
	 * removed.
	 *
	 * @param minWidth
	 *            the min-width value (if <code>null</code>, the property will be removed)
	 */
	@SuppressWarnings("unchecked")
	default T setMinWidth(final String minWidth) {
		this.getComponent().setMinWidth(minWidth);
		return (T) this;
	}

	/**
	 * Sets the min-width of the component. Negative number implies unspecified size
	 * (terminal is free to set the size).
	 *
	 * @param minWidth
	 *            the min-width of the object.
	 * @param unit
	 *            the unit used for the min-width.
	 */
	@SuppressWarnings("unchecked")
	default T setMinWidth(final float minWidth, final Unit unit) {
		this.getComponent().setMinWidth(minWidth, unit);
		return (T) this;
	}

	/**
	 * Sets the max-width of the component.
	 * <p>
	 * The width should be in a format understood by the browser, e.g. "100px"
	 * or "2.5em".
	 * <p>
	 * If the provided {@code maxWidth} value is {@literal null} then max-width is
	 * removed.
	 *
	 * @param maxWidth
	 *            the max-width value (if <code>null</code>, the property will be removed)
	 */
	@SuppressWarnings("unchecked")
	default T setMaxWidth(final String maxWidth) {
		this.getComponent().setMaxWidth(maxWidth);
		return (T) this;
	}

	/**
	 * Sets the max-width of the component. Negative number implies unspecified size
	 * (terminal is free to set the size).
	 *
	 * @param maxWidth
	 *            the max-width of the object.
	 * @param unit
	 *            the unit used for the max-width.
	 */
	@SuppressWarnings("unchecked")
	default T setMaxWidth(final float maxWidth, final Unit unit) {
		this.getComponent().setMaxWidth(maxWidth, unit);
		return (T) this;
	}

	/**
	 * Sets the height of the component.
	 * <p>
	 * The height should be in a format understood by the browser, e.g. "100px"
	 * or "2.5em".
	 * <p>
	 * If the provided {@code height} value is {@literal null} then height is
	 * removed.
	 *
	 * @param height
	 *            the height to set, may be {@code null}
	 */
	@SuppressWarnings("unchecked")
	default T setHeight(final String height) {
		this.getComponent().setHeight(height);
		return (T) this;
	}

	/**
	 * Sets the height of the component. Negative number implies unspecified size
	 * (terminal is free to set the size).
	 *
	 * @param height
	 *            the height of the object.
	 * @param unit
	 *            the unit used for the height.
	 */
	@SuppressWarnings("unchecked")
	default T setHeight(final float height, final Unit unit) {
		this.getComponent().setHeight(height, unit);
		return (T) this;
	}

	/**
	 * Sets the min-height of the component.
	 * <p>
	 * The height should be in a format understood by the browser, e.g. "100px"
	 * or "2.5em".
	 * <p>
	 * If the provided {@code minHeight} value is {@literal null} then min-height is
	 * removed.
	 *
	 * @param minHeight
	 *            the min-height value (if <code>null</code>, the property will be removed)
	 */
	@SuppressWarnings("unchecked")
	default T setMinHeight(final String minHeight) {
		this.getComponent().setMinHeight(minHeight);
		return (T) this;
	}

	/**
	 * Sets the min-height of the component. Negative number implies unspecified size
	 * (terminal is free to set the size).
	 *
	 * @param minHeight
	 *            the min-height of the object.
	 * @param unit
	 *            the unit used for the min-height.
	 */
	@SuppressWarnings("unchecked")
	default T setMinHeight(final float minHeight, final Unit unit) {
		this.getComponent().setMinHeight(minHeight, unit);
		return (T) this;
	}

	/**
	 * Sets the max-height of the component.
	 * <p>
	 * The height should be in a format understood by the browser, e.g. "100px"
	 * or "2.5em".
	 * <p>
	 * If the provided {@code maxHeight} value is {@literal null} then max-height is
	 * removed.
	 *
	 * @param maxHeight
	 *            the max-height value (if <code>null</code>, the property will be removed)
	 */
	@SuppressWarnings("unchecked")
	default T setMaxHeight(final String maxHeight) {
		this.getComponent().setMaxHeight(maxHeight);
		return (T) this;
	}

	/**
	 * Sets the max-height of the component. Negative number implies unspecified size
	 * (terminal is free to set the size).
	 *
	 * @param maxHeight
	 *            the max-height of the object.
	 * @param unit
	 *            the unit used for the max-height.
	 */
	@SuppressWarnings("unchecked")
	default T setMaxHeight(final float maxHeight, final Unit unit) {
		this.getComponent().setMaxHeight(maxHeight, unit);
		return (T) this;
	}

	/**
	 * Sets the width and the height of the component to "100%".
	 * <p>
	 * This is just a convenience method which delegates its call to the
	 * {@link #setWidth(String)} and {@link #setHeight(String)} methods with
	 * {@literal "100%"} as the argument value
	 */
	@SuppressWarnings("unchecked")
	default T setSizeFull() {
		this.getComponent().setSizeFull();
		return (T) this;
	}

	/**
	 * Sets the width of the component to "100%".
	 * <p>
	 * This is just a convenience method which delegates its call to the
	 * {@link #setWidth(String)} with
	 * {@literal "100%"} as the argument value
	 */
	@SuppressWarnings("unchecked")
	default T setWidthFull() {
		this.getComponent().setWidthFull();
		return (T) this;
	}

	/**
	 * Sets the height of the component to "100%".
	 * <p>
	 * This is just a convenience method which delegates its call to the
	 * {@link #setHeight(String)} with
	 * {@literal "100%"} as the argument value
	 */
	@SuppressWarnings("unchecked")
	default T setHeightFull() {
		this.getComponent().setHeightFull();
		return (T) this;
	}

	/**
	 * Removes the width and the height of the component.
	 * <p>
	 * This is just a convenience method which delegates its call to the
	 * {@link #setWidth(String)} and {@link #setHeight(String)} methods with
	 * {@literal null} as the argument value
	 */
	@SuppressWarnings("unchecked")
	default T setSizeUndefined() {
		this.getComponent().setSizeUndefined();
		return (T) this;
	}

}
