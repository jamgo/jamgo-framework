package org.jamgo.vaadin.ui.builder;

import java.util.Optional;

import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasAutocapitalizeBuilder;
import org.jamgo.vaadin.builder.base.HasAutocompleteBuilder;
import org.jamgo.vaadin.builder.base.HasAutocorrectBuilder;
import org.jamgo.vaadin.builder.base.HasHelperBuilder;
import org.jamgo.vaadin.builder.base.HasPrefixAndSuffixBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.jamgo.vaadin.builder.base.HasValueChangeModeBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.textfield.NumberField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class NumberFieldBuilder extends JamgoComponentBuilder<NumberField, NumberFieldBuilder> implements
	HasSizeBuilder<NumberFieldBuilder, NumberField>,
	HasValueBuilder<NumberFieldBuilder, NumberField>,
	HasValidationBuilder<NumberFieldBuilder, NumberField>,
	HasValueChangeModeBuilder<NumberFieldBuilder, NumberField>,
	HasPrefixAndSuffixBuilder<NumberFieldBuilder, NumberField>,
	HasAutocompleteBuilder<NumberFieldBuilder, NumberField>,
	HasAutocapitalizeBuilder<NumberFieldBuilder, NumberField>,
	HasAutocorrectBuilder<NumberFieldBuilder, NumberField>,
	HasHelperBuilder<NumberFieldBuilder, NumberField>,
	HasStyleBuilder<NumberFieldBuilder, NumberField>,
	HasThemeBuilder<NumberFieldBuilder, NumberField>,
	FocusableBuilder<NumberFieldBuilder, NumberField, NumberField> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new NumberField();
	}

	@Override
	public NumberField build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));

		return this.instance;
	}
}