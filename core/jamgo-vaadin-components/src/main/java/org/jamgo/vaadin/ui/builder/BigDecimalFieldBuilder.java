package org.jamgo.vaadin.ui.builder;

import java.util.Optional;

import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasAutocapitalizeBuilder;
import org.jamgo.vaadin.builder.base.HasAutocompleteBuilder;
import org.jamgo.vaadin.builder.base.HasAutocorrectBuilder;
import org.jamgo.vaadin.builder.base.HasHelperBuilder;
import org.jamgo.vaadin.builder.base.HasPrefixAndSuffixBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.jamgo.vaadin.builder.base.HasValueChangeModeBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.textfield.BigDecimalField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BigDecimalFieldBuilder extends JamgoComponentBuilder<BigDecimalField, BigDecimalFieldBuilder> implements
	HasSizeBuilder<BigDecimalFieldBuilder, BigDecimalField>,
	HasValueBuilder<BigDecimalFieldBuilder, BigDecimalField>,
	HasValidationBuilder<BigDecimalFieldBuilder, BigDecimalField>,
	HasValueChangeModeBuilder<BigDecimalFieldBuilder, BigDecimalField>,
	HasPrefixAndSuffixBuilder<BigDecimalFieldBuilder, BigDecimalField>,
	HasAutocompleteBuilder<BigDecimalFieldBuilder, BigDecimalField>,
	HasAutocapitalizeBuilder<BigDecimalFieldBuilder, BigDecimalField>,
	HasAutocorrectBuilder<BigDecimalFieldBuilder, BigDecimalField>,
	HasHelperBuilder<BigDecimalFieldBuilder, BigDecimalField>,
	HasStyleBuilder<BigDecimalFieldBuilder, BigDecimalField>,
	HasThemeBuilder<BigDecimalFieldBuilder, BigDecimalField>,
	FocusableBuilder<BigDecimalFieldBuilder, BigDecimalField, BigDecimalField> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new BigDecimalField();
		this.instance.setWidth(100, Unit.PERCENTAGE);
	}

	@Override
	public BigDecimalField build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));
		return this.instance;
	}

}
