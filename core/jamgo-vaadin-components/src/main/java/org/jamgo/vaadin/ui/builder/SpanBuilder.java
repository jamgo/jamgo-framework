package org.jamgo.vaadin.ui.builder;

import org.jamgo.vaadin.builder.base.HasEnabledBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasTextBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.html.Span;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SpanBuilder extends JamgoComponentBuilder<Span, SpanBuilder> implements
	HasSizeBuilder<SpanBuilder, Span>,
	HasStyleBuilder<SpanBuilder, Span>,
	HasEnabledBuilder<SpanBuilder, Span>,
	HasTextBuilder<SpanBuilder, Span> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new Span();
	}

}