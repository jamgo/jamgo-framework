package org.jamgo.vaadin.ui.builder;

import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

@org.springframework.stereotype.Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HorizontalLayoutBuilder extends JamgoComponentBuilder<HorizontalLayout, HorizontalLayoutBuilder> implements
	HasSizeBuilder<HorizontalLayoutBuilder, HorizontalLayout>,
	HasStyleBuilder<HorizontalLayoutBuilder, HorizontalLayout> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new HorizontalLayout();
	}

	@Override
	public HorizontalLayout build() {
		return this.instance;
	}

}