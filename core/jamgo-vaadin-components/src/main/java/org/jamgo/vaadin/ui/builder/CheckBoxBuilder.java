package org.jamgo.vaadin.ui.builder;

import java.util.Optional;
import java.util.function.Consumer;

import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.jamgo.vaadin.components.JamgoComponentConstants;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.checkbox.Checkbox;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class CheckBoxBuilder extends JamgoComponentBuilder<Checkbox, CheckBoxBuilder> implements
	HasSizeBuilder<CheckBoxBuilder, Checkbox>,
	HasStyleBuilder<CheckBoxBuilder, Checkbox>,
	HasValueBuilder<CheckBoxBuilder, Checkbox>,
	FocusableBuilder<CheckBoxBuilder, Checkbox, Checkbox> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new Checkbox();
	}

	@Override
	public Checkbox build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));

		final Consumer<String> labelSetter = (label) -> this.instance.setLabel(label);
		ComponentUtil.setData(this.instance, JamgoComponentConstants.PROPERTY_LABEL_SETTER, labelSetter);

		return this.instance;
	}

	/**
	 * Set the current label text of this checkbox with HTML formatting.
	 * <p>
	 * XSS vulnerability warning: the given HTML is rendered in the browser as
	 * is and the developer is responsible for ensuring no harmful HTML is used.
	 * </p>
	 *
	 * @param htmlContent the label html to set
	 */
	public CheckBoxBuilder setLabelAsHtml(final String htmlContent) {
		this.getComponent().setLabelAsHtml(htmlContent);
		return this;
	}

	/**
	 * Set the accessibility label of this checkbox.
	 *
	 * @param ariaLabel the accessibility label to set
	 * @see <a href=
	 *      "https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_aria-label_attribute"
	 *      >aria-label at MDN</a>
	 */
	public CheckBoxBuilder setAriaLabel(final String ariaLabel) {
		this.getComponent().setAriaLabel(ariaLabel);
		return this;
	}

	/**
	 * Set the checkbox to be input focused when the page loads.
	 *
	 * @param autofocus the boolean value to set
	 */
	public CheckBoxBuilder setAutofocus(final boolean autofocus) {
		this.getComponent().setAutofocus(autofocus);
		return this;
	}

	/**
	 * Set the indeterminate state of the checkbox.
	 * <p>
	 * <em>NOTE: As according to the HTML5 standard, this has only effect on the
	 * visual appearance, not on the checked value!</em>
	 *
	 * @param indeterminate the boolean value to set
	 * @see Checkbox#isIndeterminate()
	 */
	public CheckBoxBuilder setIndeterminate(final boolean indeterminate) {
		this.getComponent().setIndeterminate(indeterminate);
		return this;
	}

}