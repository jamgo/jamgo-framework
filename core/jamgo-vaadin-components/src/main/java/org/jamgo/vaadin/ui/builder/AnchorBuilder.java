package org.jamgo.vaadin.ui.builder;

import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasTextBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.server.AbstractStreamResource;
import com.vaadin.flow.server.StreamResource;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AnchorBuilder extends JamgoComponentBuilder<Anchor, AnchorBuilder> implements
	HasTextBuilder<AnchorBuilder, Anchor>,
	FocusableBuilder<AnchorBuilder, Anchor, Anchor> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new Anchor();
		this.instance.setText("");
		this.instance.setTarget("_blank");
	}

	/**
	 * Sets the URL that this anchor links to.
	 * <p>
	 * Use the method {@link Anchor#removeHref()} to remove the <b>href</b> attribute
	 * instead of setting it to an empty string.
	 *
	 * @see Anchor#removeHref()
	 * @see #setHref(AbstractStreamResource)
	 *
	 * @param href
	 *            the href to set
	 */
	public AnchorBuilder setHref(final String href) {
		this.getComponent().setHref(href);
		return this;
	}

	/**
	 * Sets the URL that this anchor links to with the URL of the given
	 * {@link StreamResource}.
	 *
	 * @param href
	 *            the resource value, not null
	 */
	public AnchorBuilder setHref(final AbstractStreamResource href) {
		this.getComponent().setHref(href);
		return this;
	}

	/**
	 * Sets the target window, tab or frame for this anchor. The target is
	 * either the <code>window.name</code> of a specific target, or one of these
	 * special values:
	 * <ul>
	 * <li><code>_self</code>: Open the link in the current context. This is the
	 * default behavior.
	 * <li><code>_blank</code>: Opens the link in a new unnamed context.
	 * <li><code>_parent</code>: Opens the link in the parent context, or the
	 * current context if there is no parent context.
	 * <li><code>_top</code>: Opens the link in the top most grandparent
	 * context, or the current context if there is no parent context.
	 * </ul>
	 *
	 * @param target
	 *            the target value, or <code>""</code> to remove the target
	 *            value
	 */
	public AnchorBuilder setTarget(final String target) {
		this.getComponent().setTarget(target);
		return this;
	}

}