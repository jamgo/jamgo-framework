package org.jamgo.vaadin.ui.builder;

import java.util.Optional;

import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasAutocapitalizeBuilder;
import org.jamgo.vaadin.builder.base.HasAutocompleteBuilder;
import org.jamgo.vaadin.builder.base.HasAutocorrectBuilder;
import org.jamgo.vaadin.builder.base.HasHelperBuilder;
import org.jamgo.vaadin.builder.base.HasPrefixAndSuffixBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.jamgo.vaadin.builder.base.HasValueChangeModeBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.textfield.PasswordField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PasswordFieldBuilder extends JamgoComponentBuilder<PasswordField, PasswordFieldBuilder> implements
	HasSizeBuilder<PasswordFieldBuilder, PasswordField>,
	HasValueBuilder<PasswordFieldBuilder, PasswordField>,
	HasValidationBuilder<PasswordFieldBuilder, PasswordField>,
	HasValueChangeModeBuilder<PasswordFieldBuilder, PasswordField>,
	HasPrefixAndSuffixBuilder<PasswordFieldBuilder, PasswordField>,
	HasAutocompleteBuilder<PasswordFieldBuilder, PasswordField>,
	HasAutocapitalizeBuilder<PasswordFieldBuilder, PasswordField>,
	HasAutocorrectBuilder<PasswordFieldBuilder, PasswordField>,
	HasHelperBuilder<PasswordFieldBuilder, PasswordField>,
	HasStyleBuilder<PasswordFieldBuilder, PasswordField>,
	HasThemeBuilder<PasswordFieldBuilder, PasswordField>,
	FocusableBuilder<PasswordFieldBuilder, PasswordField, PasswordField> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new PasswordField();
	}

	@Override
	public PasswordField build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));

		return this.instance;
	}
}