package org.jamgo.vaadin.ui.builder;

import org.jamgo.services.impl.LocalizedMessageService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.flow.component.Component;

public abstract class JamgoComponentBuilder<T extends Component, THIS> implements InitializingBean {

	@Autowired
	protected ApplicationContext applicationContext;
	@Autowired
	protected LocalizedMessageService messageSource;

	protected T instance;
	protected String label;

	public T getComponent() {
		return this.instance;
	}

	public T build() {
//		if (this.multiLangTextDef != null) {
//			this.instance.setCaption(this.multilangEngine.getFormattedText(this.multiLangTextDef));
//			this.multilangEngine.add(this.instance, this.multiLangTextDef);
//		}
//		if (this.instance instanceof AbstractListing) {
//			this.multilangEngine.add((AbstractListing<?>) this.instance);
//		}
		return this.instance;
	}

	@SuppressWarnings("unchecked")
	public THIS setLabel(final String label) {
		this.label = label;
		return (THIS) this;
	}

	@SuppressWarnings("unchecked")
	public THIS setLabelId(final String labelId) {
		this.label = this.messageSource.getMessage(labelId);
		return (THIS) this;
	}

	@SuppressWarnings("unchecked")
	public THIS setLabelId(final String labelId, final String defaultLabel) {
		this.label = this.messageSource.getMessage(labelId, defaultLabel);
		return (THIS) this;
	}

//	@SuppressWarnings("unchecked")
//	public THIS setMultiLanguageTextDef(final MultiLanguageTextDef multiLangTextDef) {
//		this.multiLangTextDef = multiLangTextDef;
//		return (THIS) this;
//	}

//	@SuppressWarnings("unchecked")
//	public THIS setCaption(final LocalizedString localizedString) {
//		this.multilangEngine.add(this.instance, MultiLanguageTextDef.builder().text(localizedString).build());
//		this.instance.setCaption(localizedString.get(this.multilangEngine.getLanguage().getLanguageCode()));
//		return (THIS) this;
//	}

//	public THIS setCaption(final String captionId) {
//		return this.setCaption(captionId, null);
//	}

//	@SuppressWarnings("unchecked")
//	public THIS setCaption(final String captionId, final String defaultCaption) {
//		this.multilangEngine.add(this.instance, MultiLanguageTextDef.builder().key(captionId).build());
//		if (captionId != null) {
//			final LocalizedMessage localizedMessage = this.messageSource.getLocalizedMessage(captionId);
//			if (localizedMessage.isMissing() && (defaultCaption != null)) {
//				this.instance.setCaption(defaultCaption);
//			} else {
//				this.instance.setCaption(localizedMessage.getValue().get(this.messageSource.getLocale().getLanguage()));
//				this.instance.setDescription(localizedMessage.getDescription().get(this.messageSource.getLocale().getLanguage()));
//			}
//		} else if (defaultCaption != null) {
//			this.instance.setCaption(defaultCaption);
//		}
//		return (THIS) this;
//	}

//	@SuppressWarnings("unchecked")
//	public THIS setDefaultCaption(final String defaultCaption) {
//		this.instance.setCaption(defaultCaption);
//		return (THIS) this;
//	}

//	@SuppressWarnings("unchecked")
//	public THIS setDescription(final String description) {
//		this.instance.setDescription(description);
//		return (THIS) this;
//	}

//	@SuppressWarnings("unchecked")
//	public THIS setWidth(final float width, final Unit unit) {
//		this.instance.setWidth(width, unit);
//		return (THIS) this;
//	}

}
