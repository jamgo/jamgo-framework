package org.jamgo.vaadin.ui.builder;

import org.jamgo.vaadin.builder.base.HasEnabledBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasTextBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.html.H3;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class H3Builder extends JamgoComponentBuilder<H3, H3Builder> implements
	HasSizeBuilder<H3Builder, H3>,
	HasStyleBuilder<H3Builder, H3>,
	HasEnabledBuilder<H3Builder, H3>,
	HasTextBuilder<H3Builder, H3> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new H3();
	}

}