package org.jamgo.vaadin.builder.base;

import com.vaadin.flow.data.value.HasValueChangeMode;
import com.vaadin.flow.data.value.ValueChangeMode;

@SuppressWarnings("rawtypes")
public interface HasValueChangeModeBuilder<T extends HasValueChangeModeBuilder, S extends HasValueChangeMode> extends BaseComponentBuilder<T, S> {

	/**
	 * Sets new value change mode for the component.
	 *
	 * @param valueChangeMode
	 *            new value change mode, or {@code null} to disable the value
	 *            synchronization
	 */
	@SuppressWarnings("unchecked")
	default T setValueChangeMode(final ValueChangeMode valueChangeMode) {
		this.getComponent().setValueChangeMode(valueChangeMode);
		return (T) this;
	}

}
