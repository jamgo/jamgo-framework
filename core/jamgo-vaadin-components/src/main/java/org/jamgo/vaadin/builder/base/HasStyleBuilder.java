package org.jamgo.vaadin.builder.base;

import com.vaadin.flow.component.HasStyle;

@SuppressWarnings("rawtypes")
public interface HasStyleBuilder<T extends HasStyleBuilder, S extends HasStyle> extends BaseComponentBuilder<T, S> {

	/**
	 * Adds a CSS class name to this component.
	 *
	 * @param className
	 *            the CSS class name to add, not <code>null</code>
	 */
	@SuppressWarnings("unchecked")
	default T addClassName(final String className) {
		this.getComponent().addClassName(className);
		return (T) this;
	}

	/**
	 * Sets the CSS class names of this component. This method overwrites any
	 * previous set class names.
	 *
	 * @param className
	 *            a space-separated string of class names to set, or
	 *            <code>null</code> to remove all class names
	 */
	@SuppressWarnings("unchecked")
	default T setClassName(final String className) {
		this.getComponent().setClassName(className);
		return (T) this;
	}

	/**
	 * Sets or removes the given class name for this component.
	 *
	 * @param className
	 *            the class name to set or remove, not <code>null</code>
	 * @param set
	 *            <code>true</code> to set the class name, <code>false</code> to
	 *            remove it
	 */
	@SuppressWarnings("unchecked")
	default T setClassName(final String className, final boolean set) {
		this.getComponent().setClassName(className, set);
		return (T) this;
	}

	/**
	 * Adds one or more CSS class names to this component. Multiple class names can be
	 * specified by using multiple parameters.
	 *
	 * @param classNames the CSS class name or class names to be added to the component
	 */
	@SuppressWarnings("unchecked")
	default T addClassNames(final String... classNames) {
		this.getComponent().addClassNames(classNames);
		return (T) this;
	}

}
