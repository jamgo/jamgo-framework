package org.jamgo.vaadin.ui.builder;

import java.util.Optional;

import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasAutocapitalizeBuilder;
import org.jamgo.vaadin.builder.base.HasAutocompleteBuilder;
import org.jamgo.vaadin.builder.base.HasAutocorrectBuilder;
import org.jamgo.vaadin.builder.base.HasHelperBuilder;
import org.jamgo.vaadin.builder.base.HasPrefixAndSuffixBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.jamgo.vaadin.builder.base.HasValueChangeModeBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.textfield.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TextFieldBuilder extends JamgoComponentBuilder<TextField, TextFieldBuilder> implements
	HasSizeBuilder<TextFieldBuilder, TextField>,
	HasValidationBuilder<TextFieldBuilder, TextField>,
	HasValueBuilder<TextFieldBuilder, TextField>,
	HasValueChangeModeBuilder<TextFieldBuilder, TextField>,
	HasPrefixAndSuffixBuilder<TextFieldBuilder, TextField>,
	HasAutocompleteBuilder<TextFieldBuilder, TextField>,
	HasAutocapitalizeBuilder<TextFieldBuilder, TextField>,
	HasAutocorrectBuilder<TextFieldBuilder, TextField>,
	HasHelperBuilder<TextFieldBuilder, TextField>,
	HasStyleBuilder<TextFieldBuilder, TextField>,
	HasThemeBuilder<TextFieldBuilder, TextField>,
	FocusableBuilder<TextFieldBuilder, TextField, TextField> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new TextField();
		this.instance.setWidth(100, Unit.PERCENTAGE);
	}

	@Override
	public TextField build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));

		return this.instance;
	}
}