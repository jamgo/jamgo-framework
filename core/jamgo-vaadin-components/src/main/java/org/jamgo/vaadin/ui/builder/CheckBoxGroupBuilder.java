package org.jamgo.vaadin.ui.builder;

import java.util.Optional;

import org.jamgo.vaadin.builder.base.HasHelperBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.checkbox.CheckboxGroup;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class CheckBoxGroupBuilder<T> extends JamgoComponentBuilder<CheckboxGroup<T>, CheckBoxGroupBuilder<T>> implements
	HasSizeBuilder<CheckBoxGroupBuilder<T>, CheckboxGroup<T>>,
	HasStyleBuilder<CheckBoxGroupBuilder<T>, CheckboxGroup<T>>,
	HasValueBuilder<CheckBoxGroupBuilder<T>, CheckboxGroup<T>>,
	HasThemeBuilder<CheckBoxGroupBuilder<T>, CheckboxGroup<T>>,
	HasValidationBuilder<CheckBoxGroupBuilder<T>, CheckboxGroup<T>>,
	HasHelperBuilder<CheckBoxGroupBuilder<T>, CheckboxGroup<T>> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new CheckboxGroup<>();
	}

	@Override
	public CheckboxGroup<T> build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));

		return this.instance;
	}

}