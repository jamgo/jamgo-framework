package org.jamgo.vaadin.ui.builder;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.shared.ThemeVariant;
import com.vaadin.flow.component.tabs.TabSheet;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasThemeVariantBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TabSheetBuilder extends JamgoComponentBuilder<TabSheet, TabSheetBuilder> implements
	HasStyleBuilder<TabSheetBuilder, TabSheet>,
	HasSizeBuilder<TabSheetBuilder, TabSheet>,
	HasThemeVariantBuilder<TabSheetBuilder, TabSheet, ThemeVariant> {
	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new TabSheet();
		this.instance.setWidth(100, Unit.PERCENTAGE);
	}
	@Override
	public TabSheet build() {
		return this.instance;
	}
}
