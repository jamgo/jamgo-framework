package org.jamgo.vaadin.renderers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.ValueProvider;

public class HtmlRenderer<T> extends ComponentRenderer<Html, T> {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(HtmlRenderer.class);

	public HtmlRenderer(final ValueProvider<T, String> valueProvider) {
		super(o -> {
			Html html = null;
			try {
				html = new Html("<span>" + valueProvider.apply(o) + "</span>");
			} catch (final IllegalArgumentException iae) {
				HtmlRenderer.logger.error("There was an error when trying to render HTML for {}. \n\n {}", valueProvider.apply(o), iae.getMessage());
				generateNotificationError(iae);
			}

			return html;
		});
	}

	private static void generateNotificationError(final IllegalArgumentException iae) {
		// When creating a notification using the constructor,
		// the duration is 0-sec by default which means that
		// the notification does not close automatically.
		final Notification notification = new Notification();
		notification.addThemeVariants(NotificationVariant.LUMO_ERROR);

		final Div text = new Div(new Text(iae.getMessage()));

		final Button closeButton = new Button(new Icon("lumo", "cross"));
		closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
		closeButton.getElement().setAttribute("aria-label", "Close");
		closeButton.addClickListener(event -> {
			notification.close();
		});

		final HorizontalLayout layout = new HorizontalLayout(text, closeButton);
		layout.setAlignItems(FlexComponent.Alignment.CENTER);

		notification.add(layout);
		notification.open();
	}
}
