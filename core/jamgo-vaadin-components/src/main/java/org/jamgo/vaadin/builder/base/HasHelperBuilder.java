package org.jamgo.vaadin.builder.base;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasHelper;

@SuppressWarnings("rawtypes")
public interface HasHelperBuilder<T extends HasHelperBuilder, S extends HasHelper> extends BaseComponentBuilder<T, S> {

	/**
	 * <p>
	 * String used for the helper text. It shows a text adjacent to the field that
	 * can be used, e.g., to inform to the users which values it expects. Example:
	 * a text "The password must contain numbers" for the PasswordField.
	 * </p>
	 * 
	 * <p>
	 * In case both {@link #setHelperText(String)} and
	 * {@link #setHelperComponent(Component)} are used, only the element defined
	 * by {@link #setHelperComponent(Component)} will be visible, regardless of
	 * the order on which they are defined.
	 * </p>
	 *
	 * @param helperText
	 *            the String value to set
	 */
	@SuppressWarnings("unchecked")
	default T setHelperText(final String helperText) {
		this.getComponent().setHelperText(helperText);
		return (T) this;
	}

	/**
	 * Adds the given component into helper slot of component, replacing any
	 * existing helper component. It adds the component adjacent to the field that
	 * can be used, e.g., to inform to the users which values it expects. Example:
	 * a component that shows the password strength for the PasswordField.
	 * 
	 * @param component
	 *            the component to set, can be {@code null} to remove existing
	 *            helper component
	 * 
	 * @see #setHelperText(String)
	 */
	@SuppressWarnings("unchecked")
	default T setHelperComponent(final Component component) {
		this.getComponent().setHelperComponent(component);
		return (T) this;
	}

}
