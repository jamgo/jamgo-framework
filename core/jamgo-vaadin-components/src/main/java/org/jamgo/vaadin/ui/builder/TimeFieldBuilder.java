package org.jamgo.vaadin.ui.builder;

import java.util.Optional;

import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasHelperBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.timepicker.TimePicker;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TimeFieldBuilder extends JamgoComponentBuilder<TimePicker, TimeFieldBuilder> implements
	HasSizeBuilder<TimeFieldBuilder, TimePicker>,
	HasStyleBuilder<TimeFieldBuilder, TimePicker>,
	HasValueBuilder<TimeFieldBuilder, TimePicker>,
	HasValidationBuilder<TimeFieldBuilder, TimePicker>,
	HasHelperBuilder<TimeFieldBuilder, TimePicker>,
	FocusableBuilder<TimeFieldBuilder, TimePicker, TimePicker> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new TimePicker();
	}

	@Override
	public TimePicker build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));

		return this.instance;
	}
}