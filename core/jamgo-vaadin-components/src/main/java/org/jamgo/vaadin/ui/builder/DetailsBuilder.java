package org.jamgo.vaadin.ui.builder;

import java.util.function.Consumer;

import org.jamgo.vaadin.builder.base.HasEnabledBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.components.JamgoComponentConstants;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.details.Details;

@org.springframework.stereotype.Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DetailsBuilder extends JamgoComponentBuilder<Details, DetailsBuilder> implements
	HasEnabledBuilder<DetailsBuilder, Details>,
	HasThemeBuilder<DetailsBuilder, Details> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new Details();
	}

	/**
	 * Creates a text wrapper and sets a summary via
	 * {@link Details#setSummary(Component)}
	 */
	public DetailsBuilder setSummaryText(final String summary) {
		this.getComponent().setSummaryText(summary);
		return this;
	}

	/**
	 * Sets the component content
	 *
	 * @see Details#getContent()
	 * @param content the content of the component to set, or <code>null</code>
	 *        to remove any previously set content
	 */
	public DetailsBuilder setContent(final Component content) {
		this.getComponent().setContent(content);
		return this;
	}

	/**
	 * <p>
	 * True if the details are opened and the content is displayed
	 * </p>
	 *
	 * @param opened the boolean value to set
	 */
	public DetailsBuilder setOpened(final boolean opened) {
		this.getComponent().setOpened(opened);
		return this;
	}

	@Override
	public Details build() {
		final Details details = super.build();

		final Consumer<String> labelSetter = (label) -> details.setSummaryText(label);
		ComponentUtil.setData(this.instance, JamgoComponentConstants.PROPERTY_LABEL_SETTER, labelSetter);
		ComponentUtil.setData(this.instance, JamgoComponentConstants.PROPERTY_DEFAULT_WIDTH, ComponentColspan.FULL_WIDTH);

		return details;
	}
}