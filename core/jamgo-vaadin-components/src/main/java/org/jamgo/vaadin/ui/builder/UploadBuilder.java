package org.jamgo.vaadin.ui.builder;

import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.upload.Upload;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class UploadBuilder extends JamgoComponentBuilder<Upload, UploadBuilder> implements
	HasSizeBuilder<UploadBuilder, Upload>,
	HasStyleBuilder<UploadBuilder, Upload> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new Upload();
	}

}