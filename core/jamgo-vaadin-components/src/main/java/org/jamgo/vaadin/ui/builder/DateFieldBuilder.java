package org.jamgo.vaadin.ui.builder;

import java.util.Locale;
import java.util.Optional;

import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasHelperBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datepicker.DatePicker.DatePickerI18n;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DateFieldBuilder extends AbstractDateFieldBuilder<DatePicker, DateFieldBuilder> implements
	HasSizeBuilder<DateFieldBuilder, DatePicker>,
	HasStyleBuilder<DateFieldBuilder, DatePicker>,
	HasValueBuilder<DateFieldBuilder, DatePicker>,
	HasValidationBuilder<DateFieldBuilder, DatePicker>,
	HasHelperBuilder<DateFieldBuilder, DatePicker>,
	FocusableBuilder<DateFieldBuilder, DatePicker, DatePicker> {

	@Autowired
	protected LocalizedMessageService messageService;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new DatePicker();
		this.instance.setWidth(100, Unit.PERCENTAGE);
		this.instance.setLocale(this.messageService.getLocale());
		this.instance.setI18n(this.getDatePickerI18n(this.messageService.getLocale()));
	}

	public DateFieldBuilder setLocale(final Locale locale) {
		this.getComponent().setLocale(locale);
		return this;
	}

	public DateFieldBuilder setI18n(final DatePickerI18n i18n) {
		this.getComponent().setI18n(i18n);
		return this;
	}

	@Override
	public DatePicker build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));

		return this.instance;
	}

}