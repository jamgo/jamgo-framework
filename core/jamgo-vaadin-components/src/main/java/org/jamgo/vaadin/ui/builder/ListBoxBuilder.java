package org.jamgo.vaadin.ui.builder;

import org.jamgo.vaadin.builder.base.HasEnabledBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.listbox.ListBox;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ListBoxBuilder<T> extends JamgoComponentBuilder<ListBox<T>, ListBoxBuilder<T>> implements
	HasSizeBuilder<ListBoxBuilder<T>, ListBox<T>>,
	HasValueBuilder<ListBoxBuilder<T>, ListBox<T>>,
	HasEnabledBuilder<ListBoxBuilder<T>, ListBox<T>> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new ListBox<>();
	}

}