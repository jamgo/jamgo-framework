package org.jamgo.vaadin.ui.builder;

import java.util.Locale;
import java.util.Optional;

import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasHelperBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.datepicker.DatePicker.DatePickerI18n;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DateTimeFieldBuilder extends AbstractDateFieldBuilder<DateTimePicker, DateTimeFieldBuilder> implements
	HasSizeBuilder<DateTimeFieldBuilder, DateTimePicker>,
	HasStyleBuilder<DateTimeFieldBuilder, DateTimePicker>,
	HasValueBuilder<DateTimeFieldBuilder, DateTimePicker>,
	HasValidationBuilder<DateTimeFieldBuilder, DateTimePicker>,
	HasThemeBuilder<DateTimeFieldBuilder, DateTimePicker>,
	HasHelperBuilder<DateTimeFieldBuilder, DateTimePicker>,
	FocusableBuilder<DateTimeFieldBuilder, DateTimePicker, DateTimePicker> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new DateTimePicker();
		this.instance.setWidth(100, Unit.PERCENTAGE);
		this.instance.setLocale(this.messageService.getLocale());
		this.instance.setDatePickerI18n(this.getDatePickerI18n(this.messageService.getLocale()));
	}

	public DateTimeFieldBuilder setLocale(final Locale locale) {
		this.getComponent().setLocale(locale);
		return this;
	}

	public DateTimeFieldBuilder setI18n(final DatePickerI18n i18n) {
		this.getComponent().setDatePickerI18n(i18n);
		return this;
	}

	@Override
	public DateTimePicker build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));

		return this.instance;
	}

}