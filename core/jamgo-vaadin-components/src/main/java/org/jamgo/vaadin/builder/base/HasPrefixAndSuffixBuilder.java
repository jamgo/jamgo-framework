package org.jamgo.vaadin.builder.base;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.HasPrefixAndSuffix;

@SuppressWarnings("rawtypes")
public interface HasPrefixAndSuffixBuilder<T extends HasPrefixAndSuffixBuilder, S extends HasPrefixAndSuffix> extends BaseComponentBuilder<T, S> {

	/**
	 * Adds the given component into this field before the content, replacing
	 * any existing prefix component.
	 * <p>
	 * This is most commonly used to add a simple icon or static text into the
	 * field.
	 * 
	 * @param component
	 *            the component to set, can be {@code null} to remove existing
	 *            prefix component
	 */
	@SuppressWarnings("unchecked")
	default T setPrefixComponent(final Component component) {
		this.getComponent().setPrefixComponent(component);
		return (T) this;
	}

	/**
	 * Adds the given component into this field after the content, replacing any
	 * existing suffix component.
	 * <p>
	 * This is most commonly used to add a simple icon or static text into the
	 * field.
	 * 
	 * @param component
	 *            the component to set, can be {@code null} to remove existing
	 *            suffix component
	 */
	@SuppressWarnings("unchecked")
	default T setSuffixComponent(final Component component) {
		this.getComponent().setSuffixComponent(component);
		return (T) this;
	}

}
