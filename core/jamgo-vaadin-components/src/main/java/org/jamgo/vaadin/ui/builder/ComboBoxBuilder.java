package org.jamgo.vaadin.ui.builder;

import java.util.Optional;

import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasHelperBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.data.renderer.Renderer;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ComboBoxBuilder<T> extends JamgoComponentBuilder<ComboBox<T>, ComboBoxBuilder<T>> implements
	HasSizeBuilder<ComboBoxBuilder<T>, ComboBox<T>>,
	HasStyleBuilder<ComboBoxBuilder<T>, ComboBox<T>>,
	HasValueBuilder<ComboBoxBuilder<T>, ComboBox<T>>,
	HasValidationBuilder<ComboBoxBuilder<T>, ComboBox<T>>,
	HasHelperBuilder<ComboBoxBuilder<T>, ComboBox<T>>,
	FocusableBuilder<ComboBoxBuilder<T>, ComboBox<T>, ComboBox<T>> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new ComboBox<>();
	}

	@Override
	public ComboBox<T> build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));

		return this.instance;
	}

	/**
	 * Sets the item label generator that is used to produce the strings shown
	 * in the combo box for each item. By default,
	 * {@link String#valueOf(Object)} is used.
	 * <p>
	 * When the {@link #setRenderer(Renderer)} is used, the ItemLabelGenerator
	 * is only used to show the selected item label.
	 *
	 * @param itemLabelGenerator
	 *            the item label provider to use, not null
	 */
	public ComboBoxBuilder<T> setItemLabelGenerator(final ItemLabelGenerator<T> itemLabelGenerator) {
		this.getComponent().setItemLabelGenerator(itemLabelGenerator);
		return this;
	}

	/**
	 * Sets the TemplateRenderer responsible to render the individual items in
	 * the list of possible choices of the ComboBox. It doesn't affect how the
	 * selected item is rendered - that can be configured by using
	 * {@link #setItemLabelGenerator(ItemLabelGenerator)}.
	 *
	 * @param renderer
	 *            a renderer for the items in the selection list of the
	 *            ComboBox, not <code>null</code>
	 *
	 *            Note that filtering of the ComboBox is not affected by the
	 *            renderer that is set here. Filtering is done on the original
	 *            values and can be affected by
	 *            {@link #setItemLabelGenerator(ItemLabelGenerator)}.
	 */
	public ComboBoxBuilder<T> setRenderer(final Renderer<T> renderer) {
		this.getComponent().setRenderer(renderer);
		return this;
	}

}