package org.jamgo.vaadin.builder.base;

import com.vaadin.flow.component.HasValue;

@SuppressWarnings("rawtypes")
public interface HasValueBuilder<T extends HasValueBuilder, S extends HasValue<?, ?>> extends BaseComponentBuilder<T, S> {

	/**
	 * Sets the read-only mode of this {@code HasValue} to given mode. The user
	 * can't change the value when in read-only mode.
	 * <p>
	 * A {@code HasValue} with a visual component in read-only mode typically
	 * looks visually different to signal to the user that the value cannot be
	 * edited.
	 *
	 * @param readOnly
	 *            a boolean value specifying whether the component is put
	 *            read-only mode or not
	 */
	@SuppressWarnings("unchecked")
	default T setReadOnly(final boolean readOnly) {
		this.getComponent().setReadOnly(readOnly);
		return (T) this;
	}

	/**
	 * Sets the required indicator visible or not.
	 * <p>
	 * If set visible, it is visually indicated in the user interface.
	 * <p>
	 * The method is intended to be used with <code>Binder</code> which does
	 * server-side validation. In case HTML element has its own (client-side)
	 * validation it should be disabled when
	 * <code>setRequiredIndicatorVisible(true)</code> is called and re-enabled
	 * back on <code>setRequiredIndicatorVisible(false)</code>. It's
	 * responsibility of each component implementation to follow the contract so
	 * that the method call doesn't do anything else than show/hide the
	 * "required" indication. Usually components provide their own
	 * <code>setRequired</code> method which should be called in case the
	 * client-side validation is required.
	 *
	 * @param requiredIndicatorVisible
	 *            <code>true</code> to make the required indicator visible,
	 *            <code>false</code> if not
	 */
	@SuppressWarnings("unchecked")
	default T setRequiredIndicatorVisible(final boolean requiredIndicatorVisible) {
		this.getComponent().setRequiredIndicatorVisible(requiredIndicatorVisible);
		return (T) this;
	}

}
