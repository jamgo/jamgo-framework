package org.jamgo.vaadin.builder.base;

import com.vaadin.flow.component.textfield.Autocomplete;
import com.vaadin.flow.component.textfield.HasAutocomplete;

@SuppressWarnings("rawtypes")
public interface HasAutocompleteBuilder<T extends HasAutocompleteBuilder, S extends HasAutocomplete> extends BaseComponentBuilder<T, S> {

	/**
	 * Sets the {@link Autocomplete} attribute for indicating whether the value
	 * of this component can be automatically completed by the browser.
	 * <p>
	 * If not set, devices may apply their own defaults.
	 * <p>
	 * See <a href=
	 * "https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#attr-autocomplete">autocomplete
	 * attribute</a> for more information.
	 *
	 * @param autocomplete
	 *            the {@code autocomplete} value, or {@code null} to unset
	 */
	@SuppressWarnings("unchecked")
	default T setAutocomplete(final Autocomplete autocomplete) {
		this.getComponent().setAutocomplete(autocomplete);
		return (T) this;
	}

}
