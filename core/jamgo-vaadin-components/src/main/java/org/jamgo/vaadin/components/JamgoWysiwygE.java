package org.jamgo.vaadin.components;

import org.apache.commons.lang3.StringUtils;
import org.vaadin.pekka.WysiwygE;

public class JamgoWysiwygE extends WysiwygE {

	private static final long serialVersionUID = 1L;

	private static final String EMPTY_VALUE = "<p><br></p>";

	@Override
	public void setValue(final String value) {
		if (StringUtils.isEmpty(value)) {
			super.setValue(JamgoWysiwygE.EMPTY_VALUE);
		} else {
			super.setValue(value);
		}
	}

}
