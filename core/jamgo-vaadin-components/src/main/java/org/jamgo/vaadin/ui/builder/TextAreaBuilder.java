package org.jamgo.vaadin.ui.builder;

import java.util.Optional;

import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasAutocapitalizeBuilder;
import org.jamgo.vaadin.builder.base.HasAutocompleteBuilder;
import org.jamgo.vaadin.builder.base.HasAutocorrectBuilder;
import org.jamgo.vaadin.builder.base.HasHelperBuilder;
import org.jamgo.vaadin.builder.base.HasPrefixAndSuffixBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.jamgo.vaadin.builder.base.HasValueChangeModeBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.textfield.TextArea;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TextAreaBuilder extends JamgoComponentBuilder<TextArea, TextAreaBuilder> implements
	HasSizeBuilder<TextAreaBuilder, TextArea>,
	HasValueBuilder<TextAreaBuilder, TextArea>,
	HasValidationBuilder<TextAreaBuilder, TextArea>,
	HasValueChangeModeBuilder<TextAreaBuilder, TextArea>,
	HasPrefixAndSuffixBuilder<TextAreaBuilder, TextArea>,
	HasAutocompleteBuilder<TextAreaBuilder, TextArea>,
	HasAutocapitalizeBuilder<TextAreaBuilder, TextArea>,
	HasAutocorrectBuilder<TextAreaBuilder, TextArea>,
	HasHelperBuilder<TextAreaBuilder, TextArea>,
	HasStyleBuilder<TextAreaBuilder, TextArea>,
	HasThemeBuilder<TextAreaBuilder, TextArea>,
	FocusableBuilder<TextAreaBuilder, TextArea, TextArea> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new TextArea();
		this.instance.setWidthFull();
	}

	@Override
	public TextArea build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));

		return this.instance;
	}

}