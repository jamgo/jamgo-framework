package org.jamgo.vaadin.builder.base;

import com.vaadin.flow.component.shared.HasThemeVariant;
import com.vaadin.flow.component.shared.ThemeVariant;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface HasThemeVariantBuilder<T extends HasThemeVariantBuilder, S extends HasThemeVariant, TVariantEnum extends ThemeVariant> extends BaseComponentBuilder<T, S> {
	/**
	 * Adds theme variants to the component.
	 *
	 * @param variants
	 *            theme variants to add
	 */
	@SuppressWarnings("unchecked")
	default T addThemeVariants(TVariantEnum... variants) {
		this.getComponent().getThemeNames()
			.addAll(Stream.of(variants).map(TVariantEnum::getVariantName)
				.collect(Collectors.toList()));
		return (T) this;
	}

	/**
	 * Removes theme variants from the component.
	 *
	 * @param variants
	 *            theme variants to remove
	 */
	@SuppressWarnings("unchecked")
	default T removeThemeVariants(TVariantEnum... variants) {
		this.getComponent().getThemeNames()
			.removeAll(Stream.of(variants).map(TVariantEnum::getVariantName)
				.collect(Collectors.toList()));
		return (T) this;
	}
}
