package org.jamgo.vaadin.builder.base;

import com.vaadin.flow.component.HasValidation;

@SuppressWarnings("rawtypes")
public interface HasValidationBuilder<T extends HasValidationBuilder, S extends HasValidation> extends BaseComponentBuilder<T, S> {

	/**
	 * Sets an error message to the component.
	 * <p>
	 * The Web Component is responsible for deciding when to show the error
	 * message to the user, and this is usually triggered by triggering the
	 * invalid state for the Web Component. Which means that there is no need to
	 * clean up the message when component becomes valid (otherwise it may lead
	 * to undesired visual effects).
	 *
	 * @param errorMessage
	 *            a new error message
	 */
	@SuppressWarnings("unchecked")
	default T setErrorMessage(final String errorMessage) {
		this.getComponent().setErrorMessage(errorMessage);
		return (T) this;
	}

	/**
	 * Sets the validity of the component input.
	 * <p>
	 * When component becomes valid it hides the error message by itself, so
	 * there is no need to clean up the error message via the
	 * {@link #setErrorMessage(String)} call.
	 *
	 * @param invalid
	 *            new value for component input validity
	 */
	@SuppressWarnings("unchecked")
	default T setInvalid(final boolean invalid) {
		this.getComponent().setInvalid(invalid);
		return (T) this;
	}

}
