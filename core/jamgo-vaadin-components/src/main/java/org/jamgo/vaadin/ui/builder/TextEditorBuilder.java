package org.jamgo.vaadin.ui.builder;

import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.jamgo.vaadin.builder.base.HasValueChangeModeBuilder;
import org.jamgo.vaadin.components.JamgoComponentConstants;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;
import org.jamgo.vaadin.components.JamgoWysiwygE;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vaadin.pekka.WysiwygE.Tool;

import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.Unit;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TextEditorBuilder extends JamgoComponentBuilder<JamgoWysiwygE, TextEditorBuilder> implements
	HasSizeBuilder<TextEditorBuilder, JamgoWysiwygE>,
	HasValueBuilder<TextEditorBuilder, JamgoWysiwygE>,
	HasValueChangeModeBuilder<TextEditorBuilder, JamgoWysiwygE>,
	HasStyleBuilder<TextEditorBuilder, JamgoWysiwygE> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new JamgoWysiwygE();
		this.instance.setHeight(20.0f, Unit.EM);
		this.instance.setWidth(100, Unit.PERCENTAGE);
		this.instance.setToolsInvisible(Tool.LINK, Tool.IMAGE, Tool.AUDIO, Tool.VIDEO, Tool.TABLE);
		this.instance.setValue("<p></br></p>");
		ComponentUtil.setData(this.instance, JamgoComponentConstants.PROPERTY_DEFAULT_WIDTH, ComponentColspan.FULL_WIDTH);
	}

	@Override
	public JamgoWysiwygE build() {
		return this.instance;
	}

}