package org.jamgo.vaadin.ui.builder;

import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.components.JamgoComponentConstants;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.GridSelectionModel;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.data.provider.DataProvider;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class GridBuilder<T> extends JamgoComponentBuilder<Grid<T>, GridBuilder<T>> implements
	HasStyleBuilder<GridBuilder<T>, Grid<T>>,
	HasSizeBuilder<GridBuilder<T>, Grid<T>>,
	HasThemeBuilder<GridBuilder<T>, Grid<T>>,
	FocusableBuilder<GridBuilder<T>, Grid<T>, Grid<T>> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new Grid<>();
		ComponentUtil.setData(this.instance, JamgoComponentConstants.PROPERTY_DEFAULT_WIDTH, ComponentColspan.FULL_WIDTH);
	}

	/**
	 * Sets the grid's selection mode.
	 * <p>
	 * To use your custom selection model, you can use
	 * {@link Grid#setSelectionModel(GridSelectionModel, SelectionMode)}, see
	 * existing selection model implementations for example.
	 *
	 * @param selectionMode the selection mode to switch to, not {@code null}
	 * @return the used selection model
	 * @see SelectionMode
	 * @see GridSelectionModel
	 * @see Grid#setSelectionModel(GridSelectionModel, SelectionMode)
	 */
	public GridBuilder<T> setSelectionMode(final SelectionMode selectionMode) {
		this.getComponent().setSelectionMode(selectionMode);
		return this;
	}

	/**
	 * Adds theme variants to the component.
	 *
	 * @param variants theme variants to add
	 */
	public GridBuilder<T> addThemeVariants(final GridVariant... variants) {
		this.getComponent().addThemeVariants(variants);
		return this;
	}

	/**
	 * If <code>true</code>, the grid's height is defined by its rows. All items
	 * are fetched from the {@link DataProvider}, and the Grid shows no vertical
	 * scroll bar.
	 * <p>
	 * Note: <code>setHeightByRows</code> disables the grid's virtual scrolling
	 * so that all the rows are rendered in the DOM at once. If the grid has a
	 * large number of items, using the feature is discouraged to avoid
	 * performance issues.
	 *
	 * @param heightByRows <code>true</code> to make Grid compute its height by
	 *        the number of rows, <code>false</code> for the default behavior
	 */
	public GridBuilder<T> setAllRowsVisible(final boolean heightByRows) {
		this.getComponent().setAllRowsVisible(heightByRows);
		return this;
	}

}