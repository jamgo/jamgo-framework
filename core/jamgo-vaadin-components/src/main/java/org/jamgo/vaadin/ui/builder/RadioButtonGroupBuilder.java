package org.jamgo.vaadin.ui.builder;

import org.jamgo.vaadin.builder.base.HasEnabledBuilder;
import org.jamgo.vaadin.builder.base.HasHelperBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasThemeBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.radiobutton.RadioButtonGroup;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RadioButtonGroupBuilder<T> extends JamgoComponentBuilder<RadioButtonGroup<T>, RadioButtonGroupBuilder<T>> implements
	HasValueBuilder<RadioButtonGroupBuilder<T>, RadioButtonGroup<T>>,
	HasValidationBuilder<RadioButtonGroupBuilder<T>, RadioButtonGroup<T>>,
	HasHelperBuilder<RadioButtonGroupBuilder<T>, RadioButtonGroup<T>>,
	HasStyleBuilder<RadioButtonGroupBuilder<T>, RadioButtonGroup<T>>,
	HasThemeBuilder<RadioButtonGroupBuilder<T>, RadioButtonGroup<T>>,
	HasEnabledBuilder<RadioButtonGroupBuilder<T>, RadioButtonGroup<T>> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new RadioButtonGroup<>();
	}

}