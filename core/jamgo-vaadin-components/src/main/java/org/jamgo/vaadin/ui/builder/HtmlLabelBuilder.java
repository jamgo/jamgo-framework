package org.jamgo.vaadin.ui.builder;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.html.Span;

/**
 *
 * @Deprecated Use {@link org.jamgo.vaadin.ui.LabelBuilder} with {@link org.jamgo.vaadin.ui.engine.MultiLanguageTextDef} instead
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@Deprecated
public class HtmlLabelBuilder extends JamgoComponentBuilder<Span, HtmlLabelBuilder> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new Span();
	}

}