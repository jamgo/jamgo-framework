package org.jamgo.vaadin.builder.base;

import com.vaadin.flow.component.textfield.HasAutocorrect;

@SuppressWarnings("rawtypes")
public interface HasAutocorrectBuilder<T extends HasAutocorrectBuilder, S extends HasAutocorrect> extends BaseComponentBuilder<T, S> {

	/**
	 * Enable or disable {@code autocorrect} for the field.
	 * <p>
	 * If not set, devices may apply their own defaults.
	 * <p>
	 * <em>Note:</em> <a href=
	 * "https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#attr-autocorrect">This
	 * only supported by Safari</a>.
	 *
	 * @param autocorrect
	 *            true to enable {@code autocorrect}, false to disable
	 */
	@SuppressWarnings("unchecked")
	default T setAutocorrect(final boolean autocorrect) {
		this.getComponent().setAutocorrect(autocorrect);
		return (T) this;
	}

}
