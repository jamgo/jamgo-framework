package org.jamgo.vaadin.ui.builder;

import org.jamgo.vaadin.builder.base.HasEnabledBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasTextBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.server.AbstractStreamResource;
import com.vaadin.flow.server.StreamResource;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ImageBuilder extends JamgoComponentBuilder<Image, ImageBuilder> implements
	HasSizeBuilder<ImageBuilder, Image>,
	HasStyleBuilder<ImageBuilder, Image>,
	HasEnabledBuilder<ImageBuilder, Image>,
	HasTextBuilder<ImageBuilder, Image> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new Image();
		this.instance.setAlt("");
	}

	/**
	 * Sets the image URL.
	 *
	 * @param src
	 *            the image URL
	 */
	public ImageBuilder setSrc(final String src) {
		this.getComponent().setSrc(src);
		return this;
	}

	/**
	 * Sets the image URL with the URL of the given {@link StreamResource}.
	 *
	 * @param src
	 *            the resource value, not null
	 */
	public ImageBuilder setSrc(final AbstractStreamResource src) {
		this.getComponent().setSrc(src);
		return this;
	}

	/**
	 * Sets the alternate text for the image.
	 *
	 * @param alt
	 *            the alternate text
	 */
	public ImageBuilder setAlt(final String alt) {
		this.getComponent().setAlt(alt);
		return this;
	}

}