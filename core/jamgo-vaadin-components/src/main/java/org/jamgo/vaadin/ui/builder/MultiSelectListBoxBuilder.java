package org.jamgo.vaadin.ui.builder;

import org.jamgo.vaadin.builder.base.HasEnabledBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.listbox.MultiSelectListBox;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MultiSelectListBoxBuilder<T> extends JamgoComponentBuilder<MultiSelectListBox<T>, MultiSelectListBoxBuilder<T>> implements
	HasSizeBuilder<MultiSelectListBoxBuilder<T>, MultiSelectListBox<T>>,
	HasValueBuilder<MultiSelectListBoxBuilder<T>, MultiSelectListBox<T>>,
	HasEnabledBuilder<MultiSelectListBoxBuilder<T>, MultiSelectListBox<T>> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new MultiSelectListBox<>();
	}

}