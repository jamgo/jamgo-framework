package org.jamgo.vaadin.ui.builder;

import java.time.DayOfWeek;
import java.time.Month;
import java.time.format.TextStyle;
import java.time.temporal.WeekFields;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jamgo.services.impl.LocalizedMessageService;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datepicker.DatePicker.DatePickerI18n;

public abstract class AbstractDateFieldBuilder<P extends Component, B> extends JamgoComponentBuilder<P, B> {

	@Autowired
	protected LocalizedMessageService messageService;
	private final Map<Locale, DatePickerI18n> datePickerI18ns = new HashMap<>();

	public AbstractDateFieldBuilder() {
		super();
	}

	protected DatePickerI18n getDatePickerI18n(final Locale locale) {
		DatePickerI18n datePickerI18n = this.datePickerI18ns.get(locale);

		if (datePickerI18n == null) {
			final DayOfWeek firstDayOfWeek = WeekFields.of(locale).getFirstDayOfWeek();
			final List<String> monthNames = Stream.of(Month.values())
				.map(each -> each.getDisplayName(TextStyle.FULL_STANDALONE, locale))
				.collect(Collectors.toList());
			final List<String> weekDays = Stream.of(DayOfWeek.values())
				.map(each -> each.getDisplayName(TextStyle.FULL_STANDALONE, locale))
				.collect(Collectors.toList());
			Collections.rotate(weekDays, 1);
			final List<String> weekDaysShort = Stream.of(DayOfWeek.values())
				.map(each -> each.getDisplayName(TextStyle.SHORT, locale))
				.collect(Collectors.toList());
			Collections.rotate(weekDaysShort, 1);
			datePickerI18n = new DatePicker.DatePickerI18n()
				.setCalendar(this.messageService.getMessage("datePicker.calendar"))
				.setCancel(this.messageService.getMessage("datePicker.cancel"))
				.setClear(this.messageService.getMessage("datePicker.clean"))
				.setToday(this.messageService.getMessage("datePicker.today"))
				.setWeek(this.messageService.getMessage("datePicker.week"))
				.setMonthNames(monthNames)
				.setWeekdays(weekDays)
				.setWeekdaysShort(weekDaysShort)
				.setFirstDayOfWeek(firstDayOfWeek.getValue() % 7);
			this.datePickerI18ns.put(locale, datePickerI18n);
		}

		return datePickerI18n;
	}

}