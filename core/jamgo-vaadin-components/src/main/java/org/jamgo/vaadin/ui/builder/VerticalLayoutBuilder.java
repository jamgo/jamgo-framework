package org.jamgo.vaadin.ui.builder;

import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;

@org.springframework.stereotype.Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class VerticalLayoutBuilder extends JamgoComponentBuilder<VerticalLayout, VerticalLayoutBuilder> implements
	HasSizeBuilder<VerticalLayoutBuilder, VerticalLayout>,
	HasStyleBuilder<VerticalLayoutBuilder, VerticalLayout> {

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new VerticalLayout();
	}

	@Override
	public VerticalLayout build() {
		return this.instance;
	}

}