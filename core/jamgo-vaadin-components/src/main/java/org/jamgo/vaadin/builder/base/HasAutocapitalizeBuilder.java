package org.jamgo.vaadin.builder.base;

import com.vaadin.flow.component.textfield.Autocapitalize;
import com.vaadin.flow.component.textfield.HasAutocapitalize;

@SuppressWarnings("rawtypes")
public interface HasAutocapitalizeBuilder<T extends HasAutocapitalizeBuilder, S extends HasAutocapitalize> extends BaseComponentBuilder<T, S> {

	/**
	 * Sets the {@link Autocapitalize} attribute for indicating whether the
	 * value of this component can be automatically completed by the browser.
	 * <p>
	 * If not set, devices may apply their own default.
	 * <p>
	 * <em>Note:</em> <a href=
	 * "https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/autocapitalize">
	 * This attribute doesn't affect behavior when typing on a physical keyboard. Instead,
	 * it affects the behavior of other input mechanisms, such as virtual keyboards on
	 * mobile devices and voice input. It is only supported by Chrome and Safari</a>.
	 *
	 * @param autocapitalize
	 *            the {@code autocapitalize} value, or {@code null} to unset
	 */
	@SuppressWarnings("unchecked")
	default T setAutocapitalize(final Autocapitalize autocapitalize) {
		this.getComponent().setAutocapitalize(autocapitalize);
		return (T) this;
	}

}
