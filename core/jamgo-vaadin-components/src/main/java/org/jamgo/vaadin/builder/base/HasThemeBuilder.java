package org.jamgo.vaadin.builder.base;

import com.vaadin.flow.component.HasTheme;

@SuppressWarnings("rawtypes")
public interface HasThemeBuilder<T extends HasThemeBuilder, S extends HasTheme> extends BaseComponentBuilder<T, S> {

	/**
	 * Adds a theme name to this component.
	 *
	 * @param themeName
	 *            the theme name to add, not <code>null</code>
	 */
	@SuppressWarnings("unchecked")
	default T addThemeName(final String themeName) {
		this.getComponent().addThemeName(themeName);
		return (T) this;
	}

	/**
	 * Sets the theme names of this component. This method overwrites any
	 * previous set theme names.
	 *
	 * @param themeName
	 *            a space-separated string of theme names to set, or empty
	 *            string to remove all theme names
	 */
	@SuppressWarnings("unchecked")
	default T setThemeName(final String themeName) {
		this.getComponent().setThemeName(themeName);
		return (T) this;
	}

	/**
	 * Sets or removes the given theme name for this component.
	 *
	 * @param themeName
	 *            the theme name to set or remove, not <code>null</code>
	 * @param set
	 *            <code>true</code> to set the theme name, <code>false</code> to
	 *            remove it
	 */
	@SuppressWarnings("unchecked")
	default T setThemeName(final String themeName, final boolean set) {
		this.getComponent().setThemeName(themeName, set);
		return (T) this;
	}

	/**
	 * Adds one or more theme names to this component. Multiple theme names can
	 * be specified by using multiple parameters.
	 *
	 * @param themeNames
	 *            the theme name or theme names to be added to the component
	 */
	@SuppressWarnings("unchecked")
	default T addThemeNames(final String... themeNames) {
		this.getComponent().addThemeNames(themeNames);
		return (T) this;
	}

}
