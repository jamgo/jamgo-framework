package org.jamgo.vaadin.ui.builder;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.vaadin.builder.base.FocusableBuilder;
import org.jamgo.vaadin.builder.base.HasHelperBuilder;
import org.jamgo.vaadin.builder.base.HasSizeBuilder;
import org.jamgo.vaadin.builder.base.HasStyleBuilder;
import org.jamgo.vaadin.builder.base.HasValidationBuilder;
import org.jamgo.vaadin.builder.base.HasValueBuilder;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.data.renderer.Renderer;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class EnumComboBoxBuilder<T extends Enum<?>> extends JamgoComponentBuilder<ComboBox<T>, EnumComboBoxBuilder<T>> implements
	InitializingBean,
	HasSizeBuilder<EnumComboBoxBuilder<T>, ComboBox<T>>,
	HasStyleBuilder<EnumComboBoxBuilder<T>, ComboBox<T>>,
	HasValueBuilder<EnumComboBoxBuilder<T>, ComboBox<T>>,
	HasValidationBuilder<EnumComboBoxBuilder<T>, ComboBox<T>>,
	HasHelperBuilder<EnumComboBoxBuilder<T>, ComboBox<T>>,
	FocusableBuilder<EnumComboBoxBuilder<T>, ComboBox<T>, ComboBox<T>> {

	@Autowired
	private LocalizedMessageService localizedMessageService;

	private final Class<T> enumClass;

	public EnumComboBoxBuilder(final Class<T> enumClass) {
		super();
		this.enumClass = enumClass;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.instance = new ComboBox<>();
	}

	@Override
	public ComboBox<T> build() {
		Optional.ofNullable(this.label).ifPresent(o -> this.instance.setLabel(o));

		this.instance.setItemLabelGenerator(o -> this.getEnumLabel(o));
		this.instance.setItems(this.enumClass.getEnumConstants());

		return this.instance;
	}

	private String getEnumLabel(final Enum<?> anEnum) {
		final String baseName = Optional.ofNullable(anEnum.getClass().getEnclosingClass())
			.map(o -> String.join(".", StringUtils.uncapitalize(o.getSimpleName()), StringUtils.uncapitalize(anEnum.getClass().getSimpleName())))
			.orElse(StringUtils.uncapitalize(anEnum.getClass().getSimpleName()));
		return this.localizedMessageService.getMessage(String.join(".", baseName, anEnum.toString()), anEnum.toString());
	}

	/**
	 * Sets the item label generator that is used to produce the strings shown
	 * in the combo box for each item. By default,
	 * {@link String#valueOf(Object)} is used.
	 * <p>
	 * When the {@link #setRenderer(Renderer)} is used, the ItemLabelGenerator
	 * is only used to show the selected item label.
	 *
	 * @param itemLabelGenerator
	 *            the item label provider to use, not null
	 */
	public EnumComboBoxBuilder<T> setItemLabelGenerator(final ItemLabelGenerator<T> itemLabelGenerator) {
		this.getComponent().setItemLabelGenerator(itemLabelGenerator);
		return this;
	}

	/**
	 * Sets the TemplateRenderer responsible to render the individual items in
	 * the list of possible choices of the ComboBox. It doesn't affect how the
	 * selected item is rendered - that can be configured by using
	 * {@link #setItemLabelGenerator(ItemLabelGenerator)}.
	 *
	 * @param renderer
	 *            a renderer for the items in the selection list of the
	 *            ComboBox, not <code>null</code>
	 *
	 *            Note that filtering of the ComboBox is not affected by the
	 *            renderer that is set here. Filtering is done on the original
	 *            values and can be affected by
	 *            {@link #setItemLabelGenerator(ItemLabelGenerator)}.
	 */
	public EnumComboBoxBuilder<T> setRenderer(final Renderer<T> renderer) {
		this.getComponent().setRenderer(renderer);
		return this;
	}

}