package org.jamgo;

import org.jamgo.app.support.BackofficeServletInitializer;

/**
 * @deprecated
 * <p> Use {@link BackofficeServletInitializer} instead
 *
 * @author martin
 *
 */

@Deprecated(forRemoval = true)
public abstract class BackofficeApplication extends BackofficeServletInitializer {

}
