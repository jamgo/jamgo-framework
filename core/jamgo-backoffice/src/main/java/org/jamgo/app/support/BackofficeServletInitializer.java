package org.jamgo.app.support;

import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.jamgo.ui.config.UIConfig;
import org.jamgo.ui.layout.ContentLayout;
import org.jamgo.ui.layout.crud.CrudManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import com.vaadin.flow.spring.annotation.UIScope;

@Import({ UIConfig.class })
public abstract class BackofficeServletInitializer extends JamgoServletInitializer {

	@Bean
	public CrudManager crudManager() {
		return new CrudManager();
	}

	@Bean
	@UIScope
	public ContentLayout contentLayout() {
		return new ContentLayout();
	}

	@Bean
	public JamgoComponentBuilderFactory jamgoComponentBuilderFactory() {
		return new JamgoComponentBuilderFactory();
	}

}
