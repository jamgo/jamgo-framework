package org.jamgo.test;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class PostgresDataCleaner extends DataCleaner {

	@Override
	protected void clear(final List<String> tableNames, final List<String> sequences) throws SQLException {
		final Statement statement = this.connection.createStatement();

		this.addTruncates(tableNames, statement);
		this.addAlterSequences(sequences, statement);

		statement.executeBatch();
	}

	@Override
	protected void addTruncates(final List<String> tableNames, final Statement statement) {
		tableNames.forEach(each -> {
			try {
				statement.addBatch("TRUNCATE TABLE \"" + each + "\" CASCADE");
			} catch (final SQLException e) {
				throw new RuntimeException(e);
			}
		});
	}

}
