package org.jamgo.test;

import java.util.Optional;
import java.util.logging.Logger;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;

public class TestLogExtension implements TestWatcher {

	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";
	public static final String ANSI_GRAY = "\u001B[38;5;244m";

	private static final Logger logger = Logger.getLogger(TestLogExtension.class.getName());

	@Override
	public void testAborted(final ExtensionContext context, final Throwable cause) {
		this.logTest(context, TestLogExtension.ANSI_RED, " [ABORTED]");
	}

	@Override
	public void testDisabled(final ExtensionContext context, final Optional<String> reason) {
		this.logTest(context, TestLogExtension.ANSI_GRAY, " [DISABLED]");
	}

	@Override
	public void testFailed(final ExtensionContext context, final Throwable cause) {
		this.logTest(context, TestLogExtension.ANSI_RED, " [FAILED]");
	}

	@Override
	public void testSuccessful(final ExtensionContext context) {
		this.logTest(context, TestLogExtension.ANSI_GREEN, " [OK]");
	}

	private void logTest(final ExtensionContext context, final String ansiColor, final String suffix) {
		final StringBuilder msgBuilder = new StringBuilder();
		msgBuilder
			.append(TestLogExtension.ANSI_CYAN).append("TEST: ")
			.append(ansiColor).append(context.getTestClass().get().getSimpleName()).append(" -> ").append(context.getTestMethod().get().getName())
			.append(suffix).append(TestLogExtension.ANSI_RESET);
		TestLogExtension.logger.info(msgBuilder.toString());
	}

}
