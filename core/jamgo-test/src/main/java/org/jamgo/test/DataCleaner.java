package org.jamgo.test;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

public abstract class DataCleaner {

	protected Connection connection;
	List<String> tableNames;
	List<String> sequences;

	public void clearTables(final DataSource dataSource) {
		try {
			this.connection = dataSource.getConnection();
			this.clear(this.getTableNames(), this.getSequences());
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (this.connection != null) {
				try {
					this.connection.close();
				} catch (final SQLException e) {
					e.printStackTrace();
				}
				this.connection = null;
			}
		}
	}

	protected List<String> getTableNames() throws SQLException {
		if (this.tableNames == null) {
			this.tableNames = new ArrayList<>();
			final DatabaseMetaData metaData = this.connection.getMetaData();
			final ResultSet rs = metaData.getTables(this.connection.getCatalog(), null, null, new String[] { "TABLE" });
			while (rs.next()) {
				this.tableNames.add(rs.getString("TABLE_NAME"));
			}
		}

		return this.tableNames;
	}

	protected List<String> getSequences() throws SQLException {
		if (this.sequences == null) {
			this.sequences = new ArrayList<>();
			final ResultSet rs = this.connection.createStatement().executeQuery("SELECT sequence_name FROM information_schema.SEQUENCES");
			while (rs.next()) {
				this.sequences.add(rs.getString(1));
			}
		}

		return this.sequences;
	}

	protected abstract void clear(final List<String> tableNames, final List<String> sequences) throws SQLException;

	protected abstract void addTruncates(final List<String> tableNames, final Statement statement);

	protected void addAlterSequences(final List<String> sequences, final Statement statement) {
		sequences.forEach(each -> {
			try {
				statement.addBatch("ALTER SEQUENCE " + each + " RESTART WITH 1");
			} catch (final SQLException e) {
				throw new RuntimeException(e);
			}
		});
	}
}
