package org.jamgo.test.p6spy;

import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.util.TablesNamesFinder;

public class TestTableNamesFinder extends TablesNamesFinder {

	@Override
	protected String extractTableName(final Table table) {
		return table.getName();
	}

}
