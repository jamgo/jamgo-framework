package org.jamgo.test;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

import org.jamgo.test.p6spy.TestJdbcEventsCollector;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.PlatformTransactionManager;

public abstract class JamgoRepositoryTest extends JamgoTest {

	@Autowired
	protected DataSource dataSource;
	@Autowired
	protected EntityManager entityManager;
	@Autowired
	protected PlatformTransactionManager transactionManager;

	private DataCleaner dataCleaner;

	@BeforeAll
	protected void init() {
		// ... TODO: Detect db engine using this.dataSource.getConnection().getClientInfo()
		this.dataCleaner = new PostgresDataCleaner();
	}

	@BeforeEach
	void clear(final TestInfo testInfo) {
		this.entityManager.clear();
		this.dataCleaner.clearTables(this.dataSource);
		TestJdbcEventsCollector.clear();
	}

	protected void commit() {
		TestTransaction.flagForCommit();
		TestTransaction.end();
	}

	protected void commitAndStart() {
		this.commit();
		TestTransaction.start();
	}

}
