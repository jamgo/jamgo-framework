package org.jamgo.test;

import java.net.HttpURLConnection;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JsonNode;

public abstract class JamgoControllerTest extends JamgoRepositoryTest {

	@Autowired
	protected WebApplicationContext context;

	protected MockMvc mockMvc;

	@BeforeEach
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
			.build();
	}

	protected void assertStatus(final JsonNode responseDtoNode) {
		this.assertStatus(responseDtoNode, HttpURLConnection.HTTP_OK, null);
	}

	protected void assertStatus(final JsonNode responseDtoNode, final Integer expectedStatusId, final String expectedMessage) {
		Assertions.assertEquals((int) expectedStatusId, responseDtoNode.get("status").asInt());
		if (expectedMessage != null) {
			Assertions.assertEquals(expectedMessage, responseDtoNode.get("message").asText());
		}
	}

}
