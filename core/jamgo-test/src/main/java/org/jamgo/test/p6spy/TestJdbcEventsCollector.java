package org.jamgo.test.p6spy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.update.Update;
import net.sf.jsqlparser.util.TablesNamesFinder;

public class TestJdbcEventsCollector {

	private enum StatementType {
		SELECT, INSERT, UPDATE, DELETE
	}

//	private static final Map<String, StatementType> statementTypesMap = new HashMap<>();
//	static {
//		for (final StatementType each : StatementType.values()) {
//			TestJdbcEventsCollector.statementTypesMap.put(each.name(), each);
//		}
//	}

	private static Map<StatementType, Map<String, Integer>> statementsCount = new HashMap<>();
	private static TablesNamesFinder tablesNamesFinder = new TestTableNamesFinder();

//	public static final TestJdbcEventsCollector INSTANCE = new TestJdbcEventsCollector();

	private TestJdbcEventsCollector() {
	}

	public static void clear() {
		TestJdbcEventsCollector.statementsCount.clear();
	}

	public static void collect(final String sqlStatement) {
		if (sqlStatement != null) {
			Statement statement = null;
			try {
				statement = CCJSqlParserUtil.parse(sqlStatement);
			} catch (final JSQLParserException e) {
				// ...	Do nothing
			}
			if (statement != null) {
				String tableName = null;
				if (statement instanceof Select) {
					final List<String> tablesList = TestJdbcEventsCollector.tablesNamesFinder.getTableList(statement);
					tableName = tablesList.get(0);
					incrementCount(StatementType.SELECT, tableName);
				} else if (statement instanceof Insert) {
					tableName = ((Insert) statement).getTable().getName();
					incrementCount(StatementType.INSERT, tableName);
				} else if (statement instanceof Update) {
					tableName = ((Update) statement).getTable().getName();
					incrementCount(StatementType.UPDATE, tableName);
				} else if (statement instanceof Delete) {
					tableName = ((Delete) statement).getTable().getName();
					incrementCount(StatementType.DELETE, tableName);
				}
			}
		}
	}

	private static void incrementCount(final StatementType statementType, final String tableName) {
		Map<String, Integer> tableCount = TestJdbcEventsCollector.statementsCount.get(statementType);
		if (tableCount == null) {
			tableCount = new HashMap<>();
			TestJdbcEventsCollector.statementsCount.put(statementType, tableCount);
		}
		tableCount.compute(tableName.toLowerCase(), (key, oldValue) -> oldValue == null ? 1 : oldValue + 1);
	}

	public static Integer getSelectCount(final String tableName) {
		return Optional.ofNullable(TestJdbcEventsCollector.statementsCount.get(StatementType.SELECT)).map(o -> o.get(tableName)).orElse(0);
	}

	public static Integer getInsertCount(final String tableName) {
		return Optional.ofNullable(TestJdbcEventsCollector.statementsCount.get(StatementType.INSERT)).map(o -> o.get(tableName)).orElse(0);
	}

	public static Integer getUpdateCount(final String tableName) {
		return Optional.ofNullable(TestJdbcEventsCollector.statementsCount.get(StatementType.UPDATE)).map(o -> o.get(tableName)).orElse(0);
	}

	public static Integer getDeleteCount(final String tableName) {
		return Optional.ofNullable(TestJdbcEventsCollector.statementsCount.get(StatementType.DELETE)).map(o -> o.get(tableName)).orElse(0);
	}

}
