package org.jamgo.test;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;

@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(TestLogExtension.class)
public abstract class JamgoTest {
	protected static final long FAKE_USER_ID = 123L;

	/**
	 * Get the expected response for a controller test. As an internal convention.
	 * This function will retrieve the expected response content from a test resource file
	 * following the next convention:
	 *  - The resource file is placed in src/test/resources/result/
	 *  - The resource file follows the naming convention: <TestFileClassName>_<TestName>.json
	 * @return String containing the JSON of the expected server response.
	 */
	public static String getTestExpectedResponse() {

		try {
			final String testName = new Throwable().getStackTrace()[1].getMethodName();
			final String[] fullPathName = new Throwable().getStackTrace()[1].getClassName().split("\\.");
			final String className = fullPathName[fullPathName.length - 1];

			return new String(Files.readAllBytes(
				Paths.get(ClassLoader.getSystemResource("result/" + className + "_" + testName + ".json").toURI())));
		} catch (final Exception e) {
			e.printStackTrace();
			return "";
		}
	}

}
