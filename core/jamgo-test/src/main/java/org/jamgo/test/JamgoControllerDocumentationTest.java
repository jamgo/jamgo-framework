package org.jamgo.test;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import capital.scalable.restdocs.AutoDocumentation;
import capital.scalable.restdocs.SnippetRegistry;
import capital.scalable.restdocs.jackson.JacksonResultHandlers;
import capital.scalable.restdocs.response.ResponseModifyingPreprocessors;

@WebMvcTest
@ExtendWith(RestDocumentationExtension.class)
public abstract class JamgoControllerDocumentationTest extends JamgoControllerTest {

	@Autowired
	private WebApplicationContext context;
	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public void setup() {
		// Do nothing
	}

//	@BeforeEach
//	public void setup(final RestDocumentationContextProvider restDocumentation) {
//		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
//			.apply(documentationConfiguration(restDocumentation).operationPreprocessors()
//				.withRequestDefaults(prettyPrint())
//				.withResponseDefaults(prettyPrint()))
//			.build();
//	}

	@BeforeEach
	public void setup(final RestDocumentationContextProvider restDocumentation) {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
			.alwaysDo(JacksonResultHandlers.prepareJackson(this.objectMapper))
			.alwaysDo(MockMvcRestDocumentation.document("{class-name}/{method-name}",
				Preprocessors.preprocessRequest(
					Preprocessors.prettyPrint()),
				Preprocessors.preprocessResponse(
					ResponseModifyingPreprocessors.replaceBinaryContent(),
					ResponseModifyingPreprocessors.limitJsonArrayLength(this.objectMapper),
					Preprocessors.prettyPrint())))
			.apply(documentationConfiguration(restDocumentation)
				.uris()
				.withScheme("http")
				.withHost("localhost")
				.withPort(8080)
				.and().snippets()
				.withDefaults(
					HttpDocumentation.httpRequest(),
					HttpDocumentation.httpResponse(),
					AutoDocumentation.pathParameters(),
					AutoDocumentation.description(),
					AutoDocumentation.methodAndPath(),
					AutoDocumentation.sectionBuilder()
						.snippetNames(
							SnippetRegistry.AUTO_METHOD_PATH,
							SnippetRegistry.AUTO_DESCRIPTION,
							SnippetRegistry.AUTO_PATH_PARAMETERS,
							SnippetRegistry.HTTP_REQUEST,
							SnippetRegistry.HTTP_RESPONSE)
						.skipEmpty(true)
						.build()))
			.build();
	}
}
