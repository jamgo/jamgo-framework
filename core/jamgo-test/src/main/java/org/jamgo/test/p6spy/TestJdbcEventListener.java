package org.jamgo.test.p6spy;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import com.p6spy.engine.common.ConnectionInformation;
import com.p6spy.engine.common.PreparedStatementInformation;
import com.p6spy.engine.common.StatementInformation;
import com.p6spy.engine.event.JdbcEventListener;

public class TestJdbcEventListener extends JdbcEventListener {

//	TestJdbcEventsCollector jdbcEventsCollector = TestJdbcEventsCollector.INSTANCE;

	public TestJdbcEventListener() {
		TestJdbcEventsCollector.clear();
	}

	/**
	 * This callback method is executed after a {@link Connection} obtained from a {@link DataSource} or a {@link Driver}.
	 * <p>
	 * The {@link ConnectionInformation} holds information about the creator of the connection which is either
	 * {@link ConnectionInformation#dataSource}, {@link ConnectionInformation#driver} or
	 * {@link ConnectionInformation#pooledConnection}, though {@link ConnectionInformation#connection} itself is <code>null</code>
	 * when {@link SQLException} is not <code>null</code> and vise versa.
	 *
	 * @param connectionInformation The meta information about the wrapped {@link Connection}
	 * @param e                     The {@link SQLException} which may be triggered by the call (<code>null</code> if
	 *                              there was no exception).
	 */
	@Override
	public void onAfterGetConnection(final ConnectionInformation connectionInformation, final SQLException e) {
	}

	/**
	 * This callback method is executed after the {@link Statement#addBatch(String)} or
	 * {@link PreparedStatement#addBatch(String)} method is invoked.
	 *
	 * @param statementInformation The meta information about the {@link Statement} being invoked
	 * @param timeElapsedNanos     The execution time of the execute call
	 * @param sql                  The SQL string provided to the execute method
	 * @param e                    The {@link SQLException} which may be triggered by the call (<code>null</code> if
	 *                             there was no exception).
	 */
	@Override
	public void onAfterAddBatch(final StatementInformation statementInformation, final long timeElapsedNanos, final String sql, final SQLException e) {
		TestJdbcEventsCollector.collect(statementInformation.getSql());
	}

	/**
	 * This callback method is executed after any the {@link Statement#execute(String)} methods are invoked.
	 *
	 * @param statementInformation The meta information about the {@link Statement} being invoked
	 * @param timeElapsedNanos     The execution time of the execute call
	 * @param sql                  The SQL string provided to the execute method
	 * @param e                    The {@link SQLException} which may be triggered by the call (<code>null</code> if
	 *                             there was no exception).
	 */
	@Override
	public void onAfterExecute(final StatementInformation statementInformation, final long timeElapsedNanos, final String sql, final SQLException e) {
		TestJdbcEventsCollector.collect(statementInformation.getSql());
	}

	/**
	 * This callback method is executed after the {@link Statement#executeBatch()} method is invoked.
	 *
	 * @param statementInformation The meta information about the {@link Statement} being invoked
	 * @param timeElapsedNanos     The execution time of the execute call
	 * @param updateCounts         An array of update counts or null if an exception was thrown
	 * @param e                    The {@link SQLException} which may be triggered by the call (<code>null</code> if
	 *                             there was no exception).
	 */
	@Override
	public void onAfterExecuteBatch(final StatementInformation statementInformation, final long timeElapsedNanos, final int[] updateCounts, final SQLException e) {
		TestJdbcEventsCollector.collect(statementInformation.getSql());
	}

	/**
	 * This callback method is executed after the {@link PreparedStatement#executeUpdate()} method is invoked.
	 *
	 * @param statementInformation The meta information about the {@link Statement} being invoked
	 * @param timeElapsedNanos     The execution time of the execute call
	 * @param rowCount             Either the row count for SQL Data Manipulation Language (DML) statements or 0 for SQL
	 *                             statements that return nothing or if an exception was thrown
	 * @param e                    The {@link SQLException} which may be triggered by the call (<code>null</code> if
	 *                             there was no exception).
	 */
	@Override
	public void onAfterExecuteUpdate(final PreparedStatementInformation statementInformation, final long timeElapsedNanos, final int rowCount, final SQLException e) {
		TestJdbcEventsCollector.collect(statementInformation.getSql());
	}

	/**
	 * This callback method is executed after any of the {@link Statement#executeUpdate(String)} methods are invoked.
	 *
	 * @param statementInformation The meta information about the {@link Statement} being invoked
	 * @param timeElapsedNanos     The execution time of the execute call
	 * @param sql                  The SQL string provided to the execute method
	 * @param rowCount             Either the row count for SQL Data Manipulation Language (DML) statements or 0 for SQL
	 *                             statements that return nothing or if an exception was thrown
	 * @param e                    The {@link SQLException} which may be triggered by the call (<code>null</code> if
	 *                             there was no exception).
	 */
	@Override
	public void onAfterExecuteUpdate(final StatementInformation statementInformation, final long timeElapsedNanos, final String sql, final int rowCount, final SQLException e) {
		TestJdbcEventsCollector.collect(statementInformation.getSql());
	}

	/**
	 * This callback method is executed after the {@link Statement#executeQuery(String)} method is invoked.
	 *
	 * @param statementInformation The meta information about the {@link Statement} being invoked
	 * @param timeElapsedNanos     The execution time of the execute call
	 * @param sql                  The SQL string provided to the execute method
	 * @param e                    The {@link SQLException} which may be triggered by the call (<code>null</code> if
	 *                             there was no exception).
	 */
	@Override
	public void onAfterExecuteQuery(final StatementInformation statementInformation, final long timeElapsedNanos, final String sql, final SQLException e) {
		TestJdbcEventsCollector.collect(statementInformation.getSql());
	}

	/**
	 * This callback method is executed after the {@link Statement#getResultSet()} method is invoked.
	 *
	 * @param statementInformation The meta information about the {@link Statement} being invoked
	 * @param timeElapsedNanos     The execution time of the execute call
	 * @param e                    The {@link SQLException} which may be triggered by the call (<code>null</code> if
	 *                             there was no exception).
	 */
	@Override
	public void onAfterGetResultSet(final StatementInformation statementInformation, final long timeElapsedNanos, final SQLException e) {
		TestJdbcEventsCollector.collect(statementInformation.getSql());
	}

}
