package org.jamgo.ui.layout.details;

import org.jamgo.model.entity.AppRole;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AppRoleDetailsLayout extends CrudDetailsLayout<AppRole> {

	private static final long serialVersionUID = 1L;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		final CrudDetailsPanel panel = this.componentBuilderFactory.createCrudDetailsPanelBuilder().build();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.addNameField(panel);
		this.addDescriptionField(panel);

		return panel;
	}

	private void addNameField(final CrudDetailsPanel panel) {
		final TextField roleNameField = this.componentBuilderFactory.createTextFieldBuilder()
			.setWidth(50, Unit.PERCENTAGE)
			.build();
		panel.addFormComponent(roleNameField, "role.name");
		this.binder.bind(roleNameField, AppRole::getRolename, AppRole::setRolename);
	}

	private void addDescriptionField(final CrudDetailsPanel panel) {
		final TextArea descriptionField = this.componentBuilderFactory.createTextAreaBuilder()
			.setWidth(100, Unit.PERCENTAGE)
			.build();
		panel.addFormComponent(descriptionField, "role.description");
		this.binder.bind(descriptionField, AppRole::getDescription, AppRole::setDescription);
	}

	@Override
	protected Class<AppRole> getTargetObjectClass() {
		return AppRole.class;
	}

}
