package org.jamgo.ui.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;

public abstract class IdentitySecurityConfiguration extends VaadinSecurityConfiguration {

	@Override
	@Bean
	public JdbcUserDetailsManager userDetailsService() {
		final JdbcUserDetailsManager service = new JdbcUserDetailsManager(this.dataSource);
		service.setUsersByUsernameQuery("select username, password, enabled from " + this.getUserTableName() + " where username = ?");
		service.setAuthoritiesByUsernameQuery("select u.username, r.rolename "
			+ "from user_role ur "
			+ "join " + this.getRoleTableName() + "r on ur.id_role = r.id "
			+ "join " + this.getUserTableName() + " u on ur.id_user = u.id "
			+ "where u.username =?");
		service.setRolePrefix("");
		return service;
	}

	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(this.dataSource)
			.rolePrefix("")
			.passwordEncoder(new BCryptPasswordEncoder(11))
			.usersByUsernameQuery("select username, password, enabled from " + this.getUserTableName() + " where username=?")
			.authoritiesByUsernameQuery("select u.username, r.rolename "
				+ "from user_role ur "
				+ "join " + this.getRoleTableName() + " r on ur.id_role = r.id "
				+ "join " + this.getUserTableName() + " u on ur.id_user = u.id "
				+ "where u.username =?");
	}

	protected String getUserTableName() {
		return "app_user";
	}

	protected String getRoleTableName() {
		return "app_role";
	}

}
