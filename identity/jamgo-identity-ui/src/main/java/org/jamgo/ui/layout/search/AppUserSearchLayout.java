package org.jamgo.ui.layout.search;

import org.jamgo.model.search.AppUserSearchSpecification;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.ui.layout.crud.CrudSearchLayout;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.textfield.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AppUserSearchLayout extends CrudSearchLayout<AppUserSearchSpecification> {

	private static final long serialVersionUID = 1L;

	@Override
	protected CrudDetailsPanel createPanel() {
		final CrudDetailsPanel panel = this.componentBuilderFactory.createCrudDetailsPanelBuilder().build();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		this.addUsernameField(panel);
		this.addNameField(panel);

		return panel;
	}

	private void addUsernameField(final CrudDetailsPanel panel) {
		final TextField usernameField = this.componentBuilderFactory.createTextFieldBuilder()
			.build();
		panel.addFormComponent(usernameField, "user.username");
		this.binder.bind(usernameField, AppUserSearchSpecification::getUsername, AppUserSearchSpecification::setUsername);
	}

	private void addNameField(final CrudDetailsPanel panel) {
		final TextField nameField = this.componentBuilderFactory.createTextFieldBuilder()
			.build();
		panel.addFormComponent(nameField, "user.name");
		this.binder.bind(nameField, AppUserSearchSpecification::getName, AppUserSearchSpecification::setName);
	}

	@Override
	protected Class<AppUserSearchSpecification> getTargetObjectClass() {
		return AppUserSearchSpecification.class;
	}

}
