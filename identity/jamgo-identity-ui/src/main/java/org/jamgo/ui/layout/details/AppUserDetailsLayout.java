package org.jamgo.ui.layout.details;

import org.apache.commons.lang3.StringUtils;
import org.jamgo.model.entity.AppRole;
import org.jamgo.model.entity.AppUser;
import org.jamgo.model.entity.Language;
import org.jamgo.model.repository.LanguageRepository;
import org.jamgo.services.impl.AppRoleService;
import org.jamgo.ui.component.JamgoFormLayout;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.components.JamgoComponentConstants;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.componentfactory.EnhancedDialog;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AppUserDetailsLayout extends CrudDetailsLayout<AppUser> {

	private static final long serialVersionUID = 1L;

	@Autowired
	protected PasswordDetailsLayout passwordDetailsLayout;
	@Autowired
	protected LanguageRepository languageRepository;
	@Autowired
	protected AppRoleService roleService;

	protected JamgoFormLayout mainFormLayout;

	protected TextField usernameField;
	protected TextField nameField;
	protected TextField surnameField;
	protected TextField emailField;
	protected ComboBox<Language> languageCombo;
	protected Checkbox enabledField;
	protected CheckboxGroup<AppRole> rolesGroup;
	protected Button editPassword;

	@Override
	public void updateFields(final Object user) {
		super.updateFields(user);

		if (this.targetObject != null) {
			String passwordButtonCaption = null;
			if (StringUtils.isEmpty(this.targetObject.getPassword())) {
				passwordButtonCaption = this.messageSource.getMessage("user.password.set");
			} else {
				passwordButtonCaption = this.messageSource.getMessage("user.password.edit");
			}
			this.editPassword.setText(passwordButtonCaption);
		}
	}

//	@Override
//	public void updateTargetObject() throws ValidationException {
//		try {
//			this.validateForm();
//			super.updateTargetObject();
//		} catch (ValidationException e) {
//			// ...	TODO: Ver dónde conviene atrapar la exception.
//			e.printStackTrace();
//			throw e;
//		}
//	}

	@Override
	protected CrudDetailsPanel createMainPanel() {
		final CrudDetailsPanel panel = this.componentBuilderFactory.createCrudDetailsPanelBuilder().build();
		panel.setName(this.messageSource.getMessage("form.basic.info"));
		this.mainFormLayout = (JamgoFormLayout) panel.getFormLayout();

		this.addUsernameField(this.mainFormLayout, "user.username", ComponentColspan.SIX_COLUMNS);
		this.addNameField(this.mainFormLayout, "user.name", ComponentColspan.SIX_COLUMNS);
		this.addSurnameField(this.mainFormLayout, "user.surname", ComponentColspan.SIX_COLUMNS);
		this.addEmailField(this.mainFormLayout, "user.email", ComponentColspan.SIX_COLUMNS);
		this.addLanguageField(this.mainFormLayout, "user.language", ComponentColspan.SIX_COLUMNS);
		this.addEnabledField(this.mainFormLayout, "user.active", ComponentColspan.SIX_COLUMNS);
		this.addPasswordField(this.mainFormLayout, "user.password.set", ComponentColspan.SIX_COLUMNS);
		this.addRolesField(this.mainFormLayout, "user.roles", ComponentColspan.SIX_COLUMNS);

		return panel;
	}

	private void addUsernameField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.usernameField = this.componentBuilderFactory.createTextFieldBuilder().build();
		formLayout.addFormComponent(this.usernameField, fieldKey, colspan);
		this.binder.forField(this.usernameField)
			.asRequired(this.messageSource.getMessage("validation.emptyField"))
			.bind(o -> o.getUsername(), (o, v) -> o.setUsername(v));
	}

	private void addNameField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.nameField = this.componentBuilderFactory.createTextFieldBuilder().build();
		formLayout.addFormComponent(this.nameField, fieldKey, colspan);
		this.binder.forField(this.nameField)
			.asRequired(this.messageSource.getMessage("validation.emptyField"))
			.bind(o -> o.getName(), (o, v) -> o.setName(v));
	}

	private void addSurnameField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.surnameField = this.componentBuilderFactory.createTextFieldBuilder().build();
		formLayout.addFormComponent(this.surnameField, fieldKey);
		this.binder.forField(this.surnameField)
			.bind(o -> o.getSurname(), (o, v) -> o.setSurname(v));
	}

	private void addEmailField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.emailField = this.componentBuilderFactory.createTextFieldBuilder().build();
		formLayout.addFormComponent(this.emailField, fieldKey, colspan);
		this.binder.forField(this.emailField)
			.bind(o -> o.getEmail(), (o, v) -> o.setEmail(v));
	}

	private void addLanguageField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		final ComboBox<Language> languageField = this.componentBuilderFactory.<Language> createComboBoxBuilder()
			.setItemLabelGenerator(obj -> obj.getName())
			.build();
		languageField.setItems(this.languageRepository.findAll());
		formLayout.addFormComponent(languageField, fieldKey, colspan);
		this.binder.forField(languageField)
			.bind(o -> o.getLanguage(), (o, v) -> o.setLanguage(v));
	}

	private void addEnabledField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.enabledField = this.componentBuilderFactory.createCheckBoxBuilder().build();
		formLayout.addFormComponent(this.enabledField, fieldKey, colspan);
		this.binder.forField(this.enabledField)
			.bind(o -> o.isEnabled(), (o, v) -> o.setEnabled(v));
	}

	private void addPasswordField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		final PasswordField passwordField = this.componentBuilderFactory.createPasswordFieldBuilder()
			.setLabelId("user.password")
			.build();
		this.binder.forField(passwordField)
			.bind(o -> o.getPassword(), (o, v) -> o.setPassword(v));

		this.editPassword = this.componentBuilderFactory.createButtonBuilder().build();
		this.editPassword.addClickListener(event -> {
			final EnhancedDialog window = new EnhancedDialog();
			window.setHeader(this.messageSource.getMessage("user.password.edit"));
			window.setModal(true);
			window.setWidth(30, Unit.PERCENTAGE);
			this.passwordDetailsLayout.setPasswordField(passwordField);
			window.setContent(this.passwordDetailsLayout);
			window.open();
		});
		formLayout.addFormComponent(this.editPassword, fieldKey, colspan);
	}

	private void addRolesField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.rolesGroup = this.componentBuilderFactory.<AppRole> createCheckBoxGroupBuilder().build();
		this.rolesGroup.setItems(this.roleService.findAll());
		formLayout.addFormComponent(this.rolesGroup, fieldKey, colspan);
		this.binder.forField(this.rolesGroup)
			.bind(o -> o.getRoles(), (o, v) -> o.setRoles(v));
	}

	@Override
	protected Class<AppUser> getTargetObjectClass() {
		return AppUser.class;
	}

}
