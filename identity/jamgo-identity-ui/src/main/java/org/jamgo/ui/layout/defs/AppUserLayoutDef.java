package org.jamgo.ui.layout.defs;

import org.jamgo.model.entity.AppUser;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.details.AppUserDetailsLayout;

@LayoutDefComponent
public class AppUserLayoutDef extends ModelLayoutDef<AppUser> {

	public AppUserLayoutDef(final LocalizedMessageService messageSource, final LocalizedObjectService localizedObjectService) {
		super(messageSource, localizedObjectService);
	}

	@SuppressWarnings("unchecked")
	@Override
	public CrudLayoutDef<AppUser, AppUser> getBackofficeLayoutDef() {
		// @formatter:off
		return (CrudLayoutDef<AppUser, AppUser>) CrudLayoutDef.<AppUser> builderEntity()
			.setId("user")
			.setNameSupplier(() -> this.messageSource.getMessage("table.users"))
			.modelGridDef()
				.setEntityClass(AppUser.class)
				.columnDef()
					.setId("username")
					.setCaptionSupplier(() -> "User name")
					.setValueProvider(o -> o.getUsername())
				.end()
				.columnDef()
					.setId("enabled")
					.setCaptionSupplier(() -> "Enabled")
					.setValueProvider(o -> o.isEnabled())
				.end()
				.detailsLayoutClass(AppUserDetailsLayout.class)
			.end()
			.tableLayoutVerticalConfig()
			.build();
		// @formatter:on
	}

	@Override
	public Class<AppUser> getModelClass() {
		return AppUser.class;
	}

}
