package org.jamgo.ui.layout.details;

import org.jamgo.services.impl.AppUserService;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.componentfactory.EnhancedDialog;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.data.binder.Binder;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PasswordDetailsLayout extends VerticalLayout implements InitializingBean {

	private static final long serialVersionUID = 1L;

	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	protected AppUserService appUserService;

	private PasswordField passwordField;
	private PasswordField newPasswordField;
	private PasswordField newPasswordRepeatedField;
	private Span errorLabel;

	public class PasswordChange {
		private String password;
		private String oldPassword;
		private String newPassword;
		private String newPasswordRepeated;

		public String getPassword() {
			return this.password;
		}

		public void setPassword(final String password) {
			this.password = password;
		}

		public String getOldPassword() {
			return this.oldPassword;
		}

		public void setOldPassword(final String oldPassword) {
			this.oldPassword = oldPassword;
		}

		public String getNewPassword() {
			return this.newPassword;
		}

		public void setNewPassword(final String newPassword) {
			this.newPassword = newPassword;
		}

		public String getNewPasswordRepeated() {
			return this.newPasswordRepeated;
		}

		public void setNewPasswordRepeated(final String newPasswordRepeated) {
			this.newPasswordRepeated = newPasswordRepeated;
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	protected void init() {
		this.setSpacing(true);
		this.setMargin(true);
		this.setDefaultHorizontalComponentAlignment(Alignment.CENTER);

		final Binder<PasswordChange> passwordChangeBinder = new Binder<>(PasswordChange.class);
		passwordChangeBinder.setBean(new PasswordChange());

		this.newPasswordField = this.componentBuilderFactory.createPasswordFieldBuilder()
			.setLabelId("user.password.new")
			.build();
		passwordChangeBinder.forField(this.newPasswordField)
			.withValidator(
				value -> value != null,
				this.messageSource.getMessage("required.password.new"))
			.bind(PasswordChange::getNewPassword, PasswordChange::setNewPassword);

		this.newPasswordRepeatedField = this.componentBuilderFactory.createPasswordFieldBuilder()
			.setLabelId("user.password.repeated")
			.build();
		passwordChangeBinder.forField(this.newPasswordRepeatedField)
			.withValidator(
				value -> value != null,
				this.messageSource.getMessage("required.password.repeated"))
			.withValidator(
				value -> value.equals(this.newPasswordField.getValue()),
				this.messageSource.getMessage("error.user.password.repeated"))
			.bind(PasswordChange::getNewPasswordRepeated, PasswordChange::setNewPasswordRepeated);

		this.errorLabel = this.componentBuilderFactory.createLabelBuilder()
			.setVisible(false)
			.setClassName("error-label")
			.build();

		final Button saveButton = this.componentBuilderFactory.createButtonBuilder()
			.setLabelId("action.save")
			.build();
		saveButton.addClickListener(event -> {
			this.errorLabel.setVisible(false);
			if (passwordChangeBinder.validate().hasErrors()) {
				passwordChangeBinder.validate();
				this.errorLabel.setText(event.toString());
				this.errorLabel.setVisible(true);
				return;
			}
			final String encodedNewPassword = this.appUserService.encodePassword(this.newPasswordField.getValue());
			// TODO: show message tray notification style
			Notification.show(this.messageSource.getMessage("notification.password.changed"));

			this.passwordField.setValue(encodedNewPassword);
			this.getParent().ifPresent(o -> {
				if (o instanceof EnhancedDialog) {
					((EnhancedDialog) o).close();
				}
			});
		});

		final FormLayout passwordFieldsLayout = new FormLayout();
//		passwordFieldsLayout.setSpacing(true);
//		passwordFieldsLayout.setMargin(false);
//		passwordFieldsLayout.setWidthUndefined();
		passwordFieldsLayout.add(this.newPasswordField, this.newPasswordRepeatedField);
		this.add(passwordFieldsLayout, this.errorLabel, saveButton);
	}

	@Override
	public void onAttach(final AttachEvent event) {
		super.onAttach(event);

		this.newPasswordField.clear();
		this.newPasswordRepeatedField.clear();
		this.errorLabel.setVisible(false);
	}

	public void setPasswordField(final PasswordField passwordField) {
		this.passwordField = passwordField;
	}
}
