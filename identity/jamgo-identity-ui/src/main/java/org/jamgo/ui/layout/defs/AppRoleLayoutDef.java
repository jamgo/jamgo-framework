package org.jamgo.ui.layout.defs;

import org.jamgo.model.entity.AppRole;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.details.AppRoleDetailsLayout;

@LayoutDefComponent
public class AppRoleLayoutDef extends ModelLayoutDef<AppRole> {

	public AppRoleLayoutDef(final LocalizedMessageService messageSource, final LocalizedObjectService localizedObjectService) {
		super(messageSource, localizedObjectService);
	}


	@SuppressWarnings("unchecked")
	@Override
	public CrudLayoutDef<AppRole, AppRole> getBackofficeLayoutDef() {
		// @formatter:off
		return (CrudLayoutDef<AppRole, AppRole>) CrudLayoutDef.<AppRole> builderEntity()
			.setId("role")
			.setNameSupplier(() -> this.messageSource.getMessage("table.roles"))
			.modelGridDef()
				.setEntityClass(AppRole.class)
				.columnDef()
					.setId("rolename")
					.setCaptionSupplier(() -> "Role Name")
					.setValueProvider(o -> o.getRolename())
				.end()
				.detailsLayoutClass(AppRoleDetailsLayout.class)
			.end()
			.tableLayoutVerticalConfig()
			.build();
		// @formatter:on
	}

	@Override
	public Class<AppRole> getModelClass() {
		return AppRole.class;
	}

}
