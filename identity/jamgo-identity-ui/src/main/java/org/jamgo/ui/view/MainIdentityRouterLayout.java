package org.jamgo.ui.view;

import org.jamgo.model.entity.AppUser;
import org.jamgo.model.entity.User;

public class MainIdentityRouterLayout extends MainRouterLayout {

	@Override
	protected String getDisplayName(final User currentUser) {
		return ((AppUser) currentUser).getFullName();
	}

}
