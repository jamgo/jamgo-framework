package org.jamgo.services.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import org.jamgo.model.entity.AppRole;
import org.jamgo.model.entity.AppUser;
import org.jamgo.model.entity.User;
import org.jamgo.model.exception.InvalidPasswordException;
import org.jamgo.model.exception.UserNotExistsException;
import org.jamgo.model.repository.AppUserRepository;
import org.jamgo.model.valueobjects.Email;
import org.jamgo.services.UserService;
import org.jamgo.services.session.SessionContext;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

@Service
public class AppUserService implements UserService {

	private final SessionContext sessionContext;
	private final AppUserRepository userRepository;
	private final AppRoleService roleService;
	private final PasswordEncoder passwordEncoder;

	public AppUserService(final SessionContext sessionContext, final AppUserRepository userRepository, final AppRoleService roleService, final PasswordEncoder passwordEncoder) {
		this.sessionContext = sessionContext;
		this.userRepository = userRepository;
		this.roleService = roleService;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public AppUser getCurrentUser() {
		User currentUser = this.sessionContext.getCurrentUser();
		if (currentUser == null) {
			final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			if (authentication != null) {
				currentUser = this.userRepository.findByUsername(authentication.getName());
				this.sessionContext.setCurrentUser(currentUser);
			}
		}
		return (AppUser) currentUser;
	}

	public boolean checkUsernameExists(final String username) {
		final AppUser user = this.getUser(username);
		return (user != null);
	}

	public void login(final String username, final String password) {
		final AppUser user = this.getUser(username);
		if (user == null || !user.isEnabled()) {
			throw new UserNotExistsException(username);
		}
		if (!this.passwordEncoder.matches(password, user.getPassword())) {
			throw new InvalidPasswordException(username);
		}
		user.setLastLogin(LocalDateTime.now());
		this.save(user);
	}

	@Override
	public AppUser getUser(final String username) {
		AppUser user = null;
		if (Email.isValid(username)) {
			user = this.userRepository.findByEmail(username);
		}
		if (user == null) {
			user = this.userRepository.findByUsername(username);
		}
		return user;
	}

	public AppUser save(final AppUser user, final List<String> roleNames) {
		final Set<AppRole> roles = this.roleService.getAllByName(roleNames);
		user.setRoles(roles);
		return this.save(user);
	}

	public AppUser save(final AppUser user) {
		return this.userRepository.persist(user);
	}

	public String encodePassword(final String password) {
		return this.passwordEncoder.encode(password);
	}

	public String generatePassword() {
		// TODO: Use more sophisticated rules to generate a random password
		final List<CharacterRule> rules = Lists.newArrayList(
			new CharacterRule(EnglishCharacterData.UpperCase, 1),
			new CharacterRule(EnglishCharacterData.LowerCase, 1),
			new CharacterRule(EnglishCharacterData.Digit, 1));
		final PasswordGenerator generator = new PasswordGenerator();
		return generator.generatePassword(8, rules);
	}

	@Override
	public List<AppUser> getAllUsers() {
		return this.userRepository.findAll();
	}

}
