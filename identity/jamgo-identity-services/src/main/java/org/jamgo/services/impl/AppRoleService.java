package org.jamgo.services.impl;

import java.util.List;
import java.util.Set;

import org.jamgo.model.entity.AppRole;
import org.jamgo.model.repository.AppRoleRepository;
import org.jamgo.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppRoleService implements RoleService {

	@Autowired
	private AppRoleRepository repository;

	@Override
	public List<AppRole> findAll() {
		return this.repository.findAll();
	}

	public Set<AppRole> getAllByName(final List<String> roleNames) {
		return this.repository.findByRolenames(roleNames);
	}

}
