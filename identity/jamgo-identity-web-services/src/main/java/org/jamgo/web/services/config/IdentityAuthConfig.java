package org.jamgo.web.services.config;

import org.jamgo.web.services.token.BasicTokenHelper;
import org.jamgo.web.services.token.JWTHelper;
import org.jamgo.web.services.token.TokenHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IdentityAuthConfig {
	public static final Logger logger = LoggerFactory.getLogger(IdentityAuthConfig.class);

	@Bean
	@ConditionalOnProperty(name = "auth.method", havingValue = "JWT")
	public TokenHelper jwtTokenHelper(@Value("${auth.jwt.secret}") final String jwtSecret, @Value("${auth.jwt.issuer}") final String jwtIssuer) {
		IdentityAuthConfig.logger.info("Using JWTHelper for authentication");
		return new JWTHelper(jwtSecret, jwtIssuer);
	}

	@Bean
	@ConditionalOnMissingBean
	public TokenHelper basicTokenHelper() {
		IdentityAuthConfig.logger.info("Using BasicTokenHelper for authentication");
		return new BasicTokenHelper();
	}

}
