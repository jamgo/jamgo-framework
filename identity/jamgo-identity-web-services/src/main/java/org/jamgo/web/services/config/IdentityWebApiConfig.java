package org.jamgo.web.services.config;

import org.jamgo.web.services.interceptor.IdentityRequestInterceptor;
import org.jamgo.web.services.token.TokenHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class IdentityWebApiConfig implements WebMvcConfigurer {
	public static final Logger logger = LoggerFactory.getLogger(IdentityWebApiConfig.class);

	private final TokenHelper tokenHelper;

	public IdentityWebApiConfig(final TokenHelper tokenHelper) {
		this.tokenHelper = tokenHelper;
	}

	@Bean
	public IdentityRequestInterceptor identityRequestInterceptor(final TokenHelper tokenHelper) {
		return new IdentityRequestInterceptor(tokenHelper);
	}

	@Override
	public void addInterceptors(final InterceptorRegistry registry) {
		registry.addInterceptor(this.identityRequestInterceptor(this.tokenHelper));
		IdentityWebApiConfig.logger.debug("Interceptor registered: identityRequestInterceptor");
	}

}
