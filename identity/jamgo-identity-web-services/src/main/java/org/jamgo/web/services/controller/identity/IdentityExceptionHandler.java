package org.jamgo.web.services.controller.identity;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jamgo.model.exception.InvalidPasswordException;
import org.jamgo.model.exception.UserNotExistsException;
import org.jamgo.web.services.dto.ResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class IdentityExceptionHandler {

	@ExceptionHandler(UserNotExistsException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ResponseDto<String> handleUserNotExistsException(final UserNotExistsException e, final HttpServletResponse response) {
		final ResponseDto<String> responseDto = new ResponseDto<>();
		responseDto.setStatus(HttpStatus.NOT_FOUND.value());
		responseDto.setMessage(ExceptionUtils.getMessage(e));
		return responseDto;
	}

	@ExceptionHandler(InvalidPasswordException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ResponseDto<String> handleInvalidPasswordException(final InvalidPasswordException e, final HttpServletResponse response) {
		final ResponseDto<String> responseDto = new ResponseDto<>();
		responseDto.setStatus(HttpStatus.FORBIDDEN.value());
		responseDto.setMessage(ExceptionUtils.getMessage(e));
		return responseDto;
	}

}
