package org.jamgo.web.services.dto.identity;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Informació de login")
public class LoginDto {

	@Schema(description = "Nom d'usuari")
	private String username;
	@Schema(description = "Contrasenya")
	private String password;

	public String getUsername() {
		return this.username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

}
