package org.jamgo.web.services.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jamgo.web.services.token.TokenHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.HandlerInterceptor;

public class IdentityRequestInterceptor implements HandlerInterceptor {
	public static final Logger logger = LoggerFactory.getLogger(IdentityRequestInterceptor.class);

	@Value("${auth.token.headerName:token}")
	private String authTokenHeaderName;

	private final TokenHelper tokenHelper;

	public IdentityRequestInterceptor(final TokenHelper tokenHelper) {
		this.tokenHelper = tokenHelper;
	}

	@Override
	public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) throws Exception {
		final String token = this.getTokenFromRequest(request);
		if (token == null) {
			IdentityRequestInterceptor.logger.debug("No token received in request header '{}'", this.authTokenHeaderName);
		} else {
			IdentityRequestInterceptor.logger.debug("Token {} received in request header '{}'", token, this.authTokenHeaderName);
			if (this.tokenHelper.verifyToken(token)) {
				IdentityRequestInterceptor.logger.debug("Valid token");
			} else {
				IdentityRequestInterceptor.logger.error("Invalid token");
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid token");
				return false;
			}
		}
		return true;
	}

	private String getTokenFromRequest(final HttpServletRequest request) {
		return request.getHeader(this.authTokenHeaderName);
	}

}
