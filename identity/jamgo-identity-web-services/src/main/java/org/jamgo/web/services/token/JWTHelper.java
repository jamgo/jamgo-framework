package org.jamgo.web.services.token;

import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;

public class JWTHelper implements TokenHelper {
	public static final Logger logger = LoggerFactory.getLogger(JWTHelper.class);

	private static final String CLAIM_USERNAME = "user";

	private final String jwtSecret;
	private final String jwtIssuer;

	public JWTHelper(final String jwtSecret, final String jwtIssuer) {
		this.jwtSecret = jwtSecret;
		this.jwtIssuer = jwtIssuer;
	}

	@Override
	public String createToken(final String username) {
		JWTHelper.logger.debug("Creating token for user {}", username);
		return JWT.create()
			.withIssuer(this.jwtIssuer)
			.withIssuedAt(Instant.now())
			.withClaim(JWTHelper.CLAIM_USERNAME, username)
			.sign(Algorithm.HMAC256(this.jwtSecret));
	}

	@Override
	public boolean verifyToken(final String token) {
		JWTHelper.logger.debug("Verifying token: {}", token);
		try {
			JWT.require(Algorithm.HMAC256(this.jwtSecret))
				.withIssuer(this.jwtIssuer)
				.build()
				.verify(token);
			return true;
		} catch (final JWTVerificationException e) {
			JWTHelper.logger.error(e.getMessage(), e);
			return false;
		}
	}

	@Override
	public String getUsername(final String token) {
		final String username = JWT.decode(token).getClaim(JWTHelper.CLAIM_USERNAME).asString();
		JWTHelper.logger.debug("Extracted username from token: {}", username);
		return username;
	}

}
