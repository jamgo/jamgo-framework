package org.jamgo.web.services.controller.identity;

import org.jamgo.web.services.dto.identity.LoginDto;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "AppUserController", description = "Methods to operate with application users")
public interface AppUserControllerInterface {

	@Operation(summary = "Comprova si el username passat per paràmetre ha estat utilitzat.",
		description = "Retorna true si ja existeix un usuari enregistrat amb el mateix username, o false en cas contrari.")
	boolean checkUsernameExists(
		@Parameter(description = "username de l'usuari registrat", required = true) String username);

	@Operation(summary = "Crida per fer login a l'aplicació",
		description = "Retorna HTTP code 200 + token.\n"
			+ "Error si el username no existeix.\n"
			+ "Error si la contrasenya no correspon a l'usuari.\n"
			+ "Inicialment, el token serà el username. TODO Generar un token més críptic (JWT)\n"
			+ "Aquest token haurà de ser inclòs a totes les crides a l'API utilizant el Header \"tsfa-token\".")
	String login(
		@Parameter(description = "Informació de login") LoginDto dto);

}
