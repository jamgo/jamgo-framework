package org.jamgo.web.services.controller.identity;

import org.jamgo.services.impl.AppUserService;
import org.jamgo.web.services.dto.identity.LoginDto;
import org.jamgo.web.services.token.TokenHelper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class AppUserController implements AppUserControllerInterface {

	private final AppUserService service;
	private final TokenHelper tokenHelper;

	public AppUserController(final AppUserService service, final TokenHelper tokenHelper) {
		this.service = service;
		this.tokenHelper = tokenHelper;
	}

	@Override
	@GetMapping("/login")
	public boolean checkUsernameExists(
		@RequestParam final String username) {
		return this.service.checkUsernameExists(username);
	}

	@Override
	@PostMapping("/login")
	public String login(@RequestBody final LoginDto dto) {
		this.service.login(dto.getUsername(), dto.getPassword());
		// Returns token to authenticate future calls to the API
		return this.tokenHelper.createToken(dto.getUsername());
	}

}
