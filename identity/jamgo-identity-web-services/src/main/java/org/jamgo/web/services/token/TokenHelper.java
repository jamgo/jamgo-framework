package org.jamgo.web.services.token;

public interface TokenHelper {

	String createToken(final String username);

	boolean verifyToken(final String token);

	String getUsername(final String token);
}
