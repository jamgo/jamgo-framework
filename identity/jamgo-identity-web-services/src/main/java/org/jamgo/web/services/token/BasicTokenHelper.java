package org.jamgo.web.services.token;

public class BasicTokenHelper implements TokenHelper {

	@Override
	public String createToken(final String username) {
		return username;
	}

	@Override
	public boolean verifyToken(final String token) {
		return (token != null);
	}

	@Override
	public String getUsername(final String token) {
		return token;
	}
}
