package org.jamgo.model.repository;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.jamgo.model.entity.AppRole;
import org.jamgo.model.entity.Role;
import org.springframework.stereotype.Repository;

@Repository
public class AppRoleRepository extends ModelRepository<AppRole> {

	@Override
	protected Class<AppRole> getModelClass() {
		return AppRole.class;
	}

	public Role findByRolename(final String roleName) {
		return this.createCriteriaBuilder()
			.where("rolename").eq(roleName)
			.getSingleResult();
	}

	public Set<AppRole> findByRolenames(final List<String> roleNames) {
		return this.createCriteriaBuilder()
			.where("rolename").in(roleNames)
			.distinct()
			.getResultStream()
			.collect(Collectors.toSet());
	}

}
