package org.jamgo.model.exception;

public class UserNotExistsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UserNotExistsException(final String message) {
		super(message);
	}

}
