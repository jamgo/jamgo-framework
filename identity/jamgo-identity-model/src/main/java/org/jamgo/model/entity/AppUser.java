package org.jamgo.model.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "username" }))
public class AppUser extends Model implements User {

	private static final long serialVersionUID = 1L;

	@Column(name = "username", unique = true)
	private String username;
	@Column(nullable = false)
	private String password;
	@Column(nullable = false)
	private boolean enabled;
	private String name;
	private String surname;
	private String email;

	@ManyToOne
	@JoinColumn(name = "id_language")
	private Language language;

	@Column
	private LocalDateTime lastUpdate;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "id_user", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "id_role", referencedColumnName = "id"))
	private Set<AppRole> roles;

	@Column
	private LocalDateTime lastLogin;

	@Override
	public String getUsername() {
		return this.username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	@Deprecated
	public boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(final boolean enabled) {
		this.enabled = enabled;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public LocalDateTime getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(final LocalDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Override
	public Set<AppRole> getRoles() {
		return this.roles;
	}

	public void setRoles(final Set<AppRole> roles) {
		this.roles = roles;
	}

	public Language getLanguage() {
		return this.language;
	}

	public void setLanguage(final Language language) {
		this.language = language;
	}

	public LocalDateTime getLastLogin() {
		return this.lastLogin;
	}

	public void setLastLogin(final LocalDateTime lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getFullName() {
		final StringBuilder fullNameBuilder = new StringBuilder();
		String separator = "";
		if (this.name == null && this.surname == null) {
			fullNameBuilder.append(this.username);
		} else {
			if (this.name != null) {
				fullNameBuilder.append(this.name);
				separator = " ";
			}
			if (this.surname != null) {
				fullNameBuilder.append(separator).append(this.surname);
			}
		}
		return fullNameBuilder.toString();
	}
}
