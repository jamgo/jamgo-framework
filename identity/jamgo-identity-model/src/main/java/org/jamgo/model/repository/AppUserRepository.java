package org.jamgo.model.repository;

import javax.persistence.NoResultException;

import org.jamgo.model.entity.AppUser;
import org.springframework.stereotype.Repository;

@Repository
public class AppUserRepository extends ModelRepository<AppUser> {

	@Override
	protected Class<AppUser> getModelClass() {
		return AppUser.class;
	}

	public AppUser findByUsername(final String username) {
		try {
			return this.createCriteriaBuilder()
				.where("username").eq(username)
				.getSingleResult();
		} catch (final NoResultException e) {
			return null;
		}
	}

	public AppUser findByEmail(final String email) {
		try {
			return this.createCriteriaBuilder()
				.where("email").eq(email)
				.getSingleResult();
		} catch (final NoResultException e) {
			return null;
		}
	}

}
