package org.jamgo.model.search;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.blazebit.persistence.CriteriaBuilder;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AppUserSearchSpecification extends ModelSearchSpecification {

	private String username;
	private String name;

	@Override
	public void apply(final CriteriaBuilder<?> cb) {
		if (this.username != null) {
			cb.where("username").eq(this.username);
		}

		if (this.name != null) {
			cb.where("fileName").eq(this.name);
		}

		super.apply(cb);
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

}
