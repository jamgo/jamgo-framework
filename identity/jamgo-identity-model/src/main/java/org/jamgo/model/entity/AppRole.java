package org.jamgo.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "rolename" }))
public class AppRole extends Model implements Role {

	private static final long serialVersionUID = 1L;

	public static String ROLE_ADMIN = "ROLE_admin";
	public static String ROLE_USER = "ROLE_user";

	@Column
	private String rolename;
	private String description;

	@Override
	public String getRolename() {
		return this.rolename;
	}

	public void setRolename(final String rolename) {
		this.rolename = rolename;
	}

	@Override
	public String toString() {
		return this.rolename;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public boolean isAdmin() {
		return AppRole.ROLE_ADMIN.equals(this.rolename);
	}

}
