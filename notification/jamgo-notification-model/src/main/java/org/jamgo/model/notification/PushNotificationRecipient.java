package org.jamgo.model.notification;

public interface PushNotificationRecipient {

	String getFirebaseToken();

	default boolean isAllowPushNotifications() {
		return true;
	}

}
