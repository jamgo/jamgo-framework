package org.jamgo.model.notification;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.Model;
import org.jamgo.model.entity.User;

@Entity
public class Notification extends Model {

	private static final long serialVersionUID = 1L;

	public enum NotificationSystem {
		DESKTOP, EMAIL, PUSH;
		// TODO SMS, TELEGRAM, WHATSAPP
	}

	@Column
	private String title;

	@Column
	private String text;

	@ManyToOne
	private Language language;

	// TODO: @ManyToOne private User user;

	@Transient
	private User user;

	private String username;

	@Column
	@Enumerated(EnumType.STRING)
	private NotificationSystem notificationSystem;

	@Column
	private LocalDateTime creationDate;

	@Column
	private LocalDateTime sentDate;

	@Column
	private LocalDateTime readDate;

	@Column
	private String errorMessage;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getText() {
		return this.text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public Language getLanguage() {
		return this.language;
	}

	public void setLanguage(final Language language) {
		this.language = language;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(final User user) {
		this.user = user;
		this.username = user.getUsername();
		if (this.language == null) {
			this.language = user.getLanguage();
		}
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public NotificationSystem getNotificationSystem() {
		return this.notificationSystem;
	}

	public void setNotificationSystem(final NotificationSystem notificationSystem) {
		this.notificationSystem = notificationSystem;
	}

	public LocalDateTime getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(final LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getSentDate() {
		return this.sentDate;
	}

	public void setSentDate(final LocalDateTime sentDate) {
		this.sentDate = sentDate;
	}

	public LocalDateTime getReadDate() {
		return this.readDate;
	}

	public void setReadDate(final LocalDateTime readDate) {
		this.readDate = readDate;
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public void setErrorMessage(final String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
