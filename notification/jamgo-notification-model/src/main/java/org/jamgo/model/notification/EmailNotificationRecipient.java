package org.jamgo.model.notification;

public interface EmailNotificationRecipient {

	String getEmail();

	default boolean isAllowMailNotifications() {
		return true;
	}

}
