package org.jamgo.services.notification.email;

import java.util.Locale;

import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.User;
import org.jamgo.model.notification.EmailNotificationRecipient;
import org.jamgo.model.notification.Notification;
import org.jamgo.services.UserService;
import org.jamgo.services.notification.NotificationSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

@Service
public class NotificationEmailSender extends NotificationSender {

	public static final Logger logger = LoggerFactory.getLogger(NotificationEmailSender.class);

	private final EmailService emailService;

	public NotificationEmailSender(final EmailService emailService, final UserService userService) {
		super(userService);
		this.emailService = emailService;
	}

	@Override
	public void send(final Notification notification) {
		final String to = this.getEmail(notification);
		final String subject = notification.getTitle();
		final String text = notification.getText();
		final Language language = notification.getLanguage();
		this.sendNotificationEmail(to, subject, text, language);
	}

	public void sendNotificationEmail(final String to, final String subject, final String text, final Language language) {
		final Locale locale = language.getLocale();
		final String templateName = "email-notification";
		final Context context = new Context();
		context.setVariable("notificationText", text);
		this.emailService.sendEmail(to, null, subject, templateName, context, locale);
	}

	private String getEmail(final Notification notification) {
		String email = null;
		final User user = this.getNotificationUser(notification);
		if (user instanceof EmailNotificationRecipient) {
			email = ((EmailNotificationRecipient) user).getEmail();
		}
		return email;
	}

}
