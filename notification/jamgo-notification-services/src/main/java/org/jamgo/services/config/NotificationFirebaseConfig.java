package org.jamgo.services.config;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

@Configuration
@ConditionalOnProperty(name = "jamgo.notifications.push.provider", havingValue = "firebase")
public class NotificationFirebaseConfig implements InitializingBean {

	public static final Logger logger = LoggerFactory.getLogger(NotificationFirebaseConfig.class);

	@Override
	public void afterPropertiesSet() throws Exception {
		this.initializeFirebaseClient();
	}

	private void initializeFirebaseClient() {
		try {
			FirebaseApp.initializeApp(
				FirebaseOptions.builder()
					.setCredentials(GoogleCredentials.fromStream(this.getClass().getClassLoader().getResourceAsStream("firebase-adminsdk.json")))
					.build());
			NotificationFirebaseConfig.logger.info("Firebase client initialized");
		} catch (final IOException e) {
			NotificationFirebaseConfig.logger.warn("Error initializing Firebase client", e);
			// FIXME throw new RuntimeException(e);
		}
	}

}
