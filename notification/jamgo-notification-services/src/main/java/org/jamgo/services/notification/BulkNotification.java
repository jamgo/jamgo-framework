package org.jamgo.services.notification;

import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.notification.Notification.NotificationSystem;

public class BulkNotification {

	// TODO: How to define more recipients in subprojects? Meanwhile, we'll use a String for the field...
	public enum Recipient {
		ALL
	}

	private String recipients;
	private LocalizedString title;
	private LocalizedString text;
	private NotificationSystem notificationSystem;

	public String getRecipients() {
		return this.recipients;
	}

	public void setRecipients(final String recipients) {
		this.recipients = recipients;
	}

	public LocalizedString getTitle() {
		return this.title;
	}

	public void setTitle(final LocalizedString title) {
		this.title = title;
	}

	public LocalizedString getText() {
		return this.text;
	}

	public void setText(final LocalizedString text) {
		this.text = text;
	}

	public NotificationSystem getNotificationSystem() {
		return this.notificationSystem;
	}

	public void setNotificationSystem(final NotificationSystem notificationSystem) {
		this.notificationSystem = notificationSystem;
	}

}
