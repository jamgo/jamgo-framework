package org.jamgo.services.config;

import org.jamgo.services.notification.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NotificationServicesConfig {

	public static final Logger logger = LoggerFactory.getLogger(NotificationServicesConfig.class);

	@Bean
	@ConditionalOnMissingBean(NotificationService.class)
	public NotificationService notificationService() {
		NotificationServicesConfig.logger.info("No NotificationService bean found. Creating a default bean.");
		return new NotificationService();
	}

}
