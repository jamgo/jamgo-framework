package org.jamgo.services.notification.email;

import java.util.Locale;

import org.jamgo.services.impl.LocalizedMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

@Service
public class EmailService {

	@Autowired
	private EmailClient emailClient;
	@Autowired
	protected LocalizedMessageService messageSource;

	@Value("${jamgo.notifications.mail.from}")
	private String from;
	@Value("${jamgo.notifications.mail.cc:}")
	private String cc;
	@Value("${jamgo.notifications.mail.bcc:}")
	private String bcc;

	public void sendEmail(final String to, final String cc, final String subject, final String templateName, final Context context, final Locale locale) {
		final String from = this.from;
		final String bcc = this.bcc;
		context.setVariable("logoUrl", this.messageSource.getMessage("mail.logoURL"));
		context.setVariable("logoLink", this.messageSource.getMessage("mail.logoLink"));
		context.setVariable("signature", this.messageSource.getMessage("mail.signature", null, locale));

		this.emailClient.sendMailWithTemplate(to, from, cc, bcc, subject, templateName, context);
	}
}
