package org.jamgo.services.notification;

import org.jamgo.model.entity.User;
import org.jamgo.model.notification.Notification;
import org.jamgo.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class NotificationSender {

	public static final Logger logger = LoggerFactory.getLogger(NotificationSender.class);

	private final UserService userService;

	public NotificationSender(final UserService userService) {
		this.userService = userService;
	}

	protected abstract void send(final Notification notification);

	protected User getNotificationUser(final Notification notification) {
		User user = notification.getUser();
		if (user == null) {
			NotificationSender.logger.debug("No user found in Notification object. Retrieving user from service: {}", notification.getUsername());
			user = this.userService.getUser(notification.getUsername());
			notification.setUser(user);
		}
		return user;
	}
}
