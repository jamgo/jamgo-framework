package org.jamgo.services.notification;

import org.jamgo.model.notification.Notification.NotificationSystem;
import org.jamgo.services.notification.desktop.NotificationDesktopSender;
import org.jamgo.services.notification.email.NotificationEmailSender;
import org.jamgo.services.notification.push.NotificationPushSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationSenderFactory {

	@Autowired
	private NotificationDesktopSender desktopSender;

	@Autowired
	private NotificationEmailSender emailSender;

	@Autowired
	private NotificationPushSender pushSender;

	public NotificationSender getSender(final NotificationSystem system) {
		NotificationSender sender = null;
		switch (system) {
			case DESKTOP:
				sender = this.desktopSender;
				break;
			case EMAIL:
				sender = this.emailSender;
				break;
			case PUSH:
				sender = this.pushSender;
				break;
			default:
				throw new RuntimeException(String.format("No sender defined for notification system type: %s", system.name()));
		}
		return sender;
	}

}
