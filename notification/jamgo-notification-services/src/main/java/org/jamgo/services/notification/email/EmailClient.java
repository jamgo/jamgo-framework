package org.jamgo.services.notification.email;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Component
public class EmailClient {

	private static final Logger logger = LoggerFactory.getLogger(EmailClient.class);

	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private TemplateEngine templateEngine;

	public void sendMail(final String to, final String from, final String cc, final String bcc, final String subject, final String text, final boolean isDebug, final boolean isHTML) {
		try {
			final MimeMessage message = this.mailSender.createMimeMessage();
			final MimeMessageHelper helper = new MimeMessageHelper(message, "UTF-8");
			helper.setFrom(from);
			helper.setTo(to);
			if (StringUtils.hasLength(cc)) {
				helper.setCc(cc);
			}
			if (StringUtils.hasLength(bcc)) {
				helper.setBcc(bcc);
			}
			helper.setSubject(subject);
			helper.setText(text, true);
			this.mailSender.send(message);
		} catch (final MessagingException e) {
			EmailClient.logger.warn("Couldn't send email with HTML format, sending email without format", e);
			final SimpleMailMessage message = new SimpleMailMessage();
			message.setFrom(from);
			message.setTo(to);
			message.setCc(cc);
			message.setBcc(bcc);
			message.setSubject(subject);
			message.setText(text);
		}
	}

	public void sendMailWithTemplate(final String to, final String from, final String cc, final String bcc, final String subject, final String templateName, final Context context) {
		final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
		final MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "UTF-8");

		try {
			helper.setFrom(from);
			helper.setTo(to);
			if (StringUtils.hasLength(cc)) {
				helper.setCc(cc);
			}
			if (StringUtils.hasLength(bcc)) {
				helper.setBcc(bcc);
			}
			helper.setSubject(subject);
			final String htmlContent = this.templateEngine.process(templateName, context);
			helper.setText(htmlContent, true);
			this.mailSender.send(mimeMessage);
		} catch (final MessagingException e) {
			EmailClient.logger.error("Couldn't send email using template");
			throw new RuntimeException(e);
		}
	}

	// TODO: Refactor both methods - the code it's almost the same...

}
