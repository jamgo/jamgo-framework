package org.jamgo.services.notification.desktop;

import org.jamgo.model.notification.Notification;
import org.jamgo.services.UserService;
import org.jamgo.services.notification.NotificationSender;
import org.springframework.stereotype.Service;

@Service
public class NotificationDesktopSender extends NotificationSender {

	public NotificationDesktopSender(final UserService userService) {
		super(userService);
	}

	@Override
	public void send(final Notification notification) {
		// TODO: Add desktop notifications to jamgo-framework
	}

}
