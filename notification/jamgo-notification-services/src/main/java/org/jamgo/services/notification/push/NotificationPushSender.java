package org.jamgo.services.notification.push;

import org.jamgo.model.entity.User;
import org.jamgo.model.notification.Notification;
import org.jamgo.model.notification.PushNotificationRecipient;
import org.jamgo.services.UserService;
import org.jamgo.services.notification.NotificationSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;

@Service
public class NotificationPushSender extends NotificationSender {

	private static final Logger logger = LoggerFactory.getLogger(NotificationPushSender.class);

	public NotificationPushSender(final UserService userService) {
		super(userService);
	}

	@Override
	public void send(final Notification notification) {
		final String firebaseToken = this.getFirebaseToken(notification);
		if (firebaseToken != null) {
			NotificationPushSender.logger.info("Firebase token found, sending push notification {}", firebaseToken);
			try {
				FirebaseMessaging.getInstance().send(
					Message.builder()
						.setToken(firebaseToken)
						.setNotification(com.google.firebase.messaging.Notification.builder()
							.setTitle(notification.getTitle())
							.setBody(notification.getText())
							.build())
						.build());
			} catch (final FirebaseMessagingException e) {
				e.printStackTrace();
				throw new RuntimeException("Unable to send notification push", e);
			}
		}
	}

	private String getFirebaseToken(final Notification notification) {
		String firebaseToken = null;
		final User user = this.getNotificationUser(notification);
		if (user instanceof PushNotificationRecipient) {
			firebaseToken = ((PushNotificationRecipient) user).getFirebaseToken();
		}
		return firebaseToken;
	}

}
