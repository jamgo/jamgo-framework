package org.jamgo.services.notification;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.StringUtils;
import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.User;
import org.jamgo.model.notification.EmailNotificationRecipient;
import org.jamgo.model.notification.Notification;
import org.jamgo.model.notification.Notification.NotificationSystem;
import org.jamgo.model.notification.PushNotificationRecipient;
import org.jamgo.services.UserService;
import org.jamgo.services.impl.ModelService;
import org.jamgo.services.notification.BulkNotification.Recipient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class NotificationService extends ModelService<Notification> {

	private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);

	@Autowired
	private NotificationSenderFactory notificationSenderFactory;

	@Autowired
	private UserService userService;

	@Override
	public Notification save(final Notification item) {
		if (item.getCreationDate() == null) {
			item.setCreationDate(LocalDateTime.now());
		}
		final Notification saved = super.save(item);
		if (saved.getSentDate() == null) {
			// This method is called directly from backoffice, so all notifications will be sent to the user, without checking permissions
			this.sendNotification(saved, null, null);
		}
		return saved;
	}

	public SendBulkNotificationStats sendBulkNotification(final BulkNotification bulkNotification) {
		final List<? extends User> recipients = this.getRecipientsList(bulkNotification.getRecipients());
		return this.sendBulkNotification(bulkNotification.getTitle(), bulkNotification.getText(), recipients, bulkNotification.getNotificationSystem());
	}

	// TODO: Override this method in each project to determine the subsets of recipients of the application
	protected List<? extends User> getRecipientsList(final String recipientsName) {
		final Recipient recipients = Recipient.valueOf(recipientsName);
		List<? extends User> list;
		switch (recipients) {
			case ALL:
				list = this.userService.getAllUsers();
				break;
			default:
				throw new RuntimeException("Unknown Recipient: " + recipients.name());
		}
		return list;
	}

	public SendBulkNotificationStats sendBulkNotification(final LocalizedString localizedTitle, final LocalizedString localizedText, final List<? extends User> recipients, final NotificationSystem system) {
		final AtomicInteger counter = new AtomicInteger(0);
		final AtomicInteger errors = new AtomicInteger(0);
		recipients.forEach(eachRecipient -> this.sendNotification(localizedTitle, localizedText, eachRecipient, system, counter, errors));
		return new SendBulkNotificationStats(counter.get(), errors.get());
	}

	public void sendNotification(final LocalizedString localizedTitle, final LocalizedString localizedText, final User recipient, final NotificationSystem system) {
		this.sendNotification(localizedTitle, localizedText, recipient, system, null, null);
	}

	public void sendNotification(final LocalizedString localizedTitle, final LocalizedString localizedText, final User recipient, final NotificationSystem system, final AtomicInteger counter, final AtomicInteger errors) {
		final Language language = this.getRecipientLanguage(recipient);
		final String title = localizedTitle.get(language);
		final String text = localizedText.get(language);
		if (this.checkPermissions(recipient, system)) {
			final Notification notification = this.saveNotification(title, text, language, recipient, system);
			this.sendNotification(notification, counter, errors);
		}
	}

	private Language getRecipientLanguage(final User recipient) {
		return Optional.ofNullable(recipient.getLanguage()).orElse(this.getDefaultLanguage());
	}

	private Language getDefaultLanguage() {
		return this.languageService.getDefaultLanguage();
	}

	private boolean checkPermissions(final User user, final NotificationSystem system) {
		boolean allowed = false;
		switch (system) {
			case EMAIL:
				if (user instanceof EmailNotificationRecipient) {
					final EmailNotificationRecipient recipient = (EmailNotificationRecipient) user;
					if (!recipient.isAllowMailNotifications()) {
						NotificationService.logger.debug("User {} does not allow email notifications", user.getUsername());
					}
					allowed = recipient.isAllowMailNotifications();
				} else {
					NotificationService.logger.debug("User {} is not a valid email recipient", user.getUsername());
				}
				break;
			case PUSH:
				if (user instanceof PushNotificationRecipient) {
					final PushNotificationRecipient recipient = (PushNotificationRecipient) user;
					if (!recipient.isAllowPushNotifications()) {
						NotificationService.logger.debug("User {} does not allow push notifications", user.getUsername());
					}
					allowed = recipient.isAllowPushNotifications();
				} else {
					NotificationService.logger.debug("User {} is not a valid push notification recipient", user.getUsername());
				}
				break;
			case DESKTOP:
				allowed = true;
				break;
			default:
				NotificationService.logger.warn("Unknown notification system: {}", system.name());
				allowed = false;
		}
		return allowed;
	}

	private Notification saveNotification(final String title, final String text, final Language language, final User recipient, final NotificationSystem system) {
		final Notification notification = new Notification();
		notification.setTitle(title);
		notification.setText(text);
		notification.setLanguage(language);
		notification.setUsername(recipient.getUsername());
		notification.setNotificationSystem(system);
		notification.setCreationDate(LocalDateTime.now());
		return super.save(notification);
	}

	private void sendNotification(final Notification notification, final AtomicInteger counter, final AtomicInteger errors) {
		try {
			final NotificationSender notificationSender = this.notificationSenderFactory.getSender(notification.getNotificationSystem());
			notificationSender.send(notification);
			notification.setSentDate(LocalDateTime.now());
			notification.setErrorMessage(null);
			super.save(notification);
			NotificationService.logger.debug("Notification sent");
			Optional.ofNullable(counter).ifPresent(o -> o.addAndGet(1));
		} catch (final Exception e) {
			NotificationService.logger.warn("Error sending notification. Saving error message in Notification entity.", e);
			// Persist error message
			notification.setErrorMessage(StringUtils.left(e.getMessage(), 255));
			super.save(notification);
			Optional.ofNullable(errors).ifPresent(o -> o.addAndGet(1));
		}
	}

	public void readNotification(final Notification notification) {
		notification.setReadDate(LocalDateTime.now());
		super.save(notification);
	}

	public static class SendBulkNotificationStats {
		private final int successCount;
		private final int errorCount;

		public SendBulkNotificationStats(final int successCount, final int errorCount) {
			this.successCount = successCount;
			this.errorCount = errorCount;
		}

		public int getSuccessCount() {
			return this.successCount;
		}

		public int getErrorCount() {
			return this.errorCount;
		}

		public int getTotalCount() {
			return this.successCount + this.errorCount;
		}
	}
}
