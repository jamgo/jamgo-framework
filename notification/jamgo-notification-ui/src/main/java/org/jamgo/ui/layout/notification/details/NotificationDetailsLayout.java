package org.jamgo.ui.layout.notification.details;

import org.jamgo.model.entity.Language;
import org.jamgo.model.notification.Notification;
import org.jamgo.model.notification.Notification.NotificationSystem;
import org.jamgo.ui.component.JamgoFormLayout;
import org.jamgo.ui.component.ModelComboBox;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.vaadin.components.JamgoComponentConstants;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.textfield.TextField;

public class NotificationDetailsLayout extends CrudDetailsLayout<Notification> {

	private static final long serialVersionUID = 1L;

	private JamgoFormLayout mainFormLayout;

	protected TextField titleField;
	protected TextField textField;
	private TextField usernameField;
	protected ModelComboBox<Language> languageField;
	protected ComboBox<NotificationSystem> notificationSystemField;
	protected DateTimePicker creationDateField;
	protected DateTimePicker sentDateField;
	protected DateTimePicker readDateField;
	protected TextField errorMessageField;

	@Override
	protected CrudDetailsPanel createMainPanel() {
		final CrudDetailsPanel panel = this.componentBuilderFactory.createCrudDetailsPanelBuilder()
			.setName(this.messageSource.getMessage("form.basic.info")).build();

		this.mainFormLayout = (JamgoFormLayout) panel.getFormLayout();

		// TODO: Use code guidelines to write this method
		this.addTitleField(this.mainFormLayout, "notification.title", ComponentColspan.SIX_COLUMNS);
		this.addTextField(this.mainFormLayout, "notification.text", ComponentColspan.SIX_COLUMNS);
		this.addUserField(this.mainFormLayout, "notification.user", ComponentColspan.SIX_COLUMNS);
		this.addLanguageField(this.mainFormLayout, "notification.language", ComponentColspan.THREE_COLUMNS);
		this.addNotificationSystemField(this.mainFormLayout, "notification.notificationSystem", ComponentColspan.THREE_COLUMNS);
		this.addCreationDateField(this.mainFormLayout, "notification.creationDate", ComponentColspan.SIX_COLUMNS);
		this.addSentDateField(this.mainFormLayout, "notification.sentDate", ComponentColspan.SIX_COLUMNS);
		this.addReadDateField(this.mainFormLayout, "notification.readDate", ComponentColspan.SIX_COLUMNS);
		this.addErrorMessageField(this.mainFormLayout, "notification.errorMessage", ComponentColspan.SIX_COLUMNS);

		return panel;
	}

	protected void addTitleField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.titleField = this.componentBuilderFactory.createTextFieldBuilder()
			.build();
		formLayout.addFormComponent(this.titleField, fieldKey, colspan);
		this.binder.forField(this.titleField)
			.asRequired()
			.bind(o -> o.getTitle(), (o, v) -> o.setTitle(v));
	}

	protected void addTextField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.textField = this.componentBuilderFactory.createTextFieldBuilder()
			.build();
		formLayout.addFormComponent(this.textField, fieldKey, colspan);
		this.binder.forField(this.textField)
			.asRequired()
			.bind(o -> o.getText(), (o, v) -> o.setText(v));
	}

	protected void addUserField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.addUsernameField(formLayout, fieldKey, colspan);
	}

	private void addUsernameField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.usernameField = this.componentBuilderFactory.createTextFieldBuilder()
			.build();
		formLayout.addFormComponent(this.usernameField, fieldKey, colspan);
		this.binder.forField(this.usernameField)
			.asRequired()
			.bind(o -> o.getUsername(), (o, v) -> o.setUsername(v));
	}

	protected void addLanguageField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.languageField = this.componentBuilderFactory.createModelComboBoxBuilder(Language.class)
			.hideActions()
			.build();
		this.languageField.setItemLabelGenerator(item -> item.getName());
		this.languageField.setRequiredIndicatorVisible(true);
		formLayout.addFormComponent(this.languageField, fieldKey, colspan);
		this.binder.forField(this.languageField)
			.asRequired()
			.bind(o -> o.getLanguage(), (o, v) -> o.setLanguage(v));
	}

	protected void addNotificationSystemField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.notificationSystemField = this.componentBuilderFactory.createEnumComboBoxBuilder(NotificationSystem.class)
			.build();
		formLayout.addFormComponent(this.notificationSystemField, fieldKey, colspan);
		this.binder.forField(this.notificationSystemField)
			.asRequired()
			.bind(o -> o.getNotificationSystem(), (o, v) -> o.setNotificationSystem(v));
	}

	protected void addCreationDateField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.creationDateField = this.componentBuilderFactory.createDateTimeFieldBuilder()
			.build();
		formLayout.addFormComponent(this.creationDateField, fieldKey, colspan);
		this.binder.bindReadOnly(this.creationDateField, o -> o.getCreationDate());
	}

	protected void addSentDateField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.sentDateField = this.componentBuilderFactory.createDateTimeFieldBuilder()
			.build();
		formLayout.addFormComponent(this.sentDateField, fieldKey, colspan);
		this.binder.bindReadOnly(this.sentDateField, o -> o.getSentDate());
	}

	protected void addReadDateField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.readDateField = this.componentBuilderFactory.createDateTimeFieldBuilder()
			.build();
		formLayout.addFormComponent(this.readDateField, fieldKey, colspan);
		this.binder.bindReadOnly(this.readDateField, o -> o.getReadDate());
	}

	protected void addErrorMessageField(final JamgoFormLayout formLayout, final String fieldKey, final JamgoComponentConstants.ComponentColspan colspan) {
		this.errorMessageField = this.componentBuilderFactory.createTextFieldBuilder().build();
		formLayout.addFormComponent(this.errorMessageField, fieldKey, colspan);
		this.binder.bindReadOnly(this.errorMessageField, o -> o.getErrorMessage());
	}

	@Override
	protected Class<Notification> getTargetObjectClass() {
		return Notification.class;
	}

}
