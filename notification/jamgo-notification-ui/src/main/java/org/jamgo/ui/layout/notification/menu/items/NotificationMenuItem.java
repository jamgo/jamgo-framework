package org.jamgo.ui.layout.notification.menu.items;

import org.jamgo.model.notification.Notification;
import org.jamgo.ui.layout.menu.BackofficeMenuLayout.BackofficeMenuGroup;
import org.jamgo.ui.layout.menu.MenuItemComponent;
import org.jamgo.ui.layout.menu.ModelMenuItemDef;

@MenuItemComponent
public class NotificationMenuItem extends ModelMenuItemDef<Notification> {

	public NotificationMenuItem() {
		super(Notification.class);
		// this.setIcon(VaadinIcon.BELL);
		this.setMenuGroup(BackofficeMenuGroup.CONFIGURATION);
	}

}
