package org.jamgo.ui.layout.notification.table;

import org.jamgo.model.notification.Notification;
import org.jamgo.services.notification.NotificationService;
import org.jamgo.services.notification.NotificationService.SendBulkNotificationStats;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.crud.CrudTableLayout;
import org.jamgo.ui.layout.notification.dialog.NotificationsBulkInsertDialogLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.componentfactory.EnhancedDialog;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class NotificationsTableLayout extends CrudTableLayout<Notification, Notification, Long> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private NotificationService notificationService;

	private Button bulkInsertButton;

	public NotificationsTableLayout(final CrudLayoutDef<Notification, Notification> crudLayoutDef) {
		super(crudLayoutDef);
	}

	@Override
	protected HorizontalLayout createToolBar() {
		super.createToolBar();

		this.bulkInsertButton = this.componentBuilderFactory.createButtonBuilder()
			.setText(this.messageSource.getMessage("action.bulkInsert"))
			.build();
		this.bulkInsertButton.addClickListener(event -> this.attemptLostFocusDetails(this.detailsLayout, () -> this.startBulkInsert()));
		this.buttonsLayout.add(this.bulkInsertButton);

		return this.toolBar;
	}

	private void startBulkInsert() {
		final EnhancedDialog dialog = this.componentBuilderFactory.createEnhancedDialogBuilder()
			.setHeader(this.messageSource.getMessage("notification.bulk.title"))
			.setModal(true)
			.setCloseOnEsc(false)
			.setCloseOnOutsideClick(false)
			.build();

		final NotificationsBulkInsertDialogLayout dialogLayout = this.applicationContext.getBean(NotificationsBulkInsertDialogLayout.class);
		dialogLayout.setOkHandler(layout -> this.doBulkInsert(layout, dialog));
		dialogLayout.setCloseHandler(layout -> dialog.close());

		dialog.setContent(dialogLayout);
		dialog.open();
	}

	private void doBulkInsert(final NotificationsBulkInsertDialogLayout layout, final Dialog dialog) {
		final SendBulkNotificationStats stats = this.notificationService.sendBulkNotification(layout.getTargetObject());
		dialog.close();
		final com.vaadin.flow.component.notification.Notification notification = com.vaadin.flow.component.notification.Notification.show(
			this.messageSource.getMessage("notification.bulk.result", new String[] { Integer.toString(stats.getTotalCount()), Integer.toString(stats.getSuccessCount()), Integer.toString(stats.getErrorCount()) }));
		notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
		this.refreshAll();
	}

	@Override
	protected Notification saveTargetObject() {
		return this.notificationService.save((Notification) this.detailsLayout.getTargetObject());
	}
}
