package org.jamgo.ui.config;

import org.jamgo.ui.layout.notification.details.NotificationDetailsLayout;
import org.jamgo.ui.layout.notification.dialog.NotificationsBulkInsertDialogLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class NotificationUIConfig {

	public static final Logger logger = LoggerFactory.getLogger(NotificationUIConfig.class);

	@Bean
	@Scope(BeanDefinition.SCOPE_PROTOTYPE)
	@ConditionalOnMissingBean(NotificationDetailsLayout.class)
	public NotificationDetailsLayout notificationDetailsLayout() {
		NotificationUIConfig.logger.info("No NotificationDetailsLayout bean found. Creating a default bean.");
		return new NotificationDetailsLayout();
	}

	@Bean
	@Scope(BeanDefinition.SCOPE_PROTOTYPE)
	@ConditionalOnMissingBean(NotificationsBulkInsertDialogLayout.class)
	public NotificationsBulkInsertDialogLayout notificationsBulkInsertDialogLayout() {
		NotificationUIConfig.logger.info("No NotificationsBulkInsertDialogLayout bean found. Creating a default bean.");
		return new NotificationsBulkInsertDialogLayout();
	}
}
