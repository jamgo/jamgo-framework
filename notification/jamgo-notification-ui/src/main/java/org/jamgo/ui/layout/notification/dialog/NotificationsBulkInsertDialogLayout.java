package org.jamgo.ui.layout.notification.dialog;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.jamgo.model.notification.Notification.NotificationSystem;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.notification.BulkNotification;
import org.jamgo.services.notification.BulkNotification.Recipient;
import org.jamgo.ui.component.JamgoFormLayout;
import org.jamgo.ui.component.LocalizedField.DisplayMode;
import org.jamgo.ui.component.LocalizedTextArea;
import org.jamgo.ui.component.LocalizedTextField;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.ui.layout.crud.CrudLayout;
import org.jamgo.vaadin.components.JamgoComponentConstants.ComponentColspan;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;

public class NotificationsBulkInsertDialogLayout extends CrudLayout<BulkNotification> implements InitializingBean {

	private static final long serialVersionUID = 1L;

	@Autowired
	private LocalizedMessageService messageSource;

	private Button okButton;
	private Button cancelButton;
	private Consumer<NotificationsBulkInsertDialogLayout> okHandler;
	private Consumer<NotificationsBulkInsertDialogLayout> closeHandler;

	private ComboBox<Recipient> recipientsField;
	protected LocalizedTextField titleField;
	protected LocalizedTextArea textField;
	protected ComboBox<NotificationSystem> notificationSystemField;

	public NotificationsBulkInsertDialogLayout() {
		this.binder = new Binder<>(this.getTargetObjectClass());
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}

	private void init() {
		super.initialize();
		this.setSizeFull();
		this.addPanel();
		this.addActionButtons();
		try {
			this.updateFields();
		} catch (final Exception e) {
			// TODO: show message error style.
			Notification.show(this.messageSource.getMessage("error.internal"));
		}
	}

	private void addPanel() {
		final CrudDetailsPanel panel = this.componentBuilderFactory.createCrudDetailsPanelBuilder()
			.setName(this.messageSource.getMessage("form.basic.info")).build();

		final JamgoFormLayout mainFormLayout = (JamgoFormLayout) panel.getFormLayout();

		this.addRecipientsField(mainFormLayout, "notification.bulk.recipients", ComponentColspan.SIX_COLUMNS);
		this.addNotificationSystemField(mainFormLayout, "notification.notificationSystem", ComponentColspan.SIX_COLUMNS);
		this.addTitleField(mainFormLayout, "notification.title", ComponentColspan.FULL_WIDTH);
		this.addTextField(mainFormLayout, "notification.text", ComponentColspan.FULL_WIDTH);

		this.addAndExpand(panel);
	}

	protected void addRecipientsField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.recipientsField = this.componentBuilderFactory.createEnumComboBoxBuilder(Recipient.class)
			.build();
		formLayout.addFormComponent(this.recipientsField, fieldKey, colspan);
		this.binder.forField(this.recipientsField)
			.asRequired()
			.bind(o -> Recipient.valueOf(o.getRecipients()), (o, v) -> o.setRecipients(v.name()));
	}

	protected void addNotificationSystemField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.notificationSystemField = this.componentBuilderFactory.createEnumComboBoxBuilder(NotificationSystem.class)
			.build();
		formLayout.addFormComponent(this.notificationSystemField, fieldKey, colspan);
		this.binder.forField(this.notificationSystemField)
			.asRequired()
			.bind(o -> o.getNotificationSystem(), (o, v) -> o.setNotificationSystem(v));
	}

	protected void addTitleField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.titleField = this.componentBuilderFactory.createLocalizedTextFieldBuilder()
			.setDisplayMode(DisplayMode.ALL_LANGUAGES)
			.build();
		this.titleField.setRequiredIndicatorVisible(true);
		formLayout.addFormComponent(this.titleField, fieldKey, colspan);
		this.binder.forField(this.titleField)
			.asRequired(this.messageSource.getMessageForField("required.field", "notification.title"))
			.bind(o -> o.getTitle(), (o, v) -> o.setTitle(v));
	}

	protected void addTextField(final JamgoFormLayout formLayout, final String fieldKey, final ComponentColspan colspan) {
		this.textField = this.componentBuilderFactory.createLocalizedTextAreaBuilder()
			.setDisplayMode(DisplayMode.ALL_LANGUAGES)
			.build();
		this.textField.setRequiredIndicatorVisible(true);
		formLayout.addFormComponent(this.textField, fieldKey, colspan);
		this.binder.forField(this.textField)
			.asRequired(this.messageSource.getMessageForField("required.field", "notification.text"))
			.bind(o -> o.getText(), (o, v) -> o.setText(v));
	}

	@Override
	protected Class<BulkNotification> getTargetObjectClass() {
		return BulkNotification.class;
	}

	@Override
	protected List<? extends Component> createActionButtons() {
		final List<Button> buttons = new ArrayList<>();

		this.okButton = this.componentBuilderFactory.createButtonBuilder()
			.addThemeVariants(ButtonVariant.LUMO_PRIMARY)
			.setText(this.messageSource.getMessage("action.save"))
			.build();
		this.okButton.addClickShortcut(Key.ENTER, KeyModifier.CONTROL);
		this.okButton.addClickListener(event -> this.doOk());
		buttons.add(this.okButton);

		this.cancelButton = this.componentBuilderFactory.createButtonBuilder()
			.setText(this.messageSource.getMessage("action.cancel"))
			.build();
		this.cancelButton.addClickListener(event -> this.doCancel());
		buttons.add(this.cancelButton);

		return buttons;
	}

	private void doOk() {
		if (this.okHandler != null) {
			try {
				this.updateTargetObject();
				this.okHandler.accept(this);
			} catch (final ValidationException e) {
				// TODO: show error message style
				Notification.show(this.messageSource.getMessage("validation.error"));
			}
		}
	}

	private void doCancel() {
		if (this.closeHandler != null) {
			this.closeHandler.accept(this);
		}
	}

	public Consumer<NotificationsBulkInsertDialogLayout> getOkHandler() {
		return this.okHandler;
	}

	public void setOkHandler(final Consumer<NotificationsBulkInsertDialogLayout> okHandler) {
		this.okHandler = okHandler;
	}

	public Consumer<NotificationsBulkInsertDialogLayout> getCloseHandler() {
		return this.closeHandler;
	}

	public void setCloseHandler(final Consumer<NotificationsBulkInsertDialogLayout> closeHandler) {
		this.closeHandler = closeHandler;
	}

}
