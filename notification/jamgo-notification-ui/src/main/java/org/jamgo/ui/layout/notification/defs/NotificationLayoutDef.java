package org.jamgo.ui.layout.notification.defs;

import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.jamgo.model.notification.Notification;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.jamgo.ui.layout.defs.LayoutDefComponent;
import org.jamgo.ui.layout.defs.ModelLayoutDef;
import org.jamgo.ui.layout.notification.details.NotificationDetailsLayout;
import org.jamgo.ui.layout.notification.table.NotificationsTableLayout;

@LayoutDefComponent
public class NotificationLayoutDef extends ModelLayoutDef<Notification> {

	public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

	public NotificationLayoutDef(final LocalizedMessageService messageSource, final LocalizedObjectService localizedObjectService) {
		super(messageSource, localizedObjectService);
	}

	@SuppressWarnings("unchecked")
	@Override
	public CrudLayoutDef<Notification, Notification> getBackofficeLayoutDef() {
		// @formatter:off
		return (CrudLayoutDef<Notification, Notification>) CrudLayoutDef.<Notification> builderEntity()
			.setId("notification")
			.setNameSupplier(() -> this.messageSource.getMessage("table.notification"))
			.modelGridDef()
				.setEntityClass(Notification.class)
				.columnDef()
					.setId("notification.title")
					.setCaptionSupplier(() -> this.messageSource.getMessage("notification.title"))
					.setValueProvider(o -> o.getTitle())
				.end()
				.columnDef()
					.setId("notification.text")
					.setCaptionSupplier(() -> this.messageSource.getMessage("notification.text"))
					.setValueProvider(o -> o.getText())
				.end()
				.columnDef()
					.setId("notification.user")
					.setCaptionSupplier(() -> this.messageSource.getMessage("notification.user"))
					.setValueProvider(o -> o.getUsername())
				.end()
				.columnDef()
					.setId("notification.notificationSystem")
					.setCaptionSupplier(() -> this.messageSource.getMessage("notification.notificationSystem"))
					.setValueProvider(o -> this.messageSource.getMessage("notification.notificationSystem." + o.getNotificationSystem().name()))
				.end()
				.columnDef()
					.setId("notification.sentDate")
					.setCaptionSupplier(() -> this.messageSource.getMessage("notification.sentDate"))
					.setValueProvider(o -> Optional.ofNullable(o.getSentDate()).map(date -> NotificationLayoutDef.DATETIME_FORMATTER.format(date)).orElse(""))
				.end()
				.detailsLayoutClass(NotificationDetailsLayout.class)
			.end()
			.tableLayoutVerticalConfig()
			.tableLayoutClass(NotificationsTableLayout.class)
			.build();
		// @formatter:on
	}

	@Override
	public Class<Notification> getModelClass() {
		return Notification.class;
	}

}
