package org.jamgo.model.entity;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.Type;

@MappedSuperclass
public abstract class Territory extends Model {

	private static final long serialVersionUID = 1L;

	public static final Comparator<Territory> NAME_ORDER = new Comparator<>() {
		@Override
		public int compare(final Territory objectA, final Territory objectB) {
			return objectA.getName().getDefaultText().compareToIgnoreCase(objectB.getName().getDefaultText());
		}
	};

	public enum TerritoryType {
		PROVINCE, REGION, TOWN, DISTRICT, NEIGHBORHOOD
	}

	@Type(type = "json")
	@Column(columnDefinition = "json")
	private LocalizedString name;

	public Territory() {
		super();
	}

	public LocalizedString getName() {
		return this.name;
	}

	public void setName(final LocalizedString name) {
		this.name = name;
	}

	public abstract TerritoryType getTerritoryType();
}