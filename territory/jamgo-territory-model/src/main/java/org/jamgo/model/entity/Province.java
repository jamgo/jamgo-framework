package org.jamgo.model.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@javax.persistence.Entity
@Table(name = "province")
public class Province extends Territory {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "province_id_gen", table = "sequence_table", pkColumnName = "seq_name", valueColumnName = "seq_count", pkColumnValue = "province_seq", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "province_id_gen")
	private Long id;

	@OneToMany(mappedBy = "province", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Region> regions;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public List<Region> getRegions() {
		return this.regions;
	}

	public void setRegions(List<Region> regions) {
		this.regions = regions;
	}

	@Override
	public TerritoryType getTerritoryType() {
		return TerritoryType.PROVINCE;
	}

}
