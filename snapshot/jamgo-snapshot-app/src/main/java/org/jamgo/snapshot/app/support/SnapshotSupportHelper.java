package org.jamgo.snapshot.app.support;

import java.util.UUID;

import org.jamgo.model.entity.User;
import org.jamgo.snapshot.model.snapshot.SnapshotInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * Class with common logic for SnapshotFilter and SnapshotInterceptor.
 * AFAIK, Vaadin needs a Filter and Spring MVC an Interceptor, so same logic must be executed in both classes.
 */
@Component
@ConditionalOnProperty(value = "snapshots.enabled", havingValue = "true", matchIfMissing = false)
public class SnapshotSupportHelper {

	public static final Logger logger = LoggerFactory.getLogger(SnapshotSupportHelper.class);

	public void preHandle(final SnapshotInfo snapshotInfo, final User currentUser) {
		snapshotInfo.setTransactionId(UUID.randomUUID());
		if (currentUser != null) {
			snapshotInfo.setConnectedUsername(currentUser.getUsername());
		}
		SnapshotSupportHelper.logger.trace("Snapshot info initialized: {} - {}", snapshotInfo.getConnectedUsername(), snapshotInfo.getTransactionId().toString());
	}

	public void postHandle(final SnapshotInfo snapshotInfo) {
		snapshotInfo.clear();
		SnapshotSupportHelper.logger.trace("Snapshot info cleared");
	}

}
