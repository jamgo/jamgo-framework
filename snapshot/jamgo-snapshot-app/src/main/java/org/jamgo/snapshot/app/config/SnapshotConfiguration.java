package org.jamgo.snapshot.app.config;

import org.jamgo.snapshot.model.config.SnapshotModelConfig;
import org.jamgo.snapshot.model.entity.ModelEntitySnapshotListenerHelper;
import org.jamgo.snapshot.model.repository.SnapshotGenerator;
import org.jamgo.snapshot.model.snapshot.SnapshotInfo;
import org.jamgo.snapshot.services.SnapshotComparerService;
import org.jamgo.snapshot.services.snapshot.SnapshotBulkLoader;
import org.jamgo.snapshot.services.snapshot.SnapshotInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.aop.target.ThreadLocalTargetSource;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Snapshot configuration. Initialization of different objects required for snapshot functionality.
 *
 * Based on
 * https://dzone.com/articles/an-alternative-approach-to-threadlocal-using-sprin-1
 *
 * @author Jamgo SCCL
 *
 */

@Configuration
@Import({ SnapshotModelConfig.class })
@ComponentScan(
	basePackageClasses = {
		org.jamgo.snapshot.app.support.PackageMarker.class
	})
@EnableAsync
public class SnapshotConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(SnapshotConfiguration.class);

	@Bean(destroyMethod = "destroy")
	public ThreadLocalTargetSource threadLocalSnapshotInfo() {
		SnapshotConfiguration.logger.debug("creating new bean threadLocalSnapshotInfo");
		final ThreadLocalTargetSource result = new ThreadLocalTargetSource();
		result.setTargetBeanName("snapshotInfo");
		return result;
	}

	@Primary
	@Bean(name = "proxiedThreadLocalTargetSource")
	public ProxyFactoryBean proxiedThreadLocalTargetSource(final ThreadLocalTargetSource threadLocalTargetSource) {
		SnapshotConfiguration.logger.debug("creating new bean proxiedThreadLocalTargetSource");
		final ProxyFactoryBean result = new ProxyFactoryBean();
		result.setTargetSource(threadLocalTargetSource);
		return result;
	}

	@Bean(name = "snapshotInfo")
	@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public SnapshotInfo snapshotInfo() {
		SnapshotConfiguration.logger.debug("creating new bean SnapshotInfo");
		return new SnapshotInfo();
	}

	/**
	 * Initialization of SnapshotGenerator instance. This bean is initialized here,
	 * and not with @Component annotation, to avoid use of this snapshot
	 * functionality in tests.
	 */
	@Bean
	public SnapshotGenerator snapshotGenerator() {
		SnapshotConfiguration.logger.debug("creating new bean SnapshotGenerator");
		return new SnapshotGenerator();
	}

	@Bean
	@ConditionalOnProperty(value = "snapshots.enabled", havingValue = "true", matchIfMissing = false)
	public SnapshotBulkLoader snapshotBulkLoader() {
		SnapshotConfiguration.logger.debug("creating new bean SnapshotBulkLoader");
		return new SnapshotBulkLoader();
	}

	@Bean
	@ConditionalOnProperty(value = "snapshots.enabled", havingValue = "true", matchIfMissing = false)
	public SnapshotInitializer snapshotInitializer() {
		SnapshotConfiguration.logger.debug("creating new bean SnapshotInitializer");
		return new SnapshotInitializer();
	}

	/**
	 * Entity listener for every Model entity. JPA creates a new instance of this
	 * object out of Spring context, so that instance is not autowired with Spring
	 * objects. To solve this problem, an instance of this class is created here to
	 * autowire static objects of this class.
	 */
//	@Bean
//	@DependsOn("snapshotGenerator")
//	public ModelEntityListener modelEntityListener() {
//		SnapshotConfiguration.logger.debug("creating new bean ModelEntityListener");
//		return new ModelEntityListener();
//	}

	/**
	 * Model Entity listener helper for snapshots generation.
	 */
	@Bean
	@ConditionalOnProperty(value = "snapshots.enabled", havingValue = "true", matchIfMissing = false)
	public ModelEntitySnapshotListenerHelper modelEntitySnapshotListenerHelper() {
		SnapshotConfiguration.logger.debug("creating new bean ModelEntitySnapshotListenerHelper");
		return new ModelEntitySnapshotListenerHelper();
	}

	@Bean
	public SnapshotComparerService snapshotComparerService() {
		SnapshotConfiguration.logger.debug("creating new bean SnapshotComparerService()");
		return new SnapshotComparerService();
	}

}
