package org.jamgo.snapshot.model.snapshot;

import org.jamgo.model.entity.ModelReference;
import org.jamgo.snapshot.model.snapshot.SnapshotComparison.Result;

public class SnapshotComparisonModelAttribute extends SnapshotComparisonAttribute<ModelReference> {

	private ModelReference valueLeft;
	private ModelReference valueRight;

	public SnapshotComparisonModelAttribute() {
		this.setType(AttributeType.MODEL);
		this.setMoreInfo(true);
	}

	@Override
	public ModelReference getValueLeft() {
		return this.valueLeft;
	}

	@Override
	public ModelReference getValueRight() {
		return this.valueRight;
	}

	@Override
	protected void setInnerValueLeft(final ModelReference value) {
		this.valueLeft = value;
	}

	@Override
	protected void setInnerValueRight(final ModelReference value) {
		this.valueRight = value;
	}

	@Override
	protected void compareInnerValues() {
		if (this.valueLeft.equals(this.valueRight)) {
			this.setResult(Result.EQUAL);
		} else {
			this.setResult(Result.MODIFIED);
		}
	}

}
