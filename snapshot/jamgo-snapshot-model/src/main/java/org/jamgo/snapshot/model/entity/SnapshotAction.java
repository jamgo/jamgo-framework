package org.jamgo.snapshot.model.entity;

/**
 * Types of action allowed in snapshots.
 * 
 * @author Jamgo SCCL
 * 
 */
public enum SnapshotAction {
	CREATE, UPDATE, DELETE
}
