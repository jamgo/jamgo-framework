package org.jamgo.snapshot.model.snapshot;

import java.util.UUID;

/**
 * Information required to generate model entities' snapshots. This variable is
 * defined as ThreadLocal, so every thread will store a different instance of
 * this object.
 *
 * @author Jamgo SCCL
 *
 */
public class SnapshotInfo {

	private UUID transactionId;
	private String connectedUsername;

	public void clear() {
		this.transactionId = UUID.randomUUID();
		this.connectedUsername = null;
	}

	public UUID getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(final UUID transactionId) {
		this.transactionId = transactionId;
	}

	public String getConnectedUsername() {
		return this.connectedUsername;
	}

	public void setConnectedUsername(final String connectedUsername) {
		this.connectedUsername = connectedUsername;
	}

	@Override
	public String toString() {
		return String.format("connectedUsername: {}, transactionId: {}", this.connectedUsername, this.transactionId.toString());
	}

}
