package org.jamgo.snapshot.model.snapshot;

import java.util.ArrayList;
import java.util.List;

public class SnapshotComparison {

	public enum Result {
		EQUAL, MODIFIED, NEW, DELETED
	}

	private Result result;
	private SnapshotComparisonData left;
	private SnapshotComparisonData right;
	private List<SnapshotComparisonAttribute<?>> attributes = new ArrayList<>();

	public Result getResult() {
		return this.result;
	}

	public void setResult(final Result result) {
		this.result = result;
	}

	public SnapshotComparisonData getLeft() {
		return this.left;
	}

	public void setLeft(final SnapshotComparisonData left) {
		this.left = left;
	}

	public SnapshotComparisonData getRight() {
		return this.right;
	}

	public void setRight(final SnapshotComparisonData right) {
		this.right = right;
	}

	public List<SnapshotComparisonAttribute<?>> getAttributes() {
		return this.attributes;
	}

	public void setAttributes(final List<SnapshotComparisonAttribute<?>> attributes) {
		this.attributes = attributes;
	}

}
