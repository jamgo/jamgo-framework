package org.jamgo.snapshot.model.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to mark the model fields that must be excluded from snapshots
 * representation in the client side
 * 
 * @author Jamgo SCCL
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SnapshotHiddenField {
	public String key() default "";
}
