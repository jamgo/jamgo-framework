package org.jamgo.snapshot.model.entity;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.jamgo.model.entity.Model;

/**
 * Entity to represent an snapshot in the database. This entity does not extend
 * {@link Model} class. The id is an {@link UUID} defined as the transactionId
 * of the current request.
 *
 * @author Jamgo SCCL
 *
 */
@Entity
public class Snapshot {

	@Id
	@Column
	private String id;
	private String username;
	private LocalDateTime timestamp;

	public String getId() {
		return this.id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public LocalDateTime getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(final LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Snapshot)) {
			return false;
		}
		final Snapshot other = (Snapshot) obj;
		return Objects.equals(this.id, other.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.id);
	}

}
