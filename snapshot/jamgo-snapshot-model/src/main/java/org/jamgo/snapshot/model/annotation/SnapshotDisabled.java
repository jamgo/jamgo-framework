package org.jamgo.snapshot.model.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to indicate to SnapshotGenerator that does not have to take
 * snapshots of the instances of the annotated entity class as snapshots are
 * enabled by default for all the model entities.
 * 
 * @author Jamgo SCCL
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SnapshotDisabled {
}
