package org.jamgo.snapshot.model.snapshot;

import java.util.UUID;

import org.jamgo.model.entity.Model;
import org.jamgo.snapshot.model.entity.SnapshotAction;

/**
 * Information required to generate an snapshot, used in the EventListener
 * defined for this event.
 *
 * @author Jamgo SCCL
 *
 **/
public class SnapshotEventData {

	private Model model;
	private SnapshotAction action;
	private UUID transactionId;
	private String userName;

	public SnapshotEventData(final Model model, final SnapshotAction action, final UUID transactionId, final String userName) {
		this.model = model;
		this.action = action;
		this.transactionId = transactionId;
		this.userName = userName;
	}

	public Model getModel() {
		return this.model;
	}

	public void setModel(final Model model) {
		this.model = model;
	}

	public SnapshotAction getAction() {
		return this.action;
	}

	public void setAction(final SnapshotAction action) {
		this.action = action;
	}

	public UUID getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(final UUID transactionId) {
		this.transactionId = transactionId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(final String userName) {
		this.userName = userName;
	}

}
