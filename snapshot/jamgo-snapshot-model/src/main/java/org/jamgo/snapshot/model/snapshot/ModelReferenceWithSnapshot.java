package org.jamgo.snapshot.model.snapshot;

import org.jamgo.model.entity.ModelReference;
import org.jamgo.snapshot.model.entity.Snapshot;

public class ModelReferenceWithSnapshot extends ModelReference {

	private Snapshot snapshot;

	public Snapshot getSnapshot() {
		return this.snapshot;
	}

	public void setSnapshot(final Snapshot snapshot) {
		this.snapshot = snapshot;
	}

}
