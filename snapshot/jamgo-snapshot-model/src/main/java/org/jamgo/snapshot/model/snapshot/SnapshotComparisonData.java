package org.jamgo.snapshot.model.snapshot;

import java.util.List;

import org.jamgo.model.entity.Model;
import org.jamgo.snapshot.model.entity.Snapshot;
import org.jamgo.snapshot.model.entity.SnapshotEntity;
import org.jamgo.snapshot.model.entity.SnapshotRelationship;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;

public class SnapshotComparisonData {

	private Snapshot snapshot;
	@JsonIgnore
	private SnapshotEntity entity;
	@JsonIgnore
	private List<SnapshotRelationship> relationships;
	@JsonIgnore
	private JsonNode modelData;
	@JsonIgnore
	private Model model;

	public Snapshot getSnapshot() {
		return this.snapshot;
	}

	public void setSnapshot(final Snapshot snapshot) {
		this.snapshot = snapshot;
	}

	public SnapshotEntity getEntity() {
		return this.entity;
	}

	public void setEntity(final SnapshotEntity entity) {
		this.entity = entity;
	}

	public List<SnapshotRelationship> getRelationships() {
		return this.relationships;
	}

	public void setRelationships(final List<SnapshotRelationship> relationships) {
		this.relationships = relationships;
	}

	public JsonNode getModelData() {
		return this.modelData;
	}

	public void setModelData(final JsonNode modelData) {
		this.modelData = modelData;
	}

	public Model getModel() {
		return this.model;
	}

	public void setModel(final Model model) {
		this.model = model;
	}

}
