package org.jamgo.snapshot.model.snapshot;

import org.jamgo.model.entity.ModelReference;
import org.jamgo.snapshot.model.entity.Snapshot;
import org.jamgo.snapshot.model.entity.SnapshotAction;
import org.jamgo.snapshot.model.entity.SnapshotEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SnapshotVersion {

	private Snapshot snapshot;
	@JsonIgnore
	private SnapshotEntity snapshotEntity;

	public Snapshot getSnapshot() {
		return this.snapshot;
	}

	public void setSnapshot(final Snapshot snapshot) {
		this.snapshot = snapshot;
	}

	public ModelReference getEntity() {
		return this.snapshotEntity.getEntity();
	}

	public SnapshotAction getAction() {
		return this.snapshotEntity.getAction();
	}

	public String getDescription() {
		return this.snapshotEntity.getDescription();
	}

	public SnapshotEntity getSnapshotEntity() {
		return this.snapshotEntity;
	}

	public void setSnapshotEntity(final SnapshotEntity snapshotEntity) {
		this.snapshotEntity = snapshotEntity;
	}

}
