package org.jamgo.snapshot.model.snapshot;

import java.util.ArrayList;
import java.util.List;

import org.jamgo.snapshot.model.snapshot.SnapshotComparison.Result;

public class SnapshotComparisonEmbeddedComplexAttribute extends SnapshotComparisonAttribute<Object> {

	private Object valueLeft;
	private Object valueRight;
	private List<SnapshotComparisonAttribute<Object>> children = new ArrayList<>();

	public SnapshotComparisonEmbeddedComplexAttribute() {
		this.setType(AttributeType.EMBEDDED);
		this.setMoreInfo(true);
	}

	@Override
	public Object getValueLeft() {
		return this.valueLeft;
	}

	@Override
	public Object getValueRight() {
		return this.valueRight;
	}

	public List<SnapshotComparisonAttribute<Object>> getChildren() {
		return this.children;
	}

	public void setChildren(final List<SnapshotComparisonAttribute<Object>> children) {
		this.children = children;
	}

	@Override
	protected void setInnerValueLeft(final Object value) {
		this.valueLeft = value;
	}

	@Override
	protected void setInnerValueRight(final Object value) {
		this.valueRight = value;
	}

	@Override
	protected void compareInnerValues() {
		if (this.valueLeft.equals(this.valueRight)) {
			this.setResult(Result.EQUAL);
		} else {
			this.setResult(Result.MODIFIED);
		}
	}

	public void calculateResult(final Object valueLeft, final Object valueRight) {
		if (valueLeft == null || valueRight == null) {
			if (valueLeft != null) {
				this.setResult(Result.NEW);
			} else {
				this.setResult(Result.DELETED);
			}
		} else {
			this.setResult(Result.EQUAL);
		}
	}
}
