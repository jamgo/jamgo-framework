package org.jamgo.snapshot.model.snapshot;

import java.util.Comparator;
import java.util.Objects;

public class SnapshotComparisonAttributeComparator implements Comparator<SnapshotComparisonAttribute<?>> {

	@Override
	public int compare(final SnapshotComparisonAttribute<?> left, final SnapshotComparisonAttribute<?> right) {
		// Null check: if both left and right attributes are null, returns 0
		if (Objects.equals(left.getName(), right.getName())) {
			return 0;
		}

		// Null check: here both left and right attributes can't be null
		final int leftOrder = this.getLogicalOrder(left);
		final int rightOrder = this.getLogicalOrder(right);
		final int orderComparison = Integer.compare(leftOrder, rightOrder);
		if (orderComparison != 0) {
			return orderComparison;
		}

		// Null check: here both leftOrder and rightOrder are equal (orderComparison==0)
		// 			   + here both left and right attributes can't be null
		// 			   -> here both left and right attributes are not null

		// same logical order -> compare names
		return left.getName().compareTo(right.getName());
	}

	protected int getLogicalOrder(final SnapshotComparisonAttribute<?> attribute) {
		if (attribute.getName() == null) {
			return 100;
		}
		if (attribute.getName().equals("model.id")) {
			return 1;
		}
		if (attribute.getName().equals("model.version")) {
			return 2;
		}
		if (attribute.getName().endsWith(".name") || attribute.getName().endsWith(".title")) {
			return 3;
		}
		if (attribute.getName().endsWith(".description")) {
			return 4;
		}
		return 10;
	}

}
