package org.jamgo.snapshot.model.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(
	basePackageClasses = {
		org.jamgo.snapshot.model.entity.PackageMarker.class,
		org.jamgo.snapshot.model.repository.PackageMarker.class
	})
public class SnapshotModelConfig {

}
