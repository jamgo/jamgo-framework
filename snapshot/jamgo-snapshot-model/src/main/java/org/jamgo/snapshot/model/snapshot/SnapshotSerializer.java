package org.jamgo.snapshot.model.snapshot;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Lob;
import javax.persistence.Transient;

import org.hibernate.Hibernate;
import org.jamgo.model.entity.Model;
import org.jamgo.snapshot.model.annotation.SnapshotModelDataIgnore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * Custom serializer for Model classes. Keeps track of the fields and the
 * relationships (OneTo*, ManyTo*) of each class. Some fields are excluded from
 * JSON to avoid unneeded information in JSON.
 *
 * @author Jamgo SCCL
 *
 */
public class SnapshotSerializer extends StdSerializer<Model> {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(SnapshotSerializer.class);

	private static final List<String> EXCLUDED_MODEL_FIELDS = Arrays.asList("id", "version", "serialVersionUID",
		"saveSnapshot");

	/**
	 * Map with the simple fields (non-Model fields) of each class.
	 */
	private final Map<Class<?>, List<Field>> modelSnapshotFieldsMap = new HashMap<>();

	/**
	 * Map with the relationship fields (Model fields) of each class.
	 */
	private final Map<Class<?>, List<Field>> modelRelationshipFieldsMap = new HashMap<>();

	public SnapshotSerializer() {
		this(null);
	}

	public SnapshotSerializer(final Class<Model> t) {
		super(t);
	}

	@Override
	@Transactional
	public void serialize(final Model value, final JsonGenerator gen, final SerializerProvider provider)
		throws IOException {

		final Model model = Hibernate.unproxy(value, Model.class);

		gen.writeStartObject();
		gen.writeNumberField("id", model.getId());
		gen.writeNumberField("version", model.getVersion());

		final List<Field> modelFields = this.getSnapshotFields(model);
		for (final Field field : modelFields) {
			try {
				final String fieldName = field.getName();
				field.setAccessible(true);
				final Object fieldValue = field.get(model);
				provider.defaultSerializeField(fieldName, fieldValue, gen);
			} catch (final IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		gen.writeEndObject();
	}

	public List<Field> getSnapshotFields(final Model model) {
		final Class<?> clazz = SnapshotSerializer.getModelClass(model);
		if (!this.modelSnapshotFieldsMap.containsKey(clazz)) {
			this.processModelClass(model);
		}
		return this.modelSnapshotFieldsMap.get(clazz);
	}

	public List<Field> getRelationshipFields(final Model model) {
		final Class<?> clazz = SnapshotSerializer.getModelClass(model);
		if (!this.modelRelationshipFieldsMap.containsKey(clazz)) {
			this.processModelClass(model);
		}
		return this.modelRelationshipFieldsMap.get(clazz);
	}

	private void processModelClass(final Model model) {
		final Class<?> modelClazz = model.getClass();

		final List<Field> snapshotFields = new ArrayList<>();
		final List<Field> relationshipFields = new ArrayList<>();

		Class<?> clazz = modelClazz;
		while (clazz != null && clazz != Model.class && clazz != Object.class) {
			final Field[] declaredFields = clazz.getDeclaredFields();
			for (final Field field : declaredFields) {
				if (this.getExcludedModelFields().contains(field.getName())) {
					continue;
				}

				if (field.isAnnotationPresent(Transient.class) ||
					field.isAnnotationPresent(SnapshotModelDataIgnore.class) ||
					field.isAnnotationPresent(Lob.class) ||
					Modifier.isStatic(field.getModifiers())) {
					// ignore field
				} else {
					final String fieldName = field.getName();
					final Class<?> fieldType = field.getType();
					field.setAccessible(true);

					if (Blob.class.isAssignableFrom(fieldType)) {
						// IGNORE FIELD
					} else if (Model.class.isAssignableFrom(fieldType)) {
						relationshipFields.add(field);
					} else if (Collection.class.isAssignableFrom(fieldType)) {
						SnapshotSerializer.logger.debug("field type extends Collection: {} {}", fieldName,
							fieldType.getName());
						// Check if is a list of Model objects
						final ParameterizedType collectionType = (ParameterizedType) field.getGenericType();
						final Class<?> collectionClass = (Class<?>) collectionType.getActualTypeArguments()[0];
						if (Model.class.isAssignableFrom(collectionClass)) {
							// Model attribute -> use relationship serialization
							relationshipFields.add(field);
						} else {
							// Other typed attributes -> use default serialization
							snapshotFields.add(field);
						}
					} else {
						// Other typed attributes -> use default serialization
						snapshotFields.add(field);
					}
				}
			}
			clazz = clazz.getSuperclass();
		}

		this.modelSnapshotFieldsMap.put(modelClazz, snapshotFields);
		this.modelRelationshipFieldsMap.put(modelClazz, relationshipFields);
	}

	protected List<String> getExcludedModelFields() {
		return SnapshotSerializer.EXCLUDED_MODEL_FIELDS;
	}

	public static Class<?> getModelClass(final Model object) {
		final Model model = Hibernate.unproxy(object, Model.class);
		return model.getClass();
	}

}
