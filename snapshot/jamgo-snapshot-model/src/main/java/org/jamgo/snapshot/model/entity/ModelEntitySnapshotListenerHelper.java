package org.jamgo.snapshot.model.entity;

import java.util.Optional;

import org.jamgo.model.entity.Model;
import org.jamgo.model.entity.ModelEntityListenerHelper;
import org.jamgo.snapshot.model.annotation.SnapshotDisabled;
import org.jamgo.snapshot.model.snapshot.SnapshotEventData;
import org.jamgo.snapshot.model.snapshot.SnapshotInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;

public class ModelEntitySnapshotListenerHelper implements ModelEntityListenerHelper, InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(ModelEntitySnapshotListenerHelper.class);

	private boolean generateSnapshots = false;

	/**
	 * Event publisher from Spring context. This object is used to send events when
	 * an entity is modified (created, updated, deleted).
	 */
	private ApplicationEventPublisher publisher;

	/**
	 * ThreadLocal variable to provide information needed to identify current
	 * transaction and user.
	 */
	private SnapshotInfo snapshotInfo;

	private Environment environment;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.generateSnapshots = Optional.ofNullable(this.environment.getProperty("snapshots.enabled", Boolean.class)).orElse(false);
	}

	@Autowired
	@Lazy
	public void setPublisher(final ApplicationEventPublisher publisher) {
		this.publisher = publisher;
	}

	@Autowired
	@Lazy
	public void setSnapshotInfo(final SnapshotInfo snapshotInfo) {
		this.snapshotInfo = snapshotInfo;
	}

	@Autowired
	public void setEnvironment(final Environment environment) {
		this.environment = environment;
	}

	/**
	 * Method invoked after storing a new entity in the database (during
	 * commit or flush).
	 */
	@Override
	public void onPostPersist(final Model m) {
		ModelEntitySnapshotListenerHelper.logger.debug("ON POST PERSIST: {}", this.getModelReference(m));
		this.takeSnapshot(m, SnapshotAction.CREATE);
	}

	/**
	 * Method invoked after deleting an entity from the database (during
	 * commit or flush).
	 */
	@Override
	public void onPostUpdate(final Model m) {
		ModelEntitySnapshotListenerHelper.logger.debug("ON POST UPDATE: {}", this.getModelReference(m));
		this.takeSnapshot(m, SnapshotAction.UPDATE);
	}

	/**
	 * method invoked after deleting an entity from the database (during
	 * commit or flush).
	 */
	@Override
	public void onPostRemove(final Model m) {
		ModelEntitySnapshotListenerHelper.logger.debug("ON POST REMOVE: {}", this.getModelReference(m));
		this.takeSnapshot(m, SnapshotAction.DELETE);
	}

	private void takeSnapshot(final Model m, final SnapshotAction action) {
		final String generateSnapshotsSystemProperty = System.getProperty("snapshots.enabled", "false");
		ModelEntitySnapshotListenerHelper.logger.debug("TAKE-SNAPSHOT: ModelEntityListener.generateSnapshots = {}", this.generateSnapshots);
		ModelEntitySnapshotListenerHelper.logger.debug("TAKE-SNAPSHOT: generateSnapshotsSystemProperty = {}", generateSnapshotsSystemProperty);
		final boolean generateSnapshots = this.generateSnapshots || "true".equals(generateSnapshotsSystemProperty);
		if (generateSnapshots && !m.getClass().isAnnotationPresent(SnapshotDisabled.class)) {
			ModelEntitySnapshotListenerHelper.logger.debug("TAKE-SNAPSHOT: {}", this.getModelReference(m));
			this.publisher
				.publishEvent(new SnapshotEventData(m, action, this.snapshotInfo.getTransactionId(),
					this.snapshotInfo.getConnectedUsername()));
		}
	}

	private String getModelReference(final Model m) {
		return String.format("%s#%s#%s", m.getClass().getName(), m.getId(), m.getVersion());
	}

}
