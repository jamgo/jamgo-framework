package org.jamgo.snapshot.model.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to indicate to SnapshotGenerator that does not have to include
 * this field in model data JSON.
 *
 * @author Jamgo SCCL
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SnapshotModelDataIgnore {
}
