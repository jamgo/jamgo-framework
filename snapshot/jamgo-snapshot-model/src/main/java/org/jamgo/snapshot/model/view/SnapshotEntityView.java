package org.jamgo.snapshot.model.view;

import org.jamgo.snapshot.model.entity.Snapshot;
import org.jamgo.snapshot.model.entity.SnapshotAction;
import org.jamgo.snapshot.model.entity.SnapshotEntity;

import com.blazebit.persistence.view.EntityView;
import com.blazebit.persistence.view.FetchStrategy;
import com.blazebit.persistence.view.MappingCorrelatedSimple;

@EntityView(SnapshotEntity.class)
public abstract class SnapshotEntityView {

	@MappingCorrelatedSimple(
		correlationBasis = "snapshotId",
		correlated = Snapshot.class,
		correlationExpression = "id = correlationKey",
		fetch = FetchStrategy.JOIN)
	public abstract Snapshot getSnapshot();

	public abstract SnapshotAction getAction();

}
