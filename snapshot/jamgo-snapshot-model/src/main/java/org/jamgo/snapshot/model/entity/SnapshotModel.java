package org.jamgo.snapshot.model.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.jamgo.model.entity.Model;

@MappedSuperclass
public abstract class SnapshotModel {

	@Id
	@GeneratedValue(generator = "snapshotModel")
	@GenericGenerator(name = "snapshotModel", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
		@Parameter(name = "prefer_sequence_per_entity", value = "true"),
		@Parameter(name = "optimizer", value = "pooled-lo") })
	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		return this.getId() != null ? this.getId().hashCode() : 0;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		return (obj instanceof Model) && (((Model) obj).getId() != null) && ((Model) obj).getId().equals(this.getId());
	}
}
