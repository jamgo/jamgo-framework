package org.jamgo.snapshot.model.view;

import org.jamgo.snapshot.model.entity.SnapshotEntity;

import com.blazebit.persistence.CriteriaBuilder;
import com.blazebit.persistence.view.CorrelationBuilder;
import com.blazebit.persistence.view.CorrelationProvider;

public class SnapshotEntityViewCorrelator implements CorrelationProvider {

	@Override
	public void applyCorrelation(final CorrelationBuilder builder, final String correlationExpression) {
		final String alias = builder.getCorrelationAlias();
		final String modelClassName = this.getModelClassName(builder);

		builder.correlate(SnapshotEntity.class)
			.on(alias + ".entity.modelClassName").eq(modelClassName)
			.on(alias + ".entity.modelId").eqExpression(correlationExpression)
			.end();
	}

	/**
	 * Get model class name dynamically from CriteriaBuilder
	 * @param builder
	 * @return
	 */
	private String getModelClassName(final CorrelationBuilder builder) {
		return ((CriteriaBuilder<?>) builder.getCorrelationFromProvider()).getResultType().getCanonicalName();
	}

}
