package org.jamgo.snapshot.model.snapshot;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.jamgo.snapshot.model.snapshot.SnapshotComparison.Result;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SnapshotComparisonEmbeddedAttribute extends SnapshotComparisonAttribute<Object> {

	@JsonIgnore
	private Object valueLeft;
	@JsonIgnore
	private Object valueRight;
	private List<SnapshotComparisonSimpleAttribute> children = new ArrayList<>();

	public SnapshotComparisonEmbeddedAttribute() {
		this.setType(AttributeType.EMBEDDED);
		this.setMoreInfo(true);
	}

	@Override
	public Object getValueLeft() {
		return this.valueLeft;
	}

	@Override
	public Object getValueRight() {
		return this.valueRight;
	}

	public List<SnapshotComparisonSimpleAttribute> getChildren() {
		return this.children;
	}

	public void setChildren(final List<SnapshotComparisonSimpleAttribute> children) {
		this.children = children;
	}

	@Override
	protected void setInnerValueLeft(final Object value) {
		this.valueLeft = value;
	}

	@Override
	protected void setInnerValueRight(final Object value) {
		this.valueRight = value;
	}

	@Override
	protected void compareInnerValues() {
		if (this.valueLeft.equals(this.valueRight)) {
			this.setResult(Result.EQUAL);
		} else {
			this.setResult(Result.MODIFIED);
		}
	}

	public void calculateEmbeddedAttributeResult() {
		final Result result = this.getResult();
		if (Result.EQUAL.equals(result) && CollectionUtils.isNotEmpty(this.children)) {
			final boolean modified = this.children.stream().anyMatch(item -> !Result.EQUAL.equals(item.getResult()));
			if (modified) {
				this.setResult(Result.MODIFIED);
			}
		}
	}
}
