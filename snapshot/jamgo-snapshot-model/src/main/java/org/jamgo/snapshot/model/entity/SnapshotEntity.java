package org.jamgo.snapshot.model.entity;

import javax.persistence.Basic;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;

import org.jamgo.model.entity.ModelReference;

@Entity
public class SnapshotEntity extends SnapshotModel {

	private String snapshotId;
	@Enumerated(EnumType.STRING)
	private SnapshotAction action;
	@Embedded
	private ModelReference entity;
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] modelData;
	private String description;

	public String getSnapshotId() {
		return this.snapshotId;
	}

	public void setSnapshotId(final String snapshotId) {
		this.snapshotId = snapshotId;
	}

	public SnapshotAction getAction() {
		return this.action;
	}

	public void setAction(final SnapshotAction action) {
		this.action = action;
	}

	public ModelReference getEntity() {
		return this.entity;
	}

	public void setEntity(final ModelReference entity) {
		this.entity = entity;
	}

	public byte[] getModelData() {
		return this.modelData;
	}

	public void setModelData(final byte[] modelData) {
		this.modelData = modelData;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

}
