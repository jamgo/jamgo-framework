package org.jamgo.snapshot.model.view;

import java.util.List;

import org.jamgo.model.view.ModelView;
import org.jamgo.snapshot.model.SnapshotAuditInfo;

import com.blazebit.persistence.view.FetchStrategy;
import com.blazebit.persistence.view.MappingCorrelated;

public abstract class SnapshotAuditInfoView extends ModelView {

	@MappingCorrelated(
		correlationBasis = "id",
		correlator = SnapshotEntityViewCorrelator.class,
		fetch = FetchStrategy.JOIN)
	public abstract List<SnapshotEntityView> getSnapshotEntityViews();

	public SnapshotAuditInfo getSnapshotAuditInfo() {
		final SnapshotAuditInfo auditInfo = new SnapshotAuditInfo();
		// Not probable, but possible to have no snapshots registered for this entity...
		if (this.getSnapshotEntityViews().size() > 0) { // First snapshot must be the CREATE
			final SnapshotEntityView creationView = this.getSnapshotEntityViews().get(0);
			auditInfo.setCreatedBy(creationView.getSnapshot().getUsername());
			auditInfo.setCreatedAt(creationView.getSnapshot().getTimestamp());
			auditInfo.setLastUpdatedBy(creationView.getSnapshot().getUsername());
			auditInfo.setLastUpdatedAt(creationView.getSnapshot().getTimestamp());

			if (this.getSnapshotEntityViews().size() > 1) { // Exist more actions other than CREATE
				final SnapshotEntityView lastUpdateView = this.getSnapshotEntityViews().get(this.getSnapshotEntityViews().size() - 1);
				auditInfo.setLastUpdatedBy(lastUpdateView.getSnapshot().getUsername());
				auditInfo.setLastUpdatedAt(lastUpdateView.getSnapshot().getTimestamp());
			}
		}
		return auditInfo;
	}

}
