package org.jamgo.snapshot.model;

import java.time.LocalDateTime;

public class SnapshotAuditInfo {

	private String createdBy;
	private LocalDateTime createdAt;
	private String lastUpdatedBy;
	private LocalDateTime lastUpdatedAt;

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(final String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(final LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(final String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public LocalDateTime getLastUpdatedAt() {
		return this.lastUpdatedAt;
	}

	public void setLastUpdatedAt(final LocalDateTime lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Created at ").append(this.createdAt).append(" by ").append(this.createdBy);
		if (this.lastUpdatedAt != null) {
			sb.append(", ");
			sb.append("last updated at ").append(this.lastUpdatedAt).append(" by ").append(this.lastUpdatedBy);
		}
		sb.append(".");
		return sb.toString();
	}
}
