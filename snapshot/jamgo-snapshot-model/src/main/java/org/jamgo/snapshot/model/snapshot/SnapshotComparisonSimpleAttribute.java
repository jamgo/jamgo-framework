package org.jamgo.snapshot.model.snapshot;

import org.jamgo.snapshot.model.snapshot.SnapshotComparison.Result;

public class SnapshotComparisonSimpleAttribute extends SnapshotComparisonAttribute<String> {

	private String valueLeft;
	private String valueRight;

	public SnapshotComparisonSimpleAttribute() {
		this.setType(AttributeType.SIMPLE);
		this.setMoreInfo(false);
	}

	@Override
	public String getValueLeft() {
		return this.valueLeft;
	}

	@Override
	public String getValueRight() {
		return this.valueRight;
	}

	@Override
	protected void setInnerValueLeft(final String value) {
		this.valueLeft = value;
	}

	@Override
	protected void setInnerValueRight(final String value) {
		this.valueRight = value;
	}

	@Override
	protected void compareInnerValues() {
		if (this.valueLeft.equals(this.valueRight)) {
			this.setResult(Result.EQUAL);
		} else {
			this.setResult(Result.MODIFIED);
		}
	}

}
