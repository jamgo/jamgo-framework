package org.jamgo.snapshot.model.converter;

import java.util.List;
import java.util.Locale;

import org.jamgo.snapshot.model.snapshot.SnapshotComparisonAttribute;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonEmbeddedComplexAttribute;

public interface CustomSnapshotConverter {

	List<SnapshotComparisonAttribute<Object>> compareEmbeddedComplexValues(final Object valueLeft,
			final Object valueRight, final Locale locale);

	void calculateEmbeddedAttributeResult(final SnapshotComparisonEmbeddedComplexAttribute attribute);

}
