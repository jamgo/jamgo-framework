package org.jamgo.snapshot.model.snapshot;

import org.jamgo.snapshot.model.snapshot.SnapshotComparison.Result;

public abstract class SnapshotComparisonAttribute<T> {

	private static Long nextId = 1L;

	public enum AttributeType {
		SIMPLE, MODEL, COLLECTION, TRANSLATED, EMBEDDED, LOCALIZED_STRING, LOCALIZED_MODEL
	}

	public SnapshotComparisonAttribute() {
		this.id = SnapshotComparisonAttribute.nextId++;
	}

	private Long id;
	private String name;
	private AttributeType type;
	private boolean moreInfo;
	private Result result;

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public AttributeType getType() {
		return this.type;
	}

	public void setType(final AttributeType type) {
		this.type = type;
	}

	public boolean isMoreInfo() {
		return this.moreInfo;
	}

	public void setMoreInfo(final boolean moreInfo) {
		this.moreInfo = moreInfo;
	}

	public Result getResult() {
		return this.result;
	}

	public void setResult(final Result result) {
		this.result = result;
	}

	public abstract T getValueLeft();

	public abstract T getValueRight();

	public void setValueLeft(final T value) {
		this.setInnerValueLeft(value);
		this.calculateResult();
	}

	public void setValueRight(final T value) {
		this.setInnerValueRight(value);
		this.calculateResult();
	}

	protected void calculateResult() {
		if (this.getValueLeft() == null && this.getValueRight() == null) {
			this.result = Result.EQUAL;
		} else {
			if (this.getValueLeft() != null) {
				if (this.getValueRight() == null) {
					this.result = Result.NEW;
				} else { // Both values are not null, compare them
					this.compareInnerValues();
				}
			} else { // valueLeft == null && valueRight != null
				this.result = Result.DELETED;
			}
		}
	}

	protected abstract void setInnerValueLeft(T value);

	protected abstract void setInnerValueRight(T value);

	/**
	 * Both values are not null. This method has to set result attribute to
	 * Result.EQUAL or Result.MODIFIED
	 */
	protected abstract void compareInnerValues();

}
