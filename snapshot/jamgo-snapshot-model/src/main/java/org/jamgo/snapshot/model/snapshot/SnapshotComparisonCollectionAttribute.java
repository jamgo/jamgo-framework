package org.jamgo.snapshot.model.snapshot;

import java.util.List;

import org.jamgo.model.entity.ModelReference;
import org.jamgo.snapshot.model.snapshot.SnapshotComparison.Result;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SnapshotComparisonCollectionAttribute extends SnapshotComparisonAttribute<List<ModelReference>> {

	@JsonIgnore
	private List<ModelReference> valueLeft;
	@JsonIgnore
	private List<ModelReference> valueRight;
	private List<SnapshotComparisonModelAttribute> children;

	public SnapshotComparisonCollectionAttribute() {
		this.setType(AttributeType.COLLECTION);
		this.setMoreInfo(true);
	}

	@Override
	public List<ModelReference> getValueLeft() {
		return this.valueLeft;
	}

	@Override
	public List<ModelReference> getValueRight() {
		return this.valueRight;
	}

	public List<SnapshotComparisonModelAttribute> getChildren() {
		return this.children;
	}

	public void setChildren(final List<SnapshotComparisonModelAttribute> children) {
		this.children = children;
	}

	@Override
	protected void setInnerValueLeft(final List<ModelReference> value) {
		this.valueLeft = value;
	}

	@Override
	protected void setInnerValueRight(final List<ModelReference> value) {
		this.valueRight = value;
	}

	@Override
	protected void compareInnerValues() {
		if (this.valueLeft.size() == this.valueRight.size()) {
			this.setResult(Result.EQUAL);
			for (int i = 0; i < this.valueLeft.size(); i++) {
				if (!this.valueLeft.get(i).equals(this.valueRight.get(i))) {
					this.setResult(Result.MODIFIED);
					break;
				}
			}
		} else {
			this.setResult(Result.MODIFIED);
		}
	}

}
