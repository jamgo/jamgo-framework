package org.jamgo.snapshot.model.repository;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;
import org.jamgo.model.entity.Model;
import org.jamgo.model.entity.ModelEntityListener;
import org.jamgo.model.entity.ModelReference;
import org.jamgo.snapshot.model.annotation.SnapshotDisabled;
import org.jamgo.snapshot.model.entity.Snapshot;
import org.jamgo.snapshot.model.entity.SnapshotAction;
import org.jamgo.snapshot.model.entity.SnapshotEntity;
import org.jamgo.snapshot.model.entity.SnapshotRelationship;
import org.jamgo.snapshot.model.entity.SnapshotRelationship.RelationshipType;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonData;
import org.jamgo.snapshot.model.snapshot.SnapshotEventData;
import org.jamgo.snapshot.model.snapshot.SnapshotGeneratedEventData;
import org.jamgo.snapshot.model.snapshot.SnapshotSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class SnapshotGenerator implements InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(SnapshotGenerator.class);

	private final ObjectMapper mapper;

	/**
	 * Collects generated snapshots to avoid multiple insertion in database
	 */
	private final Map<UUID, Snapshot> snapshotsMap = new HashMap<>();

	@Autowired
	private ApplicationEventPublisher publisher;
	@Autowired
	private EntityManager entityManager;
	@Autowired
	private SnapshotRepository repository;

	protected SnapshotSerializer snapshotSerializer;

	/**
	 * This constructor initializes Jackson's {@link ObjectMapper} with:
	 * <ul>
	 * <li>{@link JavaTimeModule}: support for Java 8 date/time types (specified in
	 * JSR-310 specification)</li>
	 * <li>{@link SnapshotSerializer}: custom serializer for Model classes</li>
	 * </ul>
	 */
	public SnapshotGenerator() {
		this.mapper = new ObjectMapper().registerModule(new JavaTimeModule());
		this.initSnapshotSerializer();

		final SimpleModule module = new SimpleModule();
		module.addSerializer(Model.class, this.snapshotSerializer);
		this.mapper.registerModule(module);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		SnapshotGenerator.logger.debug("afterPropertiesSet()");
	}

	protected void initSnapshotSerializer() {
		this.snapshotSerializer = new SnapshotSerializer();
	}

	/**
	 * This method saves an snapshot of the received entity. This method is
	 * annotated with:
	 * <ul>
	 * <li>{@link EventListener} (Observer pattern implementation in Spring) to
	 * generate an snapshot of an entity. The event is thrown in
	 * {@link ModelEntityListener} class.</li>
	 * <li>{@link TransactionalEventListener} makes this listener be executed once
	 * the transaction is committed.</li>
	 * <li>{@link Transactional} creates a new transaction, needed to persist
	 * snapshot entities (the current transaction has been committed at this
	 * point).</li>
	 * <li>{@link Async} makes the execution of this listeners asynchronous, so the
	 * calling thread doesn't have to wait to its finalization to return.</li>
	 * </ul>
	 *
	 * @param event
	 */
	@Async
	@TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
	@EventListener(SnapshotEventData.class)
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void snapshotEventListener(final SnapshotEventData event) {
		final Model model = event.getModel();
		final SnapshotAction action = event.getAction();
		final UUID transactionId = event.getTransactionId();
		final String userName = event.getUserName();

		SnapshotGenerator.logger.debug("EVENT LISTENER: modelReference = {}, transactionId = {}, username = {}",
			this.getModelReference(model, action), transactionId, userName);

		if (model.getClass().isAnnotationPresent(SnapshotDisabled.class)) {
			return;
		}
		this.getOrCreateSnapshot(transactionId, userName);
		this.generateSnapshot(model, action, transactionId, userName, false);

		// Send an event to refresh grids in Snapshot viewers
		SnapshotGenerator.logger.debug("EVENT LISTENER: sending SnapshotGeneratedEventData event");
		this.publisher.publishEvent(new SnapshotGeneratedEventData(model, action, userName));
	}

	@Transactional
	public void generateSnapshot(
		final Model model,
		final SnapshotAction action,
		final UUID transactionId,
		final String userName,
		final boolean isBulkLoad) {
		if (model.getClass().isAnnotationPresent(SnapshotDisabled.class)) {
			return;
		}

		this.saveEntitySnapshot(model, action, transactionId, true, isBulkLoad);
		this.saveRelationshipSnapshots(model, transactionId, action, true, isBulkLoad);
	}

	public SnapshotComparisonData generateDynamicSnapshot(final Model model) {
		final SnapshotEntity snapshotEntity = this.saveEntitySnapshot(model, null, null, false, false);
		final List<SnapshotRelationship> snapshotRelationships = this.saveRelationshipSnapshots(model, null, null, false, false);

		final SnapshotComparisonData data = new SnapshotComparisonData();
		data.setEntity(snapshotEntity);
		try {
			data.setModelData(this.mapper.readTree(new String(snapshotEntity.getModelData())));
			data.setModel((Model) this.mapper.readValue(new String(snapshotEntity.getModelData()),
				Class.forName(snapshotEntity.getEntity().getModelClassName())));
		} catch (final JsonProcessingException e) {
			e.printStackTrace();
		} catch (final ClassNotFoundException e) {
			e.printStackTrace();
		}
		data.setRelationships(snapshotRelationships);
		data.setSnapshot(null);
		return data;
	}

	/**
	 * This method creates only once the {@link Snapshot} entity. Keeps track of the
	 * created snapshots to avoid concurrency problems due to {@link Async} methods
	 * and {@link Transactional} methods.
	 *
	 * @param transactionId
	 * @param username
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	private synchronized Snapshot getOrCreateSnapshot(final UUID transactionId, final String username) {
		// WARNING: Multiple threads can access to this method at the same time
		if (!this.snapshotsMap.containsKey(transactionId)) {
			synchronized (this) {
				if (!this.snapshotsMap.containsKey(transactionId)) {
					Snapshot snapshot = this.repository.findById(transactionId.toString());
					if (snapshot == null) {
						snapshot = new Snapshot();
						snapshot.setId(transactionId.toString());
						snapshot.setTimestamp(LocalDateTime.now());
						snapshot.setUsername(username);
						this.repository.persist(snapshot);
						this.snapshotsMap.put(transactionId, snapshot);
					}
				}
			}
		}
		return this.snapshotsMap.get(transactionId);
	}

	private SnapshotEntity saveEntitySnapshot(
		final Model model,
		final SnapshotAction action,
		final UUID transactionId,
		final boolean persist,
		final boolean isBulkLoad) {
		final SnapshotEntity entitySnapshot = new SnapshotEntity();
		entitySnapshot.setSnapshotId(Optional.ofNullable(transactionId).map(UUID::toString).orElse(null));
		entitySnapshot.setAction(action);
		entitySnapshot.setEntity(this.getModelReference(model, action));
		// When saving snapshot of a DELETE action, no JSON is saved to database.
		if (action != SnapshotAction.DELETE) {
			entitySnapshot.setModelData(this.extractModelData(model).getBytes());
		}
		if (persist) {
			this.repository.persist(entitySnapshot);
		}
		return entitySnapshot;
	}

	@SuppressWarnings("unchecked")
	private List<SnapshotRelationship> saveRelationshipSnapshots(
		final Model model,
		final UUID transactionId,
		final SnapshotAction action,
		final boolean persist,
		final boolean isBulkLoad) {
		// When saving snapshot of a DELETE action, no relationship is saved in
		// database.
		if (action == SnapshotAction.DELETE) {
			return new ArrayList<>();
		}

		final List<SnapshotRelationship> relationships = new ArrayList<>();
		final Model mergedModel = this.mergeEntity(model, isBulkLoad);
		final List<Field> fields = this.snapshotSerializer.getRelationshipFields(model);
		if (!CollectionUtils.isEmpty(fields)) {
			final ModelReference sourceReference = this.getModelReference(model, action);
			for (final Field field : fields) {
				try {
					final Object fieldValue = field.get(mergedModel);
					if (fieldValue instanceof Model) {
						final Model target = (Model) fieldValue;
						relationships.add(this.createSnapshotRelationship(transactionId, action, field.getName(),
							sourceReference, target, RelationshipType.ONE, persist));
					} else if (fieldValue instanceof Collection) {
						// Collection of Model values
						for (final Model target : (Collection<Model>) fieldValue) {
							relationships.add(this.createSnapshotRelationship(transactionId, action, field.getName(),
								sourceReference, target, RelationshipType.MANY, persist));
						}
					}
				} catch (final IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (final IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return relationships;
	}

	@SuppressWarnings("unchecked")
	private Model mergeEntity(final Model model, final boolean isBulkLoad) {
		// During bulkLoadProcess, we have to retrieve the entity and initialize lazy collections in Hibernate proxy
		if (isBulkLoad) {
			try {
				final Model entity = this.entityManager.find((Class<? extends Model>) Class.forName(SnapshotGenerator.getModelClassName(model)), model.getId());
				return (Model) Hibernate.unproxy(entity);
			} catch (final ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}

		// During event process, the model entity is already initialized
		// The model entity cannot be fetched from database because the current transaction is not committed yet.
		return model;
	}

	private SnapshotRelationship createSnapshotRelationship(
		final UUID transactionId,
		final SnapshotAction action,
		final String fieldName,
		final ModelReference sourceReference,
		final Model target,
		final RelationshipType type,
		final boolean persist) {
		final SnapshotRelationship relationship = new SnapshotRelationship();
		relationship.setSnapshotId(Optional.ofNullable(transactionId).map(UUID::toString).orElse(null));
		relationship.setName(fieldName);
		relationship.setType(type);
		relationship.setSource(sourceReference);
		relationship.setTarget(this.getModelReference(target, action));
		if (persist) {
			this.repository.persist(relationship);
		}
		return relationship;
	}

	private ModelReference getModelReference(final Model model, final SnapshotAction action) {
		final ModelReference reference = new ModelReference();
		reference.setModelClassName(SnapshotGenerator.getModelClassName(model));
		reference.setModelId(model.getId());
		// When creating a model reference for a DELETE, the version is not needed
		if (action != SnapshotAction.DELETE) {
			reference.setModelVersion(model.getVersion());
		}
		return reference;
	}

	public static String getModelClassName(final Object object) {
		if (object instanceof HibernateProxy) {
			final LazyInitializer lazyInitializer = ((HibernateProxy) object).getHibernateLazyInitializer();
			return lazyInitializer.getImplementation().getClass().getName();
		} else {
			return object.getClass().getName();
		}
	}

	private String extractModelData(final Model model) {
		String modelData = null;
		try {
			modelData = this.mapper.writeValueAsString(model);
			SnapshotGenerator.logger.debug("Snapshot data extracted: {}", modelData);
		} catch (final JsonProcessingException e) {
			SnapshotGenerator.logger.warn("Error extracting model data to JSON: ", e);
		}
		return modelData;
	}

}
