package org.jamgo.snapshot.model.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.jamgo.model.entity.Model;
import org.jamgo.model.entity.ModelReference;
import org.jamgo.snapshot.model.entity.Snapshot;
import org.jamgo.snapshot.model.entity.SnapshotEntity;
import org.jamgo.snapshot.model.entity.SnapshotRelationship;
import org.jamgo.snapshot.model.snapshot.ModelReferenceWithSnapshot;
import org.jamgo.snapshot.model.snapshot.SnapshotVersion;
import org.jamgo.snapshot.model.view.SnapshotEntityView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.blazebit.persistence.CriteriaBuilderFactory;
import com.blazebit.persistence.view.EntityViewManager;
import com.blazebit.persistence.view.EntityViewSetting;

/**
 * Repository to manage all kind of snapshot entities.
 *
 * @author Jamgo SCCL
 *
 */
@Repository
public class SnapshotRepository {

	@PersistenceContext
	protected EntityManager entityManager;
	@Autowired
	protected EntityViewManager entityViewManager;
	@Autowired
	protected CriteriaBuilderFactory criteriaBuilderFactory;

	public void persist(final Snapshot snapshot) {
		this.entityManager.persist(snapshot);
		this.entityManager.flush();
	}

	public void persist(final SnapshotEntity snapshotEntity) {
		this.entityManager.persist(snapshotEntity);
		this.entityManager.flush();
	}

	public void persist(final SnapshotRelationship snapshotRelationship) {
		this.entityManager.persist(snapshotRelationship);
		this.entityManager.flush();
	}

	public Snapshot findById(final String transactionId) {
		return this.entityManager.find(Snapshot.class, transactionId);
	}

	// FIXME: Use BlazePersistence criteria builders
	public List<SnapshotVersion> findSnapshotEntities(final String modelClassName, final Long modelId) {
		// La query és diferent a lo habitual ja que no hi ha relació entre SnapshotEntity i Snapshot
		// degut a problemes derivats de la creació de Snapshots en tasca asíncrona i multi-thread.
		// Per aquest motiu, s'ha de fer una query que retorna una Tuple amb els dos objectes i
		// posteriorment es processa el resultat per obtenir el resultat desitjat.
		final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<Tuple> criteria = criteriaBuilder.createQuery(Tuple.class);
		final Root<SnapshotEntity> snapshotEntity = criteria.from(SnapshotEntity.class);
		final Root<Snapshot> snapshot = criteria.from(Snapshot.class);
		final Join<SnapshotEntity, ModelReference> entity = snapshotEntity.join("entity");
		criteria.select(criteriaBuilder.tuple(snapshot, snapshotEntity));
		criteria.where(criteriaBuilder.and(
			criteriaBuilder.equal(snapshotEntity.get("snapshotId"), snapshot.get("id")),
			criteriaBuilder.equal(entity.get("modelClassName"), modelClassName),
			criteriaBuilder.equal(entity.get("modelId"), modelId)));
		criteria.orderBy(criteriaBuilder.desc(entity.get("modelVersion")));
		final List<Tuple> resultList = this.entityManager.createQuery(criteria).getResultList();

		List<SnapshotVersion> result = new ArrayList<>();
		if (!CollectionUtils.isEmpty(resultList)) {
			result = resultList.stream().map(this::toSnapshotVersion).collect(Collectors.toList());
		}
		return result;
	}

	// FIXME: Use BlazePersistence criteria builders
	public SnapshotVersion findSnapshotEntity(final ModelReference modelReference) {
		// La query és diferent a lo habitual ja que no hi ha relació entre SnapshotEntity i Snapshot
		// degut a problemes derivats de la creació de Snapshots en tasca asíncrona i multi-thread.
		// Per aquest motiu, s'ha de fer una query que retorna una Tuple amb els dos objectes i
		// posteriorment es processa el resultat per obtenir el resultat desitjat.
		final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<Tuple> criteria = criteriaBuilder.createQuery(Tuple.class);
		final Root<SnapshotEntity> snapshotEntity = criteria.from(SnapshotEntity.class);
		final Root<Snapshot> snapshot = criteria.from(Snapshot.class);
		final Join<SnapshotEntity, ModelReference> entity = snapshotEntity.join("entity");
		criteria.select(criteriaBuilder.tuple(snapshot, snapshotEntity));
		criteria.where(criteriaBuilder.and(
			criteriaBuilder.equal(snapshotEntity.get("snapshotId"), snapshot.get("id")),
			criteriaBuilder.equal(entity.get("modelClassName"), modelReference.getModelClassName()),
			criteriaBuilder.equal(entity.get("modelId"), modelReference.getModelId()),
			criteriaBuilder.equal(entity.get("modelVersion"), modelReference.getModelVersion())));

		try {
			final Tuple tuple = this.entityManager.createQuery(criteria).getSingleResult();
			return this.toSnapshotVersion(tuple);
		} catch (final NoResultException ex) {
			return null;
		}
	}

	private SnapshotVersion toSnapshotVersion(final Tuple tuple) {
		final SnapshotVersion snapshotVersion = new SnapshotVersion();
		snapshotVersion.setSnapshot((Snapshot) tuple.get(0));
		snapshotVersion.setSnapshotEntity((SnapshotEntity) tuple.get(1));
		return snapshotVersion;
	}

	/**
	 * Cal buscar la versió més propera a la data del snapshot passat en la modelReference
	 * @param modelReference
	 * @return
	 */
	// FIXME: Use BlazePersistence criteria builders
	public SnapshotVersion findSnapshotEntity(final ModelReferenceWithSnapshot modelReference) {
		// La query és diferent a lo habitual ja que no hi ha relació entre SnapshotEntity i Snapshot
		// degut a problemes derivats de la creació de Snapshots en tasca asíncrona i multi-thread.
		// Per aquest motiu, s'ha de fer una query que retorna una Tuple amb els dos objectes i
		// posteriorment es processa el resultat per obtenir el resultat desitjat.
		final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<Tuple> criteria = criteriaBuilder.createQuery(Tuple.class);
		final Root<SnapshotEntity> snapshotEntity = criteria.from(SnapshotEntity.class);
		final Root<Snapshot> snapshot = criteria.from(Snapshot.class);
		final Join<SnapshotEntity, ModelReference> entity = snapshotEntity.join("entity");
		criteria.select(criteriaBuilder.tuple(snapshot, snapshotEntity));
		criteria.where(criteriaBuilder.and(
			criteriaBuilder.equal(snapshotEntity.get("snapshotId"), snapshot.get("id")),
			criteriaBuilder.equal(entity.get("modelClassName"), modelReference.getModelClassName()),
			criteriaBuilder.equal(entity.get("modelId"), modelReference.getModelId()),
			criteriaBuilder.lessThanOrEqualTo(snapshot.get("timestamp"), modelReference.getSnapshot().getTimestamp())));
		criteria.orderBy(criteriaBuilder.desc(entity.get("modelVersion")));

		final List<Tuple> tuples = this.entityManager.createQuery(criteria).getResultList();
		if (CollectionUtils.isEmpty(tuples)) {
			return null;
		}
		// Ens quedem amb el primer resultat (llista ordenada per timestamp/versió)
		return this.toSnapshotVersion(tuples.get(0));
	}

	// FIXME: Use BlazePersistence criteria builders
	public List<SnapshotRelationship> findSnapshotRelationships(final ModelReference sourceModelReference) {
		final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<SnapshotRelationship> criteria = criteriaBuilder.createQuery(SnapshotRelationship.class);
		final Root<SnapshotRelationship> snapshotRelationship = criteria.from(SnapshotRelationship.class);
		criteria.select(snapshotRelationship);
		criteria.where(criteriaBuilder.equal(snapshotRelationship.get("source"), sourceModelReference));
		criteria.orderBy(criteriaBuilder.asc(snapshotRelationship.get("name")), criteriaBuilder.asc(snapshotRelationship.get("target").get("modelId")));
		return this.entityManager.createQuery(criteria).getResultList();
	}

	public Model findModelEntity(final String modelClassName, final Long modelId) {
		try {
			final Class<?> clazz = Class.forName(modelClassName);
			return (Model) this.entityManager.find(clazz, modelId);
		} catch (final ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Returns the snapshot information for the current version of the entity object.
	 * @param modelClassName
	 * @param modelId
	 * @param modelVersion
	 * @return
	 */
	public SnapshotEntityView findCurrentSnapshotEntityView(final String modelClassName, final Long modelId, final Long modelVersion) {
		final com.blazebit.persistence.CriteriaBuilder<SnapshotEntity> entityCb = this.criteriaBuilderFactory.create(this.entityManager, SnapshotEntity.class);
		final EntityViewSetting<SnapshotEntityView, com.blazebit.persistence.CriteriaBuilder<SnapshotEntityView>> setting = EntityViewSetting.create(SnapshotEntityView.class);
		final com.blazebit.persistence.CriteriaBuilder<SnapshotEntityView> cb = this.entityViewManager.applySetting(setting, entityCb);

		cb.where("entity.modelClassName").eq(modelClassName);
		cb.where("entity.modelId").eq(modelId);
		cb.where("entity.modelVersion").eq(modelVersion);
		return cb.getSingleResult();
	}

	/**
	 * Returns all the snapthots information for the entity object.
	 * First element will be the CREATE snapshot.
	 * Last element will be the last UPDATE (or DELETE) snapshot (current version of the entity object).
	 * @param modelClassName
	 * @param modelId
	 * @return
	 */
	public List<SnapshotEntityView> findAllSnapshotEntityViews(final String modelClassName, final Long modelId) {
		final com.blazebit.persistence.CriteriaBuilder<SnapshotEntity> entityCb = this.criteriaBuilderFactory.create(this.entityManager, SnapshotEntity.class);
		final EntityViewSetting<SnapshotEntityView, com.blazebit.persistence.CriteriaBuilder<SnapshotEntityView>> setting = EntityViewSetting.create(SnapshotEntityView.class);
		final com.blazebit.persistence.CriteriaBuilder<SnapshotEntityView> cb = this.entityViewManager.applySetting(setting, entityCb);

		cb.where("entity.modelClassName").eq(modelClassName);
		cb.where("entity.modelId").eq(modelId);
		cb.orderByAsc("entity.modelVersion");
		return cb.getResultList();
	}
}
