package org.jamgo.snapshot.model.snapshot;

import org.jamgo.model.entity.Model;
import org.jamgo.snapshot.model.entity.SnapshotAction;

/**
 * Information required when an snapshot is generated, used in the EventListener
 * defined for this event.
 *
 * This event will trigger an update in the SnapshotsPanel defined for the model class.
 *
 * @author Jamgo SCCL
 *
 **/
public class SnapshotGeneratedEventData {

	private Model model;
	private SnapshotAction action;
	private String userName;

	public SnapshotGeneratedEventData(final Model model, final SnapshotAction action, final String userName) {
		this.model = model;
		this.action = action;
		this.userName = userName;
	}

	public Model getModel() {
		return this.model;
	}

	public void setModel(final Model model) {
		this.model = model;
	}

	public SnapshotAction getAction() {
		return this.action;
	}

	public void setAction(final SnapshotAction action) {
		this.action = action;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(final String userName) {
		this.userName = userName;
	}

}
