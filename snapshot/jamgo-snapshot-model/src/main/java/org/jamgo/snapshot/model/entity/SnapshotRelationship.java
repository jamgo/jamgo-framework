package org.jamgo.snapshot.model.entity;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.jamgo.model.entity.ModelReference;

@Entity
public class SnapshotRelationship extends SnapshotModel {

	public enum RelationshipType {
		ONE, MANY
	}

	private String snapshotId;
	private String name;
	@Enumerated(EnumType.STRING)
	private RelationshipType type;
	@Embedded
	private ModelReference source;
	@Embedded
	private ModelReference target;

	public String getSnapshotId() {
		return this.snapshotId;
	}

	public void setSnapshotId(final String snapshotId) {
		this.snapshotId = snapshotId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public RelationshipType getType() {
		return this.type;
	}

	public void setType(final RelationshipType type) {
		this.type = type;
	}

	public ModelReference getSource() {
		return this.source;
	}

	public void setSource(final ModelReference source) {
		this.source = source;
	}

	public ModelReference getTarget() {
		return this.target;
	}

	public void setTarget(final ModelReference target) {
		this.target = target;
	}

}
