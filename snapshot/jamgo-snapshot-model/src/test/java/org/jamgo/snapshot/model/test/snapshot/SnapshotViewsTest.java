package org.jamgo.snapshot.model.test.snapshot;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.jamgo.model.entity.TestSimpleObject;
import org.jamgo.model.repository.TestSimpleObjectRepository;
import org.jamgo.model.test.TestSimpleObjectBuilder;
import org.jamgo.snapshot.model.entity.SnapshotAction;
import org.jamgo.snapshot.model.test.JamgoSnapshotRepositoryTest;
import org.jamgo.snapshot.model.view.SnapshotEntityView;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class SnapshotViewsTest extends JamgoSnapshotRepositoryTest {

	private static final Logger logger = LoggerFactory.getLogger(SnapshotViewsTest.class);

	@Autowired
	private TestSimpleObjectRepository testSimpleObjectRepository;

	@Test
	@Transactional
	public void testFindCurrentSnapshotEntityView() throws Exception {
		this.entityManager.joinTransaction();
		final TestSimpleObject model = new TestSimpleObjectBuilder(this.entityManager).setName("name").setAge(43).buildOne();
		this.commitAndStart();
		model.setAge(44);
		this.testSimpleObjectRepository.update(model);
		this.commitAndStart();

		final List<TestSimpleObject> result = this.testSimpleObjectRepository.findAll();
		assertEquals(1, result.size());

//		// TODO Wait until asynch process ends

		final SnapshotEntityView view = this.snapshotRepository.findCurrentSnapshotEntityView(TestSimpleObject.class.getName(), result.get(0).getId(), result.get(0).getVersion());
		assertNotNull(view);
	}

	@Test
	@Transactional
	public void testFindAllSnapshotEntityViews() throws Exception {
		this.entityManager.joinTransaction();
		final TestSimpleObject model = new TestSimpleObjectBuilder(this.entityManager).setName("name").setAge(43).buildOne();
		this.commitAndStart();
		model.setAge(44);
		this.testSimpleObjectRepository.update(model);
		this.commitAndStart();

		final List<TestSimpleObject> result = this.testSimpleObjectRepository.findAll();
		assertEquals(1, result.size());

//		// TODO Wait until asynch process ends

		final List<SnapshotEntityView> views = this.snapshotRepository.findAllSnapshotEntityViews(TestSimpleObject.class.getName(), result.get(0).getId());
		assertNotNull(views);
		assertEquals(2, views.size());
		final SnapshotEntityView creationView = views.get(0);
		assertEquals(SnapshotAction.CREATE, creationView.getAction());
		final SnapshotEntityView lastUpdateView = views.get(views.size() - 1);
		assertNotEquals(SnapshotAction.CREATE, lastUpdateView.getAction());
	}

}
