package org.jamgo.snapshot.model.test.snapshot;

import org.apache.commons.lang3.ArrayUtils;
import org.jamgo.model.filter.IgnoreDuringScan;
import org.jamgo.model.test.config.ModelTestConfig;
import org.jamgo.snapshot.model.entity.ModelEntitySnapshotListenerHelper;
import org.jamgo.snapshot.model.repository.SnapshotGenerator;
import org.jamgo.snapshot.model.snapshot.SnapshotInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.aop.target.ThreadLocalTargetSource;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

import com.blazebit.persistence.integration.view.spring.EnableEntityViews;

@Configuration
@ComponentScan(
	basePackageClasses = {
		org.jamgo.snapshot.model.entity.PackageMarker.class,
		org.jamgo.snapshot.model.repository.PackageMarker.class,
	})
@EnableEntityViews(
	basePackageClasses = {
		org.jamgo.snapshot.model.view.PackageMarker.class,
		org.jamgo.snapshot.model.test.snapshot.view.PackageMarker.class
	})
@IgnoreDuringScan
public class SnapshotModelTestConfig extends ModelTestConfig {

	private static final Logger logger = LoggerFactory.getLogger(SnapshotModelTestConfig.class);

	@Override
	public String[] getPackagesToScan() {
		return ArrayUtils.add(super.getPackagesToScan(), org.jamgo.snapshot.model.PackageMarker.class.getPackage().getName());
	}

	@Bean(destroyMethod = "destroy")
	public ThreadLocalTargetSource threadLocalSnapshotInfo() {
		SnapshotModelTestConfig.logger.debug("creating new bean threadLocalSnapshotInfo");
		final ThreadLocalTargetSource result = new ThreadLocalTargetSource();
		result.setTargetBeanName("snapshotInfo");
		return result;
	}

	@Primary
	@Bean(name = "proxiedThreadLocalTargetSource")
	public ProxyFactoryBean proxiedThreadLocalTargetSource(final ThreadLocalTargetSource threadLocalTargetSource) {
		SnapshotModelTestConfig.logger.debug("creating new bean proxiedThreadLocalTargetSource");
		final ProxyFactoryBean result = new ProxyFactoryBean();
		result.setTargetSource(threadLocalTargetSource);
		return result;
	}

	@Bean(name = "snapshotInfo")
	@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public SnapshotInfo snapshotInfo() {
		SnapshotModelTestConfig.logger.debug("creating new bean snapshotInfo");
		return new SnapshotInfo();
	}

	/**
	 * Model Entity listener helper for snapshots generation.
	 */
	@Bean
	public ModelEntitySnapshotListenerHelper modelEntitySnapshotListenerHelper() {
		SnapshotModelTestConfig.logger.debug("creating new bean ModelEntitySnapshotListenerHelper");
		return new ModelEntitySnapshotListenerHelper();
	}

	/**
	 * Initialization of SnapshotGenerator instance. This bean is initialized here,
	 * and not with @Component annotation, to avoid use of this snapshot
	 * functionality in tests.
	 */
	@Bean
	public SnapshotGenerator snapshotGenerator() {
		SnapshotModelTestConfig.logger.debug("creating new bean IcabSnapshotGenerator()");
		return new SnapshotGenerator();
	}

	@Bean
	public SnapshotGeneratedEventListener snapshotGeneratedEventListener() {
		return new SnapshotGeneratedEventListener();
	}

}
