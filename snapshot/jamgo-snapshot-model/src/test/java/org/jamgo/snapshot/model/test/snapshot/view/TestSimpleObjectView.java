package org.jamgo.snapshot.model.test.snapshot.view;

import org.jamgo.model.entity.TestSimpleObject;
import org.jamgo.snapshot.model.view.SnapshotAuditInfoView;

import com.blazebit.persistence.view.EntityView;

@EntityView(TestSimpleObject.class)
public abstract class TestSimpleObjectView extends SnapshotAuditInfoView {

	public abstract String getName();

	public abstract int getAge();

}
