package org.jamgo.snapshot.model.test.snapshot;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.jamgo.model.entity.TestSimpleObject;
import org.jamgo.model.repository.TestSimpleObjectRepository;
import org.jamgo.model.test.TestSimpleObjectBuilder;
import org.jamgo.snapshot.model.test.JamgoSnapshotRepositoryTest;
import org.jamgo.snapshot.model.test.snapshot.view.TestSimpleObjectView;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class TestSimpleObjectViewTest extends JamgoSnapshotRepositoryTest {

	private static final Logger logger = LoggerFactory.getLogger(TestSimpleObjectViewTest.class);

	@Autowired
	private TestSimpleObjectRepository testSimpleObjectRepository;

	@Test
	@Transactional
	public void testTestSimpleObjectView_withSnapshotAuditInfo() throws Exception {
		this.entityManager.joinTransaction();
		final TestSimpleObject model = new TestSimpleObjectBuilder(this.entityManager).setName("name").setAge(43).buildOne();
		this.commitAndStart();
		Thread.sleep(500);
		model.setAge(44);
		this.testSimpleObjectRepository.update(model);
		this.commitAndStart();

//		// TODO Wait until asynch process ends

		final TestSimpleObjectView result = this.testSimpleObjectRepository.findById(model.getId(), TestSimpleObjectView.class);
		assertEquals(model.getName(), result.getName());
		assertEquals(model.getAge(), result.getAge());
		assertNotNull(result.getSnapshotAuditInfo());
		TestSimpleObjectViewTest.logger.info("AUDIT INFO: {}", result.getSnapshotAuditInfo());
	}

}
