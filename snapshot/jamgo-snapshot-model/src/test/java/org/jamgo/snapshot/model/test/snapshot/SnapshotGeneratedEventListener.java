package org.jamgo.snapshot.model.test.snapshot;

import java.util.List;

import org.jamgo.snapshot.model.repository.SnapshotRepository;
import org.jamgo.snapshot.model.snapshot.SnapshotGeneratedEventData;
import org.jamgo.snapshot.model.snapshot.SnapshotVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;

public class SnapshotGeneratedEventListener {

	private static final Logger logger = LoggerFactory.getLogger(SnapshotGeneratedEventListener.class);

	@Autowired
	private SnapshotRepository repository;

	@EventListener(SnapshotGeneratedEventData.class)
	public void listener(final SnapshotGeneratedEventData event) {
		SnapshotGeneratedEventListener.logger.debug("listener - event: model = {}, action = {}, username = {}", event.getModel(), event.getAction().name(), event.getUserName());
//		try {
//			Thread.sleep(1000);
//		} catch (final InterruptedException e) {
//			e.printStackTrace();
//		}

		// Simulate SnapshotComparerView.refreshVersionsGrid() behaviour
		final List<SnapshotVersion> versions = this.repository.findSnapshotEntities(event.getModel().getClass().getName(), event.getModel().getId());
		SnapshotGeneratedEventListener.logger.debug("listener - retrieved {} versions", versions.size());
	}

}
