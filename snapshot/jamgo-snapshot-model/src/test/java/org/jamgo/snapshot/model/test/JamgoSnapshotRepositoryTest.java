package org.jamgo.snapshot.model.test;

import java.util.UUID;

import org.jamgo.model.entity.Model;
import org.jamgo.snapshot.model.entity.SnapshotEntity;
import org.jamgo.snapshot.model.repository.SnapshotRepository;
import org.jamgo.snapshot.model.snapshot.SnapshotInfo;
import org.jamgo.snapshot.model.snapshot.SnapshotVersion;
import org.jamgo.snapshot.model.test.snapshot.SnapshotModelTestConfig;
import org.jamgo.test.JamgoRepositoryTest;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@ContextConfiguration(classes = { SnapshotModelTestConfig.class })
@TestPropertySource(properties = { "snapshots.enabled = true" })
public class JamgoSnapshotRepositoryTest extends JamgoRepositoryTest {
	public static final Logger logger = LoggerFactory.getLogger(JamgoSnapshotRepositoryTest.class);

	@Autowired
	protected SnapshotInfo snapshotInfo;

	@Autowired
	protected SnapshotRepository snapshotRepository;

	protected final ObjectMapper objectMapper;

	public JamgoSnapshotRepositoryTest() {
		this.objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
	}

	@BeforeEach
	public void initSnapshotInfo() {
		// FIXME this method must go to JamgoRepositoryTest
		this.entityManager.clear();

		JamgoSnapshotRepositoryTest.logger.debug(String.format("Current thread id: %s", Thread.currentThread().getId()));

		this.snapshotInfo.setConnectedUsername("jamgo");
		this.snapshotInfo.setTransactionId(UUID.randomUUID());
	}

	@Override
	protected void commitAndStart() {
		super.commitAndStart();
		// We need to change transaction id to insert a new snapshot record in database
		this.snapshotInfo.setTransactionId(UUID.randomUUID());
	}

	@SuppressWarnings("unchecked")
	public <T extends Model> T getModelObjectFromVersion(final SnapshotVersion version, final Class<T> type) {
		final SnapshotEntity snapshotEntity = version.getSnapshotEntity();
		try {
			return (T) this.objectMapper.readValue(new String(snapshotEntity.getModelData()), Class.forName(snapshotEntity.getEntity().getModelClassName()));
		} catch (JsonProcessingException | ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

}
