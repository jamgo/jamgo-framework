package org.jamgo.snapshot.model.test.snapshot;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.jamgo.model.entity.TestSimpleObject;
import org.jamgo.model.repository.TestSimpleObjectRepository;
import org.jamgo.model.test.TestCategoryBuilder;
import org.jamgo.model.test.TestSimpleObjectBuilder;
import org.jamgo.snapshot.model.entity.SnapshotAction;
import org.jamgo.snapshot.model.entity.SnapshotRelationship;
import org.jamgo.snapshot.model.snapshot.SnapshotVersion;
import org.jamgo.snapshot.model.test.JamgoSnapshotRepositoryTest;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class SnapshotsTest extends JamgoSnapshotRepositoryTest {

	private static final Logger logger = LoggerFactory.getLogger(SnapshotsTest.class);

	@Autowired
	private TestSimpleObjectRepository testSimpleObjectRepository;

	@Test
	@Transactional
	public void testInsert_withoutCategories() throws Exception {
		this.entityManager.joinTransaction();
		new TestSimpleObjectBuilder(this.entityManager).setName("name").setAge(43).buildOne();

		this.commitAndStart();

		final List<TestSimpleObject> result = this.testSimpleObjectRepository.findAll();
		assertEquals(1, result.size());

//		// TODO Wait until asynch process ends

		final List<SnapshotVersion> versions = this.snapshotRepository.findSnapshotEntities(TestSimpleObject.class.getName(), 1L);
		assertEquals(1, versions.size());
		final SnapshotVersion version = versions.get(0);
		assertEquals(SnapshotAction.CREATE, version.getSnapshotEntity().getAction());
		final TestSimpleObject entity = this.getModelObjectFromVersion(version, TestSimpleObject.class);
		assertNotNull(entity);
		assertEquals("name", entity.getName());
		assertEquals(43, entity.getAge());

		final List<SnapshotRelationship> relationships = this.snapshotRepository.findSnapshotRelationships(version.getSnapshotEntity().getEntity());
		assertEquals(0, relationships.size());
	}

	@Test
	@Transactional
	public void testInsert_withCategories() throws Exception {
		this.entityManager.joinTransaction();
		new TestSimpleObjectBuilder(this.entityManager).setName("name").setAge(43).withCategories(1).buildOne();

		this.commitAndStart();
		this.entityManager.clear();

		final List<TestSimpleObject> result = this.testSimpleObjectRepository.findAll();
		assertEquals(1, result.size());

//		// TODO Wait until asynch process ends

		final List<SnapshotVersion> versions = this.snapshotRepository.findSnapshotEntities(TestSimpleObject.class.getName(), 1L);
		assertEquals(1, versions.size());
		final SnapshotVersion version = versions.get(0);
		assertEquals(SnapshotAction.CREATE, version.getSnapshotEntity().getAction());
		final TestSimpleObject entity = this.getModelObjectFromVersion(version, TestSimpleObject.class);
		assertNotNull(entity);
		assertEquals("name", entity.getName());
		assertEquals(43, entity.getAge());

		final List<SnapshotRelationship> relationships = this.snapshotRepository.findSnapshotRelationships(version.getSnapshotEntity().getEntity());
		assertEquals(1, relationships.size());
	}

	@Test
	@Transactional
	public void testUpdate_withoutCategories() throws Exception {
		this.entityManager.joinTransaction();
		final TestSimpleObject model = new TestSimpleObjectBuilder(this.entityManager).setName("name").setAge(43).buildOne();
		this.commitAndStart();
		model.setAge(44);
		this.testSimpleObjectRepository.update(model);
		this.commitAndStart();

		final List<TestSimpleObject> result = this.testSimpleObjectRepository.findAll();
		assertEquals(1, result.size());

//		// TODO Wait until asynch process ends

		final List<SnapshotVersion> versions = this.snapshotRepository.findSnapshotEntities(TestSimpleObject.class.getName(), 1L);
		assertEquals(2, versions.size());
		// version 1: age=44
		final SnapshotVersion version1 = versions.get(0);
		assertEquals(SnapshotAction.UPDATE, version1.getSnapshotEntity().getAction());
		final TestSimpleObject entity1 = this.getModelObjectFromVersion(version1, TestSimpleObject.class);
		assertNotNull(entity1);
		assertEquals("name", entity1.getName());
		assertEquals(44, entity1.getAge());
		// version 0: age=43
		final SnapshotVersion version0 = versions.get(1);
		assertEquals(SnapshotAction.CREATE, version0.getSnapshotEntity().getAction());
		final TestSimpleObject entity0 = this.getModelObjectFromVersion(version0, TestSimpleObject.class);
		assertNotNull(entity0);
		assertEquals("name", entity0.getName());
		assertEquals(43, entity0.getAge());

		final List<SnapshotRelationship> relationships1 = this.snapshotRepository.findSnapshotRelationships(version1.getSnapshotEntity().getEntity());
		assertEquals(0, relationships1.size());
		final List<SnapshotRelationship> relationships0 = this.snapshotRepository.findSnapshotRelationships(version0.getSnapshotEntity().getEntity());
		assertEquals(0, relationships0.size());
	}

	@Test
	@Transactional
	public void testUpdate_withCategories() throws Exception {
		this.entityManager.joinTransaction();
		final TestSimpleObject model = new TestSimpleObjectBuilder(this.entityManager).setName("name").setAge(43).withCategories(1).buildOne();
		this.commitAndStart();
		model.setAge(44);
		model.getCategories().add(new TestCategoryBuilder(this.entityManager).buildOne());
		this.testSimpleObjectRepository.update(model);
		this.commitAndStart();

		final List<TestSimpleObject> result = this.testSimpleObjectRepository.findAll();
		assertEquals(1, result.size());

//		// TODO Wait until asynch process ends

		final List<SnapshotVersion> versions = this.snapshotRepository.findSnapshotEntities(TestSimpleObject.class.getName(), 1L);
		assertEquals(2, versions.size());

		// assert version 1: age=44, categories.size()=2
		final SnapshotVersion version1 = versions.get(0);
		assertEquals(SnapshotAction.UPDATE, version1.getSnapshotEntity().getAction());
		final TestSimpleObject entity1 = this.getModelObjectFromVersion(version1, TestSimpleObject.class);
		assertNotNull(entity1);
		assertEquals("name", entity1.getName());
		assertEquals(44, entity1.getAge());
		final List<SnapshotRelationship> relationships1 = this.snapshotRepository.findSnapshotRelationships(version1.getSnapshotEntity().getEntity());
		assertEquals(2, relationships1.size());

		// assert version 0: age=43, categories.size()=1
		final SnapshotVersion version0 = versions.get(1);
		assertEquals(SnapshotAction.CREATE, version0.getSnapshotEntity().getAction());
		final TestSimpleObject entity0 = this.getModelObjectFromVersion(version0, TestSimpleObject.class);
		assertNotNull(entity0);
		assertEquals("name", entity0.getName());
		assertEquals(43, entity0.getAge());
		final List<SnapshotRelationship> relationships0 = this.snapshotRepository.findSnapshotRelationships(version0.getSnapshotEntity().getEntity());
		assertEquals(1, relationships0.size());
	}

}
