package org.jamgo.snapshot.model.test.snapshot;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.repository.BinaryResourceRepository;
import org.jamgo.model.test.entity.builder.BinaryResourceBuilder;
import org.jamgo.snapshot.model.test.JamgoSnapshotRepositoryTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class TestBinaryResource extends JamgoSnapshotRepositoryTest {

	@Autowired
	private BinaryResourceRepository repository;

	@Test
	@Transactional
	public void testInsertBinaryResource_withSnapshots() {
		this.entityManager.joinTransaction();
		final BinaryResource binaryResource = new BinaryResourceBuilder(this.entityManager).buildOne();
		this.commitAndStart();

		final List<BinaryResource> result = this.repository.findAll();
		assertEquals(1, result.size());
		assertEquals(binaryResource.getId(), result.get(0).getId());
	}
}
