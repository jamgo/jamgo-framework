package org.jamgo.snapshot.backoffice.ui.component.snapshot;

import org.jamgo.model.BasicModel;
import org.jamgo.model.entity.Model;
import org.jamgo.ui.layout.crud.CrudBasicPanel;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.spring.annotation.SpringComponent;

@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SnapshotsPanel extends CrudBasicPanel implements InitializingBean {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(SnapshotsPanel.class);

	@Autowired
	protected ApplicationContext applicationContext;
	@Autowired
	protected SnapshotsPanelManager snapshotsPanelManager;

	private CrudDetailsLayout<?> parentDetailsLayout;
	private SnapshotComparerView comparer;
	private boolean initializedVersionsGrid = false;

	public SnapshotsPanel(final CrudDetailsLayout<?> parentDetailsLayout) {
		super();
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.initialize();
	}

	public void initialize() {
		this.setSizeFull();
		this.setName(this.messageSource.getMessage("snapshots.title"));
		this.comparer = this.applicationContext.getBean(SnapshotComparerView.class);
		this.add(this.comparer);
	}

	@Override
	public void setParentDetailsLayout(final CrudDetailsLayout<?> parentDetailsLayout) {
		this.parentDetailsLayout = parentDetailsLayout;
	}

	@Override
	public void updateFields() {
		final Class<? extends Model> modelClass = this.getModelClass();
		final Long modelId = this.getModelId();
		this.comparer.init(modelClass, modelId);

		// TODO: Esbrinar per què es fa això! snapshotsPanelManager no s'utilitza enlloc!
		if (!this.initializedVersionsGrid) {
			this.snapshotsPanelManager.registerPanel(modelClass, this);
			this.initializedVersionsGrid = true;
		}
	}

	@Override
	public boolean isVisible(final boolean isCreation) {
		return !isCreation;
	}

	private Class<? extends Model> getModelClass() {
		return this.getTargetObjectFromParent().getClass();
	}

	private Long getModelId() {
		return this.getTargetObjectFromParent().getId();
	}

	private Model getTargetObjectFromParent() {
		final BasicModel<?> targetObject = this.parentDetailsLayout.getTargetObject();
		if (!(targetObject instanceof Model)) {
			throw new RuntimeException("SnapshotPanel needs a Model class");
		}
		return (Model) targetObject;
	}

	public void refreshVersionsGrid() {
		SnapshotsPanel.logger.debug("refreshVersionsGrid");
		this.getUI().ifPresent(ui -> ui.access(() -> {
			this.comparer.refreshVersionsGrid();
		}));
	}
}
