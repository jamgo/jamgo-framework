package org.jamgo.snapshot.backoffice.ui.component.snapshot;

import java.util.HashMap;
import java.util.Map;

import org.jamgo.model.entity.Model;
import org.jamgo.snapshot.model.snapshot.SnapshotGeneratedEventData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class SnapshotsPanelManager {

	private static final Logger logger = LoggerFactory.getLogger(SnapshotsPanelManager.class);

	private final Map<Class<? extends Model>, SnapshotsPanel> map = new HashMap<>();

	public void registerPanel(final Class<? extends Model> clazz, final SnapshotsPanel panel) {
		this.map.put(clazz, panel);
		SnapshotsPanelManager.logger.debug("panel registered for class {}", clazz.getName());
	}

	@EventListener(SnapshotGeneratedEventData.class)
	public void refreshVersionsGrid(final SnapshotGeneratedEventData event) {
		SnapshotsPanelManager.logger.debug("refreshVersionsGrid - event: model = {}, action = {}, username = {}", event.getModel(), event.getAction().name(), event.getUserName());
		final SnapshotsPanel panel = this.map.get(event.getModel().getClass());
		SnapshotsPanelManager.logger.debug("found panel? {}", panel);
		if (panel != null) {
			panel.refreshVersionsGrid();
		}
	}

}
