// FIXME Delete this file when finishing the migration
// ... keep here as reference API with client integration

//package cat.itec.pgm.controller;
//
//import java.util.List;
//import java.util.Locale;
//import java.util.UUID;
//
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import cat.itec.base.api.response.Response;
//import cat.itec.base.api.response.ResponseElement;
//import cat.itec.base.api.response.ResponseList;
//import cat.itec.base.exception.CustomException;
//import cat.itec.pgm.business.SnapshotComparerBusiness;
//import cat.itec.pgm.model.enums.SnapshotObjectType;
//import cat.itec.pgm.model.snapshot.ModelReference;
//import cat.itec.pgm.model.snapshot.ModelReferenceWithSnapshot;
//import cat.itec.pgm.model.snapshot.Snapshot;
//import cat.itec.pgm.model.snapshot.SnapshotComparison;
//import cat.itec.pgm.model.snapshot.SnapshotVersion;
//
//@Controller
//public class SnapshotComparerController extends BaseController {
//
//	@Autowired
//	private SnapshotComparerBusiness business;
//
//	@GetMapping("/snapshots/versions/{objectType}/{id}")
//	public ResponseEntity<Response> getVersions(@PathVariable final SnapshotObjectType objectType, @PathVariable final Long id, final Locale locale) throws CustomException {
//		final List<SnapshotVersion> list = this.business.getVersions(objectType, id);
//		return new ResponseEntity<>(new ResponseList<>(list), HttpStatus.OK);
//	}
//
//	@GetMapping("/arees/{areaId}/snapshot/versions")
//	public ResponseEntity<Response> getSpaceVersions(@PathVariable final Long areaId, final Locale locale) throws CustomException {
//		final List<SnapshotVersion> list = this.business.getSpaceVersions(areaId);
//		return new ResponseEntity<>(new ResponseList<>(list), HttpStatus.OK);
//	}
//
//	@GetMapping("/arees/{areaId}/snapshots")
//	public ResponseEntity<Response> compareWithLatestVersion(@PathVariable final Long areaId, @RequestParam(required = true) final boolean recursive, final Locale locale) {
//		// TODO Use "recursive" to get snapshot of spaces with subspaces
//		final SnapshotComparison comparison = this.business.compareSpaceWithLatestVersion(areaId, locale);
//		return new ResponseEntity<>(new ResponseElement<>(comparison), HttpStatus.OK);
//	}
//
//	@GetMapping("/arees/{areaId}/snapshot/version/{version}")
//	public ResponseEntity<Response> compareWithVersion(@PathVariable final Long areaId, @PathVariable final Long version, @RequestParam(required = true) final boolean recursive, final Locale locale) {
//		// TODO Use "recursive" to get snapshot of spaces with subspaces
//		final SnapshotComparison comparison = this.business.compareSpaceWithVersion(areaId, version, locale);
//		return new ResponseEntity<>(new ResponseElement<>(comparison), HttpStatus.OK);
//	}
//
//	@GetMapping("/snapshots/compare/left/{leftModelReference}/right/{rightModelReference}")
//	public ResponseEntity<Response> compareEntities(@PathVariable final String leftModelReference, @PathVariable final String rightModelReference, final Locale locale) {
//		final ModelReference left = this.getModelReference(leftModelReference);
//		final ModelReference right = this.getModelReference(rightModelReference);
//		final SnapshotComparison comparison = this.business.compareEntities(left, right, locale);
//		return new ResponseEntity<>(new ResponseElement<>(comparison), HttpStatus.OK);
//	}
//
//	private ModelReference getModelReference(final String strModelReference) {
//		if ("none".equals(strModelReference)) {
//			return null;
//		}
//		final String[] parts = strModelReference.split("_");
//		if (!StringUtils.isNumeric(parts[1])) {
//			return null;
//		}
//		final ModelReference modelReference;
//
//		if (parts[2].contains("-")) { // UUID
//			modelReference = new ModelReferenceWithSnapshot();
//			((ModelReferenceWithSnapshot) modelReference).setSnapshot(new Snapshot());
//			((ModelReferenceWithSnapshot) modelReference).getSnapshot().setId(UUID.fromString(parts[2]));
//		} else {
//			modelReference = new ModelReference();
//			if (!"current".equals(parts[2])) {
//				modelReference.setModelVersion(Long.parseLong(parts[2]));
//			}
//		}
//		modelReference.setModelClassName(parts[0]);
//		modelReference.setModelId(Long.parseLong(parts[1]));
//
//		return modelReference;
//	}
//
//}
