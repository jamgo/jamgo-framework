package org.jamgo.snapshot.backoffice.ui.component.snapshot;

import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

import org.jamgo.model.entity.Model;
import org.jamgo.model.entity.ModelReference;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.session.SessionContext;
import org.jamgo.snapshot.model.snapshot.SnapshotComparison;
import org.jamgo.snapshot.model.snapshot.SnapshotComparison.Result;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonAttribute;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonAttribute.AttributeType;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonAttributeComparator;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonCollectionAttribute;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonEmbeddedAttribute;
import org.jamgo.snapshot.model.snapshot.SnapshotVersion;
import org.jamgo.snapshot.services.SnapshotComparerService;
import org.jamgo.ui.component.builders.JamgoComponentBuilderFactory;
import org.jamgo.vaadin.renderers.HtmlRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.componentfactory.ToggleButton;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalConfigurableFilterDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@CssImport("./styles/snapshot-comparer.css")
@CssImport(value = "./styles/snapshot-comparer.css", themeFor = "vaadin-grid")
public class SnapshotComparerView extends VerticalLayout {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(SnapshotComparerView.class);

	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

	@Autowired
	protected JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	private SnapshotComparerService service;
	@Autowired
	private SessionContext sessionContext;
	@Autowired
	private SnapshotComparisonAttributeComparator attributeComparator;

	private Class<? extends Model> modelClass;
	private Long modelId;
	private Grid<SnapshotVersion> versionsGrid;

	private ToggleButton equalToggleButton;
	private ToggleButton modifiedToggleButton;
	private ToggleButton newToggleButton;
	private ToggleButton erasedToggleButton;
	private TreeGrid<SnapshotComparisonAttribute<?>> grid;
	private HierarchicalConfigurableFilterDataProvider<SnapshotComparisonAttribute<?>, Void, SnapshotComparerViewFilter> filteredDataProvider;
	private SnapshotVersion selectedVersion;

	public SnapshotComparerView() {
	}

	public <T extends Model> void init(final Class<T> modelClass, final Long modelId) {
		SnapshotComparerView.logger.debug("init");

		this.setWidth(100.0f, Unit.PERCENTAGE);
		this.setSpacing(true);
		this.removeAll();

		if (modelId == null) {
			return;
		}

		this.modelClass = modelClass;
		this.modelId = modelId;

		// Filter rows in grid depending on selected filters.
		// Filters: EQUAL, MODIFIED, NEW, ERASED
		this.equalToggleButton = this.componentBuilderFactory.createToggleButtonBuilder().setLabelId("snapshots.filter.equal").build();
		this.equalToggleButton.addValueChangeListener(event -> this.updateTreeGridFilter());
		this.modifiedToggleButton = this.componentBuilderFactory.createToggleButtonBuilder().setLabelId("snapshots.filter.modified").build();
		this.modifiedToggleButton.addValueChangeListener(event -> this.updateTreeGridFilter());
		this.newToggleButton = this.componentBuilderFactory.createToggleButtonBuilder().setLabelId("snapshots.filter.new").build();
		this.newToggleButton.addValueChangeListener(event -> this.updateTreeGridFilter());
		this.erasedToggleButton = this.componentBuilderFactory.createToggleButtonBuilder().setLabelId("snapshots.filter.erased").build();
		this.erasedToggleButton.addValueChangeListener(event -> this.updateTreeGridFilter());
		final HorizontalLayout toggleButtons = this.componentBuilderFactory.createHorizontalLayoutBuilder().setWidth(100.0f, Unit.PERCENTAGE).build();
		toggleButtons.setMargin(true);
		toggleButtons.add(this.equalToggleButton, this.modifiedToggleButton, this.newToggleButton, this.erasedToggleButton);
		// TODO Show toggleButtons when filter issues solved
		toggleButtons.setVisible(false);

		SnapshotComparerView.logger.debug("retrieving versions...");
		final List<SnapshotVersion> versions = this.service.getVersions(modelClass, modelId);
		SnapshotComparerView.logger.debug("retrieved {} versions", versions.size());
		if (!versions.isEmpty()) {
			SnapshotComparerView.logger.debug("last version retrieved: {}", versions.get(0).getEntity().getModelVersion());
		}

		// Grid of versions
		this.versionsGrid = this.componentBuilderFactory.<SnapshotVersion> createGridBuilder()
			.setHeight(128.0f, Unit.PIXELS)
			.build();
		this.versionsGrid.addColumn(version -> version.getEntity().getModelVersion()).setHeader(this.messageSource.getMessage("snapshots.version.number"));
		this.versionsGrid.addColumn(version -> version.getSnapshot().getUsername()).setHeader(this.messageSource.getMessage("snapshots.version.author"));
		this.versionsGrid.addColumn(version -> this.messageSource.getMessage("snapshots.version.action." + version.getAction().name().toLowerCase())).setHeader(this.messageSource.getMessage("snapshots.version.action"));
		this.versionsGrid.addColumn(version -> SnapshotComparerView.DATE_TIME_FORMATTER.format(version.getSnapshot().getTimestamp())).setHeader(this.messageSource.getMessage("snapshots.version.timestamp"));
		this.versionsGrid.addComponentColumn(this::getCompareButton);
		this.versionsGrid.setItems(versions);

//		// TODO List of versions as combobox (configurable or remove this commented code)
//		ComboBox<SnapshotVersion>  versionsComboBox = this.componentBuilderFactory.<SnapshotVersion> createComboBoxBuilder()
//			.setLabelId("snapshots.comparer.versions")
//			.setWidth(50.0f, Unit.PERCENTAGE)
//			.build();
//		versionsComboBox.setItems(versions);
//		versionsComboBox.setItemLabelGenerator(this::getVersionName);
//		versionsComboBox.addValueChangeListener(event -> this.updateTreeGrid());

		// Attributes grid
		this.grid = new TreeGrid<>();
		this.grid.addHierarchyColumn(this::getAttributeName)
			.setHeader(this.messageSource.getMessage("snapshots.comparer.attribute"))
			.setClassNameGenerator(this::getClassNameGenerator);
		this.grid.addColumn(new HtmlRenderer<>(attribute -> this.getGridContent(attribute, attribute.getValueLeft())))
			.setHeader(this.messageSource.getMessage("snapshots.comparer.left"))
			.setClassNameGenerator(this::getClassNameGenerator);
		this.grid.addColumn(new HtmlRenderer<>(attribute -> this.getGridContent(attribute, attribute.getValueRight())))
			.setHeader(this.messageSource.getMessage("snapshots.comparer.right"))
			.setClassNameGenerator(this::getClassNameGenerator);

		this.grid.setDataProvider(this.createDataProvider());
		this.grid.setWidth(100.0f, Unit.PERCENTAGE);

		final HorizontalLayout gridLegend = this.componentBuilderFactory.createHorizontalLayoutBuilder()
			.setClassName("snapshot-comparer-legend")
			.build();
		gridLegend.setMargin(true);
		gridLegend.setJustifyContentMode(JustifyContentMode.CENTER);
		Arrays.asList(Result.values()).forEach(result -> {
			final Div legendItem = new Div();
			final Span span = new Span();
			span.setClassName(this.getClassNameForResult(result));
			span.getStyle().set("padding-left", "1.5em");
			span.getStyle().set("margin-right", "1em");

			legendItem.add(span, new Text(this.messageSource.getMessage("snapshots.comparer.legend." + result.name().toLowerCase())));
			gridLegend.add(legendItem);
		});
		this.add(this.versionsGrid, toggleButtons, this.grid, gridLegend);
		this.setHorizontalComponentAlignment(Alignment.CENTER, toggleButtons, gridLegend);
	}

	private DataProvider<SnapshotComparisonAttribute<?>, Void> createDataProvider() {
		final HierarchicalDataProvider<SnapshotComparisonAttribute<?>, SnapshotComparerViewFilter> dataProvider = new AbstractBackEndHierarchicalDataProvider<>() {

			private static final long serialVersionUID = 1L;

			@Override
			public int getChildCount(final HierarchicalQuery<SnapshotComparisonAttribute<?>, SnapshotComparerViewFilter> query) {
				// FIXME OPTIMIZE: make a count query instead
				final SnapshotComparisonAttribute<?> attribute = query.getParent();
				if (attribute == null) {
					final SnapshotComparison data = SnapshotComparerView.this.compareVersions();
					return data.getAttributes().size();
				}

				if (attribute instanceof SnapshotComparisonCollectionAttribute) {
					final SnapshotComparisonCollectionAttribute collectionAttribute = (SnapshotComparisonCollectionAttribute) attribute;
					return collectionAttribute.getChildren().size();
				} else if (attribute instanceof SnapshotComparisonEmbeddedAttribute) {
					final SnapshotComparisonEmbeddedAttribute embeddedAttribute = (SnapshotComparisonEmbeddedAttribute) attribute;
					return embeddedAttribute.getChildren().size();
				}
				if (attribute.getType() != AttributeType.MODEL) {
					return 0;
				}

				final ModelReference left = this.getLeftModelReference(attribute);
				final ModelReference right = this.getRightModelReference(attribute);
				return SnapshotComparerView.this.service.compareEntities(left, right, SnapshotComparerView.this.getLocale()).getAttributes().size();
			}

			@Override
			public boolean hasChildren(final SnapshotComparisonAttribute<?> item) {
				return item.isMoreInfo() && (item.getValueLeft() != null || item.getValueRight() != null);
			}

			@Override
			protected Stream<SnapshotComparisonAttribute<?>> fetchChildrenFromBackEnd(final HierarchicalQuery<SnapshotComparisonAttribute<?>, SnapshotComparerViewFilter> query) {
				final SnapshotComparisonAttribute<?> attribute = query.getParent();
				if (attribute == null) {
					final SnapshotComparison data = SnapshotComparerView.this.compareVersions();
					final List<SnapshotComparisonAttribute<?>> attributes = data.getAttributes();
					this.sortAttributes(attributes);
					return attributes.stream();
				}

				if (attribute instanceof SnapshotComparisonCollectionAttribute) {
					final SnapshotComparisonCollectionAttribute collectionAttribute = (SnapshotComparisonCollectionAttribute) attribute;
					return collectionAttribute.getChildren().stream().map(modelAttribute -> (SnapshotComparisonAttribute<?>) modelAttribute);
				} else if (attribute instanceof SnapshotComparisonEmbeddedAttribute) {
					final SnapshotComparisonEmbeddedAttribute embeddedAttribute = (SnapshotComparisonEmbeddedAttribute) attribute;
					return embeddedAttribute.getChildren().stream().map(modelAttribute -> (SnapshotComparisonAttribute<?>) modelAttribute);
				}
				if (attribute.getType() != AttributeType.MODEL) {
					return null;
				}

				final ModelReference left = this.getLeftModelReference(attribute);
				final ModelReference right = this.getRightModelReference(attribute);
				final List<SnapshotComparisonAttribute<?>> attributes = SnapshotComparerView.this.service.compareEntities(left, right, SnapshotComparerView.this.getLocale()).getAttributes();
				this.sortAttributes(attributes);
				return attributes.stream();
			}

			private void sortAttributes(final List<SnapshotComparisonAttribute<?>> attributes) {
				Collections.sort(attributes, SnapshotComparerView.this.attributeComparator);
			}

			private ModelReference getLeftModelReference(final SnapshotComparisonAttribute<?> attribute) {
				return this.getModelReference(attribute.getValueLeft());
			}

			private ModelReference getRightModelReference(final SnapshotComparisonAttribute<?> attribute) {
				return this.getModelReference(attribute.getValueRight());
			}

			private ModelReference getModelReference(final Object value) {
				ModelReference modelReference = null;
				if (value instanceof ModelReference) {
					modelReference = (ModelReference) value;
				} else {
					if (value != null) {
						final Model model = (Model) value;
						modelReference = new ModelReference();
						modelReference.setModelClassName(model.getClass().getName());
						modelReference.setModelId(model.getId());
						modelReference.setModelVersion(model.getVersion());
					}
				}
				return modelReference;
			}
		};

		this.filteredDataProvider = dataProvider.withConfigurableFilter();
		return this.filteredDataProvider;
	}

	private String getAttributeName(final SnapshotComparisonAttribute<?> attribute) {
		String result = null;
		final String attributeName = attribute.getName();
		if (attributeName.startsWith("[")) { // Collection index
			result = attributeName;
		} else if (attributeName.startsWith("lang.")) {
			// TODO Get language name from repository using language code
			result = attributeName.split("\\.")[1];
		} else {
//			String key = null;
//			if (attributeName.equals("id") || attributeName.equals("version")) {
//				key = String.format("model.%s", attributeName);
//			} else {
//				key = String.format("%s.%s", this.keyPrefix, attributeName);
//			}
			result = this.messageSource.getMessage(attributeName);
		}
		return result;
	}

	private String getGridContent(final SnapshotComparisonAttribute<?> attribute, final Object value) {
		String gridContent = null;
		if (value == null) {
			gridContent = "";
		} else {
			switch (attribute.getType()) {
				case SIMPLE:
				case EMBEDDED:
					gridContent = value.toString();
					break;
				case MODEL:
					final ModelReference reference = (ModelReference) value;
					gridContent = String.format("(id=%d, version=%d)", reference.getModelId(), reference.getModelVersion());
					break;
				case TRANSLATED:
					gridContent = this.messageSource.getMessage("snapshots.comparer.attribute.translated." + value.toString());
					break;
				default:
					gridContent = "";
			}
		}
		return gridContent;
	}

	private SnapshotComparison compareVersions() {
		SnapshotComparison data;
		final SnapshotVersion selectedVersion = this.getSelectedVersion();
		if (selectedVersion == null) {
			data = this.service.compareWithLatestVersion(this.modelClass, this.modelId, this.getLocale());
		} else {
			data = this.service.compareWithVersion(this.modelClass, this.modelId, selectedVersion.getEntity().getModelVersion(), this.getLocale());
		}
		return data;
	}

	private SnapshotVersion getSelectedVersion() {
		return this.selectedVersion;
	}

	private Button getCompareButton(final SnapshotVersion version) {
		final Button button = this.componentBuilderFactory.createButtonBuilder()
			.setLabelId("snapshots.actions.compare")
			.build();
		button.addClickListener(event -> this.updateTreeGrid(version));
		return button;
	}

	private void updateTreeGrid(final SnapshotVersion selectedVersion) {
//		final SnapshotVersion selectedVersion = this.getSelectedVersion();
		SnapshotComparerView.logger.debug("Selected version: {}", this.getVersionName(selectedVersion));
		this.selectedVersion = selectedVersion;
		this.grid.getDataProvider().refreshAll();
	}

	private void updateTreeGridFilter() {
		SnapshotComparerView.logger.debug("updateTreeGridFilter: modified: {}, equal: {}, erased: {}, new: {}",
			this.modifiedToggleButton.getValue().booleanValue(),
			this.equalToggleButton.getValue().booleanValue(),
			this.erasedToggleButton.getValue().booleanValue(),
			this.newToggleButton.getValue().booleanValue());

		final SnapshotComparerViewFilter filter = new SnapshotComparerViewFilter();
		filter.setShowEqual(this.equalToggleButton.getValue());
		filter.setShowModified(this.modifiedToggleButton.getValue());
		filter.setShowNew(this.newToggleButton.getValue());
		filter.setShowErased(this.erasedToggleButton.getValue());

		this.filteredDataProvider.setFilter(filter);
	}

	private String getVersionName(final SnapshotVersion version) {
		if (version == null) {
			return null;
		}
		return String.format("version %d: %s (%s)", version.getEntity().getModelVersion(), SnapshotComparerView.DATE_TIME_FORMATTER.format(version.getSnapshot().getTimestamp()), version.getSnapshot().getUsername());
	}

	private String getClassNameGenerator(final SnapshotComparisonAttribute<?> attribute) {
		final Result result = attribute.getResult();
		return this.getClassNameForResult(result);
	}

	private String getClassNameForResult(final Result result) {
		return "attribute-" + result.name().toLowerCase();
	}

	public static class SnapshotComparerViewFilter {
		private boolean showEqual;
		private boolean showModified;
		private boolean showNew;
		private boolean showErased;

		public SnapshotComparerViewFilter() {
			// Default start values
			this.showEqual = true;
			this.showModified = true;
			this.showNew = true;
			this.showErased = true;
		}

		public boolean isShowEqual() {
			return this.showEqual;
		}

		public void setShowEqual(final boolean showEqual) {
			this.showEqual = showEqual;
		}

		public boolean isShowModified() {
			return this.showModified;
		}

		public void setShowModified(final boolean showModified) {
			this.showModified = showModified;
		}

		public boolean isShowNew() {
			return this.showNew;
		}

		public void setShowNew(final boolean showNew) {
			this.showNew = showNew;
		}

		public boolean isShowErased() {
			return this.showErased;
		}

		public void setShowErased(final boolean showErased) {
			this.showErased = showErased;
		}

	}

	public void refreshVersionsGrid() {
		SnapshotComparerView.logger.debug("refreshVersionsGrid");
		SnapshotComparerView.logger.debug("retrieving versions...");
		final List<SnapshotVersion> versions = this.service.getVersions(this.modelClass, this.modelId);
		if (versions != null) {
			SnapshotComparerView.logger.debug("retrieved {} versions", versions.size());
			SnapshotComparerView.logger.debug("last version retrieved: {}", versions.get(0).getEntity().getModelVersion());
			if (this.versionsGrid != null) {
				this.versionsGrid.setItems(versions);
			}
		}
	}

	@Override
	protected Locale getLocale() {
		Locale locale = this.sessionContext.getCurrentLocale();
		if (locale == null) {
			locale = super.getLocale();
		}
		return locale;
	}
}
