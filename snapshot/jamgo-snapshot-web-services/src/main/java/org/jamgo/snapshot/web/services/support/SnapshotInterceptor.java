package org.jamgo.snapshot.web.services.support;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jamgo.services.session.SessionContext;
import org.jamgo.snapshot.app.support.SnapshotSupportHelper;
import org.jamgo.snapshot.model.snapshot.SnapshotInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * HandlerInterceptor to create a unique transactionID for every received request.
 * This information is used to generate snapshots of all the modified (created, updated, deleted)
 * entities in the application. This information is stored in a ThreadLocal variable (snapshotInfo),
 * so every thread will store a different transactionID.
 * This interceptor also cleans ThreadLocal variable data to avoid the reuse of same id in future requests
 * that reuse the same thread.
 */
public class SnapshotInterceptor implements HandlerInterceptor {

	private static final Logger logger = LoggerFactory.getLogger(SnapshotInterceptor.class);

	@Autowired
	private SessionContext sessionContext;

	@Autowired
	private SnapshotInfo snapshotInfo;

	@Autowired
	private SnapshotSupportHelper helper;

	@Override
	public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) throws Exception {
		SnapshotInterceptor.logger.debug("preHandle()");
		this.helper.preHandle(this.snapshotInfo, this.sessionContext.getCurrentUser());
		return true;
	}

	@Override
	public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final ModelAndView modelAndView) throws Exception {
		SnapshotInterceptor.logger.debug("postHandle()");
		this.helper.postHandle(this.snapshotInfo);
	}

}
