package org.jamgo.snapshot.web.services.config;

import org.jamgo.snapshot.app.config.SnapshotConfiguration;
import org.jamgo.snapshot.web.services.support.SnapshotInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ SnapshotConfiguration.class })
@ConditionalOnProperty(value = "snapshots.enabled", havingValue = "true", matchIfMissing = false)
public class SnapshotWebServicesConfiguration {

	@Bean
	public SnapshotInterceptor snapshotInterceptor() {
		return new SnapshotInterceptor();
	}

}
