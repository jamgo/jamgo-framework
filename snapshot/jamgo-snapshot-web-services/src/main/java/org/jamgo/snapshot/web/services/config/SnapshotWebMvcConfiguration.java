package org.jamgo.snapshot.web.services.config;

import org.jamgo.snapshot.web.services.support.SnapshotInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ConditionalOnProperty(value = "snapshots.enabled", havingValue = "true", matchIfMissing = false)
public class SnapshotWebMvcConfiguration implements WebMvcConfigurer {

	public static final Logger logger = LoggerFactory.getLogger(SnapshotWebMvcConfiguration.class);

	@Autowired
	private SnapshotInterceptor snapshotInterceptor;

	@Override
	public void addInterceptors(final InterceptorRegistry registry) {
		registry.addInterceptor(this.snapshotInterceptor);
		SnapshotWebMvcConfiguration.logger.debug("Interceptor registered: snapshotInterceptor");
	}

}
