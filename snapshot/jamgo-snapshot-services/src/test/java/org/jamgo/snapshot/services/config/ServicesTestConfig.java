package org.jamgo.snapshot.services.config;

import java.util.Properties;
import java.util.UUID;

import org.jamgo.services.UserService;
import org.jamgo.services.session.SessionContext;
import org.jamgo.services.session.SessionContextImpl;
import org.jamgo.snapshot.model.snapshot.SnapshotInfo;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@ComponentScan(
	basePackageClasses = {
		org.jamgo.services.impl.PackageMarker.class
	})
public class ServicesTestConfig {

	@Bean
	public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		final PropertySourcesPlaceholderConfigurer propsConfig = new PropertySourcesPlaceholderConfigurer();
		return propsConfig;
	}

	@Bean
	public Properties permissionProperties() {
		final Properties props = new Properties();
		return props;
	}

	@Bean
	public SessionContext sessionContext() {
		return new SessionContextImpl();
	}

	@Bean
	public UserService userService() {
		return Mockito.mock(UserService.class);
	}

	@Bean(name = "snapshotInfo")
	@Scope(scopeName = "prototype")
	public SnapshotInfo snapshotInfo() {
		final SnapshotInfo snapshotInfo = new SnapshotInfo();
		snapshotInfo.setTransactionId(UUID.randomUUID());
		snapshotInfo.setConnectedUsername("test");
		return snapshotInfo;
	}

}
