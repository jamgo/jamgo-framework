package org.jamgo.snapshot.services.snapshot;

import java.lang.reflect.Modifier;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.metamodel.EntityType;

import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.jpa.QueryHints;
import org.jamgo.model.entity.Acl;
import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.LocalizedMessage;
import org.jamgo.model.entity.Model;
import org.jamgo.model.entity.SecuredObject;
import org.jamgo.snapshot.model.annotation.SnapshotDisabled;
import org.jamgo.snapshot.model.entity.Snapshot;
import org.jamgo.snapshot.model.entity.SnapshotAction;
import org.jamgo.snapshot.model.repository.SnapshotGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class SnapshotBulkLoader {

	public static final Logger logger = LoggerFactory.getLogger(SnapshotBulkLoader.class);

	@PersistenceContext
	protected EntityManager entityManager;

	@Autowired
	private SnapshotGenerator snapshotGenerator;

	private final List<Class<?>> excludedClasses = Arrays.asList(Acl.class, Language.class, LocalizedMessage.class, SecuredObject.class);

	public SnapshotBulkLoader() {
		super();
	}

	@Transactional
	public void run(final String... args) throws Exception {
		final UUID transactionId = UUID.randomUUID();
		final String userName = "system";
		boolean snapshotCreated = false;

		// Detect subclasses of Model class
		final List<Class<? extends Model>> modelSubclasses = this.getModelSubclasses();

		// For each subclass ...
		for (final Class<? extends Model> clazz : modelSubclasses) {
			// - select entities without snapshot
			if (clazz.isAnnotationPresent(SnapshotDisabled.class)) {
				continue;
			}
			final String qlString = String.format("SELECT m FROM %s m LEFT JOIN SnapshotEntity se ON se.entity.modelClassName = '%s' AND se.entity.modelId = m.id AND se.entity.modelVersion = m.version WHERE se.snapshotId IS NULL ORDER BY m.id", clazz.getName(), clazz.getName());
			SnapshotBulkLoader.logger.debug("Processing class {}: {}", clazz.getName(), qlString);

			final List<? extends Model> modelEntities = this.entityManager.createQuery(qlString, clazz).setHint(QueryHints.HINT_READONLY, true).getResultList();
			modelEntities.forEach(model -> SnapshotBulkLoader.logger.debug(model.getClass() + "#" + model.getId() + "#" + model.getVersion()));

			final String olderSnapshotsQueryString = String.format("SELECT distinct(se.entity.modelId) FROM SnapshotEntity se WHERE se.entity.modelClassName = '%s'", clazz.getName());

			final List<Long> snapshotIdsSearchResult = this.entityManager.createQuery(olderSnapshotsQueryString, Long.class).setHint(QueryHints.HINT_READONLY, true).getResultList();
			final Set<Long> snapshotIds = snapshotIdsSearchResult.stream().collect(Collectors.toSet());
			final boolean anySnapshot = CollectionUtils.isNotEmpty(snapshotIds);

			// - create snapshot for those entities
			for (final Model model : modelEntities) {
				if (!snapshotCreated) {
					// create a unique snapshot for all this process, with current date and "system" user name
					this.createSnapshot(transactionId, userName);
					snapshotCreated = true;
				}
				SnapshotAction snapshotAction = SnapshotAction.CREATE;
				if (anySnapshot && snapshotIds.contains(model.getId())) {
					snapshotAction = SnapshotAction.UPDATE;
				}
				this.snapshotGenerator.generateSnapshot(model, snapshotAction, transactionId, userName, true);
			}
		}
	}

	private void createSnapshot(final UUID transactionId, final String userName) {
		final Snapshot snapshot = new Snapshot();
		snapshot.setId(transactionId.toString());
		snapshot.setUsername(userName);
		snapshot.setTimestamp(LocalDateTime.now());
		this.entityManager.persist(snapshot);
	}

	private List<Class<? extends Model>> getModelSubclasses() {
		// Get all entity classes...
		final Set<EntityType<?>> entities = this.entityManager.getMetamodel().getEntities();
		@SuppressWarnings("unchecked")
		final List<Class<? extends Model>> modelClasses = entities.stream()
			.map(entity -> entity.getJavaType())
			.filter(clazz -> !this.isExcludedClass(clazz))
			.filter(clazz -> !Modifier.isAbstract(clazz.getModifiers())) // ... not abstract classes (e.g. MasterData)
			.filter(clazz -> Model.class.isAssignableFrom(clazz)) // ... subclasses of Model
			.map(clazz -> (Class<? extends Model>) clazz) // ... cast the class to Class<? extends Model>
			.collect(Collectors.toList());

		final List<Class<? extends Model>> classes = modelClasses.stream()
			.filter(modelClazz -> !modelClasses.stream().anyMatch(clazz -> modelClazz != clazz && modelClazz.isAssignableFrom(clazz))) // exclude superclasses of other model classes (not sure if it's a good idea...)
			.sorted((clazz1, clazz2) -> clazz1.getName().compareTo(clazz2.getName())) // ... sort by className
			.collect(Collectors.toList());

		SnapshotBulkLoader.logger.debug("All Model subclasses found:");
		classes.stream().map(clazz -> clazz.getName()).forEach(className -> SnapshotBulkLoader.logger.debug("- {}", className));
		return classes;
	}

	protected boolean isExcludedClass(final Class<?> clazz) {
		return this.getExcludedClasses().contains(clazz);
	}

	public List<Class<?>> getExcludedClasses() {
		return this.excludedClasses;
	}

}
