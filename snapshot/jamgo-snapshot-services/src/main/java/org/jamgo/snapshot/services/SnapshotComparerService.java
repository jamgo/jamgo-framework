package org.jamgo.snapshot.services;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.persistence.Embedded;

import org.apache.commons.lang3.StringUtils;
import org.jamgo.model.entity.Category;
import org.jamgo.model.entity.LocalizedModel;
import org.jamgo.model.entity.LocalizedString;
import org.jamgo.model.entity.Model;
import org.jamgo.model.entity.ModelReference;
import org.jamgo.model.valueobjects.ValueObject;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.services.impl.LocalizedObjectService;
import org.jamgo.snapshot.model.annotation.SnapshotHiddenField;
import org.jamgo.snapshot.model.annotation.SnapshotJsonEmbedded;
import org.jamgo.snapshot.model.converter.CustomSnapshotConverter;
import org.jamgo.snapshot.model.entity.SnapshotEntity;
import org.jamgo.snapshot.model.entity.SnapshotRelationship;
import org.jamgo.snapshot.model.entity.SnapshotRelationship.RelationshipType;
import org.jamgo.snapshot.model.repository.SnapshotGenerator;
import org.jamgo.snapshot.model.repository.SnapshotRepository;
import org.jamgo.snapshot.model.snapshot.ModelReferenceWithSnapshot;
import org.jamgo.snapshot.model.snapshot.SnapshotComparison;
import org.jamgo.snapshot.model.snapshot.SnapshotComparison.Result;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonAttribute;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonAttribute.AttributeType;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonCollectionAttribute;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonData;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonEmbeddedAttribute;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonEmbeddedComplexAttribute;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonModelAttribute;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonSimpleAttribute;
import org.jamgo.snapshot.model.snapshot.SnapshotVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ReflectionUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class SnapshotComparerService {

	private static final Logger logger = LoggerFactory.getLogger(SnapshotComparerService.class);

	// FIXME Evitar passar "locale" als mètodes, cal fer servir el de sessionContext (o LocalizedStringService pels LocalizedString).

	@Autowired
	private SnapshotRepository repository;
	@Autowired(required = false)
	private SnapshotGenerator snapshotGenerator;
	@Autowired
	private LocalizedObjectService localizedObjectService;
	@Autowired
	private LocalizedMessageService messageSource;

	private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	private final DateTimeFormatter datetimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

	private final ObjectMapper objectMapper;

	public SnapshotComparerService() {
		this.objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
	}

	/**
	 * Get all existing versions for this object
	 * @param modelClass - model class
	 * @param modelId - model id
	 * @return
	 * @throws CustomException
	 */
	public <T extends Model> List<SnapshotVersion> getVersions(final Class<T> modelClass, final Long modelId) {
		return this.getVersions(modelClass.getName(), modelId);
	}

	public List<SnapshotVersion> getVersions(final String modelClassName, final Long modelId) {
		return this.repository.findSnapshotEntities(modelClassName, modelId);
	}

	/**
	 *
	 * @param modelClass - model class
	 * @param modelId - model id
	 * @param locale
	 * @return
	 */
	public <T extends Model> SnapshotComparison compareWithLatestVersion(final Class<T> modelClass, final Long modelId, final Locale locale) {
		return this.compareWithLatestVersion(modelClass.getName(), modelId, locale);
	}

	public SnapshotComparison compareWithLatestVersion(final String modelClassName, final Long modelId, final Locale locale) {
		final List<SnapshotVersion> versions = this.getVersions(modelClassName, modelId);
		final SnapshotVersion currentVersion = this.getCurrentVersion(modelClassName, modelId);
		SnapshotVersion latestVersion = null;
		if (!versions.isEmpty()) {
			latestVersion = versions.get(0);
		}

		return this.compareVersions(currentVersion, latestVersion, locale);
	}

	/**
	 *
	 * @param modelClass - model class
	 * @param modelId - model id
	 * @param selectedVersion
	 * @param locale
	 * @return
	 */
	public <T extends Model> SnapshotComparison compareWithVersion(final Class<T> modelClass, final Long modelId, final Long selectedVersion, final Locale locale) {
		return this.compareWithVersion(modelClass.getName(), modelId, selectedVersion, locale);
	}

	public SnapshotComparison compareWithVersion(final String modelClassName, final Long modelId, final Long selectedVersion, final Locale locale) {
		final List<SnapshotVersion> versions = this.getVersions(modelClassName, modelId);
		final SnapshotVersion currentVersion = this.getCurrentVersion(modelClassName, modelId);
		final SnapshotVersion otherVersion = versions.stream()
			.filter(version -> version.getEntity().getModelVersion().equals(selectedVersion))
			.findAny().orElse(null);

		return this.compareVersions(currentVersion, otherVersion, locale);
	}

	public SnapshotComparison compareEntities(final ModelReference left, final ModelReference right, final Locale locale) {
		final SnapshotVersion leftVersion = this.getSnapshotVersion(left, locale);
		final SnapshotVersion rightVersion = this.getSnapshotVersion(right, locale);
		return this.compareVersions(leftVersion, rightVersion, locale);
	}

	private SnapshotVersion getSnapshotVersion(final ModelReference modelReference, final Locale locale) {
		SnapshotVersion snapshotVersion = null;
		if (modelReference != null) {
			if (modelReference instanceof ModelReferenceWithSnapshot) {
				final ModelReferenceWithSnapshot modelReferenceWithSnapshot = (ModelReferenceWithSnapshot) modelReference;
				modelReferenceWithSnapshot.setSnapshot(this.repository.findById(modelReferenceWithSnapshot.getSnapshot().getId()));
				snapshotVersion = this.repository.findSnapshotEntity(modelReferenceWithSnapshot);
			} else {
				if (modelReference.getModelVersion() == null) {
					snapshotVersion = this.getCurrentVersion(modelReference);
				} else {
					snapshotVersion = this.repository.findSnapshotEntity(modelReference);
				}
			}
		}
		return snapshotVersion;
	}

	private SnapshotVersion getCurrentVersion(final String modelClassName, final Long modelId) {
		final ModelReference modelReference = new ModelReference();
		modelReference.setModelClassName(modelClassName);
		modelReference.setModelId(modelId);
		return this.getCurrentVersion(modelReference);
	}

	private SnapshotVersion getCurrentVersion(final ModelReference modelReference) {
		final SnapshotVersion version = new SnapshotVersion();
		version.setSnapshotEntity(new SnapshotEntity());
		version.getSnapshotEntity().setEntity(modelReference);
		return version;
	}

	private SnapshotComparisonData getCurrentData(final ModelReference modelReference) {
		final Model model = this.repository.findModelEntity(modelReference.getModelClassName(), modelReference.getModelId());
		return this.snapshotGenerator.generateDynamicSnapshot(model);
	}

	private SnapshotComparison compareVersions(final SnapshotVersion currentVersion, final SnapshotVersion otherVersion, final Locale locale) {
		SnapshotComparerService.logger.debug("comparing versions: currentVersion = {}, otherVersion = {}",
			Optional.ofNullable(currentVersion).map(SnapshotVersion::getEntity).orElse(null),
			Optional.ofNullable(otherVersion).map(SnapshotVersion::getEntity).orElse(null));
		final SnapshotComparison comparison = new SnapshotComparison();
		comparison.setLeft(this.createSnapshotComparisonData(currentVersion));
		comparison.setRight(this.createSnapshotComparisonData(otherVersion));
		this.calculateComparisonResult(comparison, locale);
		return comparison;
	}

	private void calculateComparisonResult(final SnapshotComparison comparison, final Locale locale) {
		Result globalResult = Result.EQUAL;

		final Collection<String> attributeNames = this.getAttributeNames(comparison);
		// for each attribute, calculate its values (left & right) and the result of its comparison
		for (final String attributeName : attributeNames) {
			final SnapshotComparisonAttribute<?> attribute = this.getAttributeComparison(comparison, attributeName, locale);
			if (attribute != null) {
				if (attribute.getResult() != Result.EQUAL) {
					globalResult = Result.MODIFIED;
				}
				comparison.getAttributes().add(attribute);
			}
		}

		// finally, set the result of the whole comparison
		if (comparison.getRight() == null) {
			comparison.setResult(Result.NEW);
		} else {
			comparison.setResult(globalResult);
		}
	}

	private Collection<String> getAttributeNames(final SnapshotComparison comparison) {
		// Sorted attribute names, avoid repeated names
		final Set<String> attributeNames = new TreeSet<>();

		// Process JSON model data to get attribute names
		attributeNames.addAll(this.getAttributeNamesFromModelData(comparison.getLeft()));
		attributeNames.addAll(this.getAttributeNamesFromModelData(comparison.getRight()));

		// Add attribute names from entity relationships
		attributeNames.addAll(this.getAttributeNamesFromRelationships(comparison.getLeft()));
		attributeNames.addAll(this.getAttributeNamesFromRelationships(comparison.getRight()));

		this.removeSnapshotHiddenFields(attributeNames, comparison);
		return attributeNames;
	}

	private void removeSnapshotHiddenFields(final Set<String> attributeNames, final SnapshotComparison comparison) {
		final SnapshotComparisonData comparisonData = this.getComparisonData(comparison);
		final ModelReference modelReference = comparisonData.getEntity().getEntity();
		if (modelReference == null) {
			return;
		}
		try {
			final Class<?> clazz = Class.forName(modelReference.getModelClassName());
			final Set<String> snapshotFields = attributeNames.stream()
				.filter(item -> this.isSnapshotShownField(clazz, item))
				.collect(Collectors.toSet());
			attributeNames.clear();
			attributeNames.addAll(snapshotFields);
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private boolean isSnapshotShownField(final Class<?> clazz, final String fieldName) {
		final Field field = ReflectionUtils.findField(clazz, fieldName);
		if (field == null) {
			return false;
		}
		return !field.isAnnotationPresent(SnapshotHiddenField.class);
	}

	private List<String> getAttributeNamesFromModelData(final SnapshotComparisonData comparisonData) {
		final List<String> attributeNames = new ArrayList<>();
		if (comparisonData != null) {
			comparisonData.getModelData().fieldNames().forEachRemaining(attributeNames::add);
		}
		return attributeNames;
	}

	private List<String> getAttributeNamesFromRelationships(final SnapshotComparisonData comparisonData) {
		final List<String> attributeNames = new ArrayList<>();
		if (comparisonData != null) {
			for (final SnapshotRelationship relationship : comparisonData.getRelationships()) {
				attributeNames.add(relationship.getName());
			}
		}
		return attributeNames;
	}

	/**
	 * calculate its values (left & right) and the result of its comparison
	 * @param comparison
	 * @param attributeName
	 * @return
	 */
	private SnapshotComparisonAttribute<?> getAttributeComparison(final SnapshotComparison comparison, final String attributeName, final Locale locale) {
		final AttributeType attributeType = this.getAttributeType(comparison, attributeName);
		final String fullAttributeName = this.getFullAttributeName(comparison, attributeName);
		switch (attributeType) {
			case EMBEDDED:
				final Object valueLeft = this.getEmbeddedValue(comparison.getLeft(), attributeName);
				final Object valueRight = this.getEmbeddedValue(comparison.getRight(), attributeName);

				if (valueLeft == null && valueRight == null) {
					return null;
				}

				final Field field = this.getField(comparison, attributeName);
				final SnapshotJsonEmbedded snapshotJsonEmbeddedAnnotation = field.getAnnotation(SnapshotJsonEmbedded.class);

				if (snapshotJsonEmbeddedAnnotation != null) {
					CustomSnapshotConverter customSnapshotConverter = null;
					try {
						customSnapshotConverter = snapshotJsonEmbeddedAnnotation.converter().newInstance();
					} catch (final Exception e) {
						return null;
					}
					final SnapshotComparisonEmbeddedComplexAttribute attribute = new SnapshotComparisonEmbeddedComplexAttribute();
					attribute.setName(fullAttributeName);
					attribute.calculateResult(valueLeft, valueRight);
					attribute.setChildren(customSnapshotConverter.compareEmbeddedComplexValues(
						valueLeft,
						valueRight,
						locale));
					customSnapshotConverter.calculateEmbeddedAttributeResult(attribute);
					return attribute;
				}

				final SnapshotComparisonEmbeddedAttribute attribute = new SnapshotComparisonEmbeddedAttribute();
				attribute.setName(fullAttributeName);
				attribute.setValueLeft(valueLeft);
				attribute.setValueRight(valueRight);
				attribute.setChildren(this.compareEmbeddedValues(attribute.getValueLeft(), attribute.getValueRight(), fullAttributeName, locale));
				attribute.calculateEmbeddedAttributeResult();
				return attribute;
			case SIMPLE:
				final SnapshotComparisonSimpleAttribute simpleAttribute = new SnapshotComparisonSimpleAttribute();
				simpleAttribute.setName(fullAttributeName);
				simpleAttribute.setValueLeft(this.getAttributeValue(comparison.getLeft(), attributeName, locale));
				simpleAttribute.setValueRight(this.getAttributeValue(comparison.getRight(), attributeName, locale));
				return simpleAttribute;
			case TRANSLATED:
				final SnapshotComparisonSimpleAttribute translatedAttribute = new SnapshotComparisonSimpleAttribute();
				translatedAttribute.setType(AttributeType.TRANSLATED);
				translatedAttribute.setName(fullAttributeName);
				translatedAttribute.setValueLeft(this.getAttributeValue(comparison.getLeft(), attributeName, locale));
				translatedAttribute.setValueRight(this.getAttributeValue(comparison.getRight(), attributeName, locale));
				return translatedAttribute;
			case MODEL:
				// ... else, it's a relationship attribute
				final SnapshotComparisonModelAttribute relationshipAttribute = new SnapshotComparisonModelAttribute();
				relationshipAttribute.setName(fullAttributeName);
				relationshipAttribute.setValueLeft(this.getRelationshipValue(comparison.getLeft(), attributeName));
				relationshipAttribute.setValueRight(this.getRelationshipValue(comparison.getRight(), attributeName));
				return relationshipAttribute;
			case COLLECTION:
				// RelationshipType.MANY
				final SnapshotComparisonCollectionAttribute collectionAttribute = new SnapshotComparisonCollectionAttribute();
				collectionAttribute.setName(fullAttributeName);
				collectionAttribute.setValueLeft(this.getRelationshipValues(comparison.getLeft(), attributeName));
				collectionAttribute.setValueRight(this.getRelationshipValues(comparison.getRight(), attributeName));
				collectionAttribute.setChildren(this.compareCollections(collectionAttribute));
				return collectionAttribute;
			case LOCALIZED_STRING:
				final LocalizedString localizedStringLeft = this.getLocalizedStringValue(comparison.getLeft(), attributeName);
				final LocalizedString localizedStringRight = this.getLocalizedStringValue(comparison.getRight(), attributeName);

				final SnapshotComparisonEmbeddedAttribute localizedAttribute = new SnapshotComparisonEmbeddedAttribute();
				localizedAttribute.setName(fullAttributeName);
				localizedAttribute.setValueLeft(Optional.ofNullable(localizedStringLeft).map(ls -> this.localizedObjectService.getValue(ls)).orElse(null));
				localizedAttribute.setValueRight(Optional.ofNullable(localizedStringRight).map(ls -> this.localizedObjectService.getValue(ls)).orElse(null));
				localizedAttribute.setChildren(this.compareLocalizedValues(localizedStringLeft, localizedStringRight));
				localizedAttribute.calculateEmbeddedAttributeResult();
				return localizedAttribute;
			case LOCALIZED_MODEL:
				final LocalizedModel<?> localizedModelLeft = this.getLocalizedModelValue(comparison.getLeft(), attributeName);
				final LocalizedModel<?> localizedModelRight = this.getLocalizedModelValue(comparison.getRight(), attributeName);
				// TODO Add LocalizedModel attribute
//				final SnapshotComparisonEmbeddedAttribute localizedAttribute = new SnapshotComparisonEmbeddedAttribute();
//				localizedAttribute.setName(fullAttributeName);
//				localizedAttribute.setValueLeft(Optional.ofNullable(localizedModelLeft).map(ls -> this.localizedStringService.getValue(ls)).orElse(null));
//				localizedAttribute.setValueRight(Optional.ofNullable(localizedModelRight).map(ls -> this.localizedStringService.getValue(ls)).orElse(null));
//				localizedAttribute.setChildren(this.compareLocalizedValues(localizedModelLeft, localizedModelRight));
//				localizedAttribute.calculateEmbeddedAttributeResult();
//				return localizedAttribute;
			default:
				return null;
		}
	}

	private Object getEmbeddedValue(final SnapshotComparisonData comparisonData, final String attributeName) {
		if (comparisonData == null) {
			return null;
		}
		final Model model = comparisonData.getModel();
		this.getComparisonClass(comparisonData);
		final Field field = this.getField(comparisonData, attributeName);
		field.setAccessible(true);
		try {
			if (model != null) {
				return field.get(model);
			}
			return this.objectMapper.convertValue(comparisonData.getModelData().get(attributeName), field.getType());
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	protected LocalizedString getLocalizedStringValue(final SnapshotComparisonData comparisonData, final String attributeName) {
		if (comparisonData == null) {
			return null;
		}
		final Model model = comparisonData.getModel();
		final Field field = this.getField(comparisonData, attributeName);
		field.setAccessible(true);
		try {
			if (model != null) {
				final Object value = field.get(model);
				return (LocalizedString) value;
			}
			// TODO Això funciona? Revisar si mai passa per aquí i si funciona (TESTS!!!)
			return (LocalizedString) this.objectMapper.convertValue(comparisonData.getModelData().get(attributeName), field.getType());
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	protected LocalizedModel<?> getLocalizedModelValue(final SnapshotComparisonData comparisonData, final String attributeName) {
		if (comparisonData == null) {
			return null;
		}
		final Model model = comparisonData.getModel();
		final Field field = this.getField(comparisonData, attributeName);
		field.setAccessible(true);
		try {
			if (model != null) {
				final Object value = field.get(model);
				return (LocalizedModel<?>) value;
			}
			// TODO Això funciona? Revisar si mai passa per aquí i si funciona (TESTS!!!)
			return (LocalizedModel<?>) this.objectMapper.convertValue(comparisonData.getModelData().get(attributeName), field.getType());
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	private List<SnapshotComparisonSimpleAttribute> compareEmbeddedValues(final Object valueLeft, final Object valueRight, final String prefix, final Locale locale) {
		if (valueLeft == null && valueRight == null) {
			return null;
		}

		final Map<String, SnapshotComparisonSimpleAttribute> embeddedAttributes = new TreeMap<>();
		if (valueLeft != null) {
			ReflectionUtils.doWithFields(valueLeft.getClass(), field -> {
				if (this.excludeFieldFromEmbedded(field)) {
					return;
				}
				final String fieldName = field.getName();
				final String value = this.getAttributeValue(valueLeft, field, locale);
				if (value != null) {
					final SnapshotComparisonSimpleAttribute attribute = new SnapshotComparisonSimpleAttribute();
					attribute.setName(prefix + "." + fieldName);
					if (this.isTranslatedAttribute(field, fieldName)) {
						attribute.setType(AttributeType.TRANSLATED);
					}
					attribute.setValueLeft(value);
					embeddedAttributes.put(fieldName, attribute);
				}
			});
		}
		if (valueRight != null) {
			ReflectionUtils.doWithFields(valueRight.getClass(), field -> {
				if (this.excludeFieldFromEmbedded(field)) {
					return;
				}
				final String fieldName = field.getName();
				final String value = this.getAttributeValue(valueRight, field, locale);
				if (value != null) {
					SnapshotComparisonSimpleAttribute attribute;
					if (embeddedAttributes.containsKey(fieldName)) {
						attribute = embeddedAttributes.get(fieldName);
					} else {
						attribute = new SnapshotComparisonSimpleAttribute();
						if (this.isTranslatedAttribute(field, fieldName)) {
							attribute.setType(AttributeType.TRANSLATED);
						}
						attribute.setName(prefix + fieldName);
						embeddedAttributes.put(fieldName, attribute);
					}
					attribute.setValueRight(value);
				}
			});
		}

		return new ArrayList<>(embeddedAttributes.values());
	}

	/**
	 * Exclude fields from comparison of embedded attributes.
	 * At the moment, only static fields are excluded.
	 * @param field
	 * @return
	 */
	private boolean excludeFieldFromEmbedded(final Field field) {
		return Modifier.isStatic(field.getModifiers());
	}

	protected List<SnapshotComparisonSimpleAttribute> compareLocalizedValues(final LocalizedString valueLeft, final LocalizedString valueRight) {
		if (valueLeft == null && valueRight == null) {
			return Collections.emptyList();
		}

		final Map<String, SnapshotComparisonSimpleAttribute> languages = new TreeMap<>();
		if (valueLeft != null) {
			valueLeft.getLanguageCodes().forEach(language -> {
				final String fieldName = String.format("lang.%s", language);
				final String value = valueLeft.get(language);
				if (value != null) {
					final SnapshotComparisonSimpleAttribute attribute = new SnapshotComparisonSimpleAttribute();
					attribute.setName(fieldName);
					attribute.setValueLeft(value);
					languages.put(fieldName, attribute);
				}
			});
		}
		if (valueRight != null) {
			valueRight.getLanguageCodes().forEach(language -> {
				final String fieldName = String.format("lang.%s", language);
				final String value = valueRight.get(language);
				if (value != null) {
					SnapshotComparisonSimpleAttribute attribute;
					if (languages.containsKey(fieldName)) {
						attribute = languages.get(fieldName);
					} else {
						attribute = new SnapshotComparisonSimpleAttribute();
						attribute.setName(fieldName);
						languages.put(fieldName, attribute);
					}
					attribute.setValueRight(value);
				}
			});
		}

		return new ArrayList<>(languages.values());
	}

	private List<SnapshotComparisonModelAttribute> compareCollections(final SnapshotComparisonCollectionAttribute comparedAttribute) {
		final List<ModelReference> valueLeft = comparedAttribute.getValueLeft();
		final List<ModelReference> valueRight = comparedAttribute.getValueRight();

		int index = 1;
		final List<SnapshotComparisonModelAttribute> comparison = new ArrayList<>();
		if (valueLeft != null) {
			for (final ModelReference modelReference : valueLeft) {
				final SnapshotComparisonModelAttribute attribute = new SnapshotComparisonModelAttribute();
				attribute.setName(String.format("[%d]", index++));
				attribute.setValueLeft(modelReference);
				if (valueRight != null) {
					final ModelReference referenceContainedInRight = this.getContainedModelReference(valueRight, modelReference);
					if (referenceContainedInRight != null) {
						attribute.setValueRight(referenceContainedInRight);
					}
				}
				comparison.add(attribute);
			}
		}
		if (valueRight != null) {
			for (final ModelReference modelReference : valueRight) {
				if (valueLeft == null || this.getContainedModelReference(valueLeft, modelReference) == null) {
					final SnapshotComparisonModelAttribute attribute = new SnapshotComparisonModelAttribute();
					attribute.setName(String.format("[%d]", index++));
					attribute.setValueRight(modelReference);
					comparison.add(attribute);
				}
			}
		}

		return comparison;
	}

	private ModelReference getContainedModelReference(final List<ModelReference> list, final ModelReference modelReference) {
		for (final ModelReference item : list) {
			if (item.getModelClassName().equals(modelReference.getModelClassName()) && item.getModelId().equals(modelReference.getModelId())) {
				return item;
			}
		}
		return null;
	}

	// TODO 20211001 Fer aquest mètode extensible perquè es puguin afegir atributs traduïts
	private boolean isTranslatedAttribute(final Field field, final String attributeName) {
		final boolean mustBeTranslated = boolean.class.equals(field.getType())
			|| Boolean.class.equals(field.getType());
		return mustBeTranslated;
	}

	private boolean isLocalizedStringAttribute(final Field field, final String attributeName) {
		return LocalizedString.class.equals(field.getType());
	}

	private boolean isLocalizedModelAttribute(final Field field, final String attributeName) {
		// TODO LocalizedBasicModelEntity
		return LocalizedModel.class.equals(field.getType());
	}

	private RelationshipType getRelationshipType(final SnapshotComparison comparison, final String attributeName) {
		final SnapshotComparisonData comparisonData = this.getComparisonData(comparison);
		return this.getRelationshipType(comparisonData, attributeName);
	}

	private RelationshipType getRelationshipType(final SnapshotComparisonData snapshotComparisonData, final String attributeName) {
		if (snapshotComparisonData == null) {
			return null;
		}
		return snapshotComparisonData.getRelationships().stream()
			.filter(relationship -> relationship.getName().equals(attributeName))
			.map(SnapshotRelationship::getType)
			.findFirst().orElse(null);
	}

	private String getAttributeValue(final SnapshotComparisonData comparisonData, final String attributeName, final Locale locale) {
		if (comparisonData == null) {
			return null;
		}
		String attributeValue = null;
		try {
			attributeValue = this.getAttributeValueFromModel(comparisonData, attributeName, locale);
		} catch (final Exception e) {
			attributeValue = this.getAttributeValueFromJson(comparisonData.getModelData(), attributeName);
		}
		return attributeValue;
	}

	private String getAttributeValueFromModel(final SnapshotComparisonData comparisonData, final String attributeName, final Locale locale) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final Model model = comparisonData.getModel();
		final Field field = ReflectionUtils.findField(model.getClass(), attributeName);
		return this.getAttributeValue(model, field, locale);
	}

	private String getAttributeValue(final Object obj, final Field field, final Locale locale) throws IllegalAccessException {
		field.setAccessible(true);
		final Object value = field.get(obj);
		String attributeValue = "";
		if (value != null) {
			// TODO 20211001 Revisar aquest formatter, potser fer comparador configurable per
			// poder afegir transformadors per a una certa classe
			if (value instanceof Date) {
				attributeValue = this.dateFormatter.format(this.getLocalDate((Date) value));
			} else if (value instanceof LocalDate) {
				attributeValue = this.dateFormatter.format((LocalDate) value);
			} else if (value instanceof LocalDateTime) {
				attributeValue = this.datetimeFormatter.format((LocalDateTime) value);
			} else if (value instanceof LocalizedString) {
				attributeValue = ((LocalizedString) value).getDefaultText();
//			} else if (this.isEmbeddedField(field)) {
//				attributeValue = this.getAttributeValueFromJson(comparisonData.getModelData(), attributeName);
			} else if (value instanceof ValueObject) {
				attributeValue = ((ValueObject) value).getValue();
			} else if (value instanceof Enum) {
				// FIXME: Use this method to calculate prefix when jamgo-framework-plugin generate i18n properties properly
				// final String prefix = this.calculatePrefix(field.getType(), field.getName());
				final String prefix = StringUtils.uncapitalize(field.getType().getSimpleName());
				attributeValue = this.messageSource.getMessage(prefix + "." + value);
			} else {
				attributeValue = value.toString();
			}
		}
		return attributeValue;
	}

	private LocalDate getLocalDate(final Date date) {
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	private Class<?> getComparisonClass(final SnapshotComparisonData comparisonData) {
		if (comparisonData == null) {
			return null;
		}
		final Model model = comparisonData.getModel();
		if (model != null) {
			return model.getClass();
		} else {
			final String className = comparisonData.getEntity().getEntity().getModelClassName();
			if (className != null) {
				try {
					return Class.forName(className);
				} catch (final ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	protected AttributeType getAttributeType(final SnapshotComparison comparison, final String fieldName) {
		final Field field = this.getField(comparison, fieldName);
		if (field.isAnnotationPresent(Embedded.class) || field.isAnnotationPresent(SnapshotJsonEmbedded.class)) {
			return AttributeType.EMBEDDED;
		}

		final SnapshotComparisonData comparisonData = this.getComparisonData(comparison);
		if (comparisonData.getModelData().has(fieldName)) {
			if (this.isTranslatedAttribute(field, fieldName)) {
				return AttributeType.TRANSLATED;
			} else if (this.isLocalizedStringAttribute(field, fieldName)) {
				return AttributeType.LOCALIZED_STRING;
			} else if (this.isLocalizedModelAttribute(field, fieldName)) {
				return AttributeType.LOCALIZED_MODEL;
			}
			return AttributeType.SIMPLE;
		}
		if (this.getRelationshipType(comparison, fieldName) == RelationshipType.ONE) {
			return AttributeType.MODEL;
		}
		return AttributeType.COLLECTION;
	}

	private SnapshotComparisonData getComparisonData(final SnapshotComparison comparison) {
		return (comparison.getLeft() != null ? comparison.getLeft() : comparison.getRight());
	}

	protected String getFullAttributeName(final SnapshotComparison comparison, final String fieldName) {
		String fullAttributeName = null;
		if (fieldName.equals("id") || fieldName.equals("version")) {
			fullAttributeName = String.format("model.%s", fieldName);
		} else {
			final SnapshotComparisonData comparisonData = this.getComparisonData(comparison);
			final String prefix = this.calculatePrefix(comparisonData, fieldName);
			fullAttributeName = String.format("%s.%s", prefix, fieldName);
		}
		return fullAttributeName;
	}

	protected String calculatePrefix(final SnapshotComparisonData snapshotComparisonData, final String fieldName) {
		final Class<? extends Model> modelClass = snapshotComparisonData.getModel().getClass();
		return this.calculatePrefix(modelClass, fieldName);
	}

	protected String calculatePrefix(final Class<?> clazz, final String fieldName) {
		final String prefix;
		if (Category.class.isAssignableFrom(clazz) && (fieldName.equals("name") || fieldName.equals("description"))) {
			prefix = Category.class.getSimpleName().toLowerCase();
		} else {
			prefix = clazz.getSimpleName();
		}
		// TODO Change jamgo-framework-plugin to use this to create i18n properties
		// return StringUtils.uncapitalize(prefix);
		return prefix.toLowerCase();
	}

	protected Field getField(final SnapshotComparisonData comparisonData, final String fieldName) {
		final Class<?> clazz = this.getComparisonClass(comparisonData);
		if (clazz != null) {
			return ReflectionUtils.findField(clazz, fieldName);
		}
		return null;
	}

	protected Field getField(final SnapshotComparison comparison, final String fieldName) {
		final SnapshotComparisonData comparisonData = this.getComparisonData(comparison);
		final Class<?> clazz = this.getComparisonClass(comparisonData);
		if (clazz == null) {
			return null;
		}
		return ReflectionUtils.findField(clazz, fieldName);
	}

	private String getAttributeValueFromJson(final JsonNode jsonNode, final String attributeName) {
		String attributeValue = null;
		final JsonNode node = jsonNode.get(attributeName);
		if (node.isTextual()) {
			attributeValue = node.asText();
		} else if (node.isBoolean()) {
			attributeValue = Boolean.toString(node.asBoolean());
		} else if (node.isNull()) {
			attributeValue = "";
		} else if (node.isNumber()) {
			attributeValue = node.asText();
		} else if (node.isObject()) {
			attributeValue = node.toString();
		} else if (node.isArray()) {
			attributeValue = node.toString();
		} else {
			attributeValue = "<unknown-type: " + node.getNodeType() + ">";
		}

		return attributeValue;
	}

	private ModelReference getRelationshipValue(final SnapshotComparisonData snapshotComparisonData, final String attributeName) {
		if (snapshotComparisonData == null) {
			return null;
		}
		return snapshotComparisonData.getRelationships().stream()
			.filter(relationship -> relationship.getName().equals(attributeName))
			.map(SnapshotRelationship::getTarget)
			.findAny().orElse(null);
	}

	private List<ModelReference> getRelationshipValues(final SnapshotComparisonData snapshotComparisonData, final String attributeName) {
		if (snapshotComparisonData == null) {
			return null;
		}
		return snapshotComparisonData.getRelationships().stream()
			.filter(relationship -> relationship.getName().equals(attributeName))
			.map(SnapshotRelationship::getTarget)
			.sorted(Comparator.comparing(ModelReference::toString))
			.collect(Collectors.toList());
	}

	private SnapshotComparisonData createSnapshotComparisonData(final SnapshotVersion snapshotVersion) {
		if (snapshotVersion == null) {
			return null;
		}
		final SnapshotEntity snapshotEntity = snapshotVersion.getSnapshotEntity();

		final SnapshotComparisonData data;
		if (snapshotVersion.getSnapshot() == null) { // current version
			// Generate dynamic snapshot and use this data in comparison
			data = this.getCurrentData(snapshotVersion.getEntity());
		} else {
			// Retrieve info from snapshots' database
			data = new SnapshotComparisonData();
			data.setSnapshot(snapshotVersion.getSnapshot());
			data.setEntity(snapshotEntity);
			data.setRelationships(this.repository.findSnapshotRelationships(snapshotEntity.getEntity()));
			try {
				data.setModelData(this.objectMapper.readTree(new String(snapshotEntity.getModelData())));
				data.setModel((Model) this.objectMapper.readValue(new String(snapshotEntity.getModelData()), Class.forName(snapshotEntity.getEntity().getModelClassName())));
			} catch (final JsonProcessingException e) {
				e.printStackTrace();
			} catch (final ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return data;
	}

}
