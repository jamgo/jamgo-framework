package org.jamgo.snapshot.services.snapshot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

public class SnapshotInitializer implements InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(SnapshotInitializer.class);

	@Autowired
	private SnapshotBulkLoader snapshotBulkLoader;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.start();
	}

	public void start() {
		try {
			SnapshotInitializer.logger.info("Snapshot initializer - STARTING...");
			this.snapshotBulkLoader.run();
			SnapshotInitializer.logger.info("Snapshot initializer - FINISHED");
		} catch (final Exception e) {
			SnapshotInitializer.logger.error("Generic exception in SnapshotInitializer", e);
		}
	}

}
