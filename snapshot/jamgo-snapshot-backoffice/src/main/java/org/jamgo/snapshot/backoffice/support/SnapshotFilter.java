package org.jamgo.snapshot.backoffice.support;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.jamgo.services.session.SessionContext;
import org.jamgo.snapshot.app.support.SnapshotSupportHelper;
import org.jamgo.snapshot.model.snapshot.SnapshotInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Filter to create a unique transactionID for every received request.
 * This information is used to generate snapshots of all the modified (created, updated, deleted)
 * entities in the application. This information is stored in a ThreadLocal variable (snapshotInfo),
 * so every thread will store a different transactionID.
 * This filter also cleans ThreadLocal variable data to avoid the reuse of same id in future requests
 * that reuse the same thread.
 */
public class SnapshotFilter implements Filter {

	public static final Logger logger = LoggerFactory.getLogger(SnapshotFilter.class);

	@Autowired
	private SessionContext sessionContext;

	@Autowired
	private SnapshotInfo snapshotInfo;

	@Autowired
	private SnapshotSupportHelper helper;

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
		SnapshotFilter.logger.trace("doFilter()");
		this.helper.preHandle(this.snapshotInfo, this.sessionContext.getCurrentUser());
		chain.doFilter(request, response);
		this.helper.postHandle(this.snapshotInfo);
	}
}
