package org.jamgo.snapshot.backoffice.config;

import org.jamgo.snapshot.app.config.SnapshotConfiguration;
import org.jamgo.snapshot.backoffice.support.SnapshotFilter;
import org.jamgo.snapshot.model.snapshot.SnapshotComparisonAttributeComparator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ SnapshotConfiguration.class })
public class SnapshotBackofficeConfiguration {

	@Bean
	@ConditionalOnProperty(value = "snapshots.enabled", havingValue = "true", matchIfMissing = false)
	public SnapshotFilter snapshotFilter() {
		return new SnapshotFilter();
	}

	@Bean
	public SnapshotComparisonAttributeComparator attributeComparator() {
		return new SnapshotComparisonAttributeComparator();
	}

}
